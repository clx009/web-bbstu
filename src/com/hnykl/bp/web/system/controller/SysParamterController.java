package com.hnykl.bp.web.system.controller;
import java.util.List;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.system.entity.SysParamterEntity;
import com.hnykl.bp.web.system.service.SysParamterServiceI;

/**   
 * @Title: Controller
 * @Description: 参数管理
 * @author onlineGenerator
 * @date 2014-04-23 10:37:28
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/sysParamterController")
public class SysParamterController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SysParamterController.class);

	@Autowired
	private SysParamterServiceI sysParamterService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 参数管理列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "sysParamter")
	public ModelAndView sysParamter(HttpServletRequest request) {
		return new ModelAndView("system/sysParamter/sysParamterList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(SysParamterEntity sysParamter,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(SysParamterEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, sysParamter, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.sysParamterService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除参数管理
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(SysParamterEntity sysParamter, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		sysParamter = systemService.getEntity(SysParamterEntity.class, sysParamter.getId());
		message = "参数管理删除成功";
		try{
			sysParamterService.delete(sysParamter);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "参数管理删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除参数管理
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "参数管理删除成功";
		try{
			for(String id:ids.split(",")){
				SysParamterEntity sysParamter = systemService.getEntity(SysParamterEntity.class, 
				id
				);
				sysParamterService.delete(sysParamter);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "参数管理删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加参数管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(SysParamterEntity sysParamter, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "参数管理添加成功";
		try{
			sysParamterService.save(sysParamter);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "参数管理添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新参数管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(SysParamterEntity sysParamter, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "参数管理更新成功";
		SysParamterEntity t = sysParamterService.get(SysParamterEntity.class, sysParamter.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(sysParamter, t);
			sysParamterService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "参数管理更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * 参数管理新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(SysParamterEntity sysParamter, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(sysParamter.getId())) {
			sysParamter = sysParamterService.getEntity(SysParamterEntity.class, sysParamter.getId());
			req.setAttribute("sysParamterPage", sysParamter);
		}
		return new ModelAndView("system/sysParamter/sysParamter-add");
	}
	/**
	 * 参数管理编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(SysParamterEntity sysParamter, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(sysParamter.getId())) {
			sysParamter = sysParamterService.getEntity(SysParamterEntity.class, sysParamter.getId());
			req.setAttribute("sysParamterPage", sysParamter);
		}
		return new ModelAndView("system/sysParamter/sysParamter-update");
	}
}
