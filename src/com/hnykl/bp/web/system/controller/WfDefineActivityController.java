package com.hnykl.bp.web.system.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.ExceptionUtil;
import com.hnykl.bp.base.core.util.RoletoJson;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.base.core.util.TreeJson;
import com.hnykl.bp.base.core.util.UUIDGenerator;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.pojo.base.TSUser;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.system.entity.WfDefineActivityEntity;
import com.hnykl.bp.web.system.service.WfDefineActivityServiceI;

/**
 * @Title: Controller
 * @Description: 流程活动定义
 * @author onlineGenerator
 * @date 2014-05-30 11:46:09
 * @version V1.0
 * 
 */
@Controller
@RequestMapping("/wfDefineActivityController")
public class WfDefineActivityController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(WfDefineActivityController.class);

	@Autowired
	private WfDefineActivityServiceI wfDefineActivityService;
	@Autowired
	private SystemService systemService;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 流程活动定义列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "wfDefineActivity")
	public ModelAndView wfDefineActivity(HttpServletRequest request) {
		String processId = request.getParameter("processId");
		request.setAttribute("processId", processId);
		return new ModelAndView("system/wfDefineActivity/wfDefineActivityList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(WfDefineActivityEntity wfDefineActivity,
			HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(WfDefineActivityEntity.class,
				dataGrid);
		// 查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(
				cq, wfDefineActivity, request.getParameterMap());
		try {
			// 自定义追加查询条件
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.wfDefineActivityService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	@RequestMapping(params = "wfDefineCsByDept")
	public ModelAndView wfDefineCsByDept(HttpServletRequest request) {
		/*String processId = request.getParameter("processId");
		request.setAttribute("processId", processId);
		String activityId = request.getParameter("activityId");
		request.setAttribute("activityId", activityId);*/
		// 给部门查询条件中的下拉框准备数据
		List<TSDepart> departList = systemService.getList(TSDepart.class);
		request.setAttribute("departsReplace", RoletoJson.listToReplaceStr(
				departList, "departname", "id"));
		return new ModelAndView("system/wfDefineActivity/wfDefineCsByDept");
	}
	
	@RequestMapping(params = "wfDefineCsByCount")
	public ModelAndView wfDefineCsByCount(HttpServletRequest request) {
		// 给部门查询条件中的下拉框准备数据
		List<TSDepart> departList = systemService.getList(TSDepart.class);
		request.setAttribute("departsReplace", RoletoJson.listToReplaceStr(
				departList, "departname", "id"));
		return new ModelAndView("system/wfDefineActivity/wfDefineCsByCount");
	}

	@RequestMapping(params = "wfDefineFsByDept")
	public ModelAndView wfDefineFsByDept(HttpServletRequest request) {
		/*String processId = request.getParameter("processId");
		request.setAttribute("processId", processId);
		String activityId = request.getParameter("activityId");
		request.setAttribute("activityId", activityId);*/
		// 给部门查询条件中的下拉框准备数据
		List<TSDepart> departList = systemService.getList(TSDepart.class);
		request.setAttribute("departsReplace", RoletoJson.listToReplaceStr(
				departList, "departname", "id"));
		return new ModelAndView("system/wfDefineActivity/wfDefineFsByDept");
	}
	
	@RequestMapping(params = "wfDefineFsByCount")
	public ModelAndView wfDefineFsByCount(HttpServletRequest request) {
		// 给部门查询条件中的下拉框准备数据
		List<TSDepart> departList = systemService.getList(TSDepart.class);
		request.setAttribute("departsReplace", RoletoJson.listToReplaceStr(
				departList, "departname", "id"));
		return new ModelAndView("system/wfDefineActivity/wfDefineFsByCount");
	}
	
	/**
	 * easyuiAJAX用户列表请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 */
	@RequestMapping(params = "userdatagrid", method = RequestMethod.POST)
	public void userdatagrid(TSUser user, HttpServletRequest request,
			HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSUser.class, dataGrid);
		// 查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(
				cq, user);

		Short[] userstate = new Short[] { Globals.User_Normal,
				Globals.User_ADMIN };
		cq.in("status", userstate);
		cq.add();
		this.systemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 医疗机构科室树
	 * 
	 * @return
	 */
	@RequestMapping(params = "medicalTree")
	@ResponseBody
	public List<TreeJson> medicalTree(HttpServletRequest request) {
		String processId = request.getParameter("pid");
		String activityId = request.getParameter("aid");
		String userId = request.getParameter("uid");

		List<TreeJson> treeList = new ArrayList<TreeJson>();
		TreeJson rootJson = new TreeJson();
		rootJson.setId("400000");
		rootJson.setPid("-1");
		rootJson.setText("长沙市");
		treeList.add(rootJson);

		List<Map<String, Object>> mediMapList = systemService
				.findForJdbc(
						"select ins_code as id,ins_name as text,area_code as pid from base_medical_ins where area_code='"
								+ rootJson.getId() + "'", null);
		for (int i = 0; i < mediMapList.size(); i++) {
			Map<String, Object> mediMap = mediMapList.get(i);
			TreeJson mediJson = new TreeJson();
			mediJson.setId((String) mediMap.get("id"));
			mediJson.setPid((String) mediMap.get("pid"));
			mediJson.setText((String) mediMap.get("text"));
			treeList.add(mediJson);

			List<Map<String, Object>> deptMapList = systemService
					.findForJdbc(
							"select loc_dept_code as id,loc_dept_name as text,hos_code as pid from base_local_dept where hos_code ='"
									+ mediJson.getId() + "'", null);
			for (int j = 0; j < deptMapList.size(); j++) {
				Map<String, Object> deptMap = deptMapList.get(j);
				TreeJson deptJson = new TreeJson();
				deptJson.setId((String) deptMap.get("id"));
				deptJson.setPid((String) deptMap.get("pid"));
				deptJson.setText((String) deptMap.get("text"));
				treeList.add(deptJson);
			}
		}

		List<Map<String, Object>> securityMapList = systemService
				.findForJdbc(
						"select medical_id medicalId,dept_id deptId,actor_value actorValue from wf_define_ext_security_inf where actor_type='USER' and process_id='"
								+ processId
								+ "' and activity_id='"
								+ activityId + "' ", null);
		List<TreeJson> dropList = new ArrayList<TreeJson>();
		int treeSize = treeList.size();
		for (int m = 0; m < treeSize; m++) {
			for (int n = 0; n < securityMapList.size(); n++) {
				TreeJson deptJson = treeList.get(m);
				Map<String, Object> securityMap = securityMapList.get(n);
				if (deptJson.getPid().equals(securityMap.get("medicalId"))
						&& deptJson.getId().equals(securityMap.get("deptId"))) {
					if (userId.equals(securityMap.get("actorValue"))) {
						deptJson.setChecked("true");
					} else {
						dropList.add(deptJson);
					}
				}
			}
		}
		treeList.removeAll(dropList);
		List<TreeJson> list = TreeJson.formatTree(treeList);
		return list;
	}

	/**
	 * 更新用户审核医疗机构及科室
	 * 
	 * @param request
	 * @return
	 */
	@RequestMapping(params = "saveSecurity")
	@ResponseBody
	public AjaxJson saveSecurity(HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		try {
			String processId = request.getParameter("pid");
			String activityId = request.getParameter("aid");
			String nodeStr = request.getParameter("nstr");
			String userId = request.getParameter("uid");
			Map pmap = new HashMap();
			systemService
					.executeSql(
							"delete from wf_define_ext_security_inf where ACTOR_TYPE='USER' and ACTOR_VALUE='"
									+ userId
									+ "' and PROCESS_ID='"
									+ processId
									+ "' and ACTIVITY_ID='" + activityId + "'",
							pmap);
			if (nodeStr.indexOf("|") != -1) {
				String[] medicals = nodeStr.split("\\|");
				for (int i = 0; i < medicals.length; i++) {
					String hosCode = medicals[i].split(",")[0];
					String deptCode = medicals[i].split(",")[1];
					Map paramMap = new HashMap();
					systemService
							.executeSql(
									"insert into wf_define_ext_security_inf(ID,PROCESS_ID,ACTIVITY_ID,ACTOR_TYPE,ACTOR_VALUE,MEDICAL_ID,DEPT_ID) values('"
											+ UUIDGenerator.generate()
											+ "','"
											+ processId
											+ "','"
											+ activityId
											+ "','USER','"
											+ userId
											+ "','"
											+ hosCode + "','" + deptCode + "')",
									paramMap);
				}
			}
			j.setMsg("活动用户分配成功！");
		} catch (Exception e) {
			logger.error(ExceptionUtil.getExceptionMessage(e));
			j.setMsg("活动用户分配失败！");
		}
		return j;
	}
}
