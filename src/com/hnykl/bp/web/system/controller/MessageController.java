package com.hnykl.bp.web.system.controller; 
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.DataUtils;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.entity.MessageEntity; 
import com.hnykl.bp.web.system.service.MessageServiceI;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils; 
import com.hnykl.bp.easemob.server.MessageService;
import com.hnykl.bp.easemob.server.api.IMUserAPI;
import com.hnykl.bp.easemob.server.api.SendMessageAPI;
import com.hnykl.bp.easemob.server.comm.ClientContext;
import com.hnykl.bp.easemob.server.comm.EasemobRestAPIFactory;
import com.hnykl.bp.easemob.server.comm.body.CmdMessageBody;
import com.hnykl.bp.easemob.server.comm.wrapper.BodyWrapper;
import com.hnykl.bp.easemob.server.comm.wrapper.ResponseWrapper;
/**   
 * @Title: Controller
 * @Description: 
 * @author onlineGenerator
 * @date 2016-06-14 17:39:31
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/messageController")
public class MessageController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(MessageController.class); 

	@Autowired
	private MessageServiceI messageService;
	@Autowired
	private SystemService systemService;
	private String msg;
	
	public String getMessage() {
		return msg;
	}

	public void setMessage(String msg) {
		this.msg = msg;
	}


	/**
	 * t_s_message列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "message")
	public ModelAndView message(HttpServletRequest request) {
		return new ModelAndView("message/messageList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(MessageEntity message,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(MessageEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, message, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.messageService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除t_s_message
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(MessageEntity message, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = systemService.getEntity(MessageEntity.class, message.getId());
		msg = "删除成功";
		try{
			messageService.delete(message);
			systemService.addLog(msg, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			msg = "删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(msg);
		return j;
	}
	
	/**
	 * 批量删除t_s_message
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		msg = "删除成功";
		try{
			for(String id:ids.split(",")){
				MessageEntity message = systemService.getEntity(MessageEntity.class, 
				id
				);
				messageService.delete(message);
				systemService.addLog(msg, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			msg = "删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(msg);
		return j;
	}


	/**
	 * 添加t_s_message
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(MessageEntity message, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		msg = "添加成功";
		
		try{
			message.setCreateTime(DataUtils.getTimestamp());
			messageService.save(message); 

			Map<String, String> ext = new HashMap<String,String>();
			ext.put("titile", message.getTitle()); 
			ext.put("content", message.getContent()); 
			ext.put("type", message.getType()); 
			ext.put("createTime", message.getCreateTime().toString()); 
			//MessageService.sendMessageToAll(null, 20L, null, ext);
			systemService.addLog(msg, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			msg = "添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(msg);
		return j;
	}
	
	/**
	 * 更新t_s_message
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(MessageEntity message, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		msg = "更新成功";
		MessageEntity t = messageService.get(MessageEntity.class, message.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(message, t);
			messageService.saveOrUpdate(t);
			systemService.addLog(msg, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			msg = "更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(msg);
		return j;
	}
	

	/**
	 * t_s_message新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(MessageEntity message, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(message.getId())) {
			message = messageService.getEntity(MessageEntity.class, message.getId());
			req.setAttribute("messagePage", message);
		}
		return new ModelAndView("message/message-add");
	}
	/**
	 * t_s_message编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(MessageEntity message, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(message.getId())) {
			message = messageService.getEntity(MessageEntity.class, message.getId());
			req.setAttribute("messagePage", message);
		}
		return new ModelAndView("message/message-update");
	}
}
