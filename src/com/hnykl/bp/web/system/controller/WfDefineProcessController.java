package com.hnykl.bp.web.system.controller;
import java.util.List;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.ConvertUtils;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.pojo.base.TSUser;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.system.entity.WfDefineActivityEntity;
import com.hnykl.bp.web.system.entity.WfDefineProcessEntity;
import com.hnykl.bp.web.system.service.WfDefineProcessServiceI;

/**   
 * @Title: Controller
 * @Description: 自定义流程
 * @author onlineGenerator
 * @date 2014-05-30 10:41:39
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/wfDefineProcessController")
public class WfDefineProcessController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(WfDefineProcessController.class);

	@Autowired
	private WfDefineProcessServiceI wfDefineProcessService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 自定义流程列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "wfDefineProcess")
	public ModelAndView wfDefineProcess(HttpServletRequest request) {
		return new ModelAndView("system/wfDefineProcess/wfDefineProcessList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(WfDefineProcessEntity wfDefineProcess,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(WfDefineProcessEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, wfDefineProcess, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.wfDefineProcessService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * 删除自定义流程
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(WfDefineProcessEntity wfDefineProcess, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		wfDefineProcess = systemService.getEntity(WfDefineProcessEntity.class, wfDefineProcess.getId());
		message = "自定义流程删除成功";
		try{
			wfDefineProcessService.delete(wfDefineProcess);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "自定义流程删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除自定义流程
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "自定义流程删除成功";
		try{
			for(String id:ids.split(",")){
				WfDefineProcessEntity wfDefineProcess = systemService.getEntity(WfDefineProcessEntity.class, 
				id
				);
				wfDefineProcessService.delete(wfDefineProcess);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "自定义流程删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加自定义流程
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(WfDefineProcessEntity wfDefineProcess, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "自定义流程添加成功";
		try{
			wfDefineProcessService.save(wfDefineProcess);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "自定义流程添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新自定义流程
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(WfDefineProcessEntity wfDefineProcess, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "自定义流程更新成功";
		WfDefineProcessEntity t = wfDefineProcessService.get(WfDefineProcessEntity.class, wfDefineProcess.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(wfDefineProcess, t);
			wfDefineProcessService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "自定义流程更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * 自定义流程新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(WfDefineProcessEntity wfDefineProcess, HttpServletRequest req) {
		
		return new ModelAndView("system/wfDefineProcess/wfDefineProcess-add");
	}
	
	/**
	 * 自定义流程新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goMonitor")
	public ModelAndView goMonitor(WfDefineProcessEntity wfDefineProcess, HttpServletRequest req) {
		return new ModelAndView("system/wfDefineProcess/wfDefineProcess-monitor");
	}
	/**
	 * 自定义流程编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(WfDefineProcessEntity wfDefineProcess, HttpServletRequest req) {
		String processId = req.getParameter("processId");
		req.setAttribute("processId", processId);
		return new ModelAndView("system/wfDefineProcess/wfDefineProcess-update");
	}
}
