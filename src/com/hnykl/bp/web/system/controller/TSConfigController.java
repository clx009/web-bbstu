package com.hnykl.bp.web.system.controller;
import java.util.List;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.system.entity.TSConfigEntity;
import com.hnykl.bp.web.system.service.TSConfigServiceI;

/**   
 * @Title: Controller
 * @Description: 系统配置
 * @author onlineGenerator
 * @date 2014-06-24 15:59:11
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/tSConfigController")
public class TSConfigController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TSConfigController.class);

	@Autowired
	private TSConfigServiceI tSConfigService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 系统配置列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "tSConfig")
	public ModelAndView tSConfig(HttpServletRequest request) {
		return new ModelAndView("system/tSConfig/tSConfigList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(TSConfigEntity tSConfig,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSConfigEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, tSConfig, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.tSConfigService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除系统配置
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(TSConfigEntity tSConfig, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		tSConfig = systemService.getEntity(TSConfigEntity.class, tSConfig.getId());
		message = "系统配置删除成功";
		try{
			tSConfigService.delete(tSConfig);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "系统配置删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除系统配置
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "系统配置删除成功";
		try{
			for(String id:ids.split(",")){
				TSConfigEntity tSConfig = systemService.getEntity(TSConfigEntity.class, 
				id
				);
				tSConfigService.delete(tSConfig);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "系统配置删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加系统配置
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(TSConfigEntity tSConfig, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "系统配置添加成功";
		try{
			tSConfigService.save(tSConfig);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "系统配置添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新系统配置
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(TSConfigEntity tSConfig, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "系统配置更新成功";
		TSConfigEntity t = tSConfigService.get(TSConfigEntity.class, tSConfig.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(tSConfig, t);
			tSConfigService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "系统配置更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * 系统配置新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(TSConfigEntity tSConfig, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tSConfig.getId())) {
			tSConfig = tSConfigService.getEntity(TSConfigEntity.class, tSConfig.getId());
			req.setAttribute("tSConfigPage", tSConfig);
		}
		return new ModelAndView("system/tSConfig/tSConfig-add");
	}
	/**
	 * 系统配置编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(TSConfigEntity tSConfig, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tSConfig.getId())) {
			tSConfig = tSConfigService.getEntity(TSConfigEntity.class, tSConfig.getId());
			req.setAttribute("tSConfigPage", tSConfig);
		}
		return new ModelAndView("system/tSConfig/tSConfig-update");
	}
}
