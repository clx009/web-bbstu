package com.hnykl.bp.web.system.service;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.system.entity.WfDefineActivityEntity;
import java.io.Serializable;

public interface WfDefineActivityServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(WfDefineActivityEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(WfDefineActivityEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(WfDefineActivityEntity t);
}
