package com.hnykl.bp.web.system.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.web.system.service.WfDefineActivityServiceI;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.system.entity.WfDefineActivityEntity;
import java.util.UUID;
import java.io.Serializable;

@Service("wfDefineActivityService")
@Transactional
public class WfDefineActivityServiceImpl extends CommonServiceImpl implements WfDefineActivityServiceI {

	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((WfDefineActivityEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((WfDefineActivityEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((WfDefineActivityEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(WfDefineActivityEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(WfDefineActivityEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(WfDefineActivityEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,WfDefineActivityEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{process_id}",String.valueOf(t.getProcessId()));
 		sql  = sql.replace("#{activity_type_id}",String.valueOf(t.getActivityTypeId()));
 		sql  = sql.replace("#{activity_name}",String.valueOf(t.getActivityName()));
 		sql  = sql.replace("#{activity_no}",String.valueOf(t.getActivityNo()));
 		sql  = sql.replace("#{duration}",String.valueOf(t.getDuration()));
 		sql  = sql.replace("#{is_tostart}",String.valueOf(t.getIsTostart()));
 		sql  = sql.replace("#{is_async}",String.valueOf(t.getIsAsync()));
 		sql  = sql.replace("#{is_back}",String.valueOf(t.getIsBack()));
 		sql  = sql.replace("#{is_revocation}",String.valueOf(t.getIsRevocation()));
 		sql  = sql.replace("#{visual_properties}",String.valueOf(t.getVisualProperties()));
 		sql  = sql.replace("#{split_type}",String.valueOf(t.getSplitType()));
 		sql  = sql.replace("#{join_type}",String.valueOf(t.getJoinType()));
 		sql  = sql.replace("#{transfer_type}",String.valueOf(t.getTransferType()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}
}