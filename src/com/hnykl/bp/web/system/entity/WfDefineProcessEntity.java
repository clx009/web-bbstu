package com.hnykl.bp.web.system.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.lang.String;
import java.lang.Double;
import java.lang.Integer;
import java.math.BigDecimal;
import javax.xml.soap.Text;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 自定义流程
 * @author onlineGenerator
 * @date 2014-05-30 10:41:39
 * @version V1.0   
 *
 */
@Entity
@Table(name = "wf_define_process", schema = "")
@SuppressWarnings("serial")
public class WfDefineProcessEntity implements java.io.Serializable {
	/**流程定义主键*/
	private java.lang.String id;
	/**同一流程流程标识，此值相同则为同一流程的不同版本*/
	private java.lang.String processNo;
	/**流程版本*/
	private java.lang.Integer version;
	/**流程名称*/
	private java.lang.String processName;
	/**是否有效*/
	private java.lang.String isEnable;
	/**是否被软删除*/
	private java.lang.String isDelete;
	/**流程处理时间*/
	private java.lang.Integer duration;
	/**流程创建日期*/
	private java.util.Date createDate;
	/**流程创建人*/
	private java.lang.String createUserId;
	/**备注*/
	private String remark;
	/**暂存流程定义的XML*/
	private java.lang.String tempProcessXml;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  流程定义主键
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  流程定义主键
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  同一流程流程标识，此值相同则为同一流程的不同版本
	 */
	@Column(name ="PROCESS_NO",nullable=false,length=32)
	public java.lang.String getProcessNo(){
		return this.processNo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  同一流程流程标识，此值相同则为同一流程的不同版本
	 */
	public void setProcessNo(java.lang.String processNo){
		this.processNo = processNo;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  流程版本
	 */
	@Column(name ="VERSION",nullable=false,length=10)
	public java.lang.Integer getVersion(){
		return this.version;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  流程版本
	 */
	public void setVersion(java.lang.Integer version){
		this.version = version;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  流程名称
	 */
	@Column(name ="PROCESS_NAME",nullable=false,length=256)
	public java.lang.String getProcessName(){
		return this.processName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  流程名称
	 */
	public void setProcessName(java.lang.String processName){
		this.processName = processName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  是否有效
	 */
	@Column(name ="IS_ENABLE",nullable=false,length=1)
	public java.lang.String getIsEnable(){
		return this.isEnable;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  是否有效
	 */
	public void setIsEnable(java.lang.String isEnable){
		this.isEnable = isEnable;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  是否被软删除
	 */
	@Column(name ="IS_DELETE",nullable=false,length=1)
	public java.lang.String getIsDelete(){
		return this.isDelete;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  是否被软删除
	 */
	public void setIsDelete(java.lang.String isDelete){
		this.isDelete = isDelete;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  流程处理时间
	 */
	@Column(name ="DURATION",nullable=false,length=10)
	public java.lang.Integer getDuration(){
		return this.duration;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  流程处理时间
	 */
	public void setDuration(java.lang.Integer duration){
		this.duration = duration;
	}
	/**
	 *方法: 取得java.util.Date
	 *@return: java.util.Date  流程创建日期
	 */
	@Column(name ="CREATE_DATE",nullable=false)
	public java.util.Date getCreateDate(){
		return this.createDate;
	}

	/**
	 *方法: 设置java.util.Date
	 *@param: java.util.Date  流程创建日期
	 */
	public void setCreateDate(java.util.Date createDate){
		this.createDate = createDate;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  流程创建人
	 */
	@Column(name ="CREATE_USER_ID",nullable=false,length=32)
	public java.lang.String getCreateUserId(){
		return this.createUserId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  流程创建人
	 */
	public void setCreateUserId(java.lang.String createUserId){
		this.createUserId = createUserId;
	}
	/**
	 *方法: 取得javax.xml.soap.Text
	 *@return: javax.xml.soap.Text  备注
	 */
	@Column(name ="REMARK",nullable=false,length=4000)
	public String getRemark(){
		return this.remark;
	}

	/**
	 *方法: 设置javax.xml.soap.Text
	 *@param: javax.xml.soap.Text  备注
	 */
	public void setRemark(String remark){
		this.remark = remark;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  暂存流程定义的XML
	 */
	@Column(name ="TEMP_PROCESS_XML",nullable=false)
	public java.lang.String getTempProcessXml(){
		return this.tempProcessXml;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  暂存流程定义的XML
	 */
	public void setTempProcessXml(java.lang.String tempProcessXml){
		this.tempProcessXml = tempProcessXml;
	}
}
