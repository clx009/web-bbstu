package com.hnykl.bp.web.system.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.lang.String;
import java.lang.Double;
import java.lang.Integer;
import java.math.BigDecimal;
import javax.xml.soap.Text;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 参数管理
 * @author onlineGenerator
 * @date 2014-04-23 10:37:28
 * @version V1.0   
 *
 */
@Entity
@Table(name = "sys_paramter", schema = "")
@SuppressWarnings("serial")
public class SysParamterEntity implements java.io.Serializable {
	/**编号*/
	private java.lang.String id;
	/**参数名称*/
	private java.lang.String name;
	/**参数类型*/
	private java.lang.String codeType;
	/**参数代码*/
	private java.lang.String code;
	/**参数数值*/
	private java.lang.String val;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  编号
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  编号
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  参数名称
	 */
	@Column(name ="NAME",nullable=true,length=50)
	public java.lang.String getName(){
		return this.name;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  参数名称
	 */
	public void setName(java.lang.String name){
		this.name = name;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  参数类型
	 */
	@Column(name ="CODE_TYPE",nullable=true,length=2)
	public java.lang.String getCodeType(){
		return this.codeType;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  参数类型
	 */
	public void setCodeType(java.lang.String codeType){
		this.codeType = codeType;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  参数代码
	 */
	@Column(name ="CODE",nullable=true,length=50)
	public java.lang.String getCode(){
		return this.code;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  参数代码
	 */
	public void setCode(java.lang.String code){
		this.code = code;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  参数数值
	 */
	@Column(name ="VAL",nullable=true,length=100)
	public java.lang.String getVal(){
		return this.val;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  参数数值
	 */
	public void setVal(java.lang.String val){
		this.val = val;
	}
}
