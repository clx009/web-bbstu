package com.hnykl.bp.web.system.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.lang.String;
import java.lang.Double;
import java.lang.Integer;
import java.math.BigDecimal;
import javax.xml.soap.Text;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: 流程活动定义
 * @author onlineGenerator
 * @date 2014-05-30 11:46:09
 * @version V1.0   
 *
 */
@Entity
@Table(name = "wf_define_activity", schema = "")
@SuppressWarnings("serial")
public class WfDefineActivityEntity implements java.io.Serializable {
	/**主键*/
	private java.lang.String id;
	/**流程定义主键*/
	private java.lang.String processId;
	/**活动类型主键*/
	private java.lang.String activityTypeId;
	/**活动名称*/
	private java.lang.String activityName;
	/**活动序号*/
	private java.lang.String activityNo;
	/**活动处理时间*/
	private java.lang.Integer duration;
	/**是否可以回到起点*/
	private java.lang.String isTostart;
	/**是否异步？*/
	private java.lang.String isAsync;
	/**是否允许打回*/
	private java.lang.String isBack;
	/**是否被撤回*/
	private java.lang.String isRevocation;
	/**可视化定制参数*/
	private String visualProperties;
	/**拆分类型  [ AND   OR    XOR   会签 ]*/
	private java.lang.String splitType;
	/**合并类型  [ AND        XOR    RULE ]*/
	private java.lang.String joinType;
	/**转移类型  [ 手工  自动 ]*/
	private java.lang.String transferType;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  主键
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false,length=32)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  主键
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  流程定义主键
	 */
	@Column(name ="PROCESS_ID",nullable=false,length=32)
	public java.lang.String getProcessId(){
		return this.processId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  流程定义主键
	 */
	public void setProcessId(java.lang.String processId){
		this.processId = processId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  活动类型主键
	 */
	@Column(name ="ACTIVITY_TYPE_ID",nullable=false,length=32)
	public java.lang.String getActivityTypeId(){
		return this.activityTypeId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  活动类型主键
	 */
	public void setActivityTypeId(java.lang.String activityTypeId){
		this.activityTypeId = activityTypeId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  活动名称
	 */
	@Column(name ="ACTIVITY_NAME",nullable=false,length=256)
	public java.lang.String getActivityName(){
		return this.activityName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  活动名称
	 */
	public void setActivityName(java.lang.String activityName){
		this.activityName = activityName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  活动序号
	 */
	@Column(name ="ACTIVITY_NO",nullable=false,length=32)
	public java.lang.String getActivityNo(){
		return this.activityNo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  活动序号
	 */
	public void setActivityNo(java.lang.String activityNo){
		this.activityNo = activityNo;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  活动处理时间
	 */
	@Column(name ="DURATION",nullable=false,length=10)
	public java.lang.Integer getDuration(){
		return this.duration;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  活动处理时间
	 */
	public void setDuration(java.lang.Integer duration){
		this.duration = duration;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  是否可以回到起点
	 */
	@Column(name ="IS_TOSTART",nullable=false,length=1)
	public java.lang.String getIsTostart(){
		return this.isTostart;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  是否可以回到起点
	 */
	public void setIsTostart(java.lang.String isTostart){
		this.isTostart = isTostart;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  是否异步？
	 */
	@Column(name ="IS_ASYNC",nullable=false,length=1)
	public java.lang.String getIsAsync(){
		return this.isAsync;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  是否异步？
	 */
	public void setIsAsync(java.lang.String isAsync){
		this.isAsync = isAsync;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  是否允许打回
	 */
	@Column(name ="IS_BACK",nullable=false,length=1)
	public java.lang.String getIsBack(){
		return this.isBack;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  是否允许打回
	 */
	public void setIsBack(java.lang.String isBack){
		this.isBack = isBack;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  是否被撤回
	 */
	@Column(name ="IS_REVOCATION",nullable=false,length=1)
	public java.lang.String getIsRevocation(){
		return this.isRevocation;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  是否被撤回
	 */
	public void setIsRevocation(java.lang.String isRevocation){
		this.isRevocation = isRevocation;
	}
	/**
	 *方法: 取得javax.xml.soap.Text
	 *@return: javax.xml.soap.Text  可视化定制参数
	 */
	@Column(name ="VISUAL_PROPERTIES",nullable=false,length=4000)
	public String getVisualProperties(){
		return this.visualProperties;
	}

	/**
	 *方法: 设置javax.xml.soap.Text
	 *@param: javax.xml.soap.Text  可视化定制参数
	 */
	public void setVisualProperties(String visualProperties){
		this.visualProperties = visualProperties;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  拆分类型  [ AND   OR    XOR   会签 ]
	 */
	@Column(name ="SPLIT_TYPE",nullable=false,length=32)
	public java.lang.String getSplitType(){
		return this.splitType;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  拆分类型  [ AND   OR    XOR   会签 ]
	 */
	public void setSplitType(java.lang.String splitType){
		this.splitType = splitType;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  合并类型  [ AND        XOR    RULE ]
	 */
	@Column(name ="JOIN_TYPE",nullable=false,length=32)
	public java.lang.String getJoinType(){
		return this.joinType;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  合并类型  [ AND        XOR    RULE ]
	 */
	public void setJoinType(java.lang.String joinType){
		this.joinType = joinType;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  转移类型  [ 手工  自动 ]
	 */
	@Column(name ="TRANSFER_TYPE",nullable=false,length=20)
	public java.lang.String getTransferType(){
		return this.transferType;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  转移类型  [ 手工  自动 ]
	 */
	public void setTransferType(java.lang.String transferType){
		this.transferType = transferType;
	}
}
