package com.hnykl.bp.web.position.controller;
import java.util.List;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.position.entity.TPPositionEntity;
import com.hnykl.bp.web.position.service.TPPositionServiceI;

/**   
 * @Title: Controller
 * @Description: t_p_position
 * @author onlineGenerator
 * @date 2016-05-30 14:07:58
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/tPPositionController")
public class TPPositionController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TPPositionController.class);

	@Autowired
	private TPPositionServiceI tPPositionService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * t_p_position列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "tPPosition")
	public ModelAndView tPPosition(HttpServletRequest request) {
		return new ModelAndView("position/tPPosition/tPPositionList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(TPPositionEntity tPPosition,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TPPositionEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, tPPosition, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.tPPositionService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除t_p_position
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(TPPositionEntity tPPosition, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		tPPosition = systemService.getEntity(TPPositionEntity.class, tPPosition.getId());
		message = "t_p_position删除成功";
		try{
			tPPositionService.delete(tPPosition);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_p_position删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除t_p_position
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "t_p_position删除成功";
		try{
			for(String id:ids.split(",")){
				TPPositionEntity tPPosition = systemService.getEntity(TPPositionEntity.class, 
				id
				);
				tPPositionService.delete(tPPosition);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "t_p_position删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加t_p_position
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(TPPositionEntity tPPosition, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_p_position添加成功";
		try{
			tPPositionService.save(tPPosition);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_p_position添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新t_p_position
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(TPPositionEntity tPPosition, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_p_position更新成功";
		TPPositionEntity t = tPPositionService.get(TPPositionEntity.class, tPPosition.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(tPPosition, t);
			tPPositionService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_p_position更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * t_p_position新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(TPPositionEntity tPPosition, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tPPosition.getId())) {
			tPPosition = tPPositionService.getEntity(TPPositionEntity.class, tPPosition.getId());
			req.setAttribute("tPPositionPage", tPPosition);
		}
		return new ModelAndView("position/tPPosition/tPPosition-add");
	}
	/**
	 * t_p_position编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(TPPositionEntity tPPosition, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tPPosition.getId())) {
			tPPosition = tPPositionService.getEntity(TPPositionEntity.class, tPPosition.getId());
			req.setAttribute("tPPositionPage", tPPosition);
		}
		return new ModelAndView("position/tPPosition/tPPosition-update");
	}
}
