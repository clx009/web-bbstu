package com.hnykl.bp.web.plan.controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.common.UploadFile;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.common.model.json.DataGridReturn;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.RoletoJson;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.shijian.entity.TSShsjEntity;
import com.hnykl.bp.web.system.dto.StudentComboboxDTO;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.plan.entity.TSPlanContentEntity;
import com.hnykl.bp.web.plan.service.TSPlanContentServiceI;

/**   
 * @Title: Controller
 * @Description: t_s_plan_content
 * @author onlineGenerator
 * @date 2016-10-07 19:58:37
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/tSPlanContentController")
public class TSPlanContentController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TSPlanContentController.class);

	@Autowired
	private TSPlanContentServiceI tSPlanContentService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * t_s_plan_content列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "tSPlanContent")
	public ModelAndView tSPlanContent(HttpServletRequest request) {
		List<StudentComboboxDTO> lst = new ArrayList<StudentComboboxDTO>();
		lst = systemService.studentCombobox(""); 
		if (!lst.isEmpty())
			request.setAttribute("userReplace",
					RoletoJson.listToReplaceStr(lst , "realname", "id"));
		return new ModelAndView("plan/tSPlanContent/tSPlanContentList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(TSPlanContentEntity tSPlanContent,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSPlanContentEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, tSPlanContent, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		DataGridReturn dataGridRows = this.tSPlanContentService.getDataGridReturn(cq, true);
		List<TSPlanContentEntity> rows = dataGridRows.getRows(); 
		for(TSPlanContentEntity row:rows){
			String oldPath = row.getPath();
			row.setPath("<a href="+BaseConfigMgr.getWebVisitUrl()+"/"+oldPath+">"+oldPath+"</a>"); 
		}
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除t_s_plan_content
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(TSPlanContentEntity tSPlanContent, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		tSPlanContent = systemService.getEntity(TSPlanContentEntity.class, tSPlanContent.getId());
		message = "t_s_plan_content删除成功";
		try{
			tSPlanContentService.delete(tSPlanContent);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_plan_content删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除t_s_plan_content
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "t_s_plan_content删除成功";
		try{
			for(String id:ids.split(",")){
				TSPlanContentEntity tSPlanContent = systemService.getEntity(TSPlanContentEntity.class, 
				id
				);
				tSPlanContentService.delete(tSPlanContent);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_plan_content删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加t_s_plan_content
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_s_plan_content添加成功";
		try{
			String id = request.getParameter("id");
			String userId = request.getParameter("userid");
			String schoolYear = request.getParameter("schoolYear");
			String term = request.getParameter("term");
			String name = request.getParameter("name"); 
			String result = request.getParameter("result"); 
			String entrancescore = request.getParameter("entrancescore");
			String teacher = request.getParameter("teacher");
			
			TSPlanContentEntity o = new TSPlanContentEntity();
			o.setId(id);
			o.setUserId(userId);
			o.setSchoolYear(schoolYear);
			o.setTerm(term);
			o.setName(name);
			o.setResult(result);
			o.setEntrancescore(entrancescore);
			o.setTeacher(teacher);
			UploadFile uploadFile = new UploadFile(request, o);
			uploadFile.setCusPath("files");
			uploadFile.setExtend("extend"); 
			uploadFile.setRealPath("path"); 
			uploadFile.setRename(false);
			systemService.uploadFile(uploadFile);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_plan_content添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新t_s_plan_content
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(TSPlanContentEntity tSPlanContent, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_s_plan_content更新成功";
		TSPlanContentEntity t = tSPlanContentService.get(TSPlanContentEntity.class, tSPlanContent.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(tSPlanContent, t);
			tSPlanContentService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_s_plan_content更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * t_s_plan_content新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(TSPlanContentEntity tSPlanContent, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tSPlanContent.getId())) {
			tSPlanContent = tSPlanContentService.getEntity(TSPlanContentEntity.class, tSPlanContent.getId());
			req.setAttribute("tSPlanContentPage", tSPlanContent);
		}
		return new ModelAndView("plan/tSPlanContent/tSPlanContent-add");
	}
	/**
	 * t_s_plan_content编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(TSPlanContentEntity tSPlanContent, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tSPlanContent.getId())) {
			tSPlanContent = tSPlanContentService.getEntity(TSPlanContentEntity.class, tSPlanContent.getId());
			req.setAttribute("tSPlanContentPage", tSPlanContent);
		}
		return new ModelAndView("plan/tSPlanContent/tSPlanContent-update");
	}
}
