package com.hnykl.bp.web.plan.controller;
import java.util.ArrayList;
import java.util.List;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.common.UploadFile;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.RoletoJson;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.dto.StudentComboboxDTO;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.plan.entity.TSPlanContentEntity;
import com.hnykl.bp.web.plan.entity.TSPlanEntity;
import com.hnykl.bp.web.plan.service.TSPlanServiceI;

/**   
 * @Title: Controller
 * @Description: t_s_plan
 * @author onlineGenerator
 * @date 2016-10-07 19:58:27
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/tSPlanController")
public class TSPlanController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TSPlanController.class);

	@Autowired
	private TSPlanServiceI tSPlanService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * t_s_plan列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "tSPlan")
	public ModelAndView tSPlan(HttpServletRequest request) {
		
		List<StudentComboboxDTO> lst = new ArrayList<StudentComboboxDTO>();
		lst = systemService.studentCombobox(""); 
		if (!lst.isEmpty())
			request.setAttribute("userReplace",
					RoletoJson.listToReplaceStr(lst , "realname", "id"));
		return new ModelAndView("plan/tSPlan/tSPlanList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(TSPlanEntity tSPlan,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSPlanEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, tSPlan, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.tSPlanService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除t_s_plan
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(TSPlanEntity tSPlan, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		tSPlan = systemService.getEntity(TSPlanEntity.class, tSPlan.getId());
		message = "t_s_plan删除成功";
		try{
			tSPlanService.delete(tSPlan);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_plan删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除t_s_plan
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "t_s_plan删除成功";
		try{
			for(String id:ids.split(",")){
				TSPlanEntity tSPlan = systemService.getEntity(TSPlanEntity.class, 
				id
				);
				tSPlanService.delete(tSPlan);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_plan删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加t_s_plan
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_s_plan添加成功";
		try{	
			String id = request.getParameter("id");
			String userId = request.getParameter("userid");
			String note = request.getParameter("note");	
			
			TSPlanEntity tSPlan = new TSPlanEntity();	
			tSPlan.setId(id);
			tSPlan.setUserId(userId);
			tSPlan.setNote(note);	
			UploadFile uploadFile = new UploadFile(request, tSPlan);
			uploadFile.setCusPath("files");
			uploadFile.setExtend("extend"); 
			uploadFile.setRealPath("path"); 
			uploadFile.setRename(false);
			systemService.uploadFile(uploadFile);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_plan添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新t_s_plan
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(TSPlanEntity tSPlan, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_s_plan更新成功";
		TSPlanEntity t = tSPlanService.get(TSPlanEntity.class, tSPlan.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(tSPlan, t);
			tSPlanService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_s_plan更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * t_s_plan新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(TSPlanEntity tSPlan, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tSPlan.getId())) {
			tSPlan = tSPlanService.getEntity(TSPlanEntity.class, tSPlan.getId());
			req.setAttribute("tSPlanPage", tSPlan);
		}
		return new ModelAndView("plan/tSPlan/tSPlan-add");
	}
	/**
	 * t_s_plan编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(TSPlanEntity tSPlan, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tSPlan.getId())) {
			tSPlan = tSPlanService.getEntity(TSPlanEntity.class, tSPlan.getId());
			req.setAttribute("tSPlanPage", tSPlan);
		}
		return new ModelAndView("plan/tSPlan/tSPlan-update");
	}
}
