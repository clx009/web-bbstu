package com.hnykl.bp.web.school.controller;

import java.util.List;
import java.util.Map;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jersey.repackaged.com.google.common.collect.Lists;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.BrowserUtils;
import com.hnykl.bp.base.core.util.ExceptionUtil;
import com.hnykl.bp.base.core.util.RoletoJson;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.poi.excel.ExcelExportUtil;
import com.hnykl.bp.poi.excel.ExcelImportUtil;
import com.hnykl.bp.poi.excel.entity.ExcelTitle;
import com.hnykl.bp.poi.excel.entity.ImportParams;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.entity.TerritoryEntity;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.school.entity.MajorEntity;
import com.hnykl.bp.web.school.entity.SchoolEntity;
import com.hnykl.bp.web.school.entity.SchoolMajorEntity;
import com.hnykl.bp.web.school.service.MajorServiceI;
import com.hnykl.bp.web.school.service.SchoolMajorServiceI;
import com.hnykl.bp.web.school.service.SchoolServiceI;

/**
 * @Title: Controller
 * @Description: 学校信息管理
 * @author onlineGenerator
 * @date 2016-06-20 15:49:41
 * @version V1.0
 * 
 */
@Controller
@RequestMapping("/schoolMajorController")
public class SchoolMajorController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(SchoolMajorController.class);

	@Autowired
	private SchoolMajorServiceI schoolMajorService;
	@Autowired
	private SystemService systemService;
	@Autowired
	private SchoolServiceI schoolService;
	@Autowired
	private MajorServiceI majorService;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 学校信息管理列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "schoolMajor")
	public ModelAndView schoolMajor(HttpServletRequest request) {
		List<MajorEntity> majorList = majorService.getList(MajorEntity.class);
		if (!majorList.isEmpty())
			request.setAttribute("majorReplace",
					RoletoJson.listToReplaceStr(majorList, "name", "id"));
		List<SchoolEntity> schoolList = schoolService
				.getList(SchoolEntity.class);
		if (!schoolList.isEmpty())
			request.setAttribute("schoolReplace", RoletoJson.listToReplaceStr(
					schoolList, "chineseName", "id"));

		return new ModelAndView("school/schoolMajor/schoolMajorList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(SchoolMajorEntity schoolMajor,
			HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(SchoolMajorEntity.class, dataGrid);
		// 查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq,
				schoolMajor, request.getParameterMap());
		try {
			// 自定义追加查询条件
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.schoolMajorService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除学校信息管理
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(SchoolMajorEntity schoolMajor,
			HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		schoolMajor = systemService.getEntity(SchoolMajorEntity.class,
				schoolMajor.getId());
		message = "学校信息管理删除成功";
		try {
			schoolMajorService.delete(schoolMajor);
			systemService.addLog(message, Globals.Log_Type_DEL,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "学校信息管理删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 批量删除学校信息管理
	 * 
	 * @return
	 */
	@RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "学校信息管理删除成功";
		try {
			for (String id : ids.split(",")) {
				SchoolMajorEntity schoolMajor = systemService.getEntity(
						SchoolMajorEntity.class, id);
				schoolMajorService.delete(schoolMajor);
				systemService.addLog(message, Globals.Log_Type_DEL,
						Globals.Log_Leavel_INFO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			message = "学校信息管理删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 添加学校信息管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(SchoolMajorEntity schoolMajor,
			HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "添加成功";
		try {
			if (schoolMajorService.isExists(schoolMajor)!=null) {
				message = "添加失败：专业排名已存在";
			} else {
				schoolMajorService.save(schoolMajor);
				systemService.addLog(message, Globals.Log_Type_INSERT,
						Globals.Log_Leavel_INFO);
			}

		} catch (Exception e) {
			e.printStackTrace();
			message = "添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 更新学校信息管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(SchoolMajorEntity schoolMajor,
			HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "学校信息管理更新成功";
		SchoolMajorEntity t = schoolMajorService.get(SchoolMajorEntity.class,
				schoolMajor.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(schoolMajor, t);
			schoolMajorService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "学校信息管理更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 学校信息管理新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(SchoolMajorEntity schoolMajor,
			HttpServletRequest req) {
		if (StringUtil.isNotEmpty(schoolMajor.getId())) {
			schoolMajor = schoolMajorService.getEntity(SchoolMajorEntity.class,
					schoolMajor.getId());
			req.setAttribute("schoolMajorPage", schoolMajor);
		}
		return new ModelAndView("school/schoolMajor/schoolMajor-add");
	}

	/**
	 * 学校信息管理编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(SchoolMajorEntity schoolMajor,
			HttpServletRequest req) {
		if (StringUtil.isNotEmpty(schoolMajor.getId())) {
			schoolMajor = schoolMajorService.getEntity(SchoolMajorEntity.class,
					schoolMajor.getId());
			req.setAttribute("schoolMajorPage", schoolMajor);
		}
		return new ModelAndView("school/schoolMajor/schoolMajor-update");
	}
	
	/**
	 * 打开导出列表
	 * 
	 * @return
	 */
	@RequestMapping(params = "upload")
	public ModelAndView upload(HttpServletRequest req) {
		return new ModelAndView("school/schoolMajor/schoolMajorUpload");
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(params = "importExcel", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson importExcel(HttpServletRequest request,
			HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		String importMessage = "";

		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile file = entity.getValue();// 获取上传文件对象
			ImportParams params = new ImportParams();
			params.setTitleRows(1);
			params.setSecondTitleRows(2);
			params.setNeedSave(true);
			try {
				List<SchoolMajorEntity> list = (List<SchoolMajorEntity>) ExcelImportUtil
						.importExcelByIs(file.getInputStream(),
								SchoolMajorEntity.class, params);
				
				List<SchoolEntity> schoolList = schoolService.getList(SchoolEntity.class);
				
				List<MajorEntity> majorList = majorService.getList(MajorEntity.class);
				for (SchoolMajorEntity sme : list) {
					if (StringUtils.isNotBlank(sme.getSchoolCode())
							&& StringUtils.isNotBlank(sme.getSchoolCode())) {  
						for(SchoolEntity se:schoolList){
							if(se.getCode()!=null && sme.getSchoolCode()!=null && se.getCode().equals(sme.getSchoolCode())){
								sme.setSchool(se);
								break;
							}
						}
						
						for(MajorEntity me:majorList){
							if(me.getCode()!=null && sme.getMajorCode()!=null&&me.getCode().equals(sme.getMajorCode())){
								sme.setMajor(me);
								break;
							}
						} 
						
						if(sme.getSchool()==null){
							importMessage += "学校编号：'"+sme.getSchoolCode()+"'不存在\n";
							continue;
						}
						if(sme.getMajor()==null){
							importMessage += "专业编号：'"+sme.getMajorCode()+"'不存在\n";
							continue;
						}
						SchoolMajorEntity exists = schoolMajorService.isExists(sme);
						if(exists!=null){
							MyBeanUtils.copyBeanNotNull2Bean(sme, exists);
							schoolMajorService.saveOrUpdate(exists);
						}else{
							schoolMajorService.save(sme); 
						}
						
					}
				}
				j.setMsg("导入成功:<br>"+importMessage);
			} catch (Exception e) {
				e.printStackTrace();
				j.setMsg("文件导入失败！");
				logger.error(ExceptionUtil.getExceptionMessage(e));
			} finally {
				try {
					file.getInputStream().close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return j;
	}
	
	/**
	 * 导出excel
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(params = "exportXls")
	public void exportXls(SchoolMajorEntity schoolMajor,HttpServletRequest request,HttpServletResponse response
			, DataGrid dataGrid) {
		response.setContentType("application/vnd.ms-excel");
		String codedFileName = null;
		OutputStream fOut = null;
		try {
			codedFileName = "专业排名资料";
			// 根据浏览器进行转码，使其支持中文文件名
			if (BrowserUtils.isIE(request)) {
				response.setHeader(
						"content-disposition",
						"attachment;filename="
								+ java.net.URLEncoder.encode(codedFileName,
										"UTF-8") + ".xls");
			} else {
				String newtitle = new String(codedFileName.getBytes("UTF-8"),
						"ISO8859-1");
				response.setHeader("content-disposition",
						"attachment;filename=" + newtitle + ".xls");
			}
			// 产生工作簿对象
			HSSFWorkbook workbook = null;
			CriteriaQuery cq = new CriteriaQuery(SchoolMajorEntity.class, dataGrid);
			com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, schoolMajor, request.getParameterMap());
			List<SchoolMajorEntity> schoolMajors = this.schoolService.getListByCriteriaQuery(cq,false);
			
			for(SchoolMajorEntity sme:schoolMajors){
				if(sme.getSchool()!=null)
					sme.setSchoolCode(sme.getSchool().getCode());
				if(sme.getMajor()!=null){
					sme.setMajorCode(sme.getMajor().getCode());
				}
			}
			
			workbook = ExcelExportUtil.exportExcel(new ExcelTitle("专业排名资料", "可直接作为导入模板,注意：作为导入模板时专业编号和学校编号不允许修改，只允许修改排名",
					"导出信息"), SchoolMajorEntity.class, schoolMajors);
			fOut = response.getOutputStream();
			workbook.write(fOut);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				fOut.flush();
				fOut.close();
			} catch (IOException e) {

			}
		}
	}

}
