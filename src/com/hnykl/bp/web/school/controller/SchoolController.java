package com.hnykl.bp.web.school.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse; 

import jersey.repackaged.com.google.common.collect.Lists;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.BrowserUtils;
import com.hnykl.bp.base.core.util.ExceptionUtil;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.poi.excel.ExcelExportUtil;
import com.hnykl.bp.poi.excel.ExcelImportUtil;
import com.hnykl.bp.poi.excel.entity.ExcelTitle;
import com.hnykl.bp.poi.excel.entity.ImportParams;
import com.hnykl.bp.tag.core.easyui.TagUtil; 
import com.hnykl.bp.web.demo.entity.test.CourseEntity;
import com.hnykl.bp.web.system.entity.TerritoryEntity;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.school.entity.SchoolEntity;
import com.hnykl.bp.web.school.service.SchoolServiceI;

/**
 * @Title: Controller
 * @Description: 学校信息管理
 * @author onlineGenerator
 * @date 2016-06-20 15:51:32
 * @version V1.0
 * 
 */
@Controller
@RequestMapping("/schoolController")
public class SchoolController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(SchoolController.class);

	@Autowired
	private SchoolServiceI schoolService;
	@Autowired
	private SystemService systemService; 

	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 学校信息管理列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "school")
	public ModelAndView school(HttpServletRequest request) {
		return new ModelAndView("school/school/schoolList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(params = "datagrid")
	public void datagrid(SchoolEntity school, HttpServletRequest request,
			HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(SchoolEntity.class, dataGrid);
		// 查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq,
				school, request.getParameterMap());
		try {
			// 自定义追加查询条件
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.schoolService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除学校信息管理
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(SchoolEntity school, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		school = systemService.getEntity(SchoolEntity.class, school.getId());
		message = "学校信息管理删除成功";
		try {
			schoolService.delete(school);
			systemService.addLog(message, Globals.Log_Type_DEL,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "学校信息管理删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 批量删除学校信息管理
	 * 
	 * @return
	 */
	@RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "学校信息管理删除成功";
		try {
			for (String id : ids.split(",")) {
				SchoolEntity school = systemService.getEntity(
						SchoolEntity.class, id);
				schoolService.delete(school);
				systemService.addLog(message, Globals.Log_Type_DEL,
						Globals.Log_Leavel_INFO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			message = "学校信息管理删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 添加学校信息管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(SchoolEntity school, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "学校信息管理添加成功";
		try {
			schoolService.save(school);
			systemService.addLog(message, Globals.Log_Type_INSERT,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "学校信息管理添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 更新学校信息管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(SchoolEntity school, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "学校信息管理更新成功";
		SchoolEntity t = schoolService.get(SchoolEntity.class, school.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(school, t);
			schoolService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE,
					Globals.Log_Leavel_INFO);

		} catch (Exception e) {
			e.printStackTrace();
			message = "学校信息管理更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 学校信息管理新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(SchoolEntity school, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(school.getId())) {
			school = schoolService
					.getEntity(SchoolEntity.class, school.getId());
			req.setAttribute("schoolPage", school);
		}
		return new ModelAndView("school/school/school-add");
	}

	/**
	 * 学校信息管理编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(SchoolEntity school, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(school.getId())) {
			school = schoolService
					.getEntity(SchoolEntity.class, school.getId());
			req.setAttribute("schoolPage", school);
		}
		return new ModelAndView("school/school/school-update");
	}

	/**
	 * 学校combbox
	 * 
	 * @param q
	 * @return
	 */
	@RequestMapping(params = "schoolSelectItem")
	@ResponseBody
	public List<SchoolEntity> schoolSelectItem(String q) {
		List<SchoolEntity> lst = schoolService.getSchoolSelectItems(q);
		return lst;
	}

	/**
	 * 打开导出列表
	 * 
	 * @return
	 */
	@RequestMapping(params = "upload")
	public ModelAndView upload(HttpServletRequest req) {
		return new ModelAndView("school/school/schoolUpload");
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(params = "importExcel", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson importExcel(HttpServletRequest request,
			HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		String importMessage = "";

		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile file = entity.getValue();// 获取上传文件对象
			ImportParams params = new ImportParams();
			params.setTitleRows(1);
			params.setSecondTitleRows(2);
			params.setNeedSave(true);
			try {
				List<SchoolEntity> listSchool = (List<SchoolEntity>) ExcelImportUtil
						.importExcelByIs(file.getInputStream(),
								SchoolEntity.class, params);
				List<TerritoryEntity> citys = systemService.getList(TerritoryEntity.class);
				for (SchoolEntity school : listSchool) {
					if (school.getEnglishName() != null) {
						String cityId = null;
						for(TerritoryEntity te:citys){
							if(school.getCityId()!=null&&school.getCityId().equals(te.getTerritorycode())){
								cityId = te.getId();
								break;
							}
						}
						school.setCityId(cityId);
						List<SchoolEntity> findSchoolByCode = schoolService
								.findByProperty(SchoolEntity.class, "code",
										school.getCode());
						if (findSchoolByCode.isEmpty()) {
							schoolService.save(school);
							importMessage += "学校：" + school.getEnglishName()
									+ "添加成功<br>";
						} else {
							SchoolEntity schoolEntity = findSchoolByCode.get(0);
							MyBeanUtils.copyBeanNotNull2Bean(school, schoolEntity);
							schoolService.saveOrUpdate(schoolEntity);
							importMessage += "学校：" + school.getEnglishName()
									+ "修改成功<br>";
						}
					}
				}
				j.setMsg(importMessage);
			} catch (Exception e) {
				e.printStackTrace();
				j.setMsg("文件导入失败！");
				logger.error(ExceptionUtil.getExceptionMessage(e));
			} finally {
				try {
					file.getInputStream().close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return j;
	}
	
	/**
	 * 导出excel
	 * 
	 * @param request
	 * @param response
	 */
	@RequestMapping(params = "exportXls")
	public void exportXls(SchoolEntity school,HttpServletRequest request,HttpServletResponse response
			, DataGrid dataGrid) {
		response.setContentType("application/vnd.ms-excel");
		String codedFileName = null;
		OutputStream fOut = null;
		try {
			codedFileName = "学校资料";
			// 根据浏览器进行转码，使其支持中文文件名
			if (BrowserUtils.isIE(request)) {
				response.setHeader(
						"content-disposition",
						"attachment;filename="
								+ java.net.URLEncoder.encode(codedFileName,
										"UTF-8") + ".xls");
			} else {
				String newtitle = new String(codedFileName.getBytes("UTF-8"),
						"ISO8859-1");
				response.setHeader("content-disposition",
						"attachment;filename=" + newtitle + ".xls");
			}
			// 产生工作簿对象
			HSSFWorkbook workbook = null;
			CriteriaQuery cq = new CriteriaQuery(SchoolEntity.class, dataGrid);
			com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, school, request.getParameterMap());
			List<SchoolEntity> schools = this.schoolService.getListByCriteriaQuery(cq,false);
			List<SchoolEntity> newSchools = Lists.newArrayList();
			List<TerritoryEntity> citys = systemService.getList(TerritoryEntity.class);
			
			for(SchoolEntity sch :schools){
				String id = sch.getCityId();
				String cityCode = "";
				for(TerritoryEntity te:citys){
					if(id!=null&&id.equals(te.getId())){
						cityCode = te.getTerritorycode();
						break;
					}
				}  
				sch.setCityId(cityCode);
				newSchools.add(sch);
			}
			
			workbook = ExcelExportUtil.exportExcel(new ExcelTitle("学校资料", "可直接作为导入模板,注意：作为导入模板时已存在的学校记录的学校编号不允许改动",
					"导出信息"), SchoolEntity.class, newSchools);
			fOut = response.getOutputStream();
			workbook.write(fOut);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				fOut.flush();
				fOut.close();
			} catch (IOException e) {

			}
		}
	}


}
