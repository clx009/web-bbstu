package com.hnykl.bp.web.school.controller;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse; 

import org.apache.commons.lang.xwork.StringUtils;
import org.apache.log4j.Logger; 
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals; 
import com.hnykl.bp.base.core.util.ExceptionUtil;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.poi.excel.ExcelImportUtil;
import com.hnykl.bp.poi.excel.entity.ImportParams;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.school.entity.MajorEntity; 
import com.hnykl.bp.web.school.entity.SchoolEntity;
import com.hnykl.bp.web.school.service.MajorServiceI;

/**
 * @Title: Controller
 * @Description: 专业课程管理
 * @author onlineGenerator
 * @date 2016-06-20 15:59:37
 * @version V1.0
 * 
 */
@Controller
@RequestMapping("/majorController") 
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class MajorController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(MajorController.class);

	@Autowired
	private MajorServiceI majorService;
	@Autowired
	private SystemService systemService;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 专业课程管理列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "major")
	public ModelAndView major(HttpServletRequest request) { 
		return new ModelAndView("school/major/majorList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(params = "datagrid")
	public void datagrid(MajorEntity major, HttpServletRequest request,
			HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(MajorEntity.class, dataGrid);
		// 查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq,
				major, request.getParameterMap());
		try {
			// 自定义追加查询条件
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.majorService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除专业课程管理
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(MajorEntity major, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		major = systemService.getEntity(MajorEntity.class, major.getId());
		message = "专业课程管理删除成功";
		try {
			majorService.delete(major);
			systemService.addLog(message, Globals.Log_Type_DEL,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "专业课程管理删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 批量删除专业课程管理
	 * 
	 * @return
	 */
	@RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "专业课程管理删除成功";
		try {
			for (String id : ids.split(",")) {
				MajorEntity major = systemService.getEntity(MajorEntity.class,
						id);
				majorService.delete(major);
				systemService.addLog(message, Globals.Log_Type_DEL,
						Globals.Log_Leavel_INFO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			message = "专业课程管理删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 添加专业课程管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(MajorEntity major, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "专业课程管理添加成功";
		try {
			majorService.save(major);
			systemService.addLog(message, Globals.Log_Type_INSERT,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "专业课程管理添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 更新专业课程管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(MajorEntity major, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "专业课程管理更新成功";
		MajorEntity t = majorService.get(MajorEntity.class, major.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(major, t);
			majorService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "专业课程管理更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 专业课程管理新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(MajorEntity major, HttpServletRequest req) { 
		if (StringUtil.isNotEmpty(major.getId())) {
			major = majorService.getEntity(MajorEntity.class, major.getId());
			req.setAttribute("majorPage", major);
		}
		return new ModelAndView("school/major/major-add");
	}

	/**
	 * 专业课程管理编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(MajorEntity major, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(major.getId())) {
			major = majorService.getEntity(MajorEntity.class, major.getId());
			req.setAttribute("majorPage", major);
		}
		return new ModelAndView("school/major/major-update");
	}

	/**
	 * 获取选择专业combbox选项
	 * 
	 * @return
	 */
	@RequestMapping(params = "majorSelectItem")
	@ResponseBody 
	public List<MajorEntity> majorSelectItem(String q) {
		List<MajorEntity> lst = majorService.getMajorSelectItems(q);   
		return lst;
	}
	
	
	/**
	 * 打开导出列表
	 * 
	 * @return
	 */
	@RequestMapping(params = "upload")
	public ModelAndView upload(HttpServletRequest req) {
		return new ModelAndView("school/major/majorUpload");
	}
	
	/**
	 * 执行导入
	 * @param request
	 * @param response
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(params = "importExcel", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson importExcel(HttpServletRequest request, HttpServletResponse response) {
		AjaxJson j = new AjaxJson();
		String importMessage = "";
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			MultipartFile file = entity.getValue();// 获取上传文件对象
			ImportParams params = new ImportParams();
			//params.setTitleRows(2);
			//params.setSecondTitleRows(2);
			params.setNeedSave(true);
			try {
				List<MajorEntity> majorList = 
					(List<MajorEntity>)ExcelImportUtil.importExcelByIs(file.getInputStream(),MajorEntity.class,params);
				for (MajorEntity major : majorList) {
					if(StringUtils.isNotBlank(major.getName())){
						MajorEntity majorEntity = new MajorEntity();
						majorEntity.setId("0");
						major.setParent(majorEntity);
						
						List<MajorEntity> findMajorByCode = majorService.findByProperty(
								MajorEntity.class, "code", major.getCode());
						if(findMajorByCode.isEmpty()){
							majorService.save(major);
							importMessage += "专业："+major.getName()+"导入成功";
						}else{
							importMessage += "专业："+major.getName()+"导入失败：专业编号已存在";
						}
					}
				}
				j.setMsg(importMessage);
			} catch (Exception e) {
				j.setMsg("文件导入失败！");
				logger.error(ExceptionUtil.getExceptionMessage(e));
			}finally{
				try {
					file.getInputStream().close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return j;
	}
}
