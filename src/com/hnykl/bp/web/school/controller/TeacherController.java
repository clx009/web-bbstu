package com.hnykl.bp.web.school.controller;
import java.util.List; 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.school.entity.TSchTeacherEntity;
import com.hnykl.bp.web.school.service.TeacherServiceI;

/**   
 * @Title: Controller
 * @Description: t_sch_teacher
 * @author onlineGenerator
 * @date 2016-06-20 15:50:57
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/teacherController")
public class TeacherController extends BaseController {
	/**
	 * Logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(TeacherController.class);

	@Autowired
	private TeacherServiceI teacherService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * t_sch_teacher列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "teacher")
	public ModelAndView teacher(HttpServletRequest request) {
		return new ModelAndView("school/teacher/teacherList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(params = "datagrid")
	public void datagrid(TSchTeacherEntity teacher,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSchTeacherEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, teacher, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.teacherService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除t_sch_teacher
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(TSchTeacherEntity teacher, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		teacher = systemService.getEntity(TSchTeacherEntity.class, teacher.getId());
		message = "t_sch_teacher删除成功";
		try{
			teacherService.delete(teacher);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_sch_teacher删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除t_sch_teacher
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "t_sch_teacher删除成功";
		try{
			for(String id:ids.split(",")){
				TSchTeacherEntity teacher = systemService.getEntity(TSchTeacherEntity.class, 
				id
				);
				teacherService.delete(teacher);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "t_sch_teacher删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加t_sch_teacher
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(TSchTeacherEntity teacher, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_sch_teacher添加成功";
		try{
			teacherService.save(teacher);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_sch_teacher添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新t_sch_teacher
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(TSchTeacherEntity teacher, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_sch_teacher更新成功";
		TSchTeacherEntity t = teacherService.get(TSchTeacherEntity.class, teacher.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(teacher, t);
			teacherService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_sch_teacher更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * t_sch_teacher新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(TSchTeacherEntity teacher, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(teacher.getId())) {
			teacher = teacherService.getEntity(TSchTeacherEntity.class, teacher.getId());
			req.setAttribute("teacherPage", teacher);
		}
		return new ModelAndView("school/teacher/teacher-add");
	}
	/**
	 * t_sch_teacher编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(TSchTeacherEntity teacher, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(teacher.getId())) {
			teacher = teacherService.getEntity(TSchTeacherEntity.class, teacher.getId());
			req.setAttribute("teacherPage", teacher);
		}
		return new ModelAndView("school/teacher/teacher-update");
	}
	
	/**
	 * 
	 * @param q
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(params = "combobox")
	@ResponseBody
	public List<TSchTeacherEntity> combobox(String q){
		Criteria criteria = teacherService.getSession().createCriteria(TSchTeacherEntity.class);
		if(q!=null&&!"".equals(q))
			criteria.add(Restrictions.like("name", "%"+q+"%"));
		return criteria.list();
	}
}
