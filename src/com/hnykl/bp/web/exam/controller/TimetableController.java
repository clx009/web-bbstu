package com.hnykl.bp.web.exam.controller;
import java.util.ArrayList;
import java.util.List;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jersey.repackaged.com.google.common.collect.Lists;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.RoletoJson;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.school.entity.SchoolEntity;
import com.hnykl.bp.web.school.entity.TSchTeacherEntity;
import com.hnykl.bp.web.school.service.TeacherServiceI;
import com.hnykl.bp.web.system.dto.StudentComboboxDTO;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.exam.entity.SubjectEntity;
import com.hnykl.bp.web.exam.entity.timeTableEntity;
import com.hnykl.bp.web.exam.service.SubjectServiceI;
import com.hnykl.bp.web.exam.service.TimeTableServiveI;

/**   
 * @Title: Controller
 * @Description: 课程表
 * @author onlineGenerator
 * @date 2016-06-27 15:03:21
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/timetableController")
public class TimetableController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TimetableController.class);

	@Autowired
	private TimeTableServiveI timeTableService;
	@Autowired
	private TeacherServiceI teacherService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 课程表列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "timeTable")
	public ModelAndView subject(HttpServletRequest request) {
		List<StudentComboboxDTO> lst = new ArrayList<StudentComboboxDTO>();
		lst = systemService.studentCombobox(""); 
		if (!lst.isEmpty())
			request.setAttribute("userReplace",
					RoletoJson.listToReplaceStr(lst , "realname", "id"));
		
		List<TSchTeacherEntity> teacherList = teacherService
				.getList(TSchTeacherEntity.class);
		if (!teacherList.isEmpty())
			request.setAttribute("teacherReplace",
					RoletoJson.listToReplaceStr(teacherList, "name", "id"));
		
		List<SubjectEntity> subjectList = timeTableService
				.getList(SubjectEntity.class);
		if (!subjectList.isEmpty()) {
			request.setAttribute("subjectChaniseNameReplace", RoletoJson
					.listToReplaceStr(subjectList, "chineseName", "id"));
		}
		
		return new ModelAndView("exam/timeTable/timetableList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(timeTableEntity timetable,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(timeTableEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, timetable, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.timeTableService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除课程表
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(timeTableEntity timetable, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		timetable = systemService.getEntity(timeTableEntity.class, timetable.getId());
		message = "课程表删除成功";
		try{
			timeTableService.delete(timetable);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "课程表删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除课程表
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "课程表删除成功";
		try{
			for(String id:ids.split(",")){
				timeTableEntity timetable = systemService.getEntity(timeTableEntity.class, 
				id
				);
				timeTableService.delete(timetable);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "课程表删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加课程表
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(timeTableEntity timetable, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "课程表添加成功";
		try{
			
			timetable.setStatus("未发布");
			timeTableService.save(timetable);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "课程表添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新课程表
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(timeTableEntity timetable, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "课程表更新成功";
		timeTableEntity t = timeTableService.get(timeTableEntity.class, timetable.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(timetable, t);
			timeTableService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "课程表更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * 课程表新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(timeTableEntity timetable, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(timetable.getId())) {
			timetable = timeTableService.getEntity(timeTableEntity.class, timetable.getId());
			req.setAttribute("timeTablePage", timetable);
		}
		return new ModelAndView("exam/timeTable/timeTable-add");
	}
	/**
	 * 课程表编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(timeTableEntity timetable, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(timetable.getId())) {
			timetable = timeTableService.getEntity(timeTableEntity.class, timetable.getId());
			req.setAttribute("timeTablePage", timetable);
		}
		return new ModelAndView("exam/timeTable/timeTable-update");
	}

}
