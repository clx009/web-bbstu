package com.hnykl.bp.web.exam.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.MyBeanUtils;
import com.hnykl.bp.base.core.util.RoletoJson;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.exam.entity.ExamResultEntity;
import com.hnykl.bp.web.exam.entity.SubjectEntity;
import com.hnykl.bp.web.exam.service.ExamResultServiceI;
import com.hnykl.bp.web.exam.service.SubjectServiceI;
import com.hnykl.bp.web.school.entity.TSchTeacherEntity;
import com.hnykl.bp.web.school.service.TeacherServiceI;
import com.hnykl.bp.web.system.dto.StudentComboboxDTO;
import com.hnykl.bp.web.system.service.SystemService;

/**
 * @Title: Controller
 * @Description: 成绩维护
 * @author onlineGenerator
 * @date 2016-06-27 10:32:34
 * @version V1.0
 * 
 */
@Controller
@RequestMapping("/examResultController")
public class ExamResultController extends BaseController {
	/**
	 * Logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger logger = Logger
			.getLogger(ExamResultController.class);

	@Autowired
	private ExamResultServiceI examResultService;

	@Autowired
	private SubjectServiceI subjectService;

	@Autowired
	private TeacherServiceI teacherService;

	@Autowired
	private SystemService systemService;
	private String message;

	static Map<String, Double> scores = new HashMap<String, Double>();
	static {
		scores.put("A+", 4.33);
		scores.put("A", 4.0);
		scores.put("A-", 3.67);
		scores.put("B+", 3.33);
		scores.put("B", 3.0);
		scores.put("B-", 2.67);
		scores.put("C+", 2.33);
		scores.put("C", 2.0);
		scores.put("C-", 1.67);
		scores.put("D+", 1.33);
		scores.put("D", 1.0);
		scores.put("D-", 0.67);
		scores.put("F", 0.0);
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 成绩维护列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "examResult")
	public ModelAndView examResult(HttpServletRequest request) {
		// // 给部门查询条件中的下拉框准备数据
		// List<TSUser> userList = systemService.getList(TSUser.class);
		// if (!userList.isEmpty())
		// request.setAttribute("userReplace",
		// RoletoJson.listToReplaceStr(userList, "realName", "id"));
		List<StudentComboboxDTO> lst = new ArrayList<StudentComboboxDTO>();
		lst = systemService.studentCombobox("");
		if (!lst.isEmpty())
			request.setAttribute("userReplace",
					RoletoJson.listToReplaceStr(lst, "realname", "id"));

		List<TSchTeacherEntity> teacherList = teacherService
				.getList(TSchTeacherEntity.class);
		if (!teacherList.isEmpty())
			request.setAttribute("teacherReplace",
					RoletoJson.listToReplaceStr(teacherList, "name", "id"));

		List<SubjectEntity> subjectList = subjectService
				.getList(SubjectEntity.class);
		if (!subjectList.isEmpty()) {
			request.setAttribute("subjectChaniseNameReplace", RoletoJson
					.listToReplaceStr(subjectList, "chineseName", "id"));
		}
		return new ModelAndView("exam/examResult/examResultList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(params = "datagrid")
	public void datagrid(ExamResultEntity examResult,
			HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ExamResultEntity.class, dataGrid);
		// 查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq,
				examResult, request.getParameterMap());
		try {
			// 自定义追加查询条件
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.examResultService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除成绩维护
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(ExamResultEntity examResult,
			HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		examResult = systemService.getEntity(ExamResultEntity.class,
				examResult.getId());
		message = "成绩维护删除成功";
		try {
			examResultService.delete(examResult);
			systemService.addLog(message, Globals.Log_Type_DEL,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "成绩维护删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 批量删除成绩维护
	 * 
	 * @return
	 */
	@RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "成绩维护删除成功";
		try {
			for (String id : ids.split(",")) {
				ExamResultEntity examResult = systemService.getEntity(
						ExamResultEntity.class, id);
				examResultService.delete(examResult);
				systemService.addLog(message, Globals.Log_Type_DEL,
						Globals.Log_Leavel_INFO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			message = "成绩维护删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 添加成绩维护
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(ExamResultEntity examResult,
			HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "成绩维护添加成功";
		try {
			examResult.setScore(scores.get(examResult.getScorelevel()) + "");
			examResultService.save(examResult);
			systemService.addLog(message, Globals.Log_Type_INSERT,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "成绩维护添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 更新成绩维护
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(ExamResultEntity examResult,
			HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "成绩维护更新成功";
		ExamResultEntity t = examResultService.get(ExamResultEntity.class,
				examResult.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(examResult, t);
			t.setScore(scores.get(t.getScorelevel()) + "");
			examResultService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "成绩维护更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 成绩维护新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(ExamResultEntity examResult,
			HttpServletRequest req) {
		if (StringUtil.isNotEmpty(examResult.getId())) {
			examResult = examResultService.getEntity(ExamResultEntity.class,
					examResult.getId());
			req.setAttribute("examResultPage", examResult);
		}
		return new ModelAndView("exam/examResult/examResult-add");
	}

	/**
	 * 成绩维护编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(ExamResultEntity examResult,
			HttpServletRequest req) {
		if (StringUtil.isNotEmpty(examResult.getId())) {
			examResult = examResultService.getEntity(ExamResultEntity.class,
					examResult.getId());
			req.setAttribute("examResultPage", examResult);
		}
		return new ModelAndView("exam/examResult/examResult-update");
	}
}
