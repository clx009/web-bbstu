package com.hnykl.bp.web.exam.controller;
import java.util.List;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jersey.repackaged.com.google.common.collect.Lists;

import net.sf.json.JSONArray;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.school.entity.SchoolEntity;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.exam.entity.SubjectEntity;
import com.hnykl.bp.web.exam.service.SubjectServiceI;

/**   
 * @Title: Controller
 * @Description: 考试科目
 * @author onlineGenerator
 * @date 2016-06-27 15:03:21
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/subjectController")
public class SubjectController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(SubjectController.class);

	@Autowired
	private SubjectServiceI subjectService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 考试科目列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "subject")
	public ModelAndView subject(HttpServletRequest request) {
		return new ModelAndView("exam/subject/subjectList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(SubjectEntity subject,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(SubjectEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, subject, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.subjectService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除考试科目
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(SubjectEntity subject, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		subject = systemService.getEntity(SubjectEntity.class, subject.getId());
		message = "考试科目删除成功";
		try{
			subjectService.delete(subject);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "考试科目删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除考试科目
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "考试科目删除成功";
		try{
			for(String id:ids.split(",")){
				SubjectEntity subject = systemService.getEntity(SubjectEntity.class, 
				id
				);
				subjectService.delete(subject);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "考试科目删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加考试科目
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(SubjectEntity subject, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "考试科目添加成功";
		try{
			subjectService.save(subject);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "考试科目添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新考试科目
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(SubjectEntity subject, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "考试科目更新成功";
		SubjectEntity t = subjectService.get(SubjectEntity.class, subject.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(subject, t);
			subjectService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "考试科目更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * 考试科目新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(SubjectEntity subject, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(subject.getId())) {
			subject = subjectService.getEntity(SubjectEntity.class, subject.getId());
			req.setAttribute("subjectPage", subject);
		}
		return new ModelAndView("exam/subject/subject-add");
	}
	/**
	 * 考试科目编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(SubjectEntity subject, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(subject.getId())) {
			subject = subjectService.getEntity(SubjectEntity.class, subject.getId());
			req.setAttribute("subjectPage", subject);
		}
		return new ModelAndView("exam/subject/subject-update");
	}
	
	/**
	 * 专业combbox
	 * @param q
	 * @return
	 */
	@RequestMapping(params="subjectCombobox")
	@ResponseBody
	public List<SubjectEntity> subjectCombobox(String q){
		List<SubjectEntity> lst = subjectService.subjectCombobox(q);
		List<SubjectEntity> list = Lists.newArrayList();
		for(SubjectEntity se:lst){
			se.setExamItem(null);
			list.add(se);
		}
		return list;
	}
}
