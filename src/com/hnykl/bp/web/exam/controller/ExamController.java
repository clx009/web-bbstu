package com.hnykl.bp.web.exam.controller;
import java.util.List;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.exam.entity.ExamEntity;
import com.hnykl.bp.web.exam.entity.ExamItemEntity;
import com.hnykl.bp.web.exam.service.ExamServiceI;

/**   
 * @Title: Controller
 * @Description: t_exam
 * @author onlineGenerator
 * @date 2016-10-08 14:34:25
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/examController")
public class ExamController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ExamController.class);

	@Autowired
	private ExamServiceI examService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * t_exam列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "exam")
	public ModelAndView exam(HttpServletRequest request) {
		return new ModelAndView("exam/exam/examList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(ExamEntity exam,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ExamEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, exam, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.examService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除t_exam
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(ExamEntity exam, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		exam = systemService.getEntity(ExamEntity.class, exam.getId());
		message = "t_exam删除成功";
		try{
			examService.delete(exam);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_exam删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除t_exam
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "t_exam删除成功";
		try{
			for(String id:ids.split(",")){
				ExamEntity exam = systemService.getEntity(ExamEntity.class, 
				id
				);
				examService.delete(exam);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "t_exam删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加t_exam
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(ExamEntity exam, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_exam添加成功";
		try{
			examService.save(exam);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_exam添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新t_exam
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(ExamEntity exam, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_exam更新成功";
		ExamEntity t = examService.get(ExamEntity.class, exam.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(exam, t);
			examService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_exam更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * t_exam新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(ExamEntity exam, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(exam.getId())) {
			exam = examService.getEntity(ExamEntity.class, exam.getId());
			req.setAttribute("examPage", exam);
		}
		return new ModelAndView("exam/exam/exam-add");
	}
	/**
	 * t_exam编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(ExamEntity exam, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(exam.getId())) {
			exam = examService.getEntity(ExamEntity.class, exam.getId());
			req.setAttribute("examPage", exam);
		}
		return new ModelAndView("exam/exam/exam-update");
	}
	
	/**
	 * 考試 combbox
	 * 
	 * @param q
	 * @return
	 */
	@RequestMapping(params = "examSelectItem")
	@ResponseBody
	public List<ExamEntity> itemSelectItem(String q) {
		List<ExamEntity> lst = examService.getExamSelectItems(q);
		return lst;
	}
}
