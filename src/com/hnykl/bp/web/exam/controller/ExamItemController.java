package com.hnykl.bp.web.exam.controller;
import java.util.List;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.school.entity.SchoolEntity;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.exam.entity.ExamItemEntity;
import com.hnykl.bp.web.exam.service.ExamItemServiceI;

/**   
 * @Title: Controller
 * @Description: t_exam_item
 * @author onlineGenerator
 * @date 2016-10-08 14:33:51
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/examItemController")
public class ExamItemController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(ExamItemController.class);

	@Autowired
	private ExamItemServiceI examItemService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * t_exam_item列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "examItem")
	public ModelAndView examItem(HttpServletRequest request) {
		return new ModelAndView("exam/examItem/examItemList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(ExamItemEntity examItem,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(ExamItemEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, examItem, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.examItemService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除t_exam_item
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(ExamItemEntity examItem, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		examItem = systemService.getEntity(ExamItemEntity.class, examItem.getId());
		message = "t_exam_item删除成功";
		try{
			examItemService.delete(examItem);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_exam_item删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除t_exam_item
	 * 
	 * @return
	 */
	@RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "t_exam_item删除成功";
		try{
			for(String id:ids.split(",")){
				ExamItemEntity examItem = systemService.getEntity(ExamItemEntity.class, 
				id
				);
				examItemService.delete(examItem);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "t_exam_item删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加t_exam_item
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(ExamItemEntity examItem, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_exam_item添加成功";
		try{
			examItemService.save(examItem);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_exam_item添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新t_exam_item
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(ExamItemEntity examItem, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_exam_item更新成功";
		ExamItemEntity t = examItemService.get(ExamItemEntity.class, examItem.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(examItem, t);
			examItemService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_exam_item更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * t_exam_item新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(ExamItemEntity examItem, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(examItem.getId())) {
			examItem = examItemService.getEntity(ExamItemEntity.class, examItem.getId());
			req.setAttribute("examItemPage", examItem);
		}
		return new ModelAndView("exam/examItem/examItem-add");
	}
	/**
	 * t_exam_item编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(ExamItemEntity examItem, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(examItem.getId())) {
			examItem = examItemService.getEntity(ExamItemEntity.class, examItem.getId());
			req.setAttribute("examItemPage", examItem);
		}
		return new ModelAndView("exam/examItem/examItem-update");
	}
	
	/**
	 * 科目 combbox
	 * 
	 * @param q
	 * @return
	 */
	@RequestMapping(params = "itemSelectItem")
	@ResponseBody
	public List<ExamItemEntity> itemSelectItem(String q) {
		List<ExamItemEntity> lst = examItemService.getItemSelectItems(q);
	//	ExamItemEntity entiry  = (ExamItemEntity)lst.get(0);
		return lst;
	}
}
