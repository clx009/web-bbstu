package com.hnykl.bp.web.character.controller;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jersey.repackaged.com.google.common.collect.Lists;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.common.UploadFile;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.common.model.json.DataGridReturn;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.MyBeanUtils;
import com.hnykl.bp.base.core.util.RoletoJson;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.character.entity.TSCharactertestEntity;
import com.hnykl.bp.web.character.service.TSCharactertestServiceI;
import com.hnykl.bp.web.system.dto.StudentComboboxDTO;
import com.hnykl.bp.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: t_s_charactertest
 * @author onlineGenerator
 * @date 2016-10-05 16:46:58
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/tSCharactertestController")
public class TSCharactertestController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TSCharactertestController.class);

	@Autowired
	private TSCharactertestServiceI tSCharactertestService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * t_s_charactertest列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "tSCharactertest")
	public ModelAndView tSCharactertest(HttpServletRequest request) {
		//List<TSUser> userList = systemService.getList(TSUser.class);
		List<StudentComboboxDTO> lst = new ArrayList<StudentComboboxDTO>();
		lst = systemService.studentCombobox(""); 
		if (!lst.isEmpty())
			request.setAttribute("userReplace",
					RoletoJson.listToReplaceStr(lst , "realname", "id"));
		return new ModelAndView("character/tSCharactertest/tSCharactertestList");
	}
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(TSCharactertestEntity tSCharactertest,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSCharactertestEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, tSCharactertest, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		DataGridReturn dataGridRows = this.tSCharactertestService.getDataGridReturn(cq, true);
		List<TSCharactertestEntity> rows = dataGridRows.getRows(); 
		for(TSCharactertestEntity row:rows){
			String oldPath = row.getPath();
			row.setPath("<a href="+BaseConfigMgr.getWebVisitUrl()+"/"+oldPath+">"+oldPath+"</a>"); 
		}
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除t_s_charactertest
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(TSCharactertestEntity tSCharactertest, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		tSCharactertest = systemService.getEntity(TSCharactertestEntity.class, tSCharactertest.getId());
		message = "t_s_charactertest删除成功";
		try{
			tSCharactertestService.delete(tSCharactertest);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_charactertest删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除t_s_charactertest
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "t_s_charactertest删除成功";
		try{
			for(String id:ids.split(",")){
				TSCharactertestEntity tSCharactertest = systemService.getEntity(TSCharactertestEntity.class, 
				id
				);
				tSCharactertestService.delete(tSCharactertest);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_charactertest删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加t_s_charactertest
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd", method = RequestMethod.POST)
	@ResponseBody
	public AjaxJson doAdd(HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "性格测试及潜力评估添加成功";
		try{
			TSCharactertestEntity tSCharactertest = new TSCharactertestEntity();
			String id =request.getParameter("id");
			String userId =request.getParameter("userid");
			String name = request.getParameter("projectname");
			String result = request.getParameter("result");
			String description = request.getParameter("description");
			tSCharactertest.setId(id);
			tSCharactertest.setProjectName(name);
			tSCharactertest.setResult(result);
			tSCharactertest.setDescription(description);
			tSCharactertest.setUserId(userId); 
			UploadFile uploadFile = new UploadFile(request, tSCharactertest);
			uploadFile.setCusPath("files");
			uploadFile.setExtend("extend"); 
			uploadFile.setRealPath("path"); 
			uploadFile.setRename(false);
			systemService.uploadFile(uploadFile);
		}catch(Exception e){
			e.printStackTrace();
			message = "性格测试及潜力评估添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新t_s_charactertest
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(TSCharactertestEntity tSCharactertest, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_s_charactertest更新成功";
		TSCharactertestEntity t = tSCharactertestService.get(TSCharactertestEntity.class, tSCharactertest.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(tSCharactertest, t);
			tSCharactertestService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_s_charactertest更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * t_s_charactertest新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(TSCharactertestEntity tSCharactertest, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tSCharactertest.getId())) {
			tSCharactertest = tSCharactertestService.getEntity(TSCharactertestEntity.class, tSCharactertest.getId());
			req.setAttribute("tSCharactertestPage", tSCharactertest);
		}
		return new ModelAndView("character/tSCharactertest/tSCharactertest-add");
	}
	/**
	 * t_s_charactertest编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(TSCharactertestEntity tSCharactertest, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tSCharactertest.getId())) {
			tSCharactertest = tSCharactertestService.getEntity(TSCharactertestEntity.class, tSCharactertest.getId());
			req.setAttribute("tSCharactertestPage", tSCharactertest);
		}
		return new ModelAndView("character/tSCharactertest/tSCharactertest-update");
	}
}
