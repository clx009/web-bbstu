package com.hnykl.bp.web.ca.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.MyBeanUtils;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.ca.entity.TCaApplyEntity;
import com.hnykl.bp.web.ca.service.TCaApplyServiceI;
import com.hnykl.bp.web.ecm.entity.EcmContentEntity;
import com.hnykl.bp.web.system.service.SystemService;

/**
 * @Title: Controller
 * @Description: t_ca_apply
 * @author onlineGenerator
 * @date 2016-09-24 17:11:08
 * @version V1.0
 * 
 */
@Controller
@RequestMapping("/tCaApplyController")
public class TCaApplyController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger
			.getLogger(TCaApplyController.class);

	@Autowired
	private TCaApplyServiceI tCaApplyService;
	@Autowired
	private SystemService systemService;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * t_ca_apply HW辅导
	 * 
	 * @return
	 */
	@RequestMapping(params = "tCaApplyHw")
	public ModelAndView tCaApplyHw(HttpServletRequest request) {
		return new ModelAndView("ca/tCaApply/tCaApplyListHw");
	}

	/**
	 * t_ca_apply 课程辅导
	 * 
	 * @return
	 */
	@RequestMapping(params = "tCaApplyCourse")
	public ModelAndView tCaApplyCourse(HttpServletRequest request) {
		return new ModelAndView("ca/tCaApply/tCaApplyListCourse");
	}

	/**
	 * t_ca_apply 伴学导师
	 * 
	 * @return
	 */
	@RequestMapping(params = "tCaApplyStudy")
	public ModelAndView tCaApplyStudy(HttpServletRequest request) {
		return new ModelAndView("ca/tCaApply/tCaApplyListTeacher");
	}

	/**
	 * t_ca_apply热线电话
	 * 
	 * @return
	 */
	@RequestMapping(params = "tCaApplyHotline")
	public ModelAndView tCaApplyHotLine(HttpServletRequest request) {
		return new ModelAndView("ca/tCaApply/tCaApplyListHotLine");
	}
	
	/**
	 * t_ca_apply热线电话
	 * 
	 * @return
	 */
	@RequestMapping(params = "tCaApplyHelp")
	public ModelAndView tCaApplyHelp(HttpServletRequest request) {
		return new ModelAndView("ca/tCaApply/tCaApplyListHelp");
	}
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */
	@RequestMapping(params = "datagridhotline")
	public void datagridhotline(TCaApplyEntity tCaApply, HttpServletRequest request,
			HttpServletResponse response, DataGrid dataGrid, String type) {
		CriteriaQuery cq = new CriteriaQuery(TCaApplyEntity.class, dataGrid);
		cq.eq("type", "4");
		// 查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq,
				tCaApply, request.getParameterMap());

		try {
			// 自定义追加查询条件
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.tCaApplyService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */
	@RequestMapping(params = "datagridhelp")
	public void datagridhelp(TCaApplyEntity tCaApply, HttpServletRequest request,
			HttpServletResponse response, DataGrid dataGrid, String type) {
		CriteriaQuery cq = new CriteriaQuery(TCaApplyEntity.class, dataGrid);
		cq.eq("type", "5");
		// 查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq,
				tCaApply, request.getParameterMap());

		try {
			// 自定义追加查询条件
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.tCaApplyService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */
	@RequestMapping(params = "datagridhw")
	public void datagridhw(TCaApplyEntity tCaApply, HttpServletRequest request,
			HttpServletResponse response, DataGrid dataGrid, String type) {
		CriteriaQuery cq = new CriteriaQuery(TCaApplyEntity.class, dataGrid);
		cq.eq("type", "1");
		// 查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq,
				tCaApply, request.getParameterMap());

		try {
			// 自定义追加查询条件
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.tCaApplyService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */
	@RequestMapping(params = "datagridcourse")
	public void datagridcourse(TCaApplyEntity tCaApply, HttpServletRequest request,
			HttpServletResponse response, DataGrid dataGrid, String type) {
		CriteriaQuery cq = new CriteriaQuery(TCaApplyEntity.class, dataGrid);
		cq.eq("type", "2");
		// 查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq,
				tCaApply, request.getParameterMap());

		try {
			// 自定义追加查询条件
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.tCaApplyService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */
	@RequestMapping(params = "datagridteacher")
	public void datagridteacher(TCaApplyEntity tCaApply, HttpServletRequest request,
			HttpServletResponse response, DataGrid dataGrid, String type) {
		CriteriaQuery cq = new CriteriaQuery(TCaApplyEntity.class, dataGrid);
		cq.eq("type", "3");
		// 查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq,
				tCaApply, request.getParameterMap());

		try {
			// 自定义追加查询条件
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.tCaApplyService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除t_ca_apply
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(TCaApplyEntity tCaApply, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		tCaApply = systemService.getEntity(TCaApplyEntity.class,
				tCaApply.getId());
		message = "t_ca_apply删除成功";
		try {
			tCaApplyService.delete(tCaApply);
			systemService.addLog(message, Globals.Log_Type_DEL,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_ca_apply删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 批量删除t_ca_apply
	 * 
	 * @return
	 */
	@RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_ca_apply删除成功";
		try {
			for (String id : ids.split(",")) {
				TCaApplyEntity tCaApply = systemService.getEntity(
						TCaApplyEntity.class, id);
				tCaApplyService.delete(tCaApply);
				systemService.addLog(message, Globals.Log_Type_DEL,
						Globals.Log_Leavel_INFO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_ca_apply删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 添加t_ca_apply
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(TCaApplyEntity tCaApply, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_ca_apply添加成功";
		try {
			tCaApplyService.save(tCaApply);
			systemService.addLog(message, Globals.Log_Type_INSERT,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_ca_apply添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 更新t_ca_apply
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(TCaApplyEntity tCaApply, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_ca_apply更新成功";
		TCaApplyEntity t = tCaApplyService.get(TCaApplyEntity.class,
				tCaApply.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(tCaApply, t);
			tCaApplyService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_ca_apply更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 更新t_ca_apply
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doHandle")
	@ResponseBody
	public AjaxJson doHandle(TCaApplyEntity tCaApply, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "处理成功";
		TCaApplyEntity t = tCaApplyService.get(TCaApplyEntity.class,
				tCaApply.getId());
		try {
			// MyBeanUtils.copyBeanNotNull2Bean(tCaApply, t);
			t.setStatus(tCaApply.getStatus());
			t.setResults(tCaApply.getResults());
			tCaApplyService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "处理成功";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * t_ca_apply新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(TCaApplyEntity tCaApply, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tCaApply.getId())) {
			tCaApply = tCaApplyService.getEntity(TCaApplyEntity.class,
					tCaApply.getId());
			req.setAttribute("tCaApplyPage", tCaApply);
		}
		return new ModelAndView("ca/tCaApply/tCaApply-add");
	}

	/**
	 * t_ca_apply编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(TCaApplyEntity tCaApply, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tCaApply.getId())) {
			tCaApply = tCaApplyService.getEntity(TCaApplyEntity.class,
					tCaApply.getId());
			req.setAttribute("tCaApplyPage", tCaApply);
		}
		return new ModelAndView("ca/tCaApply/tCaApply-update");
	}

	/**
	 * t_ca_apply处理页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goHandle")
	public ModelAndView goHandle(TCaApplyEntity tCaApply, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tCaApply.getId())) {
			tCaApply = tCaApplyService.getEntity(TCaApplyEntity.class,
					tCaApply.getId());
			tCaApply.setAttachments(tCaApplyService.findAttachments(tCaApply
					.getId()));
			req.setAttribute("tCaApplyPage", tCaApply);
		}
		return new ModelAndView("ca/tCaApply/tCaApply-handle");
	}
}
