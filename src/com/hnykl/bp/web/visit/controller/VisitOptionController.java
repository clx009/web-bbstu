package com.hnykl.bp.web.visit.controller;
import java.util.List;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.visit.entity.VisitOptionEntity;
import com.hnykl.bp.web.visit.service.VisitOptionServiceI;

/**   
 * @Title: Controller
 * @Description: 访问关注内容项管理
 * @author onlineGenerator
 * @date 2016-06-17 16:09:48
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/visitOptionController")
public class VisitOptionController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(VisitOptionController.class);

	@Autowired
	private VisitOptionServiceI visitOptionService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 访问关注内容项管理列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "visitOption")
	public ModelAndView visitOption(HttpServletRequest request) {
		return new ModelAndView("visit/visitOption/visitOptionList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(VisitOptionEntity visitOption,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(VisitOptionEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, visitOption, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.visitOptionService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除访问关注内容项管理
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(VisitOptionEntity visitOption, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		visitOption = systemService.getEntity(VisitOptionEntity.class, visitOption.getId());
		message = "访问关注内容项管理删除成功";
		try{
			visitOptionService.delete(visitOption);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "访问关注内容项管理删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除访问关注内容项管理
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "访问关注内容项管理删除成功";
		try{
			for(String id:ids.split(",")){
				VisitOptionEntity visitOption = systemService.getEntity(VisitOptionEntity.class, 
				id
				);
				visitOptionService.delete(visitOption);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "访问关注内容项管理删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加访问关注内容项管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(VisitOptionEntity visitOption, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "访问关注内容项管理添加成功";
		try{
			visitOptionService.save(visitOption);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "访问关注内容项管理添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新访问关注内容项管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(VisitOptionEntity visitOption, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "访问关注内容项管理更新成功";
		VisitOptionEntity t = visitOptionService.get(VisitOptionEntity.class, visitOption.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(visitOption, t);
			visitOptionService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "访问关注内容项管理更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * 访问关注内容项管理新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(VisitOptionEntity visitOption, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(visitOption.getId())) {
			visitOption = visitOptionService.getEntity(VisitOptionEntity.class, visitOption.getId());
			req.setAttribute("visitOptionPage", visitOption);
		}
		return new ModelAndView("visit/visitOption/visitOption-add");
	}
	/**
	 * 访问关注内容项管理编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(VisitOptionEntity visitOption, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(visitOption.getId())) {
			visitOption = visitOptionService.getEntity(VisitOptionEntity.class, visitOption.getId());
			req.setAttribute("visitOptionPage", visitOption);
		}
		return new ModelAndView("visit/visitOption/visitOption-update");
	}
}
