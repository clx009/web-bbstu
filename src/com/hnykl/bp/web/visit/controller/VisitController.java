package com.hnykl.bp.web.visit.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.common.UploadFile;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.DataUtils;
import com.hnykl.bp.base.core.util.MyClassLoader;
import com.hnykl.bp.base.core.util.RoletoJson;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.tag.vo.datatable.SortDirection;
import com.hnykl.bp.web.school.entity.TSchTeacherEntity;
import com.hnykl.bp.web.school.service.TeacherServiceI;
import com.hnykl.bp.web.system.pojo.base.TSType;
import com.hnykl.bp.web.system.pojo.base.TSTypegroup;
import com.hnykl.bp.web.system.pojo.base.TSUser;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.visit.entity.MaterialEntity;
import com.hnykl.bp.web.visit.entity.VisitEntity;
import com.hnykl.bp.web.visit.service.VisitServiceI;

/**
 * @Title: Controller
 * @Description: 访问管里
 * @author onlineGenerator
 * @date 2016-06-17 16:10:30
 * @version V1.0
 * 
 */
@Controller
@RequestMapping("/visitController")
public class VisitController extends BaseController {
	/**
	 * Logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger logger = Logger
			.getLogger(VisitController.class);

	@Autowired
	private VisitServiceI visitService;
	@Autowired
	private SystemService systemService;
	
	@Autowired
	private TeacherServiceI teacherService;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * 访问管里列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "visit")
	public ModelAndView visit(HttpServletRequest request) {
		List<TSUser> userList = systemService.getList(TSUser.class);
		if (!userList.isEmpty())
			request.setAttribute("userReplace",
					RoletoJson.listToReplaceStr(userList, "realName", "id"));
		/*List<TSchTeacherEntity> teacherList = teacherService.getList(TSchTeacherEntity.class);
		if (!teacherList.isEmpty())
			request.setAttribute("teacherReplace",
					RoletoJson.listToReplaceStr(teacherList, "name", "id"));*/
		return new ModelAndView("visit/visit/visitList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(params = "datagrid")
	public void datagrid(VisitEntity visit, HttpServletRequest request,
			HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(VisitEntity.class, dataGrid);
		// 查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq,
				visit, request.getParameterMap());
		try {
			// 自定义追加查询条件
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.addOrder("createTime", SortDirection.desc);
		cq.add();
		this.visitService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除访问管里
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(VisitEntity visit, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		visit = systemService.getEntity(VisitEntity.class, visit.getId());
		message = "访问管里删除成功";
		try {
			visitService.delete(visit);
			systemService.addLog(message, Globals.Log_Type_DEL,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "访问管里删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 批量删除访问管里
	 * 
	 * @return
	 */
	@RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "访问管里删除成功";
		try {
			for (String id : ids.split(",")) {
				VisitEntity visit = systemService.getEntity(VisitEntity.class,
						id);
				visitService.delete(visit);
				systemService.addLog(message, Globals.Log_Type_DEL,
						Globals.Log_Leavel_INFO);
			}
		} catch (Exception e) {
			e.printStackTrace();
			message = "访问管里删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 添加访问管里
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(VisitEntity visit, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "访问管里添加成功";
		try {
			visitService.save(visit);
			systemService.addLog(message, Globals.Log_Type_INSERT,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "访问管里添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 更新访问管里
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(VisitEntity visit, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "访问管里更新成功";
		//VisitEntity t = visitService.get(VisitEntity.class, visit.getId());
		try {
			//MyBeanUtils.copyBeanNotNull2Bean(visit, t);
			visitService.appointHandler(visit);
			systemService.addLog(message, Globals.Log_Type_UPDATE,
					Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "访问管里更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * 上传访谈材料
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUploadMaterial")
	@ResponseBody
	public AjaxJson doUploadMaterial(HttpServletRequest request,
			HttpServletResponse response, MaterialEntity material) {
		AjaxJson j = new AjaxJson();
		Map<String, Object> attributes = new HashMap<String, Object>();
		try {
			material.setSubclassname(MyClassLoader.getPackPath(material));
			material.setCreatedate(DataUtils.gettimestamp());
			UploadFile uploadFile = new UploadFile(request, material);
			uploadFile.setCusPath("files");
			uploadFile.setSwfpath("swfpath");

			TSTypegroup tsTypegroup = systemService.getTypeGroup(
					"VisitFileType", "访谈文件类别");
			TSType tsType = systemService.getType("VisitMaterial", "访谈资料",
					tsTypegroup);

			material.setTSType(tsType);

			uploadFile.setByteField(null);// 不保存二进制到数据库
			material = visitService.uploadFile(uploadFile);
			attributes.put("url", material.getRealpath());
			attributes.put("fileKey", material.getId());
			attributes.put("name", material.getAttachmenttitle());
			attributes.put(
					"viewhref",
					"commonController.do?objfileList&fileKey="
							+ material.getId());
			attributes.put("delurl", "commonController.do?delObjFile&fileKey="
					+ material.getId());

			j.setMsg("访谈材料上传成功");
			j.setAttributes(attributes);
		} catch (Exception e) {
			e.printStackTrace();
			j.setMsg("访谈材料上传失败 ");
			throw new BusinessException(e.getMessage());
		}
		return j;
	}

	/**
	 * 上传访谈报告
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUploadReport")
	@ResponseBody
	public AjaxJson doUploadReport(HttpServletRequest request,
			HttpServletResponse response, MaterialEntity material) {
		AjaxJson j = new AjaxJson();
		Map<String, Object> attributes = new HashMap<String, Object>();
		try {
			
			material.setSubclassname(MyClassLoader.getPackPath(material));
			material.setCreatedate(DataUtils.gettimestamp());

			UploadFile uploadFile = new UploadFile(request, material);

			uploadFile.setCusPath("files");
			uploadFile.setSwfpath("swfpath");

			TSTypegroup tsTypegroup = systemService.getTypeGroup(
					"VisitFileType", "访谈文件类别");
			TSType tsType = systemService.getType("VisitReport", "访谈报告",
					tsTypegroup);

			material.setTSType(tsType);

			uploadFile.setByteField(null);// 不保存文件二进制到数据库

			material = visitService.uploadFile(uploadFile);
			attributes.put("url", material.getRealpath());
			attributes.put("fileKey", material.getId());
			attributes.put("name", material.getAttachmenttitle());
			attributes.put(
					"viewhref",
					"commonController.do?objfileList&fileKey="
							+ material.getId());
			attributes.put("delurl", "commonController.do?delObjFile&fileKey="
					+ material.getId());
			j.setMsg("访谈报告已上传 ");
			j.setAttributes(attributes);
		} catch (Exception e) {
			e.printStackTrace();
			j.setMsg("访谈报告上传失败  ");
			throw new BusinessException(e.getMessage());
		}
		return j;
	}

	/**
	 * 访问管里新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(VisitEntity visit, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(visit.getId())) {
			visit = visitService.getEntity(VisitEntity.class, visit.getId());
			req.setAttribute("visitPage", visit);
		}
		return new ModelAndView("visit/visit/visit-add");
	}

	/**
	 * 访问管里编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(VisitEntity visit, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(visit.getId())) {
			visit = visitService.getEntity(VisitEntity.class, visit.getId());
			req.setAttribute("visitPage", visit);
		}
		return new ModelAndView("visit/visit/visit-update");
	}
	/**
	 * 访问管里编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goView")
	public ModelAndView goView(VisitEntity visit, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(visit.getId())) {
			visit = visitService.getEntity(VisitEntity.class, visit.getId());
			req.setAttribute("visitPage", visit);
			
			TSTypegroup tsTypegroup = systemService.getTypeGroup(
					"VisitFileType", "访谈文件类别");
			TSType tsType = systemService.getType("VisitReport", "访谈报告",
					tsTypegroup);
			@SuppressWarnings("unchecked")
			List<MaterialEntity> reports = visitService.getSession()
					.createCriteria(MaterialEntity.class)
					.add(Restrictions.eq("TSType", tsType))
					.add(Restrictions.eq("businessKey", visit.getId())).list();
			req.setAttribute("reports", reports); 
			
			tsType = systemService.getType("VisitMaterial", "访谈资料",
					tsTypegroup);
			@SuppressWarnings("unchecked")
			List<MaterialEntity> materials = visitService.getSession()
					.createCriteria(MaterialEntity.class)
					.add(Restrictions.eq("TSType", tsType))
					.add(Restrictions.eq("businessKey", visit.getId())).list();
			req.setAttribute("materials", materials);
			
		}
		return new ModelAndView("visit/visit/visit-view");
	}
	/**
	 * 访问管里编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goPrint")
	public ModelAndView goPrint(VisitEntity visit, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(visit.getId())) {
			visit = visitService.getEntity(VisitEntity.class, visit.getId());
			req.setAttribute("visitPage", visit); 
		}
		return new ModelAndView("visit/visit/visit-print");
	}

	/**
	 * 访问管里编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUploadMaterial")
	public ModelAndView goMaterial(VisitEntity visit, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(visit.getId())) {
			visit = visitService.getEntity(VisitEntity.class, visit.getId());
			req.setAttribute("visitPage", visit);
			TSTypegroup tsTypegroup = systemService.getTypeGroup(
					"VisitFileType", "访谈文件类别"); 
			TSType tsType = systemService.getType("VisitMaterial", "访谈资料",
					tsTypegroup);
			@SuppressWarnings("unchecked")
			List<MaterialEntity> reports = visitService.getSession()
					.createCriteria(MaterialEntity.class)
					.add(Restrictions.eq("TSType", tsType))
					.add(Restrictions.eq("businessKey", visit.getId())).list();
			req.setAttribute("reports", reports);
		}
		return new ModelAndView("visit/visit/visit-uploadMaterial");
	}

	/**
	 * 访问管里编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUploadReport")
	public ModelAndView goUploadReport(VisitEntity visit, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(visit.getId())) {
			visit = visitService.getEntity(VisitEntity.class, visit.getId());
			req.setAttribute("visitPage", visit);
			TSTypegroup tsTypegroup = systemService.getTypeGroup(
					"VisitFileType", "访谈文件类别");
			TSType tsType = systemService.getType("VisitReport", "访谈报告",
					tsTypegroup);
			@SuppressWarnings("unchecked")
			List<MaterialEntity> reports = visitService.getSession()
					.createCriteria(MaterialEntity.class)
					.add(Restrictions.eq("TSType", tsType))
					.add(Restrictions.eq("businessKey", visit.getId())).list();
			req.setAttribute("reports", reports);
		}

		return new ModelAndView("visit/visit/visit-uploadReport");
	}
}
