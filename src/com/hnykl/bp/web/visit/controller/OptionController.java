package com.hnykl.bp.web.visit.controller; 
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil; 
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.visit.entity.OptionEntity;
import com.hnykl.bp.web.visit.service.OptionServiceI;

/**   
 * @Title: Controller
 * @Description: 访问关注内容项管理
 * @author onlineGenerator
 * @date 2016-06-17 17:19:16
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/optionController")
public class OptionController extends BaseController {
	/**
	 * Logger for this class
	 */
	@SuppressWarnings("unused")
	private static final Logger logger = Logger.getLogger(OptionController.class);

	@Autowired
	private OptionServiceI optionService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 访问关注内容项管理列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "option")
	public ModelAndView option(HttpServletRequest request) {
		return new ModelAndView("visit/option/optionList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(params = "datagrid")
	public void datagrid(OptionEntity option,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(OptionEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, option, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.optionService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除访问关注内容项管理
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(OptionEntity option, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		option = systemService.getEntity(OptionEntity.class, option.getId());
		message = "访问关注内容项管理删除成功";
		try{
			optionService.delete(option);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "访问关注内容项管理删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除访问关注内容项管理
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "访问关注内容项管理删除成功";
		try{
			for(String id:ids.split(",")){
				OptionEntity option = systemService.getEntity(OptionEntity.class, 
				id
				);
				optionService.delete(option);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "访问关注内容项管理删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加访问关注内容项管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(OptionEntity option, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "访问关注内容项管理添加成功";
		try{
			optionService.save(option);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "访问关注内容项管理添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新访问关注内容项管理
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(OptionEntity option, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "访问关注内容项管理更新成功";
		OptionEntity t = optionService.get(OptionEntity.class, option.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(option, t);
			optionService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "访问关注内容项管理更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * 访问关注内容项管理新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(OptionEntity option, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(option.getId())) {
			option = optionService.getEntity(OptionEntity.class, option.getId());
			req.setAttribute("optionPage", option);
		}
		return new ModelAndView("visit/option/option-add");
	}
	/**
	 * 访问关注内容项管理编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(OptionEntity option, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(option.getId())) {
			option = optionService.getEntity(OptionEntity.class, option.getId());
			req.setAttribute("optionPage", option);
		}
		return new ModelAndView("visit/option/option-update");
	}
}
