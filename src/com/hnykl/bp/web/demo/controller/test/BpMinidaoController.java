package com.hnykl.bp.web.demo.controller.test;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hnykl.bp.web.demo.entity.test.BpMinidaoEntity;
import com.hnykl.bp.web.demo.service.test.BpMinidaoServiceI;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;

import org.apache.log4j.Logger;
import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.MyBeanUtils;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**   
 * @Title: Controller
 * @Description: Minidao例子
 * @author fancq
 * @date 2013-12-23 21:18:59
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/bpMinidaoController")
public class BpMinidaoController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(BpMinidaoController.class);

	@Autowired
	private BpMinidaoServiceI bpMinidaoService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * Minidao例子列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "bpMinidao")
	public ModelAndView bpMinidao(HttpServletRequest request) {
		return new ModelAndView("bp/demo/test/bpMinidaoList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(BpMinidaoEntity bpMinidao,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		List<BpMinidaoEntity> list = bpMinidaoService.listAll(bpMinidao, dataGrid.getPage(), dataGrid.getRows());
		Integer count = bpMinidaoService.getCount();
		dataGrid.setTotal(count);
		dataGrid.setResults(list);
		String total_salary = String.valueOf(bpMinidaoService.getSumSalary());
		/*
		 * 说明：格式为 字段名:值(可选，不写该值时为分页数据的合计) 多个合计 以 , 分割
		 */
		dataGrid.setFooter("salary:"+(total_salary.equalsIgnoreCase("null")?"0.0":total_salary)+",age,email:合计");
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除Minidao例子
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(BpMinidaoEntity bpMinidao, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		bpMinidao = bpMinidaoService.getEntity(BpMinidaoEntity.class, bpMinidao.getId());
		message = "Minidao例子删除成功";
		bpMinidaoService.delete(bpMinidao);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加Minidao例子
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(BpMinidaoEntity bpMinidao, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(bpMinidao.getId())) {
			message = "Minidao例子更新成功";
			BpMinidaoEntity t = bpMinidaoService.getEntity(BpMinidaoEntity.class, bpMinidao.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(bpMinidao, t);
				bpMinidaoService.update(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "Minidao例子更新失败";
			}
		} else {
			message = "Minidao例子添加成功";
			bpMinidao.setStatus("0");
			bpMinidaoService.insert(bpMinidao);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * Minidao例子列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(BpMinidaoEntity bpMinidao, HttpServletRequest req) {
		//获取部门信息
		List<TSDepart> departList = systemService.getList(TSDepart.class);
		req.setAttribute("departList", departList);
		
		if (StringUtil.isNotEmpty(bpMinidao.getId())) {
			bpMinidao = bpMinidaoService.getEntity(BpMinidaoEntity.class, bpMinidao.getId());
			req.setAttribute("bpMinidaoPage", bpMinidao);
		}
		return new ModelAndView("bp/demo/test/bpMinidao");
	}
}
