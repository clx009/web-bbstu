package com.hnykl.bp.web.demo.controller.test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.demo.entity.test.BpDemoCkfinderEntity;
import com.hnykl.bp.web.demo.service.test.BpDemoCkfinderServiceI;

/**
 * @Title: Controller
 * @Description: ckeditor+ckfinder例子
 * @author Alexander
 * @date 2013-09-19 18:29:20
 * @version V1.0
 * 
 */
@Controller
@RequestMapping("/bpDemoCkfinderController")
public class BpDemoCkfinderController extends BaseController {

	@Autowired
	private BpDemoCkfinderServiceI bpDemoCkfinderService;
	@Autowired
	private SystemService systemService;
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * ckeditor+ckfinder例子列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "bpDemoCkfinder")
	public ModelAndView bpDemoCkfinder(HttpServletRequest request) {
		return new ModelAndView("bp/demo/test/bpDemoCkfinderList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(BpDemoCkfinderEntity bpDemoCkfinder,
			HttpServletRequest request, HttpServletResponse response,
			DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(BpDemoCkfinderEntity.class,
				dataGrid);
		// 查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq,
				bpDemoCkfinder, request.getParameterMap());
		this.bpDemoCkfinderService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除ckeditor+ckfinder例子
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(BpDemoCkfinderEntity bpDemoCkfinder,
			HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		bpDemoCkfinder = systemService.getEntity(
				BpDemoCkfinderEntity.class, bpDemoCkfinder.getId());
		message = "ckeditor+ckfinder例子删除成功";
		bpDemoCkfinderService.delete(bpDemoCkfinder);
		systemService.addLog(message, Globals.Log_Type_DEL,
				Globals.Log_Leavel_INFO);

		j.setMsg(message);
		return j;
	}

	/**
	 * 添加ckeditor+ckfinder例子
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(BpDemoCkfinderEntity bpDemoCkfinder,
			HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(bpDemoCkfinder.getId())) {
			message = "ckeditor+ckfinder例子更新成功";
			BpDemoCkfinderEntity t = bpDemoCkfinderService.get(
					BpDemoCkfinderEntity.class, bpDemoCkfinder.getId());
			try {
				MyBeanUtils.copyBeanNotNull2Bean(bpDemoCkfinder, t);
				bpDemoCkfinderService.saveOrUpdate(t);
				systemService.addLog(message, Globals.Log_Type_UPDATE,
						Globals.Log_Leavel_INFO);
			} catch (Exception e) {
				e.printStackTrace();
				message = "ckeditor+ckfinder例子更新失败";
			}
		} else {
			message = "ckeditor+ckfinder例子添加成功";
			bpDemoCkfinderService.save(bpDemoCkfinder);
			systemService.addLog(message, Globals.Log_Type_INSERT,
					Globals.Log_Leavel_INFO);
		}
		j.setMsg(message);
		return j;
	}

	/**
	 * ckeditor+ckfinder例子列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(BpDemoCkfinderEntity bpDemoCkfinder,
			HttpServletRequest req) {
		if (StringUtil.isNotEmpty(bpDemoCkfinder.getId())) {
			bpDemoCkfinder = bpDemoCkfinderService.getEntity(
					BpDemoCkfinderEntity.class, bpDemoCkfinder.getId());
			req.setAttribute("bpDemoCkfinderPage", bpDemoCkfinder);
		}
		return new ModelAndView("bp/demo/test/bpDemoCkfinder");
	}
	
	/**
	 * ckeditor+ckfinder例子预览
	 * 
	 * @return
	 */
	@RequestMapping(params = "preview")
	public ModelAndView preview(BpDemoCkfinderEntity bpDemoCkfinder,
			HttpServletRequest req) {
		if (StringUtil.isNotEmpty(bpDemoCkfinder.getId())) {
			bpDemoCkfinder = bpDemoCkfinderService.getEntity(
					BpDemoCkfinderEntity.class, bpDemoCkfinder.getId());
			req.setAttribute("ckfinderPreview", bpDemoCkfinder);
		}
		return new ModelAndView("bp/demo/test/bpDemoCkfinderPreview");
	}
}
