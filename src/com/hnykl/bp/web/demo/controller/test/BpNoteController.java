package com.hnykl.bp.web.demo.controller.test;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;

import com.hnykl.bp.web.demo.entity.test.BpNoteEntity;
import com.hnykl.bp.web.demo.service.test.BpNoteServiceI;

/**   
 * @Title: Controller
 * @Description: 公告
 * @author 
 * @date 2013-03-12 14:06:34
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/bpNoteController")
public class BpNoteController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(BpNoteController.class);

	@Autowired
	private BpNoteServiceI bpNoteService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * 公告列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "bpNote")
	public ModelAndView bpNote(HttpServletRequest request) {
		return new ModelAndView("bp/demo/test/bpNoteList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(BpNoteEntity bpNote,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(BpNoteEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, bpNote);
		this.bpNoteService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除公告
	 * 
	 * @return
	 */
	@RequestMapping(params = "del")
	@ResponseBody
	public AjaxJson del(BpNoteEntity bpNote, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		bpNote = systemService.getEntity(BpNoteEntity.class, bpNote.getId());
		message = "删除成功";
		bpNoteService.delete(bpNote);
		systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加公告
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "save")
	@ResponseBody
	public AjaxJson save(BpNoteEntity bpNote, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		if (StringUtil.isNotEmpty(bpNote.getId())) {
			message = "更新成功";
			bpNoteService.saveOrUpdate(bpNote);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} else {
			message = "添加成功";
			bpNoteService.save(bpNote);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}
		
		return j;
	}

	/**
	 * 公告列表页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "addorupdate")
	public ModelAndView addorupdate(BpNoteEntity bpNote, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(bpNote.getId())) {
			bpNote = bpNoteService.getEntity(BpNoteEntity.class, bpNote.getId());
			req.setAttribute("bpNotePage", bpNote);
		}
		return new ModelAndView("bp/demo/test/bpNote");
	}
}
