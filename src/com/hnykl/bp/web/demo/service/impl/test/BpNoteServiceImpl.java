package com.hnykl.bp.web.demo.service.impl.test;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.web.demo.service.test.BpNoteServiceI;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;

@Service("bpNoteService")
@Transactional
public class BpNoteServiceImpl extends CommonServiceImpl implements BpNoteServiceI {
	
}