package com.hnykl.bp.web.demo.service.impl.test;

import java.io.IOException;
import java.sql.Blob;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.hnykl.bp.web.demo.entity.test.BpBlobDataEntity;
import com.hnykl.bp.web.demo.service.test.BpBlobDataServiceI;

import org.hibernate.LobHelper;
import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;

@Service("bpBlobDataService")
@Transactional
public class BpBlobDataServiceImpl extends CommonServiceImpl implements BpBlobDataServiceI {
	
	public void saveObj(String documentTitle, MultipartFile file) {
		BpBlobDataEntity obj = new BpBlobDataEntity();
		LobHelper lobHelper = commonDao.getSession().getLobHelper();
		Blob data;
		try {
			data = lobHelper.createBlob(file.getInputStream(), 0);
			obj.setAttachmentcontent(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
		obj.setAttachmenttitle(documentTitle);
		String sFileName = file.getOriginalFilename();
		int iPos = sFileName.lastIndexOf('.');
		if (iPos >= 0) {
			obj.setExtend(sFileName.substring(iPos+1));
		}
		super.save(obj);
	}
}