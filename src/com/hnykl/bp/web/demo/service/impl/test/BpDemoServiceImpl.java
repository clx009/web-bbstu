package com.hnykl.bp.web.demo.service.impl.test;

import com.hnykl.bp.web.demo.service.test.BpDemoServiceI;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service("bpDemoService")
@Transactional
public class BpDemoServiceImpl extends CommonServiceImpl implements BpDemoServiceI {
	
}
