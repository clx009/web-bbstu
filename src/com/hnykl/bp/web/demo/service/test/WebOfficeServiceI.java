package com.hnykl.bp.web.demo.service.test;

import com.hnykl.bp.web.demo.entity.test.WebOfficeEntity;

import com.hnykl.bp.base.core.common.service.CommonService;
import org.springframework.web.multipart.MultipartFile;

public interface WebOfficeServiceI extends CommonService{
	public void saveObj(WebOfficeEntity docObj, MultipartFile file);
}
