package com.hnykl.bp.web.demo.service.test;

import java.util.List;

import com.hnykl.bp.web.demo.entity.test.BpOrderCustomEntity;
import com.hnykl.bp.web.demo.entity.test.BpOrderMainEntity;
import com.hnykl.bp.web.demo.entity.test.BpOrderProductEntity;

import com.hnykl.bp.base.core.common.service.CommonService;


public interface BpOrderMainServiceI extends CommonService{
	/**
	 * 添加一对多
	 * 
	 */
	public void addMain(BpOrderMainEntity bpOrderMain,List<BpOrderProductEntity> bpOrderProducList,List<BpOrderCustomEntity> bpOrderCustomList) ;
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(BpOrderMainEntity bpOrderMain,List<BpOrderProductEntity> bpOrderProducList,List<BpOrderCustomEntity> bpOrderCustomList,boolean bpOrderCustomShow) ;
	public void delMain (BpOrderMainEntity bpOrderMain);
}
