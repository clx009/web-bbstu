package com.hnykl.bp.web.shijian.controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.configuration.BaseConfigMgr;
import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.common.UploadFile;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.common.model.json.DataGridReturn;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.RoletoJson;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.character.entity.TSCharactertestEntity;
import com.hnykl.bp.web.system.dto.StudentComboboxDTO;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.shijian.entity.TSShsjEntity;
import com.hnykl.bp.web.shijian.service.TSShsjServiceI;

/**   
 * @Title: Controller
 * @Description: t_s_shsj
 * @author onlineGenerator
 * @date 2016-10-07 16:33:37
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/tSShsjController")
public class TSShsjController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TSShsjController.class);

	@Autowired
	private TSShsjServiceI tSShsjService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * t_s_shsj列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "tSShsj")
	public ModelAndView tSShsj(HttpServletRequest request) {
		List<StudentComboboxDTO> lst = new ArrayList<StudentComboboxDTO>();
		lst = systemService.studentCombobox(""); 
		if (!lst.isEmpty())
			request.setAttribute("userReplace",
					RoletoJson.listToReplaceStr(lst , "realname", "id"));
		return new ModelAndView("shijian/tSShsj/tSShsjList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(TSShsjEntity tSShsj,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSShsjEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, tSShsj, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		DataGridReturn dataGridRows = this.tSShsjService.getDataGridReturn(cq, true);
		List<TSShsjEntity> rows = dataGridRows.getRows(); 
		for(TSShsjEntity row:rows){
			String oldPath = row.getPath();
			row.setPath("<a href="+BaseConfigMgr.getWebVisitUrl()+"/"+oldPath+">"+oldPath+"</a>"); 
		}
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除t_s_shsj
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(TSShsjEntity tSShsj, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		tSShsj = systemService.getEntity(TSShsjEntity.class, tSShsj.getId());
		message = "t_s_shsj删除成功";
		try{
			tSShsjService.delete(tSShsj);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_shsj删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除t_s_shsj
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "t_s_shsj删除成功";
		try{
			for(String id:ids.split(",")){
				TSShsjEntity tSShsj = systemService.getEntity(TSShsjEntity.class, 
				id
				);
				tSShsjService.delete(tSShsj);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_shsj删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加t_s_shsj
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_s_shsj添加成功";
		try{
			String id = request.getParameter("id");
			String userId = request.getParameter("userid");
			String type = request.getParameter("type");
			String name = request.getParameter("name");
			String description = request.getParameter("description");
			String createtime = request.getParameter("createtime");
			String endtime = request.getParameter("endtime");
			String result = request.getParameter("result"); 
			TSShsjEntity o = new TSShsjEntity();
			o.setId(id);
			o.setUserId(userId);
			o.setName(name);
			o.setDescription(description);
			o.setCreatetime(createtime);
			o.setEndtime(endtime);
			o.setType(type);
			o.setResult(result);
			UploadFile uploadFile = new UploadFile(request, o);
			uploadFile.setCusPath("files");
			uploadFile.setExtend("extend"); 
			uploadFile.setRealPath("path"); 
			uploadFile.setRename(false);
			systemService.uploadFile(uploadFile);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_shsj添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新t_s_shsj
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(TSShsjEntity tSShsj, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_s_shsj更新成功";
		TSShsjEntity t = tSShsjService.get(TSShsjEntity.class, tSShsj.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(tSShsj, t);
			tSShsjService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_s_shsj更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * t_s_shsj新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(TSShsjEntity tSShsj, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tSShsj.getId())) {
			tSShsj = tSShsjService.getEntity(TSShsjEntity.class, tSShsj.getId());
			req.setAttribute("tSShsjPage", tSShsj);
		}
		return new ModelAndView("shijian/tSShsj/tSShsj-add");
	}
	/**
	 * t_s_shsj编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(TSShsjEntity tSShsj, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tSShsj.getId())) {
			tSShsj = tSShsjService.getEntity(TSShsjEntity.class, tSShsj.getId());
			req.setAttribute("tSShsjPage", tSShsj);
		}
		return new ModelAndView("shijian/tSShsj/tSShsj-update");
	}
}
