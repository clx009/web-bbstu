package com.hnykl.bp.web.stuinfo.controller;
import java.util.List;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.stuinfo.entity.StuEntity;
import com.hnykl.bp.web.stuinfo.service.StudentServiceI;

/**   
 * @Title: Controller
 * @Description: t_s_user
 * @author onlineGenerator
 * @date 2016-10-08 10:40:46
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/studentController")
public class StudentController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(StudentController.class);

	@Autowired
	private StudentServiceI studentService;
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * t_s_user列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "student")
	public ModelAndView student(HttpServletRequest request) {
		return new ModelAndView("stuinfo/student/studentList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(StuEntity student,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(StuEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, student, request.getParameterMap());
		try{
			cq.eq("type","12");//这个是最高级的
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.studentService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除t_s_user
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(StuEntity student, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		student = systemService.getEntity(StuEntity.class, student.getId());
		message = "学生信息删除成功";
		try{
			studentService.delete(student);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "学生信息删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除t_s_user
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "学生信息删除成功";
		try{
			for(String id:ids.split(",")){
				StuEntity student = systemService.getEntity(StuEntity.class, 
				id
				);
				studentService.delete(student);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "学生信息删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加t_s_user
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(StuEntity student, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "学生信息添加成功";
		try{
			studentService.save(student);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_user添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新t_s_user
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(StuEntity student, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "学生信息更新成功";
		StuEntity t = studentService.get(StuEntity.class, student.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(student, t);
			studentService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "学生信息更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * t_s_user新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(StuEntity student, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(student.getId())) {
			student = studentService.getEntity(StuEntity.class, student.getId());
			req.setAttribute("studentPage", student);
		}
		return new ModelAndView("stuinfo/student/student-add");
	}
	/**
	 * t_s_user编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(StuEntity student, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(student.getId())) {
			student = studentService.getEntity(StuEntity.class, student.getId());
			req.setAttribute("studentPage", student);
		}
		return new ModelAndView("stuinfo/student/student-update");
	}
}
