package com.hnykl.bp.web.kaoshi.controller;
import java.util.ArrayList;
import java.util.List;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.RoletoJson;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.web.system.dto.StudentComboboxDTO;
import com.hnykl.bp.web.system.pojo.base.TSDepart;
import com.hnykl.bp.web.system.service.SystemService;
import com.hnykl.bp.base.core.util.MyBeanUtils;

import com.hnykl.bp.web.exam.entity.SubjectEntity;
import com.hnykl.bp.web.exam.service.SubjectServiceI;
import com.hnykl.bp.web.kaoshi.entity.TSKaoshiEntity;
import com.hnykl.bp.web.kaoshi.service.TSKaoshiServiceI;

/**   
 * @Title: Controller
 * @Description: t_s_kaoshi
 * @author onlineGenerator
 * @date 2016-10-05 16:47:17
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/tSKaoshiController")
public class TSKaoshiController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(TSKaoshiController.class);

	@Autowired
	private TSKaoshiServiceI tSKaoshiService;
	@Autowired
	private SystemService systemService;
	
	@Autowired
	private SubjectServiceI subjectService;
	
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * t_s_kaoshi列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "tSKaoshi")
	public ModelAndView tSKaoshi(HttpServletRequest request) {
		
		List<StudentComboboxDTO> lst = new ArrayList<StudentComboboxDTO>();
		lst = systemService.studentCombobox(""); 
		if (!lst.isEmpty())
			request.setAttribute("userReplace",
					RoletoJson.listToReplaceStr(lst , "realname", "id"));
		return new ModelAndView("kaoshi/tSKaoshi/tSKaoshiList");
	}

	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(TSKaoshiEntity tSKaoshi,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid) {
		CriteriaQuery cq = new CriteriaQuery(TSKaoshiEntity.class, dataGrid);
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, tSKaoshi, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.tSKaoshiService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除t_s_kaoshi
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(TSKaoshiEntity tSKaoshi, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		tSKaoshi = systemService.getEntity(TSKaoshiEntity.class, tSKaoshi.getId());
		message = "t_s_kaoshi删除成功";
		try{
			tSKaoshiService.delete(tSKaoshi);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_kaoshi删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除t_s_kaoshi
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "t_s_kaoshi删除成功";
		try{
			for(String id:ids.split(",")){
				TSKaoshiEntity tSKaoshi = systemService.getEntity(TSKaoshiEntity.class, 
				id
				);
				tSKaoshiService.delete(tSKaoshi);
				systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_kaoshi删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加t_s_kaoshi
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(TSKaoshiEntity tSKaoshi, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_s_kaoshi添加成功";
		try{
			tSKaoshiService.save(tSKaoshi);
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "t_s_kaoshi添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新t_s_kaoshi
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(TSKaoshiEntity tSKaoshi, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "t_s_kaoshi更新成功";
		TSKaoshiEntity t = tSKaoshiService.get(TSKaoshiEntity.class, tSKaoshi.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(tSKaoshi, t);
			tSKaoshiService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "t_s_kaoshi更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * t_s_kaoshi新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(TSKaoshiEntity tSKaoshi, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tSKaoshi.getId())) {
			tSKaoshi = tSKaoshiService.getEntity(TSKaoshiEntity.class, tSKaoshi.getId());
			req.setAttribute("tSKaoshiPage", tSKaoshi);
		}
		return new ModelAndView("kaoshi/tSKaoshi/tSKaoshi-add");
	}
	/**
	 * t_s_kaoshi编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(TSKaoshiEntity tSKaoshi, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(tSKaoshi.getId())) {
			tSKaoshi = tSKaoshiService.getEntity(TSKaoshiEntity.class, tSKaoshi.getId());
			req.setAttribute("tSKaoshiPage", tSKaoshi);
		}
		return new ModelAndView("kaoshi/tSKaoshi/tSKaoshi-update");
	}
}
