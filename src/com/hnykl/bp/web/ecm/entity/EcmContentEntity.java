package com.hnykl.bp.web.ecm.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**   
 * @Title: Entity
 * @Description: 内容管理
 * @author onlineGenerator
 * @date 2014-11-24 16:25:07
 * @version V1.0   
 *
 */
@Entity
@Table(name = "ecm_content", schema = "")
@SuppressWarnings("serial")
public class EcmContentEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**title*/
	private java.lang.String title;
	/**content*/
	private java.lang.String content;
	/**attachmentPath*/
	private java.lang.String attachmentPath;
	/**userId*/
	private java.lang.String userId;
	/**loginName*/
	private java.lang.String loginName;
	/**isEnable*/
	private java.lang.String isEnable;
	/**status*/
	private java.lang.String status;
	/**priority*/
	private java.lang.String priority;
	/**origin*/
	private java.lang.String origin;
	/**createTime*/
	private Date createTime;
	/**modifyTime*/
	private Date modifyTime;
	
	

	

	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  title
	 */
	@Column(name ="TITLE",nullable=true)
	public java.lang.String getTitle(){
		return this.title;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  title
	 */
	public void setTitle(java.lang.String title){
		this.title = title;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  content
	 */
	@Column(name ="CONTENT",nullable=true)
	public java.lang.String getContent(){
		return this.content;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  content
	 */
	public void setContent(java.lang.String content){
		this.content = content;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  attachmentPath
	 */
	@Column(name ="ATTACHMENT_PATH",nullable=true)
	public java.lang.String getAttachmentPath(){
		return this.attachmentPath;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  attachmentPath
	 */
	public void setAttachmentPath(java.lang.String attachmentPath){
		this.attachmentPath = attachmentPath;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  userId
	 */
	@Column(name ="USER_ID",nullable=true)
	public java.lang.String getUserId(){
		return this.userId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  userId
	 */
	public void setUserId(java.lang.String userId){
		this.userId = userId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  loginName
	 */
	@Column(name ="LOGIN_NAME",nullable=true)
	public java.lang.String getLoginName(){
		return this.loginName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  loginName
	 */
	public void setLoginName(java.lang.String loginName){
		this.loginName = loginName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  isEnable
	 */
	@Column(name ="IS_ENABLE",nullable=true)
	public java.lang.String getIsEnable(){
		return this.isEnable;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  isEnable
	 */
	public void setIsEnable(java.lang.String isEnable){
		this.isEnable = isEnable;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  status
	 */
	@Column(name ="STATUS",nullable=true)
	public java.lang.String getStatus(){
		return this.status;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  status
	 */
	public void setStatus(java.lang.String status){
		this.status = status;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  priority
	 */
	@Column(name ="PRIORITY",nullable=true,length=3)
	public java.lang.String getPriority(){
		return this.priority;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  priority
	 */
	public void setPriority(java.lang.String priority){
		this.priority = priority;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  origin
	 */
	@Column(name ="ORIGIN",nullable=true)
	public java.lang.String getOrigin(){
		return this.origin;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  origin
	 */
	public void setOrigin(java.lang.String origin){
		this.origin = origin;
	}
	
	@Column(name ="CREATE_TIME")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name ="MODIFY_TIME")
	public Date getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(Date modifyTime) {
		this.modifyTime = modifyTime;
	}
	
}
