package com.hnykl.bp.web.ecm.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.lang.String;
import java.lang.Double;
import java.lang.Integer;
import java.math.BigDecimal;
import javax.xml.soap.Text;
import java.sql.Blob;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;
import javax.persistence.SequenceGenerator;

/**   
 * @Title: Entity
 * @Description: ecm_content_category
 * @author onlineGenerator
 * @date 2016-07-27 23:54:57
 * @version V1.0   
 *
 */
@Entity
@Table(name = "ecm_content_category", schema = "")
@SuppressWarnings("serial")
public class EcmContentCategoryEntity implements java.io.Serializable {
	/**id*/
	private java.lang.String id;
	/**contentId*/
	private java.lang.String contentId;
	/**categoryId*/
	private java.lang.String categoryId;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  id
	 */
	@Id
	@GeneratedValue(generator = "paymentableGenerator")
	@GenericGenerator(name = "paymentableGenerator", strategy = "uuid")
	@Column(name ="ID",nullable=false)
	public java.lang.String getId(){
		return this.id;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  id
	 */
	public void setId(java.lang.String id){
		this.id = id;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  contentId
	 */
	@Column(name ="CONTENT_ID",nullable=false)
	public java.lang.String getContentId(){
		return this.contentId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  contentId
	 */
	public void setContentId(java.lang.String contentId){
		this.contentId = contentId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  categoryId
	 */
	@Column(name ="CATEGORY_ID",nullable=false)
	public java.lang.String getCategoryId(){
		return this.categoryId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  categoryId
	 */
	public void setCategoryId(java.lang.String categoryId){
		this.categoryId = categoryId;
	}
}
