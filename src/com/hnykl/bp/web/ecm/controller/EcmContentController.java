package com.hnykl.bp.web.ecm.controller;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jersey.repackaged.com.google.common.collect.Lists;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hnykl.bp.base.core.common.controller.BaseController;
import com.hnykl.bp.base.core.common.exception.BusinessException;
import com.hnykl.bp.base.core.common.hibernate.qbc.CriteriaQuery;
import com.hnykl.bp.base.core.common.model.common.UploadFile;
import com.hnykl.bp.base.core.common.model.json.AjaxJson;
import com.hnykl.bp.base.core.common.model.json.ComboTree;
import com.hnykl.bp.base.core.common.model.json.DataGrid;
import com.hnykl.bp.base.core.constant.Globals;
import com.hnykl.bp.base.core.util.MyBeanUtils;
import com.hnykl.bp.base.core.util.StringUtil;
import com.hnykl.bp.tag.core.easyui.TagUtil;
import com.hnykl.bp.tag.vo.easyui.ComboTreeModel;
import com.hnykl.bp.web.ecm.entity.EcmCategoryEntity;
import com.hnykl.bp.web.ecm.entity.EcmContentEntity;
import com.hnykl.bp.web.ecm.service.EcmCategoryServiceI;
import com.hnykl.bp.web.ecm.service.EcmContentServiceI;
import com.hnykl.bp.web.system.service.SystemService;

/**   
 * @Title: Controller
 * @Description: ecm_content
 * @author onlineGenerator
 * @date 2016-07-27 18:40:00
 * @version V1.0   
 *
 */
@Controller
@RequestMapping("/ecmContentController")
public class EcmContentController extends BaseController {
	/**
	 * Logger for this class
	 */
	private static final Logger logger = Logger.getLogger(EcmContentController.class);

	@Autowired
	private EcmContentServiceI ecmContentService;
	
	@Autowired
	private EcmCategoryServiceI ecmCategoryService; 
	
	@Autowired
	private SystemService systemService;
	private String message;
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * ecm_content列表 页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "ecmContent")
	public ModelAndView ecmContent(HttpServletRequest request) {
		return new ModelAndView("ecm/ecmContent/ecmContentList");
	}

	@RequestMapping(params = "queryCategoryTree")
	@ResponseBody
	public List<ComboTree> queryCategoryTree(HttpServletRequest request,
			ComboTree comboTree) {
		List<EcmCategoryEntity> catList = ecmCategoryService.queryCategory(comboTree.getId());
		List<ComboTree> comboTrees = new ArrayList<ComboTree>();
		ComboTreeModel comboTreeModel = new ComboTreeModel("id", "name", "ecmCategoryEntitys");
		comboTrees = systemService.ComboTree(catList,comboTreeModel,null);
		return comboTrees;
	}
	
	
	/**
	 * easyui AJAX请求数据
	 * 
	 * @param request
	 * @param response
	 * @param dataGrid
	 * @param user
	 */

	@RequestMapping(params = "datagrid")
	public void datagrid(EcmContentEntity ecmContent,HttpServletRequest request, HttpServletResponse response, DataGrid dataGrid,String catId) {
		CriteriaQuery cq = new CriteriaQuery(EcmContentEntity.class, dataGrid);
		if(catId != null){
			String[] contentIds = ecmContentService.queryContentIdByCatId(catId);
			if(contentIds != null && contentIds.length > 0){
				cq.in("id", contentIds  );
			}else{ //如果没有，就是空的
				cq.in("id", new String[]{"-1"});
			}
		}else{
			cq.in("id", new String[]{"-1"});
		}
		//查询条件组装器
		com.hnykl.bp.base.core.extend.hqlsearch.HqlGenerateUtil.installHql(cq, ecmContent, request.getParameterMap());
		try{
		//自定义追加查询条件
		}catch (Exception e) {
			throw new BusinessException(e.getMessage());
		}
		cq.add();
		this.ecmContentService.getDataGridReturn(cq, true);
		TagUtil.datagrid(response, dataGrid);
	}

	/**
	 * 删除ecm_content
	 * 
	 * @return
	 */
	@RequestMapping(params = "doDel")
	@ResponseBody
	public AjaxJson doDel(EcmContentEntity ecmContent, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		ecmContent = systemService.getEntity(EcmContentEntity.class, ecmContent.getId());
		message = "ecm_content删除成功";
		try{
			ecmContentService.delete(ecmContent);
			systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
		}catch(Exception e){
			e.printStackTrace();
			message = "ecm_content删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 批量删除ecm_content
	 * 
	 * @return
	 */
	 @RequestMapping(params = "doBatchDel")
	@ResponseBody
	public AjaxJson doBatchDel(String ids,HttpServletRequest request){
		AjaxJson j = new AjaxJson();
		message = "ecm_content删除成功";
		try{
			if(StringUtils.isNotEmpty(ids)){
				for(String id:ids.split(",")){
					EcmContentEntity ecmContent = systemService.getEntity(EcmContentEntity.class, id
				);
					ecmContentService.delete(ecmContent);
					systemService.addLog(message, Globals.Log_Type_DEL, Globals.Log_Leavel_INFO);
				}
				ecmContentService.delCatContentEntity(ids.split(","));
			}
		}catch(Exception e){
			e.printStackTrace();
			message = "ecm_content删除失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}


	/**
	 * 添加ecm_content
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doAdd")
	@ResponseBody
	public AjaxJson doAdd(EcmContentEntity ecmContent, HttpServletRequest request,String catId) {
		AjaxJson j = new AjaxJson();
		message = "内容添加成功";
		try{
			catId = request.getParameter("catId");
			String content = request.getParameter("content_editor");
			ecmContent.setIsEnable("1");
			Date nowDate = new Date();
			ecmContent.setContent(content);
			ecmContent.setCreateTime(nowDate);
			ecmContent.setModifyTime(nowDate);
			ecmContentService.save(ecmContent);
			ecmContentService.saveCatContentEntity(catId, ecmContent.getId() );
			systemService.addLog(message, Globals.Log_Type_INSERT, Globals.Log_Leavel_INFO);
			UploadFile uploadFile = new UploadFile(request, ecmContent);
			uploadFile.setCusPath("files");
			uploadFile.setExtend("extend"); 
			uploadFile.setRealPath("attachmentPath"); 
			uploadFile.setRename(false);
			systemService.uploadFile(uploadFile);
		}catch(Exception e){
			e.printStackTrace();
			message = "内容添加失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	
	/**
	 * 更新ecm_content
	 * 
	 * @param ids
	 * @return
	 */
	@RequestMapping(params = "doUpdate")
	@ResponseBody
	public AjaxJson doUpdate(EcmContentEntity ecmContent, HttpServletRequest request) {
		AjaxJson j = new AjaxJson();
		message = "内容更新成功";
		EcmContentEntity t = ecmContentService.get(EcmContentEntity.class, ecmContent.getId());
		try {
			MyBeanUtils.copyBeanNotNull2Bean(ecmContent, t);
			ecmContentService.saveOrUpdate(t);
			systemService.addLog(message, Globals.Log_Type_UPDATE, Globals.Log_Leavel_INFO);
		} catch (Exception e) {
			e.printStackTrace();
			message = "内容更新失败";
			throw new BusinessException(e.getMessage());
		}
		j.setMsg(message);
		return j;
	}
	

	/**
	 * ecm_content新增页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goAdd")
	public ModelAndView goAdd(EcmContentEntity ecmContent, HttpServletRequest req,String catId) {
		if (StringUtil.isNotEmpty(ecmContent.getId())) {
			ecmContent = ecmContentService.getEntity(EcmContentEntity.class, ecmContent.getId());
			req.setAttribute("ecmContentPage", ecmContent);
		}
		req.setAttribute("catId", catId);
		return new ModelAndView("ecm/ecmContent/ecmContent-add");
	}
	/**
	 * ecm_content编辑页面跳转
	 * 
	 * @return
	 */
	@RequestMapping(params = "goUpdate")
	public ModelAndView goUpdate(EcmContentEntity ecmContent, HttpServletRequest req) {
		if (StringUtil.isNotEmpty(ecmContent.getId())) {
			ecmContent = ecmContentService.getEntity(EcmContentEntity.class, ecmContent.getId());
			req.setAttribute("ecmContentPage", ecmContent);
		}
		return new ModelAndView("ecm/ecmContent/ecmContent-update");
	}
}
