package com.hnykl.bp.web.ecm.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.base.tool.character.StringUtils;
import com.hnykl.bp.web.ecm.entity.EcmContentCategoryEntity;
import com.hnykl.bp.web.ecm.entity.EcmContentEntity;
import com.hnykl.bp.web.ecm.service.EcmContentServiceI;

@Service("ecmContentService")
@Transactional
public class EcmContentServiceImpl extends CommonServiceImpl implements EcmContentServiceI {

	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((EcmContentEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((EcmContentEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((EcmContentEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(EcmContentEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(EcmContentEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(EcmContentEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,EcmContentEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{title}",String.valueOf(t.getTitle()));
 		sql  = sql.replace("#{content}",String.valueOf(t.getContent()));
 		sql  = sql.replace("#{attachment_path}",String.valueOf(t.getAttachmentPath()));
 		sql  = sql.replace("#{user_id}",String.valueOf(t.getUserId()));
 		sql  = sql.replace("#{login_name}",String.valueOf(t.getLoginName()));
 		sql  = sql.replace("#{is_enable}",String.valueOf(t.getIsEnable()));
 		sql  = sql.replace("#{status}",String.valueOf(t.getStatus()));
 		sql  = sql.replace("#{priority}",String.valueOf(t.getPriority()));
 		sql  = sql.replace("#{origin}",String.valueOf(t.getOrigin()));
 		sql  = sql.replace("#{create_time}",String.valueOf(t.getCreateTime()));
 		sql  = sql.replace("#{modify_time}",String.valueOf(t.getModifyTime()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}
 	
 	public String[] queryContentIdByCatId(String catId){
 		String hql = " from EcmContentCategoryEntity ecce where ecce.categoryId=:categoryId";
 		Query query = getSession().createQuery(hql);
 		query.setParameter("categoryId", catId);
 		List<EcmContentCategoryEntity> dataList = query.list();
 		String [] contentIds = new String[dataList.size()];
 		for(int i=0;dataList != null &&  i< dataList.size();i++){
 			contentIds[i] = dataList.get(i).getContentId();
 		}
 		return contentIds;
 	}
 	
 	public void saveCatContentEntity(String catId,String contentId){
 		try {
			String sql = "insert into ecm_content_category(id,content_id,category_id) values(lower(REPLACE( uuid(), '-', '')),?,?) ";
			executeSql(sql,contentId,catId);
		} catch (Exception e) {
			e.printStackTrace();
		}
 	}
 	
 	public void delCatContentEntity(String[] contentIds){
 		try {
			String sql = "delete from ecm_content_category  where content_id in("+StringUtils.array2Str(contentIds) +") ";
			executeSql(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
 	}
 	
}