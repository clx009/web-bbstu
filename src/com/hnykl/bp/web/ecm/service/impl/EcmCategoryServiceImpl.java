package com.hnykl.bp.web.ecm.service.impl;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import org.hibernate.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hnykl.bp.base.core.common.service.impl.CommonServiceImpl;
import com.hnykl.bp.web.ecm.entity.EcmCategoryEntity;
import com.hnykl.bp.web.ecm.service.EcmCategoryServiceI;

@Service("ecmCategoryService")
@Transactional
public class EcmCategoryServiceImpl extends CommonServiceImpl implements EcmCategoryServiceI {

	
 	public <T> void delete(T entity) {
 		super.delete(entity);
 		//执行删除操作配置的sql增强
		this.doDelSql((EcmCategoryEntity)entity);
 	}
 	
 	public <T> Serializable save(T entity) {
 		Serializable t = super.save(entity);
 		//执行新增操作配置的sql增强
 		this.doAddSql((EcmCategoryEntity)entity);
 		return t;
 	}
 	
 	public <T> void saveOrUpdate(T entity) {
 		super.saveOrUpdate(entity);
 		//执行更新操作配置的sql增强
 		this.doUpdateSql((EcmCategoryEntity)entity);
 	}
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(EcmCategoryEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(EcmCategoryEntity t){
	 	return true;
 	}
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(EcmCategoryEntity t){
	 	return true;
 	}
 	
 	/**
	 * 替换sql中的变量
	 * @param sql
	 * @return
	 */
 	public String replaceVal(String sql,EcmCategoryEntity t){
 		sql  = sql.replace("#{id}",String.valueOf(t.getId()));
 		sql  = sql.replace("#{name}",String.valueOf(t.getName()));
 		sql  = sql.replace("#{type}",String.valueOf(t.getType()));
 		sql  = sql.replace("#{code}",String.valueOf(t.getCode()));
 		sql  = sql.replace("#{is_enable}",String.valueOf(t.getIsEnable()));
// 		sql  = sql.replace("#{parent_category_id}",String.valueOf(t.getParentCategoryId()));
 		sql  = sql.replace("#{description}",String.valueOf(t.getDescription()));
 		sql  = sql.replace("#{create_time}",String.valueOf(t.getCreateTime()));
 		sql  = sql.replace("#{modify_time}",String.valueOf(t.getModifyTime()));
 		sql  = sql.replace("#{create_user_id}",String.valueOf(t.getCreateUserId()));
 		sql  = sql.replace("#{UUID}",UUID.randomUUID().toString());
 		return sql;
 	}
 	
 	public List<EcmCategoryEntity> queryCategory(String catId){
 		Query query = null;
 		if(catId != null){
	 		String hql = " from EcmCategoryEntity ece where ece.ecmCategoryEntity.id=:id order by code ";
	 		query = getSession().createQuery(hql);
	 		query.setParameter("id", catId);
 		}else{
 			String hql = " from EcmCategoryEntity ece where ece.ecmCategoryEntity.id is null  order by code ";
	 		query = getSession().createQuery(hql);
 		}
 		return query.list();
 	}
 	
 	
}