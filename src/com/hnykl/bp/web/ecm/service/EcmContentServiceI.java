package com.hnykl.bp.web.ecm.service;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.ecm.entity.EcmContentEntity;
import java.io.Serializable;

public interface EcmContentServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(EcmContentEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(EcmContentEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(EcmContentEntity t);
 	
 	public String[] queryContentIdByCatId(String catId);
 	
	public void saveCatContentEntity(String catId,String contentId);
	
	public void delCatContentEntity(String[] contentIds);
}
