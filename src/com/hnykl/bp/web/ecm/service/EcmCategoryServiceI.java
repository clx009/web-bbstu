package com.hnykl.bp.web.ecm.service;

import com.hnykl.bp.base.core.common.service.CommonService;
import com.hnykl.bp.web.ecm.entity.EcmCategoryEntity;
import java.io.Serializable;
import java.util.List;

public interface EcmCategoryServiceI extends CommonService{
	
 	public <T> void delete(T entity);
 	
 	public <T> Serializable save(T entity);
 	
 	public <T> void saveOrUpdate(T entity);
 	
 	/**
	 * 默认按钮-sql增强-新增操作
	 * @param id
	 * @return
	 */
 	public boolean doAddSql(EcmCategoryEntity t);
 	/**
	 * 默认按钮-sql增强-更新操作
	 * @param id
	 * @return
	 */
 	public boolean doUpdateSql(EcmCategoryEntity t);
 	/**
	 * 默认按钮-sql增强-删除操作
	 * @param id
	 * @return
	 */
 	public boolean doDelSql(EcmCategoryEntity t);
 	
 	public List<EcmCategoryEntity> queryCategory(String catId);
}
