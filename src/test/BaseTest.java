package test;
 

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;  

public class BaseTest {

	@Test
	public void test() throws Throwable, ClassNotFoundException { 
		Method method = Class.forName("com.hnykl.bp.web.school.controller.SchoolController").getMethod("school", HttpServletRequest.class); 
		Annotation[] annotations =  method.getAnnotations();
		for(Annotation annotation:annotations){
			System.out.println(annotation.toString());
		} 
	}

}
