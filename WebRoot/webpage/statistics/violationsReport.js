var dayChart;
$(function() {
	// 日统计
	dayChart = new Highcharts.Chart(
			{
				chart : {
					renderTo: 'dayMoneyContainer',
					type : 'column'
				},
				credits : {
					enabled : false
				},
				title : {
					text : '2013年按金额统计'
				},
				subtitle : {
					text : ''
				},
				xAxis : {
					categories : [ '1-1', '1-2', '1-3',
							'1-4', '1-5', '1-6', '1-7',
							'1-8', '1-9', '1-10', '1-11',
							'1-12' ]
				},
				yAxis : {
					min : 0,
					title : {
						text : '金额 (元)'
					}
				},
				tooltip : {
					headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
					pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
							+ '<td style="padding:0"><b>{point.y:.1f} 元</b></td></tr>',
					footerFormat : '</table>',
					shared : true,
					useHTML : true
				},
				plotOptions : {
					column : {
						pointPadding : 0.2,
						borderWidth : 0
					}
				},
				series : [
						{
							name : '违规金额',
							data : [ 4900.9, 7100.5, 10600.4, 12900.2, 14400.0,
									17600.0, 13500.6, 14800.5, 21600.4,
									19400.1, 9500.6, 5400.4 ]

						}]
			});

	$('#dayAmountContainer')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						credits : {
							enabled : false
						},
						title : {
							text : '2013年按数量统计'
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : [ '1-1', '1-2', '1-3',
									'1-4', '1-5', '1-6',
									'1-7', '1-8', '1-9',
									'1-10', '1-11', '1-12' ]
						},
						yAxis : {
							min : 0,
							title : {
								text : '数量 (条)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y} 条</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [
								{
									name : '违规数量',
									data : [ 1490, 1710, 1060, 1290, 1440,
											1760, 1350, 1480, 2160, 1940, 950,
											1540 ]

								},
								{
									name : '违规次数',
									data : [ 830, 780, 980, 930, 1060, 840,
											1050, 1040, 910, 830, 1060, 920 ]

								},
								{
									name : '违规条目数',
									data : [ 480, 380, 390, 410, 470, 480, 590,
											590, 520, 650, 590, 510 ]

								},
								{
									name : '违规药品数',
									data : [ 480, 380, 390, 410, 470, 480, 590,
											590, 520, 650, 590, 510 ]

								},
								{
									name : '违规诊疗数',
									data : [ 480, 380, 390, 410, 470, 480, 590,
											590, 520, 650, 590, 510 ]

								},
								{
									name : '违规耗材数',
									data : [ 480, 380, 390, 410, 470, 480, 590,
											590, 520, 650, 590, 510 ]

								}]
					});

	// 周统计
	$('#weekMoneyContainer')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						credits : {
							enabled : false
						},
						title : {
							text : '2013年按金额统计'
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : [ '1月第1周', '1月第2周',
									'1月第3周', '1月第4周' ]
						},
						yAxis : {
							min : 0,
							title : {
								text : '金额 (元)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y:.1f} 元</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [ {
							name : '违规金额',
							data : [ 14900.9, 17100.5, 10600.4, 12900.2 ]

						}]
					});

	$('#weekAmountContainer')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						credits : {
							enabled : false
						},
						title : {
							text : '2013年按数量统计'
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : [ '1月第1周', '1月第2周',
									'1月第3周', '1月第4周' ]
						},
						yAxis : {
							min : 0,
							title : {
								text : '数量 (条)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y} 条</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [ {
							name : '违规数量',
							data : [ 1490, 1710, 1060, 1290 ]

						}, {
							name : '违规次数',
							data : [ 830, 780, 980, 930 ]

						}, {
							name : '违规条目数',
							data : [ 480, 380, 390, 410 ]

						}, {
							name : '违规药品数',
							data : [ 580, 280, 490, 510 ]

						}, {
							name : '违规诊疗数',
							data : [ 240, 540, 340, 610 ]

						}, {
							name : '违规耗材数',
							data : [ 280, 480, 190, 310 ]

						} ]
					});
	// 月统计
	$('#monthMoneyContainer')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						credits : {
							enabled : false
						},
						title : {
							text : '2013年按金额统计'
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : ['1月', '2月', '3月',
									'4月', '5月', '6月', '7月',
									'8月', '9月', '10月', '11月',
									'12月' ]
						},
						yAxis : {
							min : 0,
							title : {
								text : '金额 (元)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y:.1f} 元</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [
								{
									name : '违规金额',
									data : [ 4900.9, 7100.5, 10600.4, 12900.2,
											14400.0, 17600.0, 13500.6, 14800.5,
											21600.4, 19400.1, 9500.6, 5400.4 ]

								}]
					});

	$('#monthAmountContainer')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						credits : {
							enabled : false
						},
						title : {
							text : '2013年按数量统计'
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : [ '1月', '2月', '3月',
									'4月', '5月', '6月', '7月',
									'8月', '9月', '10月', '11月',
									'12月' ]
						},
						yAxis : {
							min : 0,
							title : {
								text : '数量 (条)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y} 条</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [
								{
									name : '违规数量',
									data : [ 1490, 1710, 1060, 1290, 1440,
											1760, 1350, 1480, 2160, 1940, 950,
											1540 ]

								},
								{
									name : '违规次数',
									data : [ 830, 780, 980, 930, 1060, 840,
											1050, 1040, 910, 830, 1060, 920 ]

								},
								{
									name : '违规条目数',
									data : [ 480, 380, 390, 410, 470, 480, 590,
											590, 520, 650, 590, 510 ]

								},
								{
									name : '违规药品数',
									data : [ 580, 280, 490, 460, 430, 470, 590,
											590, 560, 650, 590, 610 ]

								},
								{
									name : '违规诊疗数',
									data : [ 180, 780, 490, 460, 370, 450, 490,
											390, 580, 660, 490, 610 ]

								},
								{
									name : '违规耗材数',
									data : [ 380, 280, 320, 460, 410, 580, 590,
											690, 570, 450, 390, 410 ]

								} ]
					});

	// 季度统计
	$('#seasonMoneyContainer')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						credits : {
							enabled : false
						},
						title : {
							text : '2013年按金额统计'
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : [ '1季度', '2季度', '3季度',
									'4季度']
						},
						yAxis : {
							min : 0,
							title : {
								text : '金额 (元)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y:.1f} 元</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [ {
							name : '违规金额',
							data : [ 4900.9, 7100.5, 10600.4, 12900.2 ]

						}]
					});

	$('#seasonAmountContainer')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						credits : {
							enabled : false
						},
						title : {
							text : '2013年按数量统计'
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : ['1季度', '2季度', '3季度',
									'4季度']
						},
						yAxis : {
							min : 0,
							title : {
								text : '数量 (条)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y} 条</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [ {
							name : '违规数量',
							data : [ 1490, 1710, 1060, 1290 ]

						}, {
							name : '违规次数',
							data : [ 830, 780, 980, 930 ]

						}, {
							name : '违规条目数',
							data : [ 480, 380, 390, 410 ]

						}, {
							name : '违规药品数',
							data : [ 580, 280, 490, 310 ]

						}, {
							name : '违规诊疗数',
							data : [ 410, 300, 310, 460 ]

						}, {
							name : '违规耗材数',
							data : [ 430, 480, 290, 310 ]

						} ]
					});

	// 半年统计
	$('#halfyearMoneyContainer')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						credits : {
							enabled : false
						},
						title : {
							text : '按金额半年统计'
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : [ '2012上半年', '2012下半年', '2013上半年',
									'2013下半年' ]
						},
						yAxis : {
							min : 0,
							title : {
								text : '金额 (元)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y:.1f} 元</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [ {
							name : '违规金额',
							data : [ 4900.9, 7100.5, 10600.4, 12900.2 ]

						}]
					});

	$('#halfyearAmountContainer')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						credits : {
							enabled : false
						},
						title : {
							text : '按数量半年统计'
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : [ '2012上半年', '2012下半年', '2013上半年',
									'2013下半年' ]
						},
						yAxis : {
							min : 0,
							title : {
								text : '数量 (条)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y} 条</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [ {
							name : '违规数量',
							data : [ 1490, 1710, 1060, 1290 ]

						}, {
							name : '违规次数',
							data : [ 830, 780, 980, 930 ]

						}, {
							name : '违规条目数',
							data : [ 480, 380, 390, 410 ]

						}, {
							name : '违规药品数',
							data : [ 320, 280, 420, 450 ]

						}, {
							name : '违规诊疗数',
							data : [ 240, 180, 510, 420 ]

						}, {
							name : '违规耗材数',
							data : [ 380, 480, 320, 510 ]

						} ]
					});

	// 年统计
	$('#yearMoneyContainer')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						credits : {
							enabled : false
						},
						title : {
							text : '按金额年统计'
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : [ '2010年', '2011年', '2012年', '2013年' ]
						},
						yAxis : {
							min : 0,
							title : {
								text : '金额 (元)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y:.1f} 元</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [ {
							name : '违规金额',
							data : [ 4900.9, 7100.5, 10600.4, 12900.2 ]

						}]
					});

	$('#yearAmountContainer')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						credits : {
							enabled : false
						},
						title : {
							text : '按数量年统计'
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : [ '2010年', '2011年', '2012年', '2013年' ]
						},
						yAxis : {
							min : 0,
							title : {
								text : '数量 (条)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y} 条</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [ {
							name : '违规数量',
							data : [ 1490, 1710, 1060, 1290 ]

						}, {
							name : '违规次数',
							data : [ 830, 780, 980, 930 ]

						}, {
							name : '违规条目数',
							data : [ 480, 380, 390, 410 ]

						}, {
							name : '违规药品数',
							data : [ 580, 280, 290, 310 ]

						}, {
							name : '违规诊疗数',
							data : [ 390, 510, 380, 350 ]

						}, {
							name : '违规耗材数',
							data : [ 420, 480, 190, 310 ]

						} ]
					});
});

function reload(row) {
	$('#dayGrid').datagrid('reload');
	$('#weekGrid').datagrid('reload');
	$('#monthGrid').datagrid('reload');
	$('#seasonGrid').datagrid('reload');
	$('#halfyearGrid').datagrid('reload');
	$('#yearGrid').datagrid('reload');
	//dayChart.redraw();
}