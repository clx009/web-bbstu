<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!-- context path -->
<t:base type="jquery,easyui"></t:base>
<script type="text/javascript"
	src="plug-in/Highcharts-4.0.1/js/highcharts.src.js"></script>
<script type="text/javascript"
	src="plug-in/Highcharts-4.0.1/js/modules/exporting.src.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/webpage/statistics/violationsReport.js"></script>

<c:set var="ctxPath" value="${pageContext.request.contextPath}" />

<script type="text/javascript">
	
</script>
</head>
<body>
<div class="easyui-layout" data-options="fit:true">
	
	<div data-options="region:'center'" border="false">
		<div class="easyui-tabs" fit="true">
			<div title="日报">
				<div class="easyui-panel"
					data-options="region:'north',split:false,border:false,collapsible:true"
					style="overflow: hidden;">

					<table id="dayGrid" class="easyui-datagrid" title="" style="height: 200px"
						data-options="pagination:true,
								  pageSize:10,
								  rownumbers:true,
								  collapsible:true,
								  toolbar:'#day_tb',
								  url:'<%=basePath%>/webpage/statistics/medicalgroup_violations_day_datagrid_data.json',
								  method:'get'">
						<thead>
							<tr>
								<th data-options="field:'id',checkbox:true,width:60">序号</th>
								<th data-options="field:'date',width:80">日期</th>
								<th
									data-options="field:'medicalgroup',width:100,align:'center'">
									医疗机构</th>
								<th data-options="field:'violationsMoney',width:60,align:'center'">
									违规金额</th>
								<th
									data-options="field:'violationsAmount',width:60,align:'center'">
									违规数量</th>
								<th
									data-options="field:'violationsTimes',width:60,align:'center'">
									违规次数</th>
								<th
									data-options="field:'violationsItemAmount',width:90,align:'center'">
									违规条目数</th>
								<th
									data-options="field:'violationsDrugAmount',width:80,align:'center'">
									违规药品数</th>
								<th
									data-options="field:'violationsTreatAmount',width:80,align:'center'">
									违规诊疗数</th>
								<th
									data-options="field:'violationsConsumableAmount',width:80,align:'center'">
									违规耗材数</th>
								<th
									data-options="field:'outpatientPersonTime',width:80,align:'center'">
									门诊人次均</th>
								<th
									data-options="field:'seeDoctorPersonTime',width:80,align:'center'">
									就诊人次</th>
								<th
									data-options="field:'inHospitalNum',width:80,align:'center'">
									住院人数</th>
								<th
									data-options="field:'inHospitalPersonTime',width:80,align:'center'">
									住院人次均</th>
								<th
									data-options="field:'rateOfDrugsInRecord',width:80,align:'center'">
									药品备案率</th>
								<th
									data-options="field:'medicineProportion',width:80,align:'center'">
									药占比</th>
								<th
									data-options="field:'ownExpenseMedicineProportion',width:80,align:'center'">
									自费药占比</th>
								<th
									data-options="field:'ownExpenseTreatmentProportion',width:80,align:'center'">
									自费诊疗占比</th>
								<th
									data-options="field:'bigCheckProportion',width:80,align:'center'">
									大型检查占比</th>
							</tr>
						</thead>
					</table>
				</div>
				<div class="easyui-panel"
					data-options="region:'center',split:false,border:false,collapsible:true"
					style="height:320px">
					<div class="easyui-tabs" fit="true">
						<div title="按金额统计" fit="true">
							<div id="dayMoneyContainer"
								style="height:280px;overflow: hidden;"></div>
						</div>
						<div title="按数量统计" fit="true">
							<div id="dayAmountContainer"
								style="width:1200px;height:280px;overflow: hidden;"></div>
						</div>
					</div>
				</div>
			</div>
			<div title="周报">
				<div class="easyui-panel"
					data-options="region:'north',split:false,border:false,collapsible:true"
					style="overflow: hidden;">

					<table id="weekGrid" class="easyui-datagrid" title="" style="height: 200px"
						data-options="pagination:true,
								  pageSize:10,
								  rownumbers:true,
								  collapsible:true,
								  toolbar:'#week_tb',
								  url:'<%=basePath%>/webpage/statistics/medicalgroup_violations_week_datagrid_data.json',
								  method:'get'">
						<thead>
							<tr>
								<th data-options="field:'id',checkbox:true,width:60">序号</th>
								<th data-options="field:'date',width:80">日期</th>
								<th
									data-options="field:'medicalgroup',width:100,align:'center'">
									医疗机构</th>
								<th data-options="field:'violationsMoney',width:60,align:'center'">
									违规金额</th>
								<th
									data-options="field:'violationsAmount',width:60,align:'center'">
									违规数量</th>
								<th
									data-options="field:'violationsTimes',width:60,align:'center'">
									违规次数</th>
								<th
									data-options="field:'violationsItemAmount',width:90,align:'center'">
									违规条目数</th>
								<th
									data-options="field:'violationsDrugAmount',width:80,align:'center'">
									违规药品数</th>
								<th
									data-options="field:'violationsTreatAmount',width:80,align:'center'">
									违规诊疗数</th>
								<th
									data-options="field:'violationsConsumableAmount',width:80,align:'center'">
									违规耗材数</th>
								<th
									data-options="field:'outpatientPersonTime',width:80,align:'center'">
									门诊人次均</th>
								<th
									data-options="field:'seeDoctorPersonTime',width:80,align:'center'">
									就诊人次</th>
								<th
									data-options="field:'inHospitalNum',width:80,align:'center'">
									住院人数</th>
								<th
									data-options="field:'inHospitalPersonTime',width:80,align:'center'">
									住院人次均</th>
								<th
									data-options="field:'rateOfDrugsInRecord',width:80,align:'center'">
									药品备案率</th>
								<th
									data-options="field:'medicineProportion',width:80,align:'center'">
									药占比</th>
								<th
									data-options="field:'ownExpenseMedicineProportion',width:80,align:'center'">
									自费药占比</th>
								<th
									data-options="field:'ownExpenseTreatmentProportion',width:80,align:'center'">
									自费诊疗占比</th>
								<th
									data-options="field:'bigCheckProportion',width:80,align:'center'">
									大型检查占比</th>
							</tr>
						</thead>
					</table>
				</div>
				<div class="easyui-panel"
					data-options="region:'center',split:false,border:false,collapsible:true"
					style="height:320px">
					<div class="easyui-tabs" fit="true">
						<div title="按金额统计" fit="true">
							<div id="weekMoneyContainer"
								style="width:1200px;height:280px;overflow: hidden;"></div>
						</div>
						<div title="按数量统计" fit="true">
							<div id="weekAmountContainer"
								style="width:1200px;height:280px;overflow: hidden;"></div>
						</div>
					</div>
				</div>
			</div>
			<div title="月报">
				<div class="easyui-panel"
					data-options="region:'north',split:false,border:false,collapsible:true"
					style="overflow: hidden;">

					<table id="monthGrid" class="easyui-datagrid" title="" style="height: 200px"
						data-options="pagination:true,
								  pageSize:10,
								  rownumbers:true,
								  collapsible:true,
								  toolbar:'#month_tb',
								  url:'<%=basePath%>/webpage/statistics/medicalgroup_violations_month_datagrid_data.json',
								  method:'get'">
						<thead>
							<tr>
								<th data-options="field:'id',checkbox:true,width:60">序号</th>
								<th data-options="field:'date',width:80">日期</th>
								<th
									data-options="field:'medicalgroup',width:100,align:'center'">
									医疗机构</th>
								<th data-options="field:'violationsMoney',width:60,align:'center'">
									违规金额</th>
								<th
									data-options="field:'violationsAmount',width:60,align:'center'">
									违规数量</th>
								<th
									data-options="field:'violationsTimes',width:60,align:'center'">
									违规次数</th>
								<th
									data-options="field:'violationsItemAmount',width:90,align:'center'">
									违规条目数</th>
								<th
									data-options="field:'violationsDrugAmount',width:80,align:'center'">
									违规药品数</th>
								<th
									data-options="field:'violationsTreatAmount',width:80,align:'center'">
									违规诊疗数</th>
								<th
									data-options="field:'violationsConsumableAmount',width:80,align:'center'">
									违规耗材数</th>
								<th
									data-options="field:'outpatientPersonTime',width:80,align:'center'">
									门诊人次均</th>
								<th
									data-options="field:'seeDoctorPersonTime',width:80,align:'center'">
									就诊人次</th>
								<th
									data-options="field:'inHospitalNum',width:80,align:'center'">
									住院人数</th>
								<th
									data-options="field:'inHospitalPersonTime',width:80,align:'center'">
									住院人次均</th>
								<th
									data-options="field:'rateOfDrugsInRecord',width:80,align:'center'">
									药品备案率</th>
								<th
									data-options="field:'medicineProportion',width:80,align:'center'">
									药占比</th>
								<th
									data-options="field:'ownExpenseMedicineProportion',width:80,align:'center'">
									自费药占比</th>
								<th
									data-options="field:'ownExpenseTreatmentProportion',width:80,align:'center'">
									自费诊疗占比</th>
								<th
									data-options="field:'bigCheckProportion',width:80,align:'center'">
									大型检查占比</th>
							</tr>
						</thead>
					</table>
				</div>
				<div class="easyui-panel"
					data-options="region:'center',split:false,border:false,collapsible:true"
					style="height:320px">
					<div class="easyui-tabs" fit="true">
						<div title="按金额统计" fit="true">
							<div id="monthMoneyContainer"
								style="width:1200px;height:280px;overflow: hidden;"></div>
						</div>
						<div title="按数量统计" fit="true">
							<div id="monthAmountContainer"
								style="width:1200px;height:280px;overflow: hidden;"></div>
						</div>
					</div>
				</div>
			</div>
			<div title="季报">
				<div class="easyui-panel"
					data-options="region:'north',split:false,border:false,collapsible:true"
					style="overflow: hidden;">

					<table id="seasonGrid" class="easyui-datagrid" title="" style="height: 200px"
						data-options="pagination:true,
								  pageSize:10,
								  rownumbers:true,
								  collapsible:true,
								  toolbar:'#season_tb',
								  url:'<%=basePath%>/webpage/statistics/medicalgroup_violations_season_datagrid_data.json',
								  method:'get'">
						<thead>
							<tr>
								<th data-options="field:'id',checkbox:true,width:60">序号</th>
								<th data-options="field:'date',width:80">日期</th>
								<th
									data-options="field:'medicalgroup',width:100,align:'center'">
									医疗机构</th>
								<th data-options="field:'violationsMoney',width:60,align:'center'">
									违规金额</th>
								<th
									data-options="field:'violationsAmount',width:60,align:'center'">
									违规数量</th>
								<th
									data-options="field:'violationsTimes',width:60,align:'center'">
									违规次数</th>
								<th
									data-options="field:'violationsItemAmount',width:90,align:'center'">
									违规条目数</th>
								<th
									data-options="field:'violationsDrugAmount',width:80,align:'center'">
									违规药品数</th>
								<th
									data-options="field:'violationsTreatAmount',width:80,align:'center'">
									违规诊疗数</th>
								<th
									data-options="field:'violationsConsumableAmount',width:80,align:'center'">
									违规耗材数</th>
								<th
									data-options="field:'outpatientPersonTime',width:80,align:'center'">
									门诊人次均</th>
								<th
									data-options="field:'seeDoctorPersonTime',width:80,align:'center'">
									就诊人次</th>
								<th
									data-options="field:'inHospitalNum',width:80,align:'center'">
									住院人数</th>
								<th
									data-options="field:'inHospitalPersonTime',width:80,align:'center'">
									住院人次均</th>
								<th
									data-options="field:'rateOfDrugsInRecord',width:80,align:'center'">
									药品备案率</th>
								<th
									data-options="field:'medicineProportion',width:80,align:'center'">
									药占比</th>
								<th
									data-options="field:'ownExpenseMedicineProportion',width:80,align:'center'">
									自费药占比</th>
								<th
									data-options="field:'ownExpenseTreatmentProportion',width:80,align:'center'">
									自费诊疗占比</th>
								<th
									data-options="field:'bigCheckProportion',width:80,align:'center'">
									大型检查占比</th>
							</tr>
						</thead>
					</table>
				</div>
				<div class="easyui-panel"
					data-options="region:'center',split:false,border:false,collapsible:true"
					style="height:320px">
					<div class="easyui-tabs" fit="true">
						<div title="按金额统计" fit="true">
							<div id="seasonMoneyContainer"
								style="width:1200px;height:280px;overflow: hidden;"></div>
						</div>
						<div title="按数量统计" fit="true">
							<div id="seasonAmountContainer"
								style="width:1200px;height:280px;overflow: hidden;"></div>
						</div>
					</div>
				</div>
			</div>
			<div title="半年报">
				<div class="easyui-panel"
					data-options="region:'north',split:false,border:false,collapsible:true"
					style="overflow: hidden;">

					<table id="halfyearGrid" class="easyui-datagrid" title="" style="height: 200px"
						data-options="pagination:true,
								  pageSize:10,
								  rownumbers:true,
								  collapsible:true,
								  toolbar:'#halfyear_tb',
								  url:'<%=basePath%>/webpage/statistics/medicalgroup_violations_halfyear_datagrid_data.json',
								  method:'get'">
						<thead>
							<tr>
								<th data-options="field:'id',checkbox:true,width:60">序号</th>
								<th data-options="field:'date',width:80">日期</th>
								<th
									data-options="field:'medicalgroup',width:100,align:'center'">
									医疗机构</th>
								<th data-options="field:'violationsMoney',width:60,align:'center'">
									违规金额</th>
								<th
									data-options="field:'violationsAmount',width:60,align:'center'">
									违规数量</th>
								<th
									data-options="field:'violationsTimes',width:60,align:'center'">
									违规次数</th>
								<th
									data-options="field:'violationsItemAmount',width:90,align:'center'">
									违规条目数</th>
								<th
									data-options="field:'violationsDrugAmount',width:80,align:'center'">
									违规药品数</th>
								<th
									data-options="field:'violationsTreatAmount',width:80,align:'center'">
									违规诊疗数</th>
								<th
									data-options="field:'violationsConsumableAmount',width:80,align:'center'">
									违规耗材数</th>
								<th
									data-options="field:'outpatientPersonTime',width:80,align:'center'">
									门诊人次均</th>
								<th
									data-options="field:'seeDoctorPersonTime',width:80,align:'center'">
									就诊人次</th>
								<th
									data-options="field:'inHospitalNum',width:80,align:'center'">
									住院人数</th>
								<th
									data-options="field:'inHospitalPersonTime',width:80,align:'center'">
									住院人次均</th>
								<th
									data-options="field:'rateOfDrugsInRecord',width:80,align:'center'">
									药品备案率</th>
								<th
									data-options="field:'medicineProportion',width:80,align:'center'">
									药占比</th>
								<th
									data-options="field:'ownExpenseMedicineProportion',width:80,align:'center'">
									自费药占比</th>
								<th
									data-options="field:'ownExpenseTreatmentProportion',width:80,align:'center'">
									自费诊疗占比</th>
								<th
									data-options="field:'bigCheckProportion',width:80,align:'center'">
									大型检查占比</th>
							</tr>
						</thead>
					</table>
				</div>
				<div class="easyui-panel"
					data-options="region:'center',split:false,border:false,collapsible:true"
					style="height:320px">
					<div class="easyui-tabs" fit="true">
						<div title="按金额统计" fit="true">
							<div id="halfyearMoneyContainer"
								style="width:1200px;height:280px;overflow: hidden;"></div>
						</div>
						<div title="按数量统计" fit="true">
							<div id="halfyearAmountContainer"
								style="width:1200px;height:280px;overflow: hidden;"></div>
						</div>
					</div>
				</div>
			</div>
			<div title="年报">
				<div class="easyui-panel"
					data-options="region:'north',split:false,border:false,collapsible:true"
					style="overflow: hidden;">

					<table id="yearGrid" class="easyui-datagrid" title="" style="height: 200px"
						data-options="pagination:true,
								  pageSize:10,
								  rownumbers:true,
								  collapsible:true,
								  toolbar:'#year_tb',
								  url:'<%=basePath%>/webpage/statistics/medicalgroup_violations_year_datagrid_data.json',
								  method:'get'">
						<thead>
							<tr>
								<th data-options="field:'id',checkbox:true,width:60">序号</th>
								<th data-options="field:'date',width:80">日期</th>
								<th
									data-options="field:'medicalgroup',width:100,align:'center'">
									医疗机构</th>
								<th data-options="field:'violationsMoney',width:60,align:'center'">
									违规金额</th>
								<th
									data-options="field:'violationsAmount',width:60,align:'center'">
									违规数量</th>
								<th
									data-options="field:'violationsTimes',width:60,align:'center'">
									违规次数</th>
								<th
									data-options="field:'violationsItemAmount',width:90,align:'center'">
									违规条目数</th>
								<th
									data-options="field:'violationsDrugAmount',width:80,align:'center'">
									违规药品数</th>
								<th
									data-options="field:'violationsTreatAmount',width:80,align:'center'">
									违规诊疗数</th>
								<th
									data-options="field:'violationsConsumableAmount',width:80,align:'center'">
									违规耗材数</th>
								<th
									data-options="field:'outpatientPersonTime',width:80,align:'center'">
									门诊人次均</th>
								<th
									data-options="field:'seeDoctorPersonTime',width:80,align:'center'">
									就诊人次</th>
								<th
									data-options="field:'inHospitalNum',width:80,align:'center'">
									住院人数</th>
								<th
									data-options="field:'inHospitalPersonTime',width:80,align:'center'">
									住院人次均</th>
								<th
									data-options="field:'rateOfDrugsInRecord',width:80,align:'center'">
									药品备案率</th>
								<th
									data-options="field:'medicineProportion',width:80,align:'center'">
									药占比</th>
								<th
									data-options="field:'ownExpenseMedicineProportion',width:80,align:'center'">
									自费药占比</th>
								<th
									data-options="field:'ownExpenseTreatmentProportion',width:80,align:'center'">
									自费诊疗占比</th>
								<th
									data-options="field:'bigCheckProportion',width:80,align:'center'">
									大型检查占比</th>
							</tr>
						</thead>
					</table>
				</div>
				<div class="easyui-panel"
					data-options="region:'center',split:false,border:false,collapsible:true"
					style="height:320px">
					<div class="easyui-tabs" fit="true">
						<div title="按金额统计" fit="true">
							<div id="yearMoneyContainer"
								style="width:1200px;height:280px;overflow: hidden;"></div>
						</div>
						<div title="按数量统计" fit="true">
							<div id="yearAmountContainer"
								style="width:1200px;height:280px;overflow: hidden;"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="day_tb">
	<a class="easyui-linkbutton" icon="icon-putout" plain="true">导出</a>
	<a class="easyui-linkbutton" icon="icon-print" plain="true">打印</a>
</div>
<div id="week_tb">
	<a class="easyui-linkbutton" icon="icon-putout" plain="true">导出</a>
	<a class="easyui-linkbutton" icon="icon-print" plain="true">打印</a>
</div>
<div id="month_tb">
	<a class="easyui-linkbutton" icon="icon-putout" plain="true">导出</a>
	<a class="easyui-linkbutton" icon="icon-print" plain="true">打印</a>
</div>
<div id="season_tb">
	<a class="easyui-linkbutton" icon="icon-putout" plain="true">导出</a>
	<a class="easyui-linkbutton" icon="icon-print" plain="true">打印</a>
</div>
<div id="halfyear_tb">
	<a class="easyui-linkbutton" icon="icon-putout" plain="true">导出</a>
	<a class="easyui-linkbutton" icon="icon-print" plain="true">打印</a>
</div>
<div id="year_tb">
	<a class="easyui-linkbutton" icon="icon-putout" plain="true">导出</a>
	<a class="easyui-linkbutton" icon="icon-print" plain="true">打印</a>
</div>
</body>