$(function() {
	// 日统计
	$('#dayMoneyContainer')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						title : {
							text : ''
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : [ '2013-1-1', '2013-1-2', '2013-1-3',
									'2013-1-4', '2013-1-5', '2013-1-6',
									'2013-1-7', '2013-1-8', '2013-1-9',
									'2013-1-10', '2013-1-11', '2013-1-12' ]
						},
						yAxis : {
							min : 0,
							title : {
								text : '金额 (元)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y:.1f} 元</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [
								{
									name : '处方金额',
									data : [ 4900.9, 7100.5, 10600.4, 12900.2,
											14400.0, 17600.0, 13500.6, 14800.5,
											21600.4, 19400.1, 9500.6, 5400.4 ]

								},
								{
									name : '异常金额',
									data : [ 8300.6, 7800.8, 9800.5, 9300.4,
											10600.0, 8400.5, 10500.0, 10400.3,
											9100.2, 8300.5, 10600.6, 9200.3 ]

								},
								{
									name : '拒付金额',
									data : [ 4800.9, 3800.8, 3900.3, 4100.4,
											4700.0, 4800.3, 5900.0, 5900.6,
											5200.4, 6500.2, 5900.3, 5100.2 ]

								} ]
					});

	$('#dayAmountContainer')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						title : {
							text : ''
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : [ '2013-1-1', '2013-1-2', '2013-1-3',
									'2013-1-4', '2013-1-5', '2013-1-6',
									'2013-1-7', '2013-1-8', '2013-1-9',
									'2013-1-10', '2013-1-11', '2013-1-12' ]
						},
						yAxis : {
							min : 0,
							title : {
								text : '数量 (条)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y} 条</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [
								{
									name : '处方数量',
									data : [ 1490, 1710, 1060, 1290, 1440,
											1760, 1350, 1480, 2160, 1940, 950,
											1540 ]

								},
								{
									name : '异常数量',
									data : [ 830, 780, 980, 930, 1060, 840,
											1050, 1040, 910, 830, 1060, 920 ]

								},
								{
									name : '拒付数量',
									data : [ 480, 380, 390, 410, 470, 480, 590,
											590, 520, 650, 590, 510 ]

								} ]
					});

	// 周统计
	$('#weekMoneyContainer')
			.highcharts(
					{
						chart : {
							type : 'column'
						},
						title : {
							text : ''
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : [ '201301第1周', '201301第2周', '201301第3周','201301第4周']
						},
						yAxis : {
							min : 0,
							title : {
								text : '金额 (元)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y:.1f} 元</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [
								{
									name : '处方金额',
									data : [ 14900.9, 17100.5, 10600.4, 12900.2]

								},
								{
									name : '异常金额',
									data : [ 8300.6, 7800.8, 9800.5, 9300.4 ]

								},
								{
									name : '拒付金额',
									data : [ 4800.9, 3800.8, 3900.3, 4100.4]

								} ]
					});

	$('#weekAmountContainer').highcharts({
						chart : {
							type : 'column'
						},
						title : {
							text : ''
						},
						subtitle : {
							text : ''
						},
						xAxis : {
							categories : ['201301第1周', '201301第2周', '201301第3周','201301第4周']
						},
						yAxis : {
							min : 0,
							title : {
								text : '数量 (条)'
							}
						},
						tooltip : {
							headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
							pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
									+ '<td style="padding:0"><b>{point.y} 条</b></td></tr>',
							footerFormat : '</table>',
							shared : true,
							useHTML : true
						},
						plotOptions : {
							column : {
								pointPadding : 0.2,
								borderWidth : 0
							}
						},
						series : [
								{
									name : '处方数量',
									data : [ 1490, 1710, 1060, 1290]

								},
								{
									name : '异常数量',
									data : [ 830, 780, 980, 930]

								},
								{
									name : '拒付数量',
									data : [ 480, 380, 390, 410]

								} ]
					});
	// 月统计
	$('#monthMoneyContainer')
	.highcharts(
			{
				chart : {
					type : 'column'
				},
				title : {
					text : ''
				},
				subtitle : {
					text : ''
				},
				xAxis : {
					categories : [ '201301', '201302', '201303','201304','201305','201306','201307','201308','201309','201310','201311','201312']
				},
				yAxis : {
					min : 0,
					title : {
						text : '金额 (元)'
					}
				},
				tooltip : {
					headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
					pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
						+ '<td style="padding:0"><b>{point.y:.1f} 元</b></td></tr>',
						footerFormat : '</table>',
						shared : true,
						useHTML : true
				},
				plotOptions : {
					column : {
						pointPadding : 0.2,
						borderWidth : 0
					}
				},
				series : [
				          {
				        	  name : '处方金额',
				        	  data : [ 4900.9, 7100.5, 10600.4, 12900.2,
										14400.0, 17600.0, 13500.6, 14800.5,
										21600.4, 19400.1, 9500.6, 5400.4]
				          
				          },
				          {
				        	  name : '异常金额',
				        	  data : [  8300.6, 7800.8, 9800.5, 9300.4,
										10600.0, 8400.5, 10500.0, 10400.3,
										9100.2, 8300.5, 10600.6, 9200.3 ]
				          
				          },
				          {
				        	  name : '拒付金额',
				        	  data : [ 4800.9, 3800.8, 3900.3, 4100.4,
										4700.0, 4800.3, 5900.0, 5900.6,
										5200.4, 6500.2, 5900.3, 5100.2]
				          
				          } ]
			});
	
	$('#monthAmountContainer').highcharts({
		chart : {
			type : 'column'
		},
		title : {
			text : ''
		},
		subtitle : {
			text : ''
		},
		xAxis : {
			categories : ['201301', '201302', '201303','201304','201305','201306','201307','201308','201309','201310','201311','201312']
		},
		yAxis : {
			min : 0,
			title : {
				text : '数量 (条)'
			}
		},
		tooltip : {
			headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
				+ '<td style="padding:0"><b>{point.y} 条</b></td></tr>',
				footerFormat : '</table>',
				shared : true,
				useHTML : true
		},
		plotOptions : {
			column : {
				pointPadding : 0.2,
				borderWidth : 0
			}
		},
		series : [
					{
						name : '处方数量',
						data : [ 1490, 1710, 1060, 1290, 1440,
								1760, 1350, 1480, 2160, 1940, 950,
								1540 ]

					},
					{
						name : '异常数量',
						data : [ 830, 780, 980, 930, 1060, 840,
								1050, 1040, 910, 830, 1060, 920 ]

					},
					{
						name : '拒付数量',
						data : [ 480, 380, 390, 410, 470, 480, 590,
								590, 520, 650, 590, 510 ]

					} ]
	});
	
	// 季度统计
	$('#seasonMoneyContainer')
	.highcharts(
			{
				chart : {
					type : 'column'
				},
				title : {
					text : ''
				},
				subtitle : {
					text : ''
				},
				xAxis : {
					categories : [ '2013第1季度', '2013第2季度', '2013第3季度','2013第4季度']
				},
				yAxis : {
					min : 0,
					title : {
						text : '金额 (元)'
					}
				},
				tooltip : {
					headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
					pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
						+ '<td style="padding:0"><b>{point.y:.1f} 元</b></td></tr>',
						footerFormat : '</table>',
						shared : true,
						useHTML : true
				},
				plotOptions : {
					column : {
						pointPadding : 0.2,
						borderWidth : 0
					}
				},
				series : [
				          {
				        	  name : '处方金额',
				        	  data : [ 4900.9, 7100.5, 10600.4, 12900.2]
				          
				          },
				          {
				        	  name : '异常金额',
				        	  data : [  8300.6, 7800.8, 9800.5, 9300.4]
				          
				          },
				          {
				        	  name : '拒付金额',
				        	  data : [ 4800.9, 3800.8, 3900.3, 4100.4]
				          
				          } ]
			});
	
	$('#seasonAmountContainer').highcharts({
		chart : {
			type : 'column'
		},
		title : {
			text : ''
		},
		subtitle : {
			text : ''
		},
		xAxis : {
			categories : ['2013第1季度', '2013第2季度', '2013第3季度','2013第4季度']
		},
		yAxis : {
			min : 0,
			title : {
				text : '数量 (条)'
			}
		},
		tooltip : {
			headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
				+ '<td style="padding:0"><b>{point.y} 条</b></td></tr>',
				footerFormat : '</table>',
				shared : true,
				useHTML : true
		},
		plotOptions : {
			column : {
				pointPadding : 0.2,
				borderWidth : 0
			}
		},
		series : [
		          {
		        	  name : '处方数量',
		        	  data : [ 1490, 1710, 1060, 1290]
		          
		          },
		          {
		        	  name : '异常数量',
		        	  data : [ 830, 780, 980, 930]
		          
		          },
		          {
		        	  name : '拒付数量',
		        	  data : [ 480, 380, 390, 410]
		          
		          } ]
	});
	
	// 半年统计
	$('#halfyearMoneyContainer')
	.highcharts(
			{
				chart : {
					type : 'column'
				},
				title : {
					text : ''
				},
				subtitle : {
					text : ''
				},
				xAxis : {
					categories : [ '2012上半年', '2012下半年', '2013上半年', '2013下半年']
				},
				yAxis : {
					min : 0,
					title : {
						text : '金额 (元)'
					}
				},
				tooltip : {
					headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
					pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
						+ '<td style="padding:0"><b>{point.y:.1f} 元</b></td></tr>',
						footerFormat : '</table>',
						shared : true,
						useHTML : true
				},
				plotOptions : {
					column : {
						pointPadding : 0.2,
						borderWidth : 0
					}
				},
				series : [
				          {
				        	  name : '处方金额',
				        	  data : [ 4900.9, 7100.5, 10600.4, 12900.2]
				          
				          },
				          {
				        	  name : '异常金额',
				        	  data : [  8300.6, 7800.8, 9800.5, 9300.4]
				          
				          },
				          {
				        	  name : '拒付金额',
				        	  data : [ 4800.9, 3800.8, 3900.3, 4100.4]
				          
				          } ]
			});
	
	$('#halfyearAmountContainer').highcharts({
		chart : {
			type : 'column'
		},
		title : {
			text : ''
		},
		subtitle : {
			text : ''
		},
		xAxis : {
			categories : ['2012上半年', '2012下半年', '2013上半年', '2013下半年']
		},
		yAxis : {
			min : 0,
			title : {
				text : '数量 (条)'
			}
		},
		tooltip : {
			headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
				+ '<td style="padding:0"><b>{point.y} 条</b></td></tr>',
				footerFormat : '</table>',
				shared : true,
				useHTML : true
		},
		plotOptions : {
			column : {
				pointPadding : 0.2,
				borderWidth : 0
			}
		},
		series : [
		          {
		        	  name : '处方数量',
		        	  data : [ 1490, 1710, 1060, 1290]
		          
		          },
		          {
		        	  name : '异常数量',
		        	  data : [ 830, 780, 980, 930]
		          
		          },
		          {
		        	  name : '拒付数量',
		        	  data : [ 480, 380, 390, 410]
		          
		          } ]
	});
	
	// 年统计
	$('#yearMoneyContainer')
	.highcharts(
			{
				chart : {
					type : 'column'
				},
				title : {
					text : ''
				},
				subtitle : {
					text : ''
				},
				xAxis : {
					categories : [ '2010年', '2011年', '2012年', '2013年']
				},
				yAxis : {
					min : 0,
					title : {
						text : '金额 (元)'
					}
				},
				tooltip : {
					headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
					pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
						+ '<td style="padding:0"><b>{point.y:.1f} 元</b></td></tr>',
						footerFormat : '</table>',
						shared : true,
						useHTML : true
				},
				plotOptions : {
					column : {
						pointPadding : 0.2,
						borderWidth : 0
					}
				},
				series : [
				          {
				        	  name : '处方金额',
				        	  data : [ 4900.9, 7100.5, 10600.4, 12900.2]
				          
				          },
				          {
				        	  name : '异常金额',
				        	  data : [  8300.6, 7800.8, 9800.5, 9300.4]
				          
				          },
				          {
				        	  name : '拒付金额',
				        	  data : [ 4800.9, 3800.8, 3900.3, 4100.4]
				          
				          } ]
			});
	
	$('#yearAmountContainer').highcharts({
		chart : {
			type : 'column'
		},
		title : {
			text : ''
		},
		subtitle : {
			text : ''
		},
		xAxis : {
			categories : ['2010年', '2011年', '2012年', '2013年']
		},
		yAxis : {
			min : 0,
			title : {
				text : '数量 (条)'
			}
		},
		tooltip : {
			headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
			pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
				+ '<td style="padding:0"><b>{point.y} 条</b></td></tr>',
				footerFormat : '</table>',
				shared : true,
				useHTML : true
		},
		plotOptions : {
			column : {
				pointPadding : 0.2,
				borderWidth : 0
			}
		},
		series : [
		          {
		        	  name : '处方数量',
		        	  data : [ 1490, 1710, 1060, 1290]
		          
		          },
		          {
		        	  name : '异常数量',
		        	  data : [ 830, 780, 980, 930]
		          
		          },
		          {
		        	  name : '拒付数量',
		        	  data : [ 480, 380, 390, 410]
		          
		          } ]
	});
});