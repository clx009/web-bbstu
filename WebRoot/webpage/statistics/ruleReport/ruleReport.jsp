<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!-- context path -->
<t:base type="jquery,easyui"></t:base>
<script type="text/javascript" src="plug-in/Highcharts-2.2.5/js/highcharts.src.js"></script>
<script type="text/javascript" src="plug-in/Highcharts-2.2.5/js/modules/exporting.src.js"></script>

<c:set var="ctxPath" value="${pageContext.request.contextPath}" />

<script type="text/javascript">
	$(function() {
		$(document).ready(function() {
			var chart;
			$.ajax({
				type : "POST",
				url : "ruleReportController.do?statisticsCount&reportType=column",
				success : function(jsondata) {
					data = eval(jsondata);
					chart = new Highcharts.Chart({
						chart : {
							renderTo : 'containerCol',
							plotBackgroundColor : null,
							plotBorderWidth : null,
							plotShadow : false
						},
						title : {
							text : '按违规规则统计'
						},
						xAxis : {
							categories : data[0]
						},
						yAxis: [
						{
							labels: {
				                format: '{value}个',
				                style: {
				                    color: '#89A54E'
				                }
				            },
				            title: {
				                text: '处方数',
				                style: {
				                    color: '#89A54E'
				                }
				            }
						},
						{
							 title: {
				                text: '金额',
				                style: {
				                    color: '#4572A7'
				                }
				            },
				            labels: {
				                format: '{value} 元',
				                style: {
				                    color: '#4572A7'
				                }
				            },
				            opposite: true
						}
						],
						tooltip : {
							 shared: true //公用一个提示框 
						},
						exporting:{ 
			                filename:'column',  
			                url:'${ctxPath}/ruleReportController.do?export'//
			            },
						plotOptions : {
							column : {
								allowPointSelect : true,
								cursor : 'pointer',
								showInLegend : true,
								dataLabels : {
									enabled : true,
									color : '#000000',
									connectorColor : '#000000',
									formatter : function() {
										//return  '<b>'+this.point.name + '</b>:' +  this.point.y;
										return null;
									}
								}
							}
						},
						series: [{ 
						name: '异常处方总数', 
						type: 'column', 
						data: data[1], 
						tooltip: { 
						valueSuffix: '个'
						} 
						},{ 
						name: '拒付处方总数', 
						type: 'column', 
						data: data[2], 
						tooltip: { 
						valueSuffix: '个'
						} 
						}, { 
						name: '异常处方金额', 
						type: 'column', 
						data: data[3], 
						yAxis: 1, 
						tooltip: { 
						valueSuffix: '元'
						} 
						},{ 
						name: '拒付处方金额', 
						type: 'column', 
						data: data[4], 
						yAxis: 1, 
						tooltip: { 
						valueSuffix: '元'
						} 
						}] 
					});
				}
			});
		});
	});
</script>


<span id="containerCol" style="float: left; width: 38%; height: 60%"></span>

<div style="width: 98%; height: 280px"><t:datagrid name="statisticList" title="按违规规则统计" actionUrl="ruleReportController.do?listAllStatisticByJdbc" idField="id" fit="true">
	<t:dgCol title="序号" field="id" hidden="false"></t:dgCol>
	<t:dgCol title="违规规则名称" field="RULE_NAME" width="130"></t:dgCol>
	<t:dgCol title="异常处方总数" field="EXC_TOTALCOUNT" width="130"></t:dgCol>
	<t:dgCol title="拒付处方总数" field="REJ_TOTALCOUNT" width="130"></t:dgCol>
	<t:dgCol title="异常处方金额" field="EXC_TOTALMONEY" width="130"></t:dgCol>
	<t:dgCol title="拒付处方金额" field="REJ_TOTALMONEY" width="130"></t:dgCol>
</t:datagrid></div>