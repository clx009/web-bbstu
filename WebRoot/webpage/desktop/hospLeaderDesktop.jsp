<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!-- context path -->
<head>
	<c:set var="ctxPath" value="${pageContext.request.contextPath}" />
	<script type="text/javascript" src="<%=basePath%>/plug-in/jquery/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/tools/dataformat.js"></script>
	<link id="easyuiTheme" rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/default/easyui.css" type="text/css"></link>
	<link rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/icon.css" type="text/css"></link>
	<link rel="stylesheet" type="text/css" href="<%=basePath%>/plug-in/accordion/css/accordion.css">
	<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/jquery.easyui.min.1.3.2.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/tools/syUtil.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>
	<link rel="stylesheet" href="<%=basePath%>/plug-in/tools/css/common.css" type="text/css"></link>
	<script type="text/javascript" src="<%=basePath%>/plug-in/lhgDialog/lhgdialog.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/tools/curdtools.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/tools/easyuiextend.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/highcharts.src.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/modules/exporting.src.js"></script>
		<script type="text/javascript">
			$(function () {
		        
		         $('#rightDiv1').highcharts({
			        chart: {
			            plotBackgroundColor: null,
			            plotBorderWidth: null,
			            plotShadow: false
			        },
			        credits : {
						enabled : false
					},
			        title: {
			            text: '当日任务状态图'
			        },
			        tooltip: {
			    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}% （{point.y}条）</b>'
			        },
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                dataLabels: {
			                    enabled: true,
				                crop: false,
				                overflow: 'none'
			                }
			            }
			        },
			        series: [{
			            type: 'pie',
			            name: '处方',
			            data: [
			            	{name:'已确认',y:145,color:'#95CEFF'},{name:'待确认',y:130,color:'#FFBC75'}
			            ]
			        }]
			    });
			    
			    $('#rightDiv2').highcharts({
			        chart: {
			            plotBackgroundColor: null,
			            plotBorderWidth: null,
			            plotShadow: false
			        },
			        credits : {
						enabled : false
					},
			        title: {
			            text: '当日已确认任务状态图'
			        },
			        tooltip: {
			    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}% （{point.y}条）</b>'
			        },
			        plotOptions: {
			            pie: {
			                allowPointSelect: true,
			                cursor: 'pointer',
			                dataLabels: {
				                enabled: true,
				                crop: false,
				                overflow: 'none'
				            }
			            }
			        },
			        series: [{
			            type: 'pie',
			            name: '处方',
			            data: [
			            	{name:'确认支付数',y:115,color:'#95CEFF'},{name:'复议数',y:30,color:'#FFBC75'}
			            ]
			        }]
			    });
		        
		        
		        $('#datagridDiv2').datagrid({  
					title:'当日日志',
					method:'get', 
					fit:true,
				    url:'<%=basePath%>/webpage/desktop/datagrid_hospworklog.json',   
				    singleSelect:true,
				    columns:[[   
				    	{field:'userName',title:'操作人',width:fixWidth(0.13)},
				        {field:'logcontent',title:'日志内容',width:fixWidth(0.3)},   
				        {field:'operatetime',title:'操作时间',width:fixWidth(0.15)}
				    ]]   
				}); 
				
				$('#datagridDiv2').datagrid({
					toolbar: '#toolbarDiv2'
				});
				
				$('#datagridDiv1').datagrid({ 
					title:'确认任务',
					method:'get', 
				    url:'<%=basePath%>/webpage/desktop/datagrid_hosp.json',   
				    singleSelect:true,
				    columns:[[   
				    	{field:'',title:'',width:fixWidth(0.08)},
				    	{field:'',title:'门诊',colspan:2}, 
				        {field:'',title:'住院',colspan:2}  
				    ],[   
				    	{field:'SHQK',title:'确认情况',width:fixWidth(0.08)},
				    	{field:'MZSL',title:'数量',width:fixWidth(0.1)}, 
				        {field:'MZJE',title:'金额',width:fixWidth(0.15)},
				        {field:'ZYSL',title:'数量',width:fixWidth(0.1)}, 
				        {field:'ZYJE',title:'金额',width:fixWidth(0.15)}  
				    ]]   
				}); 
				
				$('#datagridDiv1').datagrid({
					toolbar: '#toolbarDiv1'
				});
				
				$('#datagridDiv3').datagrid({ 
					title:'申请复议',
					method:'get', 
				    url:'<%=basePath%>/webpage/desktop/datagrid_hosp_fuyi.json',   
				    singleSelect:true,
				    columns:[[   
				    	{field:'',title:'',width:fixWidth(0.08)},
				    	{field:'',title:'门诊',colspan:2}, 
				        {field:'',title:'住院',colspan:2}  
				    ],[   
				    	{field:'SHQK',title:'复议情况',width:fixWidth(0.08)},
				    	{field:'MZSL',title:'数量',width:fixWidth(0.1)}, 
				        {field:'MZJE',title:'金额',width:fixWidth(0.15)},
				        {field:'ZYSL',title:'数量',width:fixWidth(0.1)}, 
				        {field:'ZYJE',title:'金额',width:fixWidth(0.15)}  
				    ]]   
				}); 
				
				$('#datagridDiv3').datagrid({
					toolbar: '#toolbarDiv3'
				});
				
				
				
		    });
		    
		    function detailAudit() {
				createwindow('确认任务详情', '<%=basePath%>/webpage/desktop/hospLeaderDetail.jsp',800,450);
		    }
		</script>
	</head>
	<body style="margin:0px;padding:0px;">
		<div style="margin:0px;padding:0px;">
			<div id="leftDiv" style="width: 59%;height: 100%;float: left;text-align: center;margin:0px;padding:0px;">
				<div id="leftDiv1" style="width: 100%;overflow:auto;float: left; text-align: center;margin:0px;padding:0px;">
					<div id="datagridDiv1"></div>
					<div id="toolbarDiv1">
						<a type="button" class="easyui-linkbutton" data-options="iconCls:'icon-putout',plain:true" onclick="detailAudit()">详情导出</a>
					</div>
					<div style="padding:5px;text-align:left;">总待确认任务：<span style="padding-left:10px;padding-right:10px;">130条</span>3453.00元；已确认任务：<span style="padding-left:10px;padding-right:10px;">145条</span>5320.00元</div>
				</div>
				<div id="leftDiv2" style="width: 100%; overflow:auto;float: left; text-align: center;margin:0px;padding:0px;">
					<div id="datagridDiv3"></div>
					<div id="toolbarDiv3">
						<a type="button" class="easyui-linkbutton" data-options="iconCls:'icon-putout',plain:true">导出</a>
						<a type="button" class="easyui-linkbutton" data-options="iconCls:'icon-print',plain:true">打印</a>
					</div>
					<div style="padding:5px;text-align:left;">总复议任务：<span style="padding-left:10px;padding-right:10px;">30条</span>2320.00元</div>
				</div>
				<div id="leftDiv3" style="width: 100%; overflow:auto;height:48%; float: left; text-align: center;margin-top:2px;padding:0px;">
					<div id="datagridDiv2"></div>
					<div id="toolbarDiv2">
						<a type="button" class="easyui-linkbutton" data-options="iconCls:'icon-putout',plain:true">导出</a>
						<a type="button" class="easyui-linkbutton" data-options="iconCls:'icon-print',plain:true">打印</a>
					</div>
				</div>
			</div>
			<div id="rigthDiv" style="width: 41%;height: 100%;overflow:hidden; float: left; text-align: center;margin:0px;padding:0px;background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-x;">
				<div id="rightDiv1" style="width: 100%; height:50%; float: left; text-align: center;margin:0px;padding:0px;border:1px solid #95B8E7;"></div>
				<div id="rightDiv2" style="width: 100%; height:50%; float: left; text-align: center;margin:0px;padding:0px;border:1px solid #95B8E7;"></div>
			</div>
		</div>
	</body>
</html>
