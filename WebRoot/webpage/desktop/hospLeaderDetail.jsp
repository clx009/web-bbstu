<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!-- context path -->
<head>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" />
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/jquery/jquery-1.8.3.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/tools/dataformat.js"></script>
		<link id="easyuiTheme" rel="stylesheet"
			href="<%=basePath%>/plug-in/easyui/themes/default/easyui.css"
			type="text/css"></link>
		<link rel="stylesheet"
			href="<%=basePath%>/plug-in/easyui/themes/icon.css" type="text/css"></link>

		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>/plug-in/accordion/css/accordion.css"></link>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/easyui/jquery.easyui.min.1.3.2.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/easyui/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/tools/syUtil.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>


		<script type="text/javascript"
			src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/highcharts.src.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/modules/exporting.src.js"></script>
		<script type="text/javascript">
		$(function () {
	        $('#datagridDiv').datagrid({  
				title:'详情',
				method:'get', 
			    url:'<%=basePath%>/webpage/desktop/datagrid_taskinf.json',   
			    singleSelect:true,
			    columns:[[   
			    	{field:'RQ',title:'日期'},
			    	{field:'YLJG',title:'医疗机构'}, 
			        {field:'KS',title:'科室'},   
			        {field:'DSMZSL',title:'待确认门诊数量'},
			        {field:'DSMZJE',title:'待确认门诊金额'},
			        {field:'YSMZSL',title:'已确认门诊数量'},
			        {field:'YSMZJE',title:'已确认门诊金额'},
			        {field:'DSZYSL',title:'待确认住院数量'},
			        {field:'DSZYJE',title:'待确认住院金额'},
			        {field:'YSZYSL',title:'已确认住院数量'},
			        {field:'YSZYJE',title:'已确认住院金额'}
			    ]]   
			}); 
			
			$('#datagridDiv').datagrid({
				toolbar: '#toolbarDiv'
			});
	    });
		</script>
	</head>
	<body style="margin:0px;padding:0px;">
		<div style="margin:0px;padding:0px;">
			<div id="centerDiv" style="width: 798px;overflow:hidden; height: 430px; float: left;border:0px solid #95B8E7; text-align: center;margin:0px;padding:0px;">
				<div id="datagridDiv" ></div>
			</div>
		</div>
		<div id="toolbarDiv">
			时间起：<input style="width:80px;"/>
			时间止：<input style="width:80px;"/>
			<a type="button" class="easyui-linkbutton">查询</a>
			<a type="button" class="easyui-linkbutton" data-options="iconCls:'icon-putout',plain:true">导出</a>
			<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-print',plain:true" onclick="">打印</a>
		</div>
	</body>
</html>