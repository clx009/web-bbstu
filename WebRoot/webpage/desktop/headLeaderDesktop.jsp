<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!-- context path -->
<head>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" />
		<script type="text/javascript" src="<%=basePath%>/plug-in/jquery/jquery-1.8.3.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/dataformat.js"></script>
		<link id="easyuiTheme" rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/default/easyui.css" type="text/css"></link>
		<link rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/icon.css" type="text/css"></link>
		<link rel="stylesheet" type="text/css" href="<%=basePath%>/plug-in/accordion/css/accordion.css">
		<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/jquery.easyui.min.1.3.2.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/syUtil.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>
		<link rel="stylesheet" href="<%=basePath%>/plug-in/tools/css/common.css" type="text/css"></link>
		<script type="text/javascript" src="<%=basePath%>/plug-in/lhgDialog/lhgdialog.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/curdtools.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/easyuiextend.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/highcharts.src.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/modules/exporting.src.js"></script>
		<script type="text/javascript">
		function addOneTab(subtitle, url, icon) {
			if (icon == '') {
				icon = 'icon folder';
			}
			window.top.$.messager.progress({
				text : '页面加载中....',
				interval : 300
			});
			window.top.$('#maintabs').tabs({
				onClose : function(subtitle, index) {
					window.top.$.messager.progress('close');
				}
			});
			if (window.top.$('#maintabs').tabs('exists', subtitle)) {
				window.top.$('#maintabs').tabs('select', subtitle);
				window.top.$('#maintabs').tabs('update', {
					tab : window.top.$('#maintabs').tabs('getSelected'),
					options : {
						title : subtitle,
						href : url,
						// content : '<iframe name="tabiframe" scrolling="no"
						// frameborder="0" src="' + url + '"
						// style="width:100%;height:99%;"></iframe>',
						closable : true,
						icon : icon
					}
				});
			} else {
				if (url.indexOf('isIframe') != -1) {
					window.top
							.$('#maintabs')
							.tabs(
									'add',
									{
										title : subtitle,
										content : '<iframe src="'
												+ url
												+ '" frameborder="0" style="border:0;width:100%;height:99.4%;"></iframe>',
										closable : true,
										icon : icon
									});
				} else {
					window.top.$('#maintabs').tabs('add', {
						title : subtitle,
						href : url,
						closable : true,
						icon : icon
					});
				}
			}
		}
		function preSummarySheetForHeadLeader() {
	    	window.top.addTab('经办负责人待审汇总单','preSummarySheetController.do?preSummarySheetForHeadLeader','pictures');
	    }
	    function preSummarySheetForHeadLeaderHadAudit() {
	    	window.top.addTab('经办负责人已审汇总单','preSummarySheetController.do?preSummarySheetForHeadLeaderHadAudit','pictures');
	    }
	    
		$(function () {
	        
	         $('#rightDiv1').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        credits : {
					enabled : false
				},
		        title: {
		            text: '当日汇总单人工审批任务状态图' 
		        },
		        tooltip: {
		    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}% （{point.y}条）</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    connectorColor: '#000000',
		                    format: '<b>{point.name}</b>: {point.percentage:.1f}% （{point.y}条）'
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: '处方',
		            data: [
		            	{name:'已审批',y:157,color:'#95CEFF'},{name:'待审批',y:122,color:'#FFBC75'}
		            ]
		        }]
		    });
	        
	        $('#rightDiv2').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        credits : {
					enabled : false
				},
		        title: {
		            text: '当日汇总单人工已审批状态图'
		        },
		        tooltip: {
		    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}% （{point.y}条）</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    connectorColor: '#000000',
		                    format: '<b>{point.name}</b>: {point.percentage:.1f}% （{point.y}条）'
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: '处方',
		            data: [
		                {name:'正常',y:87,color:'#A9FF96'},{name:'违规',y:70,color:'#999EFF'}
		            ]
		        }]
		    });
	        
	        $('#rightDiv3').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        credits : {
					enabled : false
				},
		        title: {
		            text: '自动审核处方状态图'
		        },
		        tooltip: {
		    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}% （{point.y}条）</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    connectorColor: '#000000',
		                    format: '<b>{point.name}</b>: {point.percentage:.1f}% （{point.y}条）'
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: '处方',
		            data: [
		            	{name:'正常',y:1120,color:'#A9FF96'},{name:'违规',y:633,color:'#999EFF'}
		            ]
		        }]
		    });
	        
	        
	        $('#datagridDiv2').datagrid({  
				title:'当日日志',
				method:'get',
			    url:'<%=basePath%>/webpage/desktop/datagrid_worklog.json',   
			    singleSelect:true,
			    columns:[[   
			    	{field:'userName',title:'操作人',width:fixWidth(0.14)},
			        {field:'logcontent',title:'日志内容',width:fixWidth(0.29)},   
			        {field:'operatetime',title:'操作时间',width:fixWidth(0.15)}
			    ]]   
			}); 
			
			$('#datagridDiv2').datagrid({
				toolbar: '#toolbarDiv2'
			});
			$('#datagridDiv1').datagrid({ 
				title:'审批任务',
				method:'get', 
			    url:'<%=basePath%>/webpage/desktop/datagrid_task_headLeader.json',   
			    singleSelect:true,
			    columns:[[   
			    	{field:'',title:'',width:fixWidth(0.06)},
			    	{field:'',title:'门诊',colspan:2}, 
			        {field:'',title:'住院',colspan:2}  
			    ],[   
			    	{field:'SHQK',title:'审批情况',width:fixWidth(0.08)},
			    	{field:'MZSL',title:'数量',width:fixWidth(0.1)}, 
			        {field:'MZJE',title:'金额',width:fixWidth(0.15)},
			        {field:'ZYSL',title:'数量',width:fixWidth(0.1)}, 
			        {field:'ZYJE',title:'金额',width:fixWidth(0.15)}  
			    ]]   
			}); 
			$('#datagridDiv31').datagrid({ 
				title:'医疗机构申诉',
				method:'get', 
			    url:'<%=basePath%>/webpage/desktop/datagrid_complain.json',   
			    singleSelect:true,
			    columns:[[   
			    	{field:'',title:'',width:fixWidth(0.06)},
			    	{field:'',title:'门诊申诉',colspan:2}, 
			        {field:'',title:'住院申诉',colspan:2}  
			    ],[   
			    	{field:'SHQK',title:'复审情况',width:fixWidth(0.08)},
			    	{field:'MZSL',title:'数量',width:fixWidth(0.1)}, 
			        {field:'MZJE',title:'金额',width:fixWidth(0.15)},
			        {field:'ZYSL',title:'数量',width:fixWidth(0.1)}, 
			        {field:'ZYJE',title:'金额',width:fixWidth(0.15)}  
			    ]]   
			}); 
			
			$(" div[class='panel-header'] ").each(function(){
				$(this).removeAttr("style"); 
			});
			
			$("div[class='datagrid-wrap panel-body']").each(function(){
				 $(this).removeAttr("style"); 
			});
			
			
			$('#datagridDiv1').datagrid({
				toolbar: '#toolbarDiv1'
			});
			$('#datagridDiv31').datagrid({
				toolbar: '#toolbarDiv3'
			});
			
	    });
	    
	    function detailAudit() {
			createwindow('审核任务详情', '',800,500);
	    }
		</script>
	</head>
	<body style="margin:0px;padding:0px;">
		<div style="margin:0px;padding:0px;">
			<div id="leftDiv" style="width: 60%; height: 100%; float: left;background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-x; text-align: center;margin:0px;padding:0px;">
				<div id="leftDiv1" style="width: 100%;overflow:scroll;height: 30%; float: left;background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-x; text-align: center;margin:0px;padding:0px;">
					<div id="datagridDiv1"></div>
					<div style="padding:5px;text-align:left;">总待审批任务：<span style="padding-left:10px;padding-right:10px;">122条</span>3453.00元</div>
				</div>
				<div id="leftDiv3" style="width: 100%;overflow:scroll;height: 30%; float: left;background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-x; text-align: center;margin:0px;padding:0px;">
					<div id="datagridDiv31"></div>
					<div style="padding:5px;text-align:left;">专家意见<span style="padding-left:10px;padding-right:10px;">门诊：<a href='' style='color:green;'>100条</a> 金额：20万  住院：<a href='' style='color:green;'>80条</a> 金额：15万</span></div>
				</div>
				<div id="leftDiv2" style="overflow:scroll;width: 100%; height: 40%;float: left;background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-x;text-align: center;margin:0px;padding:0px;">
					<div id="datagridDiv2"></div>
				</div>
			</div>
			<div id="rigthDiv" style="width: 40%;overflow:hidden; height: 100%; float: left;background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-x; text-align: center;margin:0px;padding:0px;">
				<div id="rightDiv1" style="width: 100%; height: 50%; overflow:hidden;float: left;padding-top:1px;background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-x;text-align: center;margin:0px;">
				</div>
				<div id="rightDiv2" style="width: 100%; height: 50%;padding-top:1px;overflow:hidden;float: left;background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-x; text-align: center;margin:0px;">
				</div>
			</div>
		</div>
		<div id="toolbarDiv2">
			<a type="button" class="easyui-linkbutton" data-options="iconCls:'icon-putout',plain:true">导出</a>
			<a type="button" class="easyui-linkbutton" data-options="iconCls:'icon-print',plain:true">打印</a>
		</div>
		<div id="toolbarDiv1">
			<a type="button" class="easyui-linkbutton" data-options="iconCls:'icon-putout',plain:true" onclick="">详情导出</a>
		</div>
		<div id="toolbarDiv3">
			<a type="button" class="easyui-linkbutton" data-options="iconCls:'icon-putout',plain:true" onclick="">详情导出</a>
		</div>
	</body>
</html>