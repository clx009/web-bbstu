<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!-- context path -->
<head>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" />
		<script type="text/javascript" src="<%=basePath%>/plug-in/jquery/jquery-1.8.3.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/dataformat.js"></script>
		<link id="easyuiTheme" rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/default/easyui.css" type="text/css"></link>
		<link rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/icon.css" type="text/css"></link>
		<link rel="stylesheet" type="text/css" href="<%=basePath%>/plug-in/accordion/css/accordion.css">
		<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/jquery.easyui.min.1.3.2.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/syUtil.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>
		<link rel="stylesheet" href="<%=basePath%>/plug-in/tools/css/common.css" type="text/css"></link>
		<script type="text/javascript" src="<%=basePath%>/plug-in/lhgDialog/lhgdialog.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/curdtools.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/easyuiextend.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/highcharts.src.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/modules/exporting.src.js"></script>
		<script type="text/javascript">
		$(function () {
	        
	         $('#rightDiv1').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        title: {
		            text: '当日任务状态图'
		        },
		        tooltip: {
		    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}% （{point.y}条）</b>'
		        },
		        credits : {
					enabled : false
				},
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    connectorColor: '#000000',
		                    format: '<b>{point.name}</b>: {point.percentage:.1f}% （{point.y}条）'
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: '处方',
		            data: [
		            	{name:'已审核',y:157,color:'#95CEFF'},{name:'待审核',y:122,color:'#FFBC75'}
		            ]
		        }]
		    });
	        
	        $('#rightDiv2').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        title: {
		            text: '当日已审核处方状态图'
		        },
		        tooltip: {
		    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}% （{point.y}条）</b>'
		        },
		        credits : {
					enabled : false
				},
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    connectorColor: '#000000',
		                    format: '<b>{point.name}</b>: {point.percentage:.1f}% （{point.y}条）'
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: '处方',
		            data: [
		                {name:'正常',y:87,color:'#A9FF96'},{name:'违规',y:70,color:'#999EFF'}
		            ]
		        }]
		    });
	        
	        $('#rightDiv3').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        title: {
		            text: '系统审核处方状态图'
		        },
		        tooltip: {
		    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}% （{point.y}条）</b>'
		        },
		        credits : {
					enabled : false
				},
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    connectorColor: '#000000',
		                    format: '<b>{point.name}</b>: {point.percentage:.1f}% （{point.y}条）'
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: '处方',
		            data: [
		            	{name:'正常',y:1120,color:'#A9FF96'},{name:'违规',y:633,color:'#999EFF'}
		            ]
		        }]
		    });
	        
	        
	        $('#datagridDiv2').datagrid({  
				title:'当日日志',
				method:'get', 
				fit:true,
			    url:'<%=basePath%>/webpage/desktop/datagrid_worklog.json',   
			    singleSelect:true,
			    columns:[[   
			    	{field:'userName',title:'操作人',width:fixWidth(0.13)},
			        {field:'logcontent',title:'日志内容',width:fixWidth(0.3)},   
			        {field:'operatetime',title:'操作时间',width:fixWidth(0.15)}
			    ]]   
			}); 
			
			$('#datagridDiv2').datagrid({
				toolbar: '#toolbarDiv2'
			});
			
			$('#datagridDiv1').datagrid({ 
				title:'审核任务',
				method:'get', 
			    url:'<%=basePath%>/webpage/desktop/datagrid_task.json',   
			    singleSelect:true,
			    columns:[[   
			    	{field:'',title:'',width:fixWidth(0.08)},
			    	{field:'',title:'门诊',colspan:2}, 
			        {field:'',title:'住院',colspan:2}  
			    ],[   
			    	{field:'SHQK',title:'审核情况',width:fixWidth(0.08)},
			    	{field:'MZSL',title:'数量',width:fixWidth(0.1)}, 
			        {field:'MZJE',title:'金额',width:fixWidth(0.15)},
			        {field:'ZYSL',title:'数量',width:fixWidth(0.1)}, 
			        {field:'ZYJE',title:'金额',width:fixWidth(0.15)}  
			    ]]   
			}); 
			
			$('#datagridDiv1').datagrid({
				toolbar: '#toolbarDiv1'
			});
	    });
	    
	    function detailAudit() {
			createwindow('审核任务详情', '<%=basePath%>/webpage/desktop/firstauditDeskDetail.jsp',800,450);
	    }
		</script>
	</head>
	<body style="margin:0px;padding:0px;">
		<div style="margin:0px;padding:0px;">
			<div id="leftDiv" style="width: 59%; height: 100%; float: left;text-align: center;margin:0px;padding:0px;">
				<div id="leftDiv1" style="width: 99%;overflow:auto;height: 39%; float: left; text-align: center;margin:0px;padding:0px;">
					<div id="datagridDiv1"></div>
					<div style="padding:5px;text-align:left;">总待审任务：<span style="padding-left:10px;padding-right:10px;">122条</span>3453.00元</div>
				</div>
				<div id="leftDiv2" style="width: 100%;overflow:hidden; height: 60%; float: left; text-align: center;margin:0px;padding:0px;">
					<div id="datagridDiv2"></div>
				</div>
			</div>
			<div id="rigthDiv" style="width: 41%;overflow:hidden; height: 100%; float: left; text-align: center;margin:0px;padding:0px;background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-x;">
				<div id="rightDiv1" style="width: 100%; height: 33%; float: left; text-align: center;margin:0px;padding:0px;border:1px solid #95B8E7;">
				</div>
				<div id="rightDiv2" style="width: 100%; height: 33%; float: left; text-align: center;margin:0px;padding:0px;border:1px solid #95B8E7;">
				</div>
				<div id="rightDiv3" style="width: 100%; height: 33%; float: left; text-align: center;margin:0px;padding:0px;border:1px solid #95B8E7;">
				</div>
			</div>
		</div>
		<div id="toolbarDiv2">
			<a type="button" class="easyui-linkbutton" data-options="iconCls:'icon-putout',plain:true">导出</a>
			<a type="button" class="easyui-linkbutton" data-options="iconCls:'icon-print',plain:true">打印</a>
		</div>
		<div id="toolbarDiv1">
			<a type="button" class="easyui-linkbutton" data-options="iconCls:'icon-putout',plain:true" onclick="detailAudit()">详情导出</a>
		</div>
	</body>
</html>