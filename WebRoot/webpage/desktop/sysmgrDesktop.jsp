<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!-- context path -->
<head>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" />
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/jquery/jquery-1.8.3.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/tools/dataformat.js"></script>
		<link id="easyuiTheme" rel="stylesheet"
			href="<%=basePath%>/plug-in/easyui/themes/default/easyui.css"
			type="text/css"></link>
		<link rel="stylesheet"
			href="<%=basePath%>/plug-in/easyui/themes/icon.css" type="text/css"></link>

		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>/plug-in/accordion/css/accordion.css"></link>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/easyui/jquery.easyui.min.1.3.2.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/easyui/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/tools/syUtil.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>


		<script type="text/javascript"
			src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/highcharts.src.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/modules/exporting.src.js"></script>
		<script type="text/javascript">
		$(function () {
	        $('#leftDiv1').highcharts({
	            title: {
	                text: '24小时用户数',
	                x: -20 //center
	            },
	            xAxis: {
	            	title: {
	                    text: '时间'
	                },
	                categories: ['0点','2点', '4点', '6点', '8点', '10点', '12点',
	                    '14点', '16点', '18点', '20点', '22点']
	            },
	            yAxis: {
	                title: {
	                    text: '用户数'
	                },
	                plotLines: [{
	                    value: 0,
	                    width: 1,
	                    color: '#808080'
	                }]
	            },
	             tooltip: {
	                valueSuffix: '个'
	            },
	            credits : {
					enabled : false
				},
	            series: [{
	                name: '用户数',
	                data: [5,6,8,10,15,22,24,26,22,18,15,10]
	            }]
	        });
	        
	        $('#leftDiv2').highcharts({
	            title: {
	                text: '系统故障监控',
	                x: -20 //center
	            },
	            xAxis: {
	            	title: {
	                    text: '时间'
	                },
	                categories: ['0点','2点', '4点', '6点', '8点', '10点', '12点',
	                    '14点', '16点', '18点', '20点', '22点']
	            },
	            yAxis: {
	                title: {
	                    text: '响应时间'
	                },
	                plotLines: [{
	                    value: 0,
	                    width: 1,
	                    color: '#808080'
	                }]
	            },
	             tooltip: {
	                valueSuffix: '个'
	            },
	            credits : {
					enabled : false
				},
	            series: [{
	                name: '响应时间',
	                data: [5,6,8,10,15,22,24,0,0,0,0,0]
	            }]
	        });
	        
	        $('#leftDiv3').highcharts({
	            title: {
	                text: 'CPU占用率监控',
	                x: -20 //center
	            },
	            xAxis: {
	            	title: {
	                    text: '时间'
	                },
	                categories: ['0点','2点', '4点', '6点', '8点', '10点', '12点',
	                    '14点', '16点', '18点', '20点', '22点']
	            },
	            yAxis: {
	                title: {
	                    text: 'CPU占用率(%)'
	                },
	                plotLines: [{
	                    value: 0,
	                    width: 1,
	                    color: '#808080'
	                }]
	            },
	             tooltip: {
	                valueSuffix: '%'
	            },
	            credits : {
					enabled : false
				},
	            series: [{
	                name: 'CPU占用率',
	                data: [5,6,8,10,15,22,24,50,45,21,5,2]
	            }]
	        });
	        
	        $('#leftDiv4').highcharts({
	            title: {
	                text: '内存占用率监控',
	                x: -20 //center
	            },
	            xAxis: {
	            	title: {
	                    text: '时间'
	                },
	                categories: ['0点','2点', '4点', '6点', '8点', '10点', '12点',
	                    '14点', '16点', '18点', '20点', '22点']
	            },
	            yAxis: {
	                title: {
	                    text: '内存占用率(%)'
	                },
	                plotLines: [{
	                    value: 0,
	                    width: 1,
	                    color: '#808080'
	                }]
	            },
	             tooltip: {
	                valueSuffix: '%'
	            },
	            credits : {
					enabled : false
				},
	            series: [{
	                name: '内存占用率',
	                data: [15,16,18,16,15,22,24,55,35,24,15,2]
	            }]
	        });
	        
	        $('#datagridDiv').datagrid({  
				title:'系统日志',
				method:'get', 
			    url:'<%=basePath%>/webpage/desktop/datagrid_sysLog.json',   
			    singleSelect:true,
			    columns:[[   
			    	{field:'userName',title:'操作人',width:fixWidth(0.04)},
			    	{field:'note',title:'操作人IP',width:fixWidth(0.08)}, 
			        {field:'logcontent',title:'日志内容',width:fixWidth(0.15)},   
			        {field:'operatetime',title:'操作时间',width:fixWidth(0.15)}
			    ]]   
			}); 
			
			$('#datagridDiv').datagrid({
				toolbar: '#toolbarDiv'
			});
	    });
		</script>
		<style>
			#highcharts-4{border-left:#95B8E7 1px solid;}
			#highcharts-12{border-left:#95B8E7 1px solid;}
		</style>
	</head>
	<body style="margin:0px;padding:0px;">
		<div style="margin:0px;padding:0px;">
			<div id="leftDiv" style="width: 60%; height: 100%; float: left; text-align: center;margin:0px;padding:0px;">
				<div id="leftDiv1" style="width: 50%; height: 50%; float: left; text-align: center;margin:0px;padding:0px;">
				</div>
				<div id="leftDiv2" style="width: 50%; height: 50%; float: left; background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-y;text-align: center;margin:0px;padding-left:0px;">
				</div>
				<div id="leftDiv3" style="width: 50%; height: 50%; float: left;background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-x;text-align: center;margin:0px;padding-top:1px;">
				</div>
				<div id="leftDiv4" style="width: 50%; height: 50%; float: left;background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-x; text-align: center;margin:0px;padding-top:1px;">
				</div>
			</div>
			<div id="rigthDiv" style="width: 40%;overflow:hidden; height: 100%; float: left; text-align: center;margin:0px;padding:0px;">
				<div id="datagridDiv"></div>
			</div>
		</div>
		<div id="toolbarDiv">
			操作人：<input style="width:80px;"/>
			时间起：<input style="width:80px;"/>
			时间止：<input style="width:80px;"/>
			<a type="button" class="easyui-linkbutton">查询</a>
		</div>
	</body>
</html>