<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>t_s_shsj</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body>
 <t:formvalid formid="formobj" layout="div" dialog="true" beforeSubmit="upload">
		<fieldset class="step">
		    <div class="form">
		      <label class="Validform_label">姓名:</label>
		      	<input id="userid" name="userid" type="hidden">
		     	 <input name="userid" type="text" style="width: 150px" class="inputxt"  
					               >
		      <span class="Validform_checktip"></span>
		    </div>
		    <div class="form">
				<label class="Validform_label">类型:</label> <select
					id="type" name="type">
					<option value="1">学术成就</option>
					<option value="2">社会实践</option>
					<option value="3">拓展训练</option>
					<option value="4">实习经历</option>
					<option value="5">音体美才能</option>
				</select> <span class="Validform_checktip"></span>
			</div>
			<div class="form">
		      <label class="Validform_label">名称:</label>
		     	 <input id="name" name="name" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">描述:</label>
		     	 <input id="description" name="description" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">参与开始时间:</label>
		      	<input id="createtime" type="hidden">
		     	 <input name="createtime" type="text" style="width: 150px"> 
		      <span class="Validform_checktip"></span>
		    </div>
		    <div class="form">
		      <label class="Validform_label">参与结束时间:</label>
		      		      	<input id="endtime" type="hidden">
		     	 <input name="endtime" type="text" style="width: 150px"> 
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">成果奖项:</label>
		     	 <input id="result" name="result" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
		    
		     <div class="form" id="filediv"></div>
			<div class="form"><t:upload name="file_upload" uploader="tSShsjController.do?doAdd" extend="office" id="file_upload" formData="id,userid,type,name,description,createtime,endtime,result"></t:upload></div>
	    </fieldset>
  </t:formvalid>
 </body>
  <script src = "webpage/shijian/tSShsj/tSShsj.js"></script>		