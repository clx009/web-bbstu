$(function() {
	$("input[name='userid']").combobox({
		url : 'userController.do?studentCombobox',
		valueField : 'id',
		textField : 'realname',
		mode : 'remote',
		onSelect:function(record){ 
			if(record&&record.id)
				$('#formobj').find('#userid').val(record.id); 
		}
	});
	
	$('#formobj').find('input[name="createtime"]').datebox({
		/*formatter: function(date){ return date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate();},
	    parser: function(date){ return new Date(Date.parse(date.replace(/-/g,"/")));},*/
		onSelect: function(date){ 
			var mydate = new Date(date);
			$('#formobj').find('#createtime').val(mydate.getFullYear()+'-'+(mydate.getMonth()+1)+'-'+mydate.getDate());
		}
	});
	
	$('#formobj').find('input[name="endtime"]').datebox({
		onSelect: function(date){ 
			var mydate = new Date(date);
			$('#formobj').find('#endtime').val(mydate.getFullYear()+'-'+(mydate.getMonth()+1)+'-'+mydate.getDate()); 
		}
	});
}); 


function browseImages(inputId, Img) {// 图片管理器，可多个上传共用
		var finder = new CKFinder();
		finder.selectActionFunction = function(fileUrl, data) {//设置文件被选中时的函数 
			$("#" + Img).attr("src", fileUrl);
			$("#" + inputId).attr("value", fileUrl);
		};
		finder.resourceType = 'Images';// 指定ckfinder只为图片进行管理
		finder.selectActionData = inputId; //接收地址的input ID
		finder.removePlugins = 'help';// 移除帮助(只有英文)
		finder.defaultLanguage = 'zh-cn';
		finder.popup();
	}
function browseFiles(inputId, file) {// 文件管理器，可多个上传共用
	var finder = new CKFinder();
	finder.selectActionFunction = function(fileUrl, data) {//设置文件被选中时的函数 
		$("#" + file).attr("href", fileUrl);
		$("#" + inputId).attr("value", fileUrl);
		decode(fileUrl, file);
	};
	finder.resourceType = 'Files';// 指定ckfinder只为文件进行管理
	finder.selectActionData = inputId; //接收地址的input ID
	finder.removePlugins = 'help';// 移除帮助(只有英文)
	finder.defaultLanguage = 'zh-cn';
	finder.popup();
}
function decode(value, id) {//value传入值,id接受值
	var last = value.lastIndexOf("/");
	var filename = value.substring(last + 1, value.length);
	$("#" + id).text(decodeURIComponent(filename));
}