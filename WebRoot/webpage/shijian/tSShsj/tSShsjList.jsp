<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="tSShsjList" checkbox="true" fitColumns="false" title="社会实践" actionUrl="tSShsjController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="id"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="姓名" replace="${userReplace}" field="userId"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="类型" replace="学术成就_1,社会实践_2,拓展训练_3,实习经历_4,音体美才能_5" field="type"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="名称"  field="name"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="描述"  field="description"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="参与开始时间"  field="createtime"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="参与结束时间"  field="endtime"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="成果奖项"  field="result"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="附件"  field="path"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="tSShsjController.do?doDel&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="tSShsjController.do?goAdd" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="tSShsjController.do?goUpdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="批量删除"  icon="icon-remove" url="tSShsjController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="tSShsjController.do?goUpdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/shijian/tSShsj/tSShsjList.js"></script>		
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
 });
 </script>