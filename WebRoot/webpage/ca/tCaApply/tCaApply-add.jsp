<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>t_ca_apply</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="tCaApplyController.do?doAdd" tiptype="3">
				<input id="id" name="id" type="hidden" value="${tCaApplyPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">申请人:</label>
		     	 <input id="userId" name="userId" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">类型: 1-家庭作业辅导,2-课程辅导,3-伴学导师:</label>
		     	 <input id="type" name="type" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">科目:</label>
		     	 <input id="subejctId" name="subejctId" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">处理状态:0-待处理,1-已处理:</label>
		     	 <input id="status" name="status" type="text" style="width: 150px" class="inputxt"  
					               datatype="*">
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">申请时间:</label>
		     	 <input id="createTime" name="createTime" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
				>
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">描述:</label>
		     	 <input id="comments" name="comments" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </t:formvalid>
 </body>
  <script src = "webpage/ca/tCaApply/tCaApply.js"></script>		