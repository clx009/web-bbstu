<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>t_ca_apply</title>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
</head>
<body>
	<t:formvalid formid="formobj" dialog="true" usePlugin="password"
		layout="div" action="tCaApplyController.do?doHandle" tiptype="3">
		<input id="id" name="id" type="hidden" value="${tCaApplyPage.id }">
		<fieldset class="step">
			<div class="form">
				<label class="Validform_label">申请人:</label> <input
					id="applicant1.nickname" name="applicant1.nickname" type="text"
					style="width: 150px" class="inputxt" datatype="*"
					value='${tCaApplyPage.applicant.nickname}' disabled="disabled">
				<span class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">类型:</label> <select id="type"
					name="type" disabled="disabled">
					<option value="1"
						<c:if test="${tCaApplyPage.type==1}">selected="selected"</c:if>>作业辅导</option>
					<option value="2"
						<c:if test="${tCaApplyPage.type==2}">selected="selected"</c:if>>课程辅导</option>
					<option value="3"
						<c:if test="${tCaApplyPage.type==3}">selected="selected"</c:if>>伴学导师</option>
					<option value="4"
						<c:if test="${tCaApplyPage.type==4}">selected="selected"</c:if>>热线电话</option>
					<option value="5"
						<c:if test="${tCaApplyPage.type==5}">selected="selected"</c:if>>紧急求助</option>
				</select> <span class="Validform_checktip"></span>
			</div>

			<c:if test="${!empty tCaApplyPage.subject}">
				<div class="form">
					<label class="Validform_label">科目:</label> <input
						id="subject1.chineseName" name="subject1.chineseName" type="text"
						style="width: 150px" class="inputxt" datatype="*"
						value='${tCaApplyPage.subject.chineseName}' disabled="disabled">
					<span class="Validform_checktip"></span>
				</div>
			</c:if>

			<div class="form">
				<label class="Validform_label">申请时间:</label> <input id="createTime"
					name="createTime" type="text" style="width: 150px" class="inputxt"
					datatype="*" value='${tCaApplyPage.createTime}' disabled="disabled">
				<span class="Validform_checktip"></span>
			</div>
			<c:if test="${!empty tCaApplyPage.comments}">
				<div class="form">
					<label class="Validform_label">描述:</label>
					<textarea rows="3" cols="80" id="comments" name="comments"
						type="text" style="width: 300px" class="inputxt" height="200px"
						autofocus="true" wrap="soft" datatype="*" disabled="disabled">${tCaApplyPage.comments} </textarea>
					<span class="Validform_checktip"></span>
				</div>
			</c:if>
			<div class="form">
				<label class="Validform_label">处理结果:</label>
				<textarea rows="3" cols="80" id="results" name="results" type="text"
					style="width: 300px" class="inputxt" height="200px"
					autofocus="true" wrap="soft" datatype="*">${tCaApplyPage.results}</textarea>
				<span class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label"></label> <select id="status"
					name="status">
					<option value="1"
						<c:if test="${tCaApplyPage.status==1}">selected="selected"</c:if>>已处理</option>
					<option value="0"
						<c:if test="${tCaApplyPage.status==0}">selected="selected"</c:if>>待处理</option>
				</select><input type="submit" value="确认处理"> <span
					class="Validform_checktip"></span>
			</div>
		</fieldset>
	</t:formvalid>
</body>
<script src="webpage/ca/tCaApply/tCaApply.js"></script>