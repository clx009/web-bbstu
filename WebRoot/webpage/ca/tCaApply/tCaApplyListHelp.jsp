<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="tCaApplyListHelp" checkbox="true" fitColumns="true" title="辅导申请列表" actionUrl="tCaApplyController.do?datagridhelp" idField="id" fit="true" queryMode="group">
   <t:dgCol title="id"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="申请人"  field="applicant.nickname"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="类型" replace="家庭作业辅导_1,课程辅导_2,伴学导师_3,热线电话_4,紧急求助_5"  field="type"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
      <t:dgCol title="求助地址"  field="addressdesc"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="处理状态" replace="待处理_0,已处理_1"  field="status"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="处理结果"  field="results"  hidden="true" queryMode="group"  width="200"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
  <%--  <t:dgDelOpt title="删除" url="tCaApplyController.do?doDel&id={id}" /> --%>
  <%--  <t:dgToolBar title="录入" icon="icon-add" url="tCaApplyController.do?goAdd" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="tCaApplyController.do?goUpdate" funname="update"></t:dgToolBar>
    --%>
   <t:dgToolBar title="删除"  icon="icon-remove" url="tCaApplyController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
   <t:dgOpenOpt title="处理" url="tCaApplyController.do?goHandle&id={id}" openModel="OpenWin" width="800" height="400" />
   <t:dgToolBar title="查看" icon="icon-search" url="tCaApplyController.do?goUpdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/ca/tCaApply/tCaApplyList.js"></script>		
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
 }); 
 </script>