<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" fit="true" style="padding:1px;">
	<div id="preSummarySheetList1" fit="true"></div>
  </div>
 </div>
 <div id="tb">
 	<div id="preSummarySheetList2_toolbar">
 	 医疗机构名称：<input type="text"/>
  	月份：<input id="month" name="month" type="text" style="width: 150px" 
						      						class="Wdate" onClick="WdatePicker()"
									               datatype="*" 
						      						 value='<fmt:formatDate value='${preSummarySheetPage.month}' type="date" pattern="yyyy-MM"/>'> 
  	<a class="easyui-linkbutton">查询</a><br/>
 	<a class="easyui-linkbutton" plain="true">导出</a>
 	<a class="easyui-linkbutton" plain="true">打印</a>
 </div>
 <script src = "webpage/audit/preSummarySheet/preSummarySheetList.js"></script>		
 <script type="text/javascript" src="<%=basePath %>/webpage/audit/preSummarySheet/WdatePicker.js"></script>	
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
		$("#preSummarySheetList1tb").find("input[name='month_begin']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
		$("#preSummarySheetList1tb").find("input[name='month_end']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});

		$('#preSummarySheetList1').datagrid({  
		title:'按医院科室汇总表',
		toolbar:'#tb',
		method:'get',
	    url:'<%=basePath%>/webpage/audit/preSummarySheet/datagrid_preSummarySheetList_data.json',   
	    singleSelect:true,
	    columns:[[   
	    	{field:'medicalCode',title:'医疗机构编码',width:fixWidth(0.05)},
	        {field:'medicalName',title:'医疗机构名称',width:fixWidth(0.1)},   
	        {field:'departmentCode',title:'科室编码',width:fixWidth(0.05)},   
	        {field:'department',title:'科室名称',width:fixWidth(0.05)},   
	        {field:'month',title:'月份',width:fixWidth(0.05)},
	        {field:'gcExpedientCost',title:'普通门诊合理费用',width:fixWidth(0.1)},
	        {field:'gcInexpedientCost',title:'普通门诊不合理费用',width:fixWidth(0.1)},
	        {field:'scdExpedientCost',title:'慢性病重症合理费用',width:fixWidth(0.1)},
	        {field:'scdInexpedientCost',title:'慢性病重症不合理费用',width:fixWidth(0.1)},
	        {field:'hospitalExpedientCost',title:'住院合理费用',width:fixWidth(0.1)},
	        {field:'hospitalInexpedientCost',title:'住院不合理费用',width:fixWidth(0.1)},
	        {field:'expedientCost',title:'总合理金额',width:fixWidth(0.05)},
	        {field:'inexpedientCost',title:'总不合理金额',width:fixWidth(0.05)}
	    ]]   
	}); 
});
 </script> 
