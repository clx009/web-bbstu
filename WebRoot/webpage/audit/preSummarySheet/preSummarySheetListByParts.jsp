<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!-- context path -->
<head>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" />
		<script type="text/javascript" src="<%=basePath%>/plug-in/jquery/jquery-1.8.3.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/dataformat.js"></script>
		<link id="easyuiTheme" rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/default/easyui.css" type="text/css"></link>
		<link rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/icon.css" type="text/css"></link>
		<link rel="stylesheet" type="text/css" href="<%=basePath%>/plug-in/accordion/css/accordion.css">
		<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/jquery.easyui.min.1.3.2.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/syUtil.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>
		<link rel="stylesheet" href="<%=basePath%>/plug-in/tools/css/common.css" type="text/css"></link>
		<script type="text/javascript" src="<%=basePath%>/plug-in/lhgDialog/lhgdialog.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/curdtools.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/easyuiextend.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/highcharts.src.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/modules/exporting.src.js"></script>
		<script type="text/javascript">
		$(function () {
	         $('#rightDiv1').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        credits : {
					enabled : false
				},
		        title: {
		            text: '合理费用图' 
		        },
		        tooltip: {
		    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}% （{point.y}条）</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    connectorColor: '#000000',
		                    format: '<b>{point.name}</b>: {point.percentage:.1f}% （{point.y}条）'
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: '处方',
		            data: [
		            	{name:'普通门诊',y:157},{name:'慢性病重症',y:122},{name:'住院',y:90}
		            ]
		        }]
		    });
			    
	         $('#rightDiv2').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        credits : {
					enabled : false
				},
		        title: {
		            text: '不合理费用图' 
		        },
		        tooltip: {
		    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}% （{point.y}条）</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    connectorColor: '#000000',
		                    format: '<b>{point.name}</b>: {point.percentage:.1f}% （{point.y}条）'
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: '处方',
		            data: [
		            	{name:'普通门诊',y:121},{name:'慢性病重症',y:64},{name:'住院',y:45}
		            ]
		        }]
		    });
			    
	         $('#rightDiv3').highcharts({
		        chart: {
		            plotBackgroundColor: null,
		            plotBorderWidth: null,
		            plotShadow: false
		        },
		        credits : {
					enabled : false
				},
		        title: {
		            text: '合理/不合理费用比例图' 
		        },
		        tooltip: {
		    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}% （{point.y}条）</b>'
		        },
		        plotOptions: {
		            pie: {
		                allowPointSelect: true,
		                cursor: 'pointer',
		                dataLabels: {
		                    enabled: true,
		                    color: '#000000',
		                    connectorColor: '#000000',
		                    format: '<b>{point.name}</b>: {point.percentage:.1f}% （{point.y}条）'
		                }
		            }
		        },
		        series: [{
		            type: 'pie',
		            name: '处方',
		            data: [
		            	{name:'合理',y:10200},{name:'不合理',y:15000}
		            ]
		        }]
		    });
			    
	        $('#leftDiv11').datagrid({  
				title:'合理费用汇总表',
				toolbar:'#tb1',
				method:'get',
			    url:'<%=basePath%>/webpage/audit/preSummarySheet/datagrid_preSummarySheetList_data.json',   
			    singleSelect:true,
			    showFooter: true,
			    columns:[[   
			    	{field:'medicalCode',title:'医疗机构编码',width:fixWidth(0.07)},
			        {field:'medicalName',title:'医疗机构名称',width:fixWidth(0.1)},   
			        {field:'month',title:'月份',width:fixWidth(0.05)},
			        {field:'gcExpedientCost',title:'普通门诊合理费用',width:fixWidth(0.1)},
			        {field:'scdExpedientCost',title:'慢性病重症合理费用',width:fixWidth(0.1)},
			        {field:'hospitalExpedientCost',title:'住院合理费用',width:fixWidth(0.1)},
			        {field:'expedientCost',title:'总合理金额',width:fixWidth(0.06)}
			    ]]   
			}); 
			
	        $('#leftDiv21').datagrid({  
				title:'不合理费用汇总表',
				toolbar:'#tb2',
				method:'get',
			    url:'<%=basePath%>/webpage/audit/preSummarySheet/datagrid_preSummarySheetList_data.json',   
			    singleSelect:true,
			    showFooter: true,
			    columns:[[   
			    	{field:'medicalCode',title:'医疗机构编码',width:fixWidth(0.07)},
			        {field:'medicalName',title:'医疗机构名称',width:fixWidth(0.1)},   
			        {field:'month',title:'月份',width:fixWidth(0.05)},
			        {field:'gcInexpedientCost',title:'普通门诊不合理费用',width:fixWidth(0.1)},
			        {field:'scdInexpedientCost',title:'慢性病重症不合理费用',width:fixWidth(0.1)},
			        {field:'hospitalInexpedientCost',title:'住院不合理费用',width:fixWidth(0.1)},
			        {field:'inexpedientCost',title:'总不合理金额',width:fixWidth(0.06)}
			    ]]   
			}); 
			

			$("div[class='datagrid-view']").each(function(){
				$(this).removeAttr("style");
				$(this).css({"height":"500"});
			});
	    });
		</script>
		<style>
			#highcharts-0{border-top:#95B8E7 1px solid;}
			#highcharts-2{border-top:#95B8E7 1px solid;}
		</style>
	</head>
	<body style="margin:0px;padding:0px;" fit="true">
		<div style="margin:0px;padding:0px;"  fit="true">
			<div id="leftDiv"  fit="true" style="width: 60%;overflow:hidden; height: 100%; background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-y;float: left; text-align: center;margin:0px;">
				<div id="leftDiv1"  fit="true" style="width: 100%; height: 50%; overflow:hidden;float: left;text-align: center;margin:0px;">
					<div id="leftDiv11"  fit="true"></div>
				</div>
				<div id="leftDiv2"  fit="true" style="width: 100%; height: 50%;overflow:hidden;float: left;background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-x; text-align: center;margin:0px;">
					<div id="leftDiv21"  fit="true"></div>
				</div>
			</div>
			<div id="rightDiv" style="width: 40%; height: 100%; float: left; text-align: center;margin:0px;padding:0px;">
				<div id="rightDiv3"  fit="true" style="width: 100%; height: 33%; overflow:hidden;float: left;text-align: center;margin:0px;">
				</div>
				<div id="rightDiv1"  fit="true" style="width: 100%; height: 33%; overflow:hidden;float: left;text-align: center;margin:0px;">
				</div>
				<div id="rightDiv2"  fit="true" style="width: 100%; height: 34%;overflow:hidden;float: left;background:url(<%=basePath%>/plug-in/easyui/themes/default/images/borderBg.png) repeat-x; text-align: center;margin:0px;">
				</div>
			</div>
		</div>
		 <div id="tb1">
		 	<a class="easyui-linkbutton" plain="true">导出</a>
		 	<a class="easyui-linkbutton" plain="true">打印</a>
		 </div>
		 <div id="tb2">
		 	<a class="easyui-linkbutton" plain="true">导出</a>
		 	<a class="easyui-linkbutton" plain="true">打印</a>
		 </div>
	</body>
</html>