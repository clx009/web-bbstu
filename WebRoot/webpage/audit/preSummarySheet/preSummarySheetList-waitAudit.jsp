<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="preSummarySheetList" checkbox="true" fitColumns="false" title="汇总支付信息" 

actionUrl="preSummarySheetController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="id"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="月"  field="month" formatter="yyyyMM" hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="医疗机构代码"  field="medicalCode"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="医疗机构名称"  field="medicalName"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="普通门诊总费用"  field="gcTotalCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="普通门诊合理费用"  field="gcExpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="普通门诊不合理费用"  field="gcInexpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="慢性病重症总费用"  field="scdTotalCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="慢性病重症合理费用"  field="scdExpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="慢性病重症不合理费用"  field="scdInexpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="住院总费用"  field="hospitalTotalCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="住院合理费用"  field="hospitalExpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="住院不合理费用"  field="hospitalInexpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="总金额"  field="totalCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="总合理金额"  field="expedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="总不合理金额"  field="inexpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="状态"  field="status"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgToolBar title="审批" icon="icon-edit" url="preSummarySheetController.do?goAudit" funname="update" ></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/audit/preSummarySheet/preSummarySheetList.js"></script>		
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
 			$("#preSummarySheetListtb").find("input[name='month_begin']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
 			$("#preSummarySheetListtb").find("input[name='month_end']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
 });
 </script>
