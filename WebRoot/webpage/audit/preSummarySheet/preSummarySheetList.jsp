<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<html>
<head>
	<c:set var="ctxPath" value="${pageContext.request.contextPath}" />
	<script type="text/javascript" src="<%=basePath%>/plug-in/jquery/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/tools/dataformat.js"></script>
	<link id="easyuiTheme" rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/default/easyui.css" type="text/css"></link>
	<link rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/icon.css" type="text/css"></link>
	<link rel="stylesheet" type="text/css" href="<%=basePath%>/plug-in/accordion/css/accordion.css">
	<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/jquery.easyui.min.1.3.2.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/tools/syUtil.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>
	<link rel="stylesheet" href="<%=basePath%>/plug-in/tools/css/common.css" type="text/css"></link>
	<script type="text/javascript" src="<%=basePath%>/plug-in/lhgDialog/lhgdialog.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/tools/curdtools.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/tools/easyuiextend.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/highcharts.src.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/modules/exporting.src.js"></script>
	<script type="text/javascript" src = "<%=basePath%>/webpage/audit/preSummarySheet/preSummarySheetList.js"></script>
	<script type="text/javascript">
	function addOneTab(subtitle, url, icon) {
		if (icon == '') {
			icon = 'icon folder';
		}
		window.top.$.messager.progress({
			text : '页面加载中....',
			interval : 300
		});
		window.top.$('#maintabs').tabs({
			onClose : function(subtitle, index) {
				window.top.$.messager.progress('close');
			}
		});
		if (window.top.$('#maintabs').tabs('exists', subtitle)) {
			window.top.$('#maintabs').tabs('select', subtitle);
			window.top.$('#maintabs').tabs('update', {
				tab : window.top.$('#maintabs').tabs('getSelected'),
				options : {
					title : subtitle,
					href : url,
					// content : '<iframe name="tabiframe" scrolling="no"
					// frameborder="0" src="' + url + '"
					// style="width:100%;height:99%;"></iframe>',
					closable : true,
					icon : icon
				}
			});
		} else {
			if (url.indexOf('isIframe') != -1) {
				window.top
						.$('#maintabs')
						.tabs(
								'add',
								{
									title : subtitle,
									content : '<iframe src="'
											+ url
											+ '" frameborder="0" style="border:0;width:100%;height:99.4%;"></iframe>',
									closable : true,
									icon : icon
								});
			} else {
				window.top.$('#maintabs').tabs('add', {
					title : subtitle,
					href : url,
					closable : true,
					icon : icon
				});
			}
		}
	}
	</script>
</head>


<body>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="preSummarySheetList" checkbox="true" fitColumns="false" title="汇总支付信息" actionUrl="/bp/preSummarySheetController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="id"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="月"  field="month" formatter="yyyyMM" hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="医疗机构代码"  field="medicalCode"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="医疗机构名称"  field="medicalName"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="总金额"  field="totalCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="总合理金额"  field="expedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="总不合理金额"  field="inexpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>  
   <t:dgCol title="普通门诊总金额"  field="gcTotalCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="普通门诊合理金额"  field="gcExpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="普通门诊不合理金额"  field="gcInexpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="慢性病重症总金额"  field="scdTotalCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="慢性病重症合理金额"  field="scdExpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="慢性病重症不合理金额"  field="scdInexpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="住院总金额"  field="hospitalTotalCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="住院合理金额"  field="hospitalExpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="住院不合理金额"  field="hospitalInexpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <%--<t:dgCol title="状态"  field="status"  hidden="true"  queryMode="group"  width="120"></t:dgCol>--%>
   <t:dgToolBar title="提交审批" icon="icon-ok" url="preSummarySheetController.do?goUpdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="查看合理不合理金额" icon="icon-search"  funname="showParts"></t:dgToolBar>
   <t:dgToolBar title="查看医院科室汇总单" icon="icon-search"  funname="showDepartment"></t:dgToolBar>
   <t:dgToolBar title="导出" icon="icon-putout"  funname=""></t:dgToolBar>
   <t:dgToolBar title="打印" icon="icon-print" funname=""></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>		
 <script type="text/javascript">

 function showParts(){
	 window.top.addTab('合理不合理金额','preSummarySheetController.do?preSummarySheetListByParts&isIframe','pictures');
 }
 function showDepartment(){
	 window.top.addTab('按医院科室汇总单','preSummarySheetController.do?preSummarySheetListByDepartment','pictures');
 }
 $(document).ready(function(){
 		//给时间控件加上样式
		$("#preSummarySheetListtb").find("input[name='month_begin']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
		$("#preSummarySheetListtb").find("input[name='month_end']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
		$("#preSummarySheetListtb .datagrid-toolbar").prepend($("#preSummarySheetList10_toolbar"));
		$("#preSummarySheetListtb .datagrid-toolbar").height(50);
});
 </script> 
 
   <div id="preSummarySheetList10_toolbar">
  医疗机构名称：<input type="text"/>
  	月份：<input id="month" name="month" type="text" style="width: 150px" 
						      						class="Wdate" onClick="WdatePicker()"
									               datatype="*" 
						      						 value='<fmt:formatDate value='${preSummarySheetPage.month}' type="date" pattern="yyyy-MM"/>'> 
  	<a class="easyui-linkbutton">查询</a>
  	</div>
 </body>
</html>