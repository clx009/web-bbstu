<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>汇总支付信息</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
  <script type="text/javascript" src="plug-in/ckeditor_new/ckeditor.js"></script>
  <script type="text/javascript" src="plug-in/ckfinder/ckfinder.js"></script>
  <script type="text/javascript" src="plug-in/ckfinder/ckfinder.js"></script>
  <script type="text/javascript" src="<%=basePath %>/webpage/audit/preSummarySheet/WdatePicker.js"></script>
  <script type="text/javascript">
  	function closeDialog(){
  		$.messager.confirm('提示框', '系统已记录您修改的内容，确定提交审批吗?',function(){});
  		return false;
  	}
  //编写自定义JS代码
  </script>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" beforeSubmit="closeDialog" action="preSummarySheetController.do?doUpdate" tiptype="1">
					<input id="id" name="id" type="hidden" value="${preSummarySheetPage.id }">
		<table style="width: 600px;" cellpadding="0" cellspacing="1" class="formtable">
					<tr>
						<td align="right">
							<label class="Validform_label">
								月1:
							</label>
						</td>
						<td class="value">
									  <input id="month" name="month" type="text" style="width: 150px" 
						      						class="Wdate" onClick="WdatePicker()"
									               datatype="*" 
						      						 value='<fmt:formatDate value='${preSummarySheetPage.month}' type="date" pattern="yyyy-MM"/>'>    
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">月</label>
						</td>
						<td align="right">
							<label class="Validform_label">
								医疗机构代码:
							</label>
						</td>
						<td class="value">
						     	${preSummarySheetPage.medicalCode}
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">医疗机构代码</label>
						</td>
					</tr>
					<tr>
						<td align="right">
							<label class="Validform_label">
								医疗机构名称:
							</label>
						</td>
						<td class="value">
						     ${preSummarySheetPage.medicalName}
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">医疗机构名称</label>
						</td>
						<td align="right">
							<label class="Validform_label">
								总金额:
							</label>
						</td>
						<td class="value">
						     	 <input id="totalCost" name="totalCost" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${preSummarySheetPage.totalCost}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">总金额</label>
						</td>
					</tr>
					<tr>
						<td align="right">
							<label class="Validform_label">
								总合理金额:
							</label>
						</td>
						<td class="value">
						     	 <input id="expedientCost" name="expedientCost" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${preSummarySheetPage.expedientCost}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">总合理金额</label>
						</td>
						<td align="right">
							<label class="Validform_label">
								总不合理金额:
							</label>
						</td>
						<td class="value">
						     	 <input id="inexpedientCost" name="inexpedientCost" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${preSummarySheetPage.inexpedientCost}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">总不合理金额</label>
						</td>
					</tr>
					<tr>
						<td align="right">
							<label class="Validform_label">
								普通门诊总费用:
							</label>
						</td>
						<td class="value">
						     	 <input id="gcTotalCost" name="gcTotalCost" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${preSummarySheetPage.gcTotalCost}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">普通门诊总费用</label>
						</td>
						<td align="right">
							<label class="Validform_label">
								普通门诊合理费用:
							</label>
						</td>
						<td class="value">
						     	 <input id="gcExpedientCost" name="gcExpedientCost" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${preSummarySheetPage.gcExpedientCost}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">普通门诊合理费用</label>
						</td>
						
					</tr>
					
					<tr>
						<td align="right">
							<label class="Validform_label">
								普通门诊不合理费用:
							</label>
						</td>
						<td class="value">
						     	 <input id="gcInexpedientCost" name="gcInexpedientCost" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${preSummarySheetPage.gcInexpedientCost}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">普通门诊不合理费用</label>
						</td>
						<td align="right">
							<label class="Validform_label">
								慢性病重症总费用:
							</label>
						</td>
						<td class="value">
						     	 <input id="scdTotalCost" name="scdTotalCost" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${preSummarySheetPage.scdTotalCost}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">慢性病重症总费用</label>
						</td>
					</tr>
					<tr>
						<td align="right">
							<label class="Validform_label">
								慢性病重症合理费用:
							</label>
						</td>
						<td class="value">
						     	 <input id="scdExpedientCost" name="scdExpedientCost" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${preSummarySheetPage.scdExpedientCost}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">慢性病重症合理费用</label>
						</td>
						<td align="right">
							<label class="Validform_label">
								慢性病重症不合理费用:
							</label>
						</td>
						<td class="value">
						     	 <input id="scdInexpedientCost" name="scdInexpedientCost" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${preSummarySheetPage.scdInexpedientCost}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">慢性病重症不合理费用</label>
						</td>
					</tr>
					<tr>
						<td align="right">
							<label class="Validform_label">
								住院总费用:
							</label>
						</td>
						<td class="value">
						     	 <input id="hospitalTotalCost" name="hospitalTotalCost" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${preSummarySheetPage.hospitalTotalCost}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">住院总费用</label>
						</td>
						<td align="right">
							<label class="Validform_label">
								住院合理费用:
							</label>
						</td>
						<td class="value">
						     	 <input id="hospitalExpedientCost" name="hospitalExpedientCost" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${preSummarySheetPage.hospitalExpedientCost}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">住院合理费用</label>
						</td>
					</tr>
					<tr>
						<td align="right">
							<label class="Validform_label">
								住院不合理费用:
							</label>
						</td>
						<td class="value">
						     	 <input id="hospitalInexpedientCost" name="hospitalInexpedientCost" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${preSummarySheetPage.hospitalInexpedientCost}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">住院不合理费用</label>
						</td>
						<td align="right">
							<label class="Validform_label">
							</label>
						</td>
						<td class="value">
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;"></label>
						</td>
						<%--
						<td align="right">
							<label class="Validform_label">
								状态:
							</label>
						</td>
						<td class="value">
						     ${preSummarySheetPage.status}
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">状态</label>
						</td>
						--%>
					</tr>
			</table>
		</t:formvalid>
 </body>
  <script src = "webpage/audit/preSummarySheet/preSummarySheet.js"></script>		