<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" fit="true" style="padding:1px;">
  	<div id="preSummarySheetList3" fit="true"></div>
  </div>
 </div>
  <div id="preSummarySheetList3_toolbar">
  医疗机构名称：<input type="text"/>
  	月份：<input id="month" name="month" type="text" style="width: 150px" 
						      						class="Wdate" onClick="WdatePicker()"
									               datatype="*" 
						      						 value='<fmt:formatDate value='${preSummarySheetPage.month}' type="date" pattern="yyyy-MM"/>'> 
  	<a class="easyui-linkbutton">查询</a><br/>
  	<a class="easyui-linkbutton" icon="icon-ok"  data-options="iconCls:'',plain:true" onclick="batchAudit();">同意</a>
  	<a class="easyui-linkbutton" icon="icon-no"  data-options="iconCls:'',plain:true" onclick="batchAudit();">返回重审</a>
  	<a class="easyui-linkbutton"  icon="icon-search"  data-options="iconCls:'',plain:true" href="javascript:void(0);" onclick="showParts();">查看合理不合理费用</a>
  	<a class="easyui-linkbutton"  icon="icon-search"  data-options="iconCls:'',plain:true" href="javascript:void(0);" onclick="showDepartment();">查看医院科室汇总单</a>
  	<a class="easyui-linkbutton" icon="icon-putout"  plain="true">导出</a>
	<a class="easyui-linkbutton" icon="icon-print"  plain="true">打印</a>
  </div>
  <script type="text/javascript" src="<%=basePath %>/webpage/audit/preSummarySheet/WdatePicker.js"></script>
 <script src = "webpage/audit/preSummarySheet/preSummarySheetList.js"></script>		
 <script type="text/javascript">
 function batchAudit(){
	 createwindowWithStaging('审核', '<%=basePath%>/webpage/audit/preSummarySheet/bacthAuditWin.jsp',600,250);
 }
 function showParts(){
	 addTab('合理不合理费用','preSummarySheetController.do?preSummarySheetListByParts&isIframe','pictures');
 }
 function showDepartment(){
	 addTab('按医院科室汇总单','preSummarySheetController.do?preSummarySheetListByDepartment','pictures');
 }
 function view(value,rowData,rowIndex) {  
		return "<input type=\"text\" style=\"width:"+fixWidth(0.09)+"px;\"/>";
 }
 function showParts(){
	 addTab('合理不合理费用','preSummarySheetController.do?preSummarySheetListByParts&isIframe','pictures');
 }
 function showDepartment(){
	 addTab('按医院科室汇总单','preSummarySheetController.do?preSummarySheetListByDepartment','pictures');
 }
 $(document).ready(function(){
 		//给时间控件加上样式
 			$("#preSummarySheetList3tb").find("input[name='month_begin']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
 			$("#preSummarySheetList3tb").find("input[name='month_end']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
 			$('#preSummarySheetList3').datagrid({  
 				title:'按医院汇总表',
 				method:'get',
 			    url:'<%=basePath%>/webpage/audit/preSummarySheet/datagrid_preSummarySheetList_data.json',   
 			    rownumbers:true,
 			    columns:[[   
			    	{field:'',width:100},
 			    	{field:'',title:'',width:100},
 			        {field:'',title:'',width:100},   
 			        {field:'',title:'',width:100},
			    	{field:'',title:'全部',colspan:3}, 
			        {field:'',title:'普通门诊',colspan:3},  
			        {field:'',title:'慢性病重症',colspan:3},  
			        {field:'',title:'住院',colspan:3},  
			        {field:'',title:'',width:100},
			        {field:'',title:'',width:100},
 			        {field:'',title:'',width:100}   
			    ],[   
					{field:'ck',checkbox:true,width:100},
 			    	{field:'medicalCode',title:'医疗机构编码',width:100},
 			        {field:'medicalName',title:'医疗机构名称',width:100},   
 			        {field:'month',title:'月份',width:100},
 			        {field:'totalCost',title:'总金额',width:100},
 			        {field:'expedientCost',title:'总合理金额',width:100},
			        {field:'inexpedientCost',title:'总不合理金额',width:100},
 			        {field:'gcTotalCost',title:'总金额',width:100},
 			        {field:'gcExpedientCost',title:'合理金额',width:110},
 			        {field:'gcInexpedientCost',title:'不合理金额',width:120},
 			        {field:'scdTotalCost',title:'总金额',width:110},
 			        {field:'scdExpedientCost',title:'合理金额',width:120},
 			        {field:'scdInexpedientCost',title:'不合理金额',width:130},
 			        {field:'hospitalTotalCost',title:'总金额',width:100},
 			        {field:'hospitalExpedientCost',title:'合理金额',width:100},
 			        {field:'hospitalInexpedientCost',title:'不合理金额',width:100},
 			        {field:'leaderStatus',title:'分管领导状态',width:100},
 			        {field:'approvalOpinion',title:'审批人员意见',width:100},
 			        {field:'leaderOpinion',title:'分管领导意见',width:100}
 			    ]]   
 			});

 			$('#preSummarySheetList3').datagrid({
 				toolbar: '#preSummarySheetList3_toolbar'
 			});
 });
 </script> 
