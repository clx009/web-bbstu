<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="preSummarySheetList1" checkbox="true" fitColumns="false" title="汇总支付信息" actionUrl="preSummarySheetController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="id"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="月"  field="month" formatter="yyyyMM" hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="医疗机构代码"  field="medicalCode"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="医疗机构名称"  field="medicalName"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="总金额"  field="totalCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="总合理金额"  field="expedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="总不合理金额"  field="inexpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="普通门诊总金额"  field="gcTotalCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="普通门诊合理金额"  field="gcExpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="普通门诊不合理金额"  field="gcInexpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="慢性病重症总金额"  field="scdTotalCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="慢性病重症合理金额"  field="scdExpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="慢性病重症不合理金额"  field="scdInexpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="住院总金额"  field="hospitalTotalCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="住院合理金额"  field="hospitalExpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="住院不合理金额"  field="hospitalInexpedientCost"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <%--<t:dgCol title="状态"  field="status"  hidden="true" queryMode="group"  width="120"></t:dgCol>--%>
  <t:dgToolBar title="查看合理不合理金额" icon="icon-search"  funname="showParts"></t:dgToolBar>
   <t:dgToolBar title="查看医院科室汇总单" icon="icon-search"  funname="showDepartment"></t:dgToolBar>
   <t:dgToolBar title="导出" icon="icon-putout"  funname=""></t:dgToolBar>
   <t:dgToolBar title="打印" icon="icon-print" funname=""></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
  <div id="preSummarySheetList11_toolbar">
  医疗机构名称：<input type="text"/>
  	月份：<input id="month" name="month" type="text" style="width: 150px" 
						      						class="Wdate" onClick="WdatePicker()"
									               datatype="*" 
						      						 value='<fmt:formatDate value='${preSummarySheetPage.month}' type="date" pattern="yyyy-MM"/>'> 
  	<a class="easyui-linkbutton">查询</a>
  	</div>
 <script src = "webpage/audit/preSummarySheet/preSummarySheetList.js"></script>		
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
 			$("#preSummarySheetList1tb").find("input[name='month_begin']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
 			$("#preSummarySheetList1tb").find("input[name='month_end']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
 			$("#preSummarySheetList1tb .datagrid-toolbar").prepend($("#preSummarySheetList11_toolbar"));
			$("#preSummarySheetList1tb .datagrid-toolbar").height(50);
 });
 </script>
