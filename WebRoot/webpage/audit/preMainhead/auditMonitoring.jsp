<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<style>
#JZXXMonitor table
 {
     border-collapse: collapse;
     border: none;
 }
#JZXXMonitor td
 {
     border: solid RGB(149,183,231) 1px;
     width:100px;
 }
.propMonitor{
	text-align: right;
	background-color: #E0ECFF;
}
.topPropMonitor{
	text-align: left;
	background-color: #E0ECFF;
}
#DZBLMonitor table
 {
     border-collapse: collapse;
     border: none;
 }
#DZBLMonitor td
 {
     border: solid RGB(149,183,231) 1px;
 }
</style>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="auditMonitor" checkbox="true" onSelect="showRules" fitColumns="false" title="处方信息" actionUrl="preMainheadController.do?datagridMonitor" idField="id" fit="true" queryMode="group">
   <t:dgCol title="编号"  field="id"  hidden="false" ></t:dgCol>
   <t:dgCol title="处方日期"  field="presTime" formatter="yyyy-MM-dd" hidden="true"  queryMode="group" query="true" width="90"></t:dgCol>
   <t:dgCol title="参保人"  field="patName"  hidden="true" query="true" width="70"></t:dgCol>
   <t:dgCol title="参保人编号" field="patCode"></t:dgCol>
   <t:dgCol title="参保人类型" field="cbType" dictionary="cbtype"></t:dgCol>
   <t:dgCol title="性别"  field="patSex"  hidden="true" width="40" dictionary="sex"></t:dgCol>
   <t:dgCol title="年龄"  field="patAge"  hidden="true" width="40"></t:dgCol>
   <t:dgCol title="处方号"  field="sxh"  hidden="false"   query="true"></t:dgCol>
   <t:dgCol title="门诊诊断"  field="mzDisname"  hidden="true" query="true"></t:dgCol>
   <t:dgCol title="（慢）特病种" hidden="false" field="peopType" query="true"></t:dgCol>
   <t:dgCol title="发生金额"  field="presMoney"  hidden="true" width="70" queryMode="group" query="true"></t:dgCol>
   
   <t:dgCol title="审核结果" field="csUid" width="100"></t:dgCol>
   <t:dgCol title="审核意见" field="csUid" width="100"></t:dgCol>
   
   <t:dgCol title="医疗机构" field="insName"></t:dgCol>
   <t:dgCol title="医疗机构级别" field="csUid" width="100"></t:dgCol>
   <t:dgCol title="科室" field="deptName"></t:dgCol>
   <t:dgCol title="医师" field="docName"></t:dgCol>
   <t:dgCol title="医师级别" field="fsUid"></t:dgCol>
   
   <t:dgCol title="医疗机构" field="PreMainheadEntity_id" hidden="false" query="true" replace="${medicalsReplace}"></t:dgCol>
   <t:dgCol title="科室" field="BaseLocalDeptEntity_id" hidden="false" query="true" replace="${deptsReplace}"></t:dgCol>
   <t:dgCol title="医师" field="BaseDoctorEntity_id" hidden="false" query="true" replace="${doctorsReplace}"></t:dgCol>
   
  </t:datagrid>
  </div>
 </div>

<div region="south" split="true" id="southMonitor">
 	<div class="easyui-layout" fit="true">   
		<div class="easyui-tabs" style="padding:0px;" region="center">   
		    <div title="就诊信息" style="padding:10px;overflow:auto;">   
		        <div id="JZXXMonitor">
		        	<table>
		        		<tr>
		        			<th colspan="8" style="font-weight: bold;text-align: left;">医疗机构信息</th>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">机构编号：</td>
		        			<td>100001</td>
		        			<td class="propMonitor">机构名称：</td>
		        			<td>沧州市中心医院</td>
		        			<td class="propMonitor">机构级别：</td>
		        			<td>三甲医院</td>
		        			<td class="propMonitor">科室编号：</td>
		        			<td>101</td>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">科室名称：</td>
		        			<td>内科</td>
		        			<td class="propMonitor">医师编号：</td>
		        			<td>201010</td>
		        			<td class="propMonitor">医师姓名：</td>
		        			<td>胡歌</td>
		        			<td class="propMonitor">医师级别：</td>
		        			<td>主任医师</td>
		        		</tr>
		        	</table>
		        	<br/>
		        	<table>
		        		<tr>
		        			<th colspan="8" style="font-weight: bold;text-align: left;">参保人信息</th>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">参保人编号：</td>
		        			<td>100001</td>
		        			<td class="propMonitor">姓名：</td>
		        			<td>李载安</td>
		        			<td class="propMonitor">性别：</td>
		        			<td>男</td>
		        			<td class="propMonitor">年龄：</td>
		        			<td>55</td>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">人员类别：</td>
		        			<td>普通参保人</td>
		        			<td class="propMonitor">参保类型：</td>
		        			<td>城镇职工社会保险</td>
		        			<td class="propMonitor"></td>
		        			<td></td>
		        			<td class="propMonitor"></td>
		        			<td></td>
		        		</tr>
		        	</table>
		        	<br/>
		        	<table>
		        		<tr>
		        			<th colspan="8" style="font-weight: bold;text-align: left;">处方信息</th>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">处方号：</td>
		        			<td>10000111</td>
		        			<td class="propMonitor">主要诊断：</td>
		        			<td>糖尿病</td>
		        			<td class="propMonitor">处方日期：</td>
		        			<td>2013-08-01</td>
		        			<td class="propMonitor">处方来源：</td>
		        			<td>门诊</td>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">总金额（元）：</td>
		        			<td>386</td>
		        			<td class="propMonitor">医保内金额：</td>
		        			<td>350</td>
		        			<td class="propMonitor"></td>
		        			<td></td>
		        			<td class="propMonitor"></td>
		        			<td></td>
		        		</tr>
		        	</table>
		        	<br/>
		        	<table>
		        		<tr>
		        			<th colspan="8" style="font-weight: bold;border: 0px;text-align: left;">审核信息</th>
		        		</tr>
		        		<tr>
		        			<td class="topPropMonitor">审核环节</td>
		        			<td class="topPropMonitor">审核结果</td>
		        			<td class="topPropMonitor">审核时间</td>
		        			<td class="topPropMonitor">审核者</td>
		        		</tr>
		        		<tr>
		        			<td>系统审核</td>
		        			<td>违规</td>
		        			<td>2014-04-16</td>
		        			<td>系统自动</td>
		        		</tr>
		        		<tr>
		        			<td>人工初审</td>
		        			<td>违规</td>
		        			<td>2014-04-17</td>
		        			<td>王晓丽</td>
		        		</tr>
		        		<tr>
		        			<td>人工复核</td>
		        			<td>违规</td>
		        			<td>2014-04-18</td>
		        			<td>马小刚</td>
		        		</tr>
		        	</table>
		        	<p style="color: red;">系统提示：门诊&nbsp;&nbsp;初审&nbsp;&nbsp;初审人员：王晓丽&nbsp;&nbsp;2014-04-17</p>
		        </div>
		    </div>   
		    <div title="费用明细"  style="overflow:auto;padding:0px;">   
				<table id="feeMonitor" class="easyui-datagrid"
						data-options="
							iconCls: 'icon-edit',
							singleSelect: true,
							toolbar: '#tbMonitor',
							url: 'webpage/audit/preMainhead/datagrid_fee.json',
							method: 'get',
							rownumbers:true,
							fit:true
						">
					<thead>
						<tr>
							<th data-options="field:'itemName',width:80">项目名称</th>   
							<th data-options="field:'itemType',width:60">项目类别</th>   
				            <th data-options="field:'ruleName',width:80">违反规则</th>   
				            <th data-options="field:'gg',width:80">规格</th>  
				            <th data-options="field:'dw',width:40">单位</th>  
				            <th data-options="field:'dj',width:40">单价</th>  
				            <th data-options="field:'sl',width:40">数量</th>  
				            <th data-options="field:'je',width:80">金额</th>  
				            <th data-options="field:'yf',width:80">用法</th>  
				            <th data-options="field:'yl',width:40">用量</th>  
				            <th data-options="field:'tj',width:60">给药途径</th>  
				            <th data-options="field:'ybnje',width:80">医保内金额</th>  
				            <th data-options="field:'jfSl',width:60">拒付数量</th>
				            <th data-options="field:'jfJe',width:60">拒付金额</th>  
							<th data-options="field:'jfLy',width:100">拒付理由</th>
						</tr>
					</thead>
				</table>
				<div id="tbMonitor" style="height:auto">
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-putout',plain:true" onclick="">导出</a>
				</div>
		    </div>  
		    <div title="电子病历" style="padding:10px;overflow:auto;">   
		        <div id="DZBLMonitor">
		        	<p align="center" style="font-size:16px;font-weight:bold;">【李载安】个人电子病历</p>
		        	<table width="99%" align="center">
		        		<tr>
		        			<td width="180" class="propMonitor">诊断医院：</td>
		        			<td>沧州市中心医院</td>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">初诊时间：</td>
		        			<td>2013-07-01 11:40</td>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">就诊科别：</td>
		        			<td>急诊</td>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">主诉：</td>
		        			<td>腹部疼痛一小时</td>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">现病历：</td>
		        			<td></td>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">既往病历：</td>
		        			<td></td>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">体格检查：</td>
		        			<td></td>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">辅助检查结果：</td>
		        			<td></td>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">初步诊断：</td>
		        			<td></td>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">处理意见：</td>
		        			<td></td>
		        		</tr>
		        		<tr>
		        			<td class="propMonitor">主治医生：</td>
		        			<td></td>
		        		</tr>
		        	</table>
		        	
		        </div>
		    </div> 
		    <div title="检查报告" style="padding:10px;overflow:auto;">   
		        <div id="JCBGMonitor">
		        </div>
		    </div>
		    <div title="公务员查体" style="padding:10px;overflow:auto;">   
		        <div id="GWYCTMonitor">
		        </div>
		    </div>
		    <div title="现金报销明细" style="padding:10px;overflow:auto;">   
		        <div id="XJBXMXMonitor">
		        </div>
		    </div>
		    <div title="医疗档案" style="padding:10px;overflow:auto;">   
		        <div id="YLDAMonitor">
		        </div>
		    </div> 
		</div> 
	</div>
</div>

 <script type="text/javascript">
 $(document).ready(function(){
 		$("#southMonitor").height(fixHeight(0.3));
 
		$("#auditMonitortb").find("input[name='presTime_begin']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
		$("#auditMonitortb").find("input[name='presTime_end']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
 		//给时间控件加上样式
		/*$("#feeMonitor").datagrid({
			showFooter : true,
  			onLoadSuccess : function() {
      			$('#feeMonitor').datagrid('statistics');
  			}
		});*/
 });
 function showRules(rowIndex,rowData) {
}
 </script>
