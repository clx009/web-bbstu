<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="auditZyfsys" checkbox="true" onSelect="showZyDetail" fitColumns="false" title="处方信息" actionUrl="preMainheadController.do?datagridZyfsys" idField="id" fit="true" queryMode="group">
   <t:dgCol title="编号"  field="id"  hidden="false" ></t:dgCol>
   <t:dgCol title="处方日期"  field="presTime" formatter="yyyy-MM-dd" hidden="true"  queryMode="group" query="true" width="90"></t:dgCol>
   <t:dgCol title="参保人"  field="patName"  hidden="true" query="true" width="70"></t:dgCol>
   <t:dgCol title="参保人编号" field="patCode"></t:dgCol>
   <t:dgCol title="参保人类型" field="cbType" dictionary="cbtype"></t:dgCol>
   <t:dgCol title="性别"  field="patSex"  hidden="true" width="40" dictionary="sex"></t:dgCol>
   <t:dgCol title="年龄"  field="patAge"  hidden="true" width="40"></t:dgCol>
   <t:dgCol title="处方号"  field="sxh"  hidden="false"   query="true"></t:dgCol>
   <t:dgCol title="入院诊断"  field="ryDisname"  hidden="true" query="true" width="120"></t:dgCol>
   <t:dgCol title="入院时间"  field="ryTime" formatter="yyyy-MM-dd" hidden="true"  queryMode="group" query="true" width="100"></t:dgCol>
   <t:dgCol title="出院时间"  field="cyTime" formatter="yyyy-MM-dd"  hidden="true" width="120"></t:dgCol>
   <t:dgCol title="发生金额"  field="presMoney"  hidden="true" width="70" queryMode="group" query="true"></t:dgCol>

   <t:dgCol title="初审意见" field="csUid"></t:dgCol>
   <t:dgCol title="复审意见" field="csUid"></t:dgCol>
   <t:dgCol title="申诉意见" field="csUid"></t:dgCol>
   <t:dgCol title="专家意见" field="csUid"></t:dgCol>
   <t:dgCol title="专家姓名" field="csUid"></t:dgCol>
   <t:dgCol title="是否本人" field="csUid"></t:dgCol>
   <t:dgCol title="咨询方式" field="csUid"></t:dgCol>

   <t:dgCol title="医疗机构" field="insName"></t:dgCol>
   <t:dgCol title="医疗机构级别" field="csUid" width="100"></t:dgCol>
   <t:dgCol title="科室" field="deptName"></t:dgCol>
   <t:dgCol title="医师" field="docName"></t:dgCol>
   <t:dgCol title="医师级别" field="fsUid"></t:dgCol>
	
   <t:dgCol title="医疗机构" field="PreMainheadEntity_id" hidden="false" query="true" replace="${medicalsReplace}"></t:dgCol>
   <t:dgCol title="科室" field="BaseLocalDeptEntity_id" hidden="false" query="true" replace="${deptsReplace}"></t:dgCol>
   <t:dgCol title="医师" field="BaseDoctorEntity_id" hidden="false" query="true" replace="${doctorsReplace}"></t:dgCol>
   <t:dgCol title="住院号"  field="ryZyh"  hidden="true"   query="true"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgFunOpt funname="queryZyDetail(patName)" title="查看详细"></t:dgFunOpt>
   <t:dgToolBar  operationCode="" title="导出" icon="icon-putout" funname=""></t:dgToolBar>
   <t:dgToolBar title="打印" icon="icon-print"  funname=""></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>

	
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
		$("#auditZyfsystb").find("input[name='ryTime_begin']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
		$("#auditZyfsystb").find("input[name='ryTime_end']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
 });
 function showZyDetail(rowIndex,rowData) {
 }
 function queryZyDetail(pname){
	addTab('住院详情-已审','preMainheadController.do?auditZyfsysDetail','pictures');
}
 </script>
