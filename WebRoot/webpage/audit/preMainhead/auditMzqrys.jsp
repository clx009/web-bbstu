<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<style>
#JZXXMzqrys table
 {
     border-collapse: collapse;
     border: none;
 }
#JZXXMzqrys td
 {
     border: solid RGB(149,183,231) 1px;
     width:100px;
 }
#rulesMzqrys table
 {
     border-collapse: collapse;
     border: none;
 }
#rulesMzqrys td
 {
     border: solid RGB(149,183,231) 1px;
     height:50px;
 }
.propMzqrys{
	text-align: right;
	background-color: #E0ECFF;
}
.topPropMzqrys{
	text-align: center;
	background-color: #E0ECFF;
}
#DZBLMzqrys table
 {
     border-collapse: collapse;
     border: none;
 }
#DZBLMzqrys td
 {
     border: solid RGB(149,183,231) 1px;
 }
</style>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
   <t:datagrid name="auditMzqrys" checkbox="true" onSelect="showRules" fitColumns="false" title="处方信息" actionUrl="preMainheadController.do?datagridMzqrys" idField="id" fit="true" queryMode="group">
   <t:dgCol title="编号"  field="id"  hidden="false" ></t:dgCol>
   <t:dgCol title="处方日期"  field="presTime" formatter="yyyy-MM-dd" hidden="true"  queryMode="group" query="true" width="90"></t:dgCol>
   <t:dgCol title="参保人"  field="patName"  hidden="true" query="true" width="70"></t:dgCol>
   <t:dgCol title="参保人编号" field="patCode"></t:dgCol>
   <t:dgCol title="参保人类型" field="cbType" dictionary="cbtype"></t:dgCol>
   <t:dgCol title="性别"  field="patSex"  hidden="true" width="40" dictionary="sex"></t:dgCol>
   <t:dgCol title="年龄"  field="patAge"  hidden="true" width="40"></t:dgCol>
   <t:dgCol title="处方号"  field="sxh"  hidden="false"   query="true"></t:dgCol>
   <t:dgCol title="门诊诊断"  field="mzDisname"  hidden="true" query="true"></t:dgCol>
   <t:dgCol title="（慢）特病种" hidden="false" field="peopType" query="true"></t:dgCol>
   <t:dgCol title="发生金额"  field="presMoney"  hidden="true" width="70" queryMode="group" query="true"></t:dgCol>
   
   <t:dgCol title="初审意见" field="csUid"></t:dgCol>
   <t:dgCol title="复审意见" field="csUid"></t:dgCol>
   <t:dgCol title="申诉意见" field="csUid"></t:dgCol>
   
   <t:dgCol title="医疗机构" field="insName"></t:dgCol>
   <t:dgCol title="医疗机构级别" field="csUid" width="100"></t:dgCol>
   <t:dgCol title="科室" field="deptName"></t:dgCol>
   <t:dgCol title="医师" field="docName"></t:dgCol>
   <t:dgCol title="医师级别" field="fsUid"></t:dgCol>
   
   <t:dgCol title="医疗机构" field="PreMainheadEntity_id" hidden="false" query="true" replace="${medicalsReplace}"></t:dgCol>
   <t:dgCol title="科室" field="BaseLocalDeptEntity_id" hidden="false" query="true" replace="${deptsReplace}"></t:dgCol>
   <t:dgCol title="医师" field="BaseDoctorEntity_id" hidden="false" query="true" replace="${doctorsReplace}"></t:dgCol>
   
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgFunOpt funname="queryHisPre(patName)" title="历史记录"></t:dgFunOpt>
   
   <t:dgToolBar  operationCode="" title="导出" icon="icon-putout" funname=""></t:dgToolBar>
   <t:dgToolBar title="打印" icon="icon-print"  funname=""></t:dgToolBar>
   
  </t:datagrid>
  </div>
 </div>
 
 <div region="east" style="overflow: hidden;" split="false" id="eastNorthMzqrys">
	<div class="easyui-panel" style="padding: 1px;" fit="true" border="false" title="审核意见">
		<div style="padding-left:20px;padding-right:20px;">
    	<p style="font-weight:bold;">初审意见：</p>
    	<p>违规 - 经审查，处方中用药违反配伍禁忌用药原则，视为违规。</p> 
    	
    	<p style="font-weight:bold;">复审意见：</p>
    	<p>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</p> 
    	
    	<p style="font-weight:bold;">申诉意见：</p>
    	<p>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</p> 
    	<p>《全国医保目录》【预览】【下载】</p>
    	
    	</div>
	</div>
 </div>

<div region="south" split="true" id="southMzqrys">
 	<div class="easyui-layout" fit="true">   
		<div class="easyui-tabs" style="padding:0px;" region="center">   
		    <div id="tabsMzqrys" title="就诊信息" style="padding:10px;overflow:auto;">   
		        <div id="JZXXMzqrys">
		        	<table>
		        		<tr>
		        			<th colspan="8" style="font-weight: bold;text-align: left;">医疗机构信息</th>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">机构编号：</td>
		        			<td>100001</td>
		        			<td class="propMzqrys">机构名称：</td>
		        			<td>沧州市中心医院</td>
		        			<td class="propMzqrys">机构级别：</td>
		        			<td>三甲医院</td>
		        			<td class="propMzqrys">科室编号：</td>
		        			<td>101</td>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">科室名称：</td>
		        			<td>内科</td>
		        			<td class="propMzqrys">医师编号：</td>
		        			<td>201010</td>
		        			<td class="propMzqrys">医师姓名：</td>
		        			<td>胡歌</td>
		        			<td class="propMzqrys">医师级别：</td>
		        			<td>主任医师</td>
		        		</tr>
		        	</table>
		        	<br/>
		        	<table>
		        		<tr>
		        			<th colspan="8" style="font-weight: bold;text-align: left;">参保人信息</th>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">参保人编号：</td>
		        			<td>100001</td>
		        			<td class="propMzqrys">姓名：</td>
		        			<td>李载安</td>
		        			<td class="propMzqrys">性别：</td>
		        			<td>男</td>
		        			<td class="propMzqrys">年龄：</td>
		        			<td>55</td>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">人员类别：</td>
		        			<td>普通参保人</td>
		        			<td class="propMzqrys">参保类型：</td>
		        			<td>城镇职工社会保险</td>
		        			<td class="propMzqrys"></td>
		        			<td></td>
		        			<td class="propMzqrys"></td>
		        			<td></td>
		        		</tr>
		        	</table>
		        	<br/>
		        	<table>
		        		<tr>
		        			<th colspan="8" style="font-weight: bold;text-align: left;">处方信息</th>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">处方号：</td>
		        			<td>10000111</td>
		        			<td class="propMzqrys">主要诊断：</td>
		        			<td>糖尿病</td>
		        			<td class="propMzqrys">处方日期：</td>
		        			<td>2013-08-01</td>
		        			<td class="propMzqrys">处方来源：</td>
		        			<td>门诊</td>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">总金额（元）：</td>
		        			<td>386</td>
		        			<td class="propMzqrys">医保内金额：</td>
		        			<td>350</td>
		        			<td class="propMzqrys"></td>
		        			<td></td>
		        			<td class="propMzqrys"></td>
		        			<td></td>
		        		</tr>
		        	</table>
		        	<br/>
		        	<table>
		        		<tr>
		        			<th colspan="8" style="font-weight: bold;border: 0px;text-align: left;">审核信息</th>
		        		</tr>
		        		<tr>
		        			<td class="topPropMzqrys">审核环节</td>
		        			<td class="topPropMzqrys">审核结果</td>
		        			<td class="topPropMzqrys">审核时间</td>
		        			<td class="topPropMzqrys">审核者</td>
		        		</tr>
		        		<tr>
		        			<td class="textCenter">系统审核</td>
		        			<td class="textCenter">违规</td>
		        			<td class="textCenter">2014-04-16</td>
		        			<td class="textCenter">系统自动</td>
		        		</tr>
		        		<tr>
		        			<td class="textCenter">人工初审</td>
		        			<td class="textCenter">违规</td>
		        			<td class="textCenter">2014-04-17</td>
		        			<td class="textCenter">王晓丽</td>
		        		</tr>
		        		<tr>
		        			<td class="textCenter">人工复核</td>
		        			<td class="textCenter">违规</td>
		        			<td class="textCenter">2014-04-18</td>
		        			<td class="textCenter">马小刚</td>
		        		</tr>
		        	</table>
		        	<p style="color: red;">系统提示：门诊&nbsp;&nbsp;初审&nbsp;&nbsp;初审人员：王晓丽&nbsp;&nbsp;2014-04-17</p>
		        </div>
		    </div>   
		    <div title="费用明细"  style="overflow:auto;padding:0px;">   
				<table id="feeMzqrys" class="easyui-datagrid"
						data-options="
							iconCls: 'icon-edit',
							singleSelect: true,
							toolbar: '#tbMzqrys',
							url: 'webpage/audit/preMainhead/datagrid_fee.json',
							method: 'get',
							rownumbers:true,
							fit:true
						">
					<thead>
						<tr>
							<th data-options="field:'itemName',width:80,formatter: gotoPage">项目名称</th>   
							<th data-options="field:'itemType',width:60">项目类别</th>   
				            <th data-options="field:'ruleName',width:80">违反规则</th>   
				            <th data-options="field:'gg',width:80">规格</th>  
				            <th data-options="field:'dw',width:40">单位</th>  
				            <th data-options="field:'sl',width:40"  sum="true">数量</th>  
				            <th data-options="field:'je',width:80"  sum="true">金额</th>  
				            <th data-options="field:'yf',width:80">用法</th>  
				            <th data-options="field:'yl',width:40">用量</th>  
				            <th data-options="field:'tj',width:60">给药途径</th>  
				            <th data-options="field:'ybnje',width:80">医保内金额</th>  
				            <th data-options="field:'jfSl',width:60"  sum="true">拒付数量</th>
				            <th data-options="field:'jfJe',width:60" sum="true">拒付金额</th>  
							<th data-options="field:'jfLy',width:100">拒付理由</th>
						</tr>
					</thead>
				</table>
				<div id="tbMzqrys" style="height:auto">
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-putout',plain:true" onclick="">导出</a>
					<a href="javascript:void(0)" class="easyui-linkbutton" data-options="iconCls:'icon-print',plain:true" onclick="">打印</a>
				</div>
		    </div> 
		    <div title="电子病历" style="padding:10px;overflow:auto;">   
		        <div id="DZBLMzqrys">
		        	<p align="center" style="font-size:16px;font-weight:bold;">【李载安】个人电子病历</p>
		        	<table width="99%" align="center">
		        		<tr>
		        			<td width="180" class="propMzqrys">诊断医院：</td>
		        			<td>沧州市中心医院</td>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">初诊时间：</td>
		        			<td>2013-07-01 11:40</td>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">就诊科别：</td>
		        			<td>急诊</td>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">主诉：</td>
		        			<td>腹部疼痛一小时</td>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">现病历：</td>
		        			<td></td>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">既往病历：</td>
		        			<td></td>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">体格检查：</td>
		        			<td></td>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">辅助检查结果：</td>
		        			<td></td>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">初步诊断：</td>
		        			<td></td>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">处理意见：</td>
		        			<td></td>
		        		</tr>
		        		<tr>
		        			<td class="propMzqrys">主治医生：</td>
		        			<td></td>
		        		</tr>
		        	</table>
		        	
		        </div>
		    </div> 
		    <div title="检查报告" style="padding:10px;overflow:auto;">   
		        <div id="JCBGMzqrys">
		        </div>
		    </div>
		    <div title="公务员查体" style="padding:10px;overflow:auto;">   
		        <div id="GWYCTMzqrys">
		        </div>
		    </div>
		    <div title="现金报销明细" style="padding:10px;overflow:auto;">   
		        <div id="XJBXMXMzqrys">
		        </div>
		    </div>
		    <div title="医疗档案" style="padding:10px;overflow:auto;">   
		        <div id="YLDAMzqrys">
		        </div>
		    </div>  
		</div> 
		<div region="east" style="overflow: hidden;" split="false" id="eastSouthMzqrys">
			<div class="easyui-panel" style="padding: 1px;" fit="true" border="false" id="rulesMzqrys" title="违反规则" >
				<table width="100%" align="center">
		       		<tr>
		       			<td class="topPropMzqrys" width="30%">规则名称</td>
		       			<td class="topPropMzqrys">违规详情</td>
		       		</tr>
		       		<tr>
		       			<td class="textCenter">配伍禁忌</td>
		       			<td>左氧氟沙星+复方丹参 出现乳白色混浊 红霉素+生理盐水 会析出结晶、沉淀。</td>
		       		</tr>
		       		<tr>
		       			<td class="textCenter">用量过少</td>
		       			<td>西洛他唑片每日服用量不得少于XXX。</td>
		       		</tr>
		       	</table>
			</div>
		</div>   
	</div>
</div>



 <script src = "webpage/audit/preMainhead/commExtend.js"></script>
 <script type="text/javascript">
 $(document).ready(function(){
 		$("#southMzqrys").height(fixHeight(0.3));
 		$("#eastNorthMzqrys").width(fixWidth(0.2));
		$("#eastSouthMzqrys").width(fixWidth(0.2));
 
 		//给时间控件加上样式
		$("#auditMzqrystb").find("input[name='presTime_begin']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
		$("#auditMzqrystb").find("input[name='presTime_end']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
		$("#feeMzqrys").datagrid({
			showFooter : true,
  			onLoadSuccess : function() {
      			$('#feeMzqrys').datagrid('statistics');
  			}
		});
 });
 function showRules(rowIndex,rowData) {
	//$("#rules-panel").panel(
	//	{
	//		title :"违反规则:",
	//		//href:"",
	//		content:
	//	}
	//);
	$('#rulesMzqrys').panel("refresh");
}
function queryHisPre(pname){
	addTab('历史记录','preMainheadController.do?auditBrlsjl','pictures');
}
 </script>
