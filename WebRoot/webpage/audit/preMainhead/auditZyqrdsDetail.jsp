<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<style>
#zygkZyqrdsDetail table
 {
     border-collapse: collapse;
     border: none;
 }
#zygkZyqrdsDetail td
 {
     border: solid RGB(149,183,231) 1px;
 }
 #rulesZyqrdsDetail table
 {
     border-collapse: collapse;
     border: none;
 }
#rulesZyqrdsDetail td
 {
     border: solid RGB(149,183,231) 1px;
     height:50px;
 }
#JZXXZyqrdsDetail table
 {
     border-collapse: collapse;
     border: none;
 }
#JZXXZyqrdsDetail td
 {
     border: solid RGB(149,183,231) 1px;
     width: 100px;
 }
.propZyqrdsDetail{
	text-align: right;
	background-color: #E0ECFF;
}
.topPropZyqrdsDetail{
	text-align: center;
	background-color: #E0ECFF;
}
</style>
<div class="easyui-layout" fit="true">
   <div data-options="region:'west',collapsible:true" id="westZyqrdsDetail">
	   	<div class="easyui-panel" style="padding: 0px;" fit="true" border="false" title="住院概况">
			<div id="zygkZyqrdsDetail">
				<table cellpadding="5" id="tblZyqrdsDetail" height="400" width="100%">
					<tr>
						<td class="propZyqrdsDetail">住院时间：</td>
						<td>2014-05-22至2014-06-03</td>
					</tr>
					<tr>
						<td class="propZyqrdsDetail">住院天数：</td>
						<td>13</td>
					</tr>
					<tr>
						<td class="propZyqrdsDetail">医疗机构：</td>
						<td>沧州市中心医院</td>
					</tr>
					<tr>
						<td class="propZyqrdsDetail">医疗机构级别：</td>
						<td>三甲医院</td>
					</tr>
					<tr>
						<td class="propZyqrdsDetail">科室：</td>
						<td>内分泌科</td>
					</tr>
					<tr>
						<td class="propZyqrdsDetail">医师：</td>
						<td>张伟</td>
					</tr>
					<tr>
						<td class="propZyqrdsDetail">医师级别：</td>
						<td>主任医师</td>
					</tr>
					<tr>
						<td class="propZyqrdsDetail">入院诊断：</td>
						<td>糖尿病</td>
					</tr>
					<tr>
						<td class="propZyqrdsDetail">出院诊断：</td>
						<td>转氨酶75乙肝各项正常</td>
					</tr>
					<tr>
						<td class="propZyqrdsDetail">住院号：</td>
						<td>200012</td>
					</tr>
					<tr>
						<td class="propZyqrdsDetail">发生金额：</td>
						<td>1300.00</td>
					</tr>
					<tr>
						<td class="propZyqrdsDetail">医保金额：</td>
						<td>1100.00</td>
					</tr>
					<tr>
						<td class="propZyqrdsDetail">自费金额：</td>
						<td>200.00</td>
					</tr>
					<tr>
						<td class="propZyqrdsDetail">拒付金额：</td>
						<td>300.00</td>
					</tr>
				</table>
	    	</div>   
		</div>
   </div>   
   <div data-options="region:'center'">   
      <div id="tabsZyqrdsDetail" class="easyui-tabs" fit="true">   
	     <div title="就诊信息" style="padding:10px;overflow:auto;">   
	        <div id="JZXXZyqrdsDetail">
	        	<table>
		        		<tr>
		        			<th colspan="8" style="font-weight: bold;text-align: left;">医疗机构信息</th>
		        		</tr>
		        		<tr>
		        			<td class="propZyqrdsDetail">机构编号：</td>
		        			<td>100001</td>
		        			<td class="propZyqrdsDetail">机构名称：</td>
		        			<td>沧州市中心医院</td>
		        			<td class="propZyqrdsDetail">机构级别：</td>
		        			<td>三甲医院</td>
		        			<td class="propZyqrdsDetail">科室编号：</td>
		        			<td>101</td>
		        		</tr>
		        		<tr>
		        			<td class="propZyqrdsDetail">科室名称：</td>
		        			<td>内科</td>
		        			<td class="propZyqrdsDetail">医师编号：</td>
		        			<td>201010</td>
		        			<td class="propZyqrdsDetail">医师姓名：</td>
		        			<td>胡歌</td>
		        			<td class="propZyqrdsDetail">医师级别：</td>
		        			<td>主任医师</td>
		        		</tr>
		        	</table>
	        	<br/>
	        	<table height="75">
	        		<tr>
	        			<th colspan="8" style="font-weight: bold;text-align: left;">参保人信息</th>
	        		</tr>
	        		<tr>
	        			<td class="propZyqrdsDetail">参保人编号：</td>
	        			<td>100001</td>
	        			<td class="propZyqrdsDetail">姓名：</td>
	        			<td>李载安</td>
	        			<td class="propZyqrdsDetail">性别：</td>
	        			<td>男</td>
	        			<td class="propZyqrdsDetail">年龄：</td>
	        			<td>55</td>
	        		</tr>
	        		<tr>
	        			<td class="propZyqrdsDetail">人员类别：</td>
	        			<td>普通参保人</td>
	        			<td class="propZyqrdsDetail">参保类型：</td>
	        			<td>城镇职工社会保险</td>
	        			<td class="propZyqrdsDetail"></td>
	        			<td></td>
	        			<td class="propZyqrdsDetail"></td>
	        			<td></td>
	        		</tr>
	        	</table>
	        	<br/>
	        	<table height="75">
	        		<tr>
	        			<th colspan="8" style="font-weight: bold;text-align: left;">处方信息</th>
	        		</tr>
	        		<tr>
	        			<td class="propZyqrdsDetail">处方号：</td>
	        			<td>10000111</td>
	        			<td class="propZyqrdsDetail">主要诊断：</td>
	        			<td>糖尿病</td>
	        			<td class="propZyqrdsDetail">处方日期：</td>
	        			<td>2013-08-01</td>
	        			<td class="propZyqrdsDetail">处方来源：</td>
	        			<td>门诊</td>
	        		</tr>
	        		<tr>
	        			<td class="propZyqrdsDetail">总金额（元）：</td>
	        			<td>386</td>
	        			<td class="propZyqrdsDetail">医保内金额：</td>
	        			<td>350</td>
	        			<td class="propZyqrdsDetail"></td>
	        			<td></td>
	        			<td class="propZyqrdsDetail"></td>
	        			<td></td>
	        		</tr>
	        	</table>
	        	<br/>
	        	<table height="125">
	        		<tr>
	        			<th colspan="8" style="font-weight: bold;border: 0px;text-align: left;">审核信息</th>
	        		</tr>
	        		<tr>
	        			<td class="topPropZyqrdsDetail">审核环节</td>
	        			<td class="topPropZyqrdsDetail">审核结果</td>
	        			<td class="topPropZyqrdsDetail">审核时间</td>
	        			<td class="topPropZyqrdsDetail">审核者</td>
	        		</tr>
	        		<tr>
	        			<td class="textCenter">系统审核</td>
	        			<td class="textCenter">违规</td>
	        			<td class="textCenter">2014-04-16</td>
	        			<td class="textCenter">系统自动</td>
	        		</tr>
	        		<tr>
	        			<td class="textCenter">人工初审</td>
	        			<td class="textCenter">违规</td>
	        			<td class="textCenter">2014-04-17</td>
	        			<td class="textCenter">王晓丽</td>
	        		</tr>
	        		<tr>
	        			<td class="textCenter">人工复核</td>
	        			<td class="textCenter">违规</td>
	        			<td class="textCenter">2014-04-18</td>
	        			<td class="textCenter">马小刚</td>
	        		</tr>
	        	</table>
	        	<p style="color: red;">系统提示：门诊&nbsp;&nbsp;初审&nbsp;&nbsp;初审人员：王晓丽&nbsp;&nbsp;2014-04-17</p>
	        </div>
	    </div>   
	    <div title="费用明细"  style="overflow:auto;padding:0px;">   
	    	<div class="easyui-layout" data-options="fit:true">   
	            <div data-options="region:'center'">
	            	<table id="xmlbZyqrdsDetail" class="easyui-treegrid" 
					data-options="url:'webpage/audit/preMainhead/treegrid_xmlb.json',
					        	 idField:'id',
					        	 treeField:'xmlb',
					        	 method: 'get',
								rownumbers: true,
								fit:true,
								onClickRow:selectXmlb">   
					    <thead>   
					        <tr>   
					            <th data-options="field:'xmlb',width:180">项目类别</th>   
					            <th data-options="field:'je',width:180">金额</th>   
					            <th data-options="field:'xms',width:180">项目数</th>   
					            <th data-options="field:'bjxms',width:180">报警项目数</th>   
					        </tr>   
					    </thead>   
					</table>  
	            </div>   
	            <div data-options="region:'south'" id="southXmZyqrdsDetail">
		            	<table id="xmZyqrdsDetail" class="easyui-datagrid"
							data-options="
								iconCls: 'icon-edit',
								singleSelect: true,
								url: 'webpage/audit/preMainhead/datagrid_fee.json',
								method: 'get',
								rownumbers:true,
								fit:true
							">
						<thead>
							<tr>
								<th data-options="field:'itemName',width:80,formatter: gotoPage">项目名称</th>   
					            <th data-options="field:'dw',width:40">单位</th>  
					            <th data-options="field:'dj',width:40">单价</th>  
					            <th data-options="field:'sl',width:40">数量</th>  
					            <th data-options="field:'je',width:80">金额</th>  
					            <th data-options="field:'yf',width:80">用法</th>  
					            <th data-options="field:'yl',width:40">用量</th>  
					            <th data-options="field:'tj',width:60">给药途径</th>  
					            <th data-options="field:'ybnje',width:80">医保内金额</th>  
					            <th data-options="field:'jfSl',width:60">拒付数量</th>
					            <th data-options="field:'jfJe',width:60">拒付金额</th>  
								<th data-options="field:'jfLy',width:100">拒付理由</th>
							</tr>
						</thead>
					</table>
	            </div>   
	        </div>   
	    </div>   
	    <div title="拒付明细" style="padding:5px;">     
	        <table id="jfZyqrdsDetail" class="easyui-datagrid"
					data-options="
						singleSelect: true,
						url: 'webpage/audit/preMainhead/datagrid_fee.json',
						method: 'get',
						rownumbers:true,
						fit:true
					">
				<thead>
					<tr>
						<th data-options="field:'itemName',width:150,formatter: gotoPage">项目名称</th>   
			            <th data-options="field:'jfSl',width:100" sum="true">拒付数量</th>
			            <th data-options="field:'jfJe',width:100" sum="true">拒付金额</th>  
						<th data-options="field:'jfLy',width:200">拒付理由</th>
					</tr>
				</thead>
			</table>
	    </div>  
	    <div title="历史记录"> 
	    	<div class="easyui-layout" data-options="fit:true">   
	            <div data-options="region:'center'" id="mzlsZyqrdsDetail">
		            <table class="easyui-datagrid"
						data-options="
							singleSelect: true,
							url: 'webpage/audit/preMainhead/datagrid_zylsjl.json',
							method: 'get',
							rownumbers:true,
							fit:true,
							title:'门诊'
						">
						<thead>
							<tr>
								<th data-options="field:'ryDisname',width:120">入院诊断</th>   
								<th data-options="field:'ryTime',width:80">入院时间</th>   
					            <th data-options="field:'cyTime',width:80">出院时间</th>   
					            <th data-options="field:'presMoney',width:80">发生金额</th>  
					            <th data-options="field:'ryZyh',width:80">住院号</th>  
					            <th data-options="field:'insName',width:120">医疗机构名称</th>  
					            <th data-options="field:'deptName',width:80">科室名称</th>  
					            <th data-options="field:'docName',width:80">医师姓名</th>  
					            <th data-options="field:'opt',width:80,formatter:mzxxFormat">操作</th>  
							</tr>
						</thead>
					</table>
	            </div>   
	            <div data-options="region:'south'" id="zylsZyqrdsDetail">
	            	<table class="easyui-datagrid"
					data-options="
						singleSelect: true,
						url: 'webpage/audit/preMainhead/datagrid_zylsjl.json',
						method: 'get',
						rownumbers:true,
						fit:true,
						title:'住院'
					">
						<thead>
							<tr>
								<th data-options="field:'ryDisname',width:120">入院诊断</th>   
								<th data-options="field:'ryTime',width:80">入院时间</th>   
					            <th data-options="field:'cyTime',width:80">出院时间</th>   
					            <th data-options="field:'presMoney',width:80">发生金额</th>  
					            <th data-options="field:'ryZyh',width:80">住院号</th>  
					            <th data-options="field:'insName',width:120">医疗机构名称</th>  
					            <th data-options="field:'deptName',width:80">科室名称</th>  
					            <th data-options="field:'docName',width:80">医师姓名</th>  
					            <th data-options="field:'opt',width:80,formatter:zyxxFormat">操作</th>
							</tr>
						</thead>
					</table>
	            </div>   
	        </div>   
	    </div>  
	    <div title="流水清单" style="padding:5px;">     
	       <div class="easyui-layout" data-options="fit:true">
				<div data-options="region:'west',split:true,border:false" style="width:130px;overflow: hidden;">
					<table fit="true" class="easyui-datagrid"
			            data-options="singleSelect:true,data:[
							{date:'2014-05-17'},
							{date:'2014-05-18'},
							{date:'2014-05-19'},
							{date:'2014-05-20'},
							{date:'2014-05-21'},
							{date:'2014-05-25'}
						]
			            ">
				        <thead>
				            <tr>
				                <th data-options="field:'date',width:120">日期</th>
				            </tr>
				        </thead>
				    </table>
				</div>
				<div data-options="region:'center'" border="false" >
					<div class="easyui-panel"  border="false" fit="true">
						<table class="easyui-datagrid"
							data-options="
								singleSelect: true,
								url: 'webpage/audit/preMainhead/datagrid_fee.json',
								method: 'get',
								rownumbers:true,
								fit:true
							">
							<thead>
								<tr>
									<th data-options="field:'itemName',width:120">项目名称</th>   
						            <th data-options="field:'dw',width:40">单位</th>  
						            <th data-options="field:'dj',width:40">单价</th>  
						            <th data-options="field:'sl',width:40">数量</th>  
						            <th data-options="field:'je',width:80">金额</th>  
						            <th data-options="field:'yf',width:120">用法</th>  
						            <th data-options="field:'yl',width:40">用量</th>  
						            <th data-options="field:'tj',width:120">给药途径</th>  
								</tr>
							</thead>
						</table>
					</div>
				</div>
			</div>  
	    </div>  
	    <div title="电子病历" style="padding:5px;">     
	         
	    </div>   
	</div>  
   </div> 
   <div data-options="region:'east',collapsible:true" id="eastZyqrdsDetail">
   		<div class="easyui-layout" fit="true">  
   		  	<div data-options="region:'center'">
		   		<div class="easyui-panel" style="padding: 1px;" fit="true" border="false" title="审核意见">
					<div style="padding-left:20px;padding-right:20px;">
			    	<p style="font-weight:bold;">初审意见：</p>
			    	<p>违规 - 经审查，处方中用药违反配伍禁忌用药原则，视为违规。</p> 
			    	
			    	<p style="font-weight:bold;">复审意见：</p>
			    	<p>XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX</p> 
			    	
			    	<br/>
			    	<p>
			    		<a href="#" class="easyui-linkbutton" data-options="iconCls:''">确认</a>  
			    		<a href="javascript:batchRefuse();" class="easyui-linkbutton" data-options="iconCls:''">复议</a>  
			    	</p>
			    	</div>
				</div>
			</div>
			<div data-options="region:'south'" id="southZyqrdsDetail">
				<div class="easyui-panel" style="padding: 1px;" fit="true" border="false" id="rulesZyqrdsDetail"  title="违规规则">
					<table width="100%" align="center">
			       		<tr>
			       			<td class="topPropZyqrdsDetail" width="30%">规则名称</td>
			       			<td class="topPropZyqrdsDetail">违规详情</td>
			       		</tr>
			       		<tr>
			       			<td class="textCenter">配伍禁忌</td>
			       			<td>左氧氟沙星+复方丹参 出现乳白色混浊 红霉素+生理盐水 会析出结晶、沉淀。</td>
			       		</tr>
			       		<tr>
			       			<td class="textCenter">用量过少</td>
			       			<td>西洛他唑片每日服用量不得少于XXX。</td>
			       		</tr>
			       	</table>
				</div>
			</div>
		</div>
   </div>   
 </div>
<script src = "webpage/audit/preMainhead/auditZyqrdsDetail.js"></script>	
<script src = "webpage/audit/preMainhead/commExtend.js"></script>
<script language="javascript">
$(document).ready(function(){
 		$("#westZyqrdsDetail").width(fixWidth(0.15));
 		$("#zygkZyqrdsDetail").width(fixWidth(0.15)-2);
		$("#eastZyqrdsDetail").width(fixWidth(0.2));
		$("#zylsZyqrdsDetail").height(fixHeight(0.4));
		$("#southZyqrdsDetail").height(fixHeight(0.4));
		$("#southXmZyqrdsDetail").height(fixHeight(0.4));
		$("#jfZyqrdsDetail").datagrid({
			showFooter : true,
  			onLoadSuccess : function() {
      			$('#jfZyqrdsDetail').datagrid('statistics');
  			}
		});
 });

function selectXmlb(row){
	//可以参考preMainheadZyDetail.js文件中onClickRow方法
}

function mzxxFormat(){
	return "<a href=javascript:addTab('【刘志少】门诊历史详情','preMainheadController.do?auditMzqrys','pictures')>查看详细</a>";
}

function zyxxFormat(){
	return "<a href=javascript:addTab('【刘志少】住院历史详情','preMainheadController.do?auditZyqrysDetail','pictures')>查看详细</a>";
}
</script>