var editIndex = undefined;
function endEditing(){
	if (editIndex == undefined){return true}
	if ($('#feeMzcsds').datagrid('validateRow', editIndex)){
		var ed = $('#feeMzcsds').datagrid('getEditor', {index:editIndex,field:'jfLy'});
		var ruleName = $(ed.target).combobox('getText');
		$('#feeMzcsds').datagrid('getRows')[editIndex]['jfLy'] = ruleName;
		$('#feeMzcsds').datagrid('endEdit', editIndex);
		
		var jfSl=$('#feeMzcsds').datagrid('getRows')[editIndex]['jfSl'];
		var dj=$('#feeMzcsds').datagrid('getRows')[editIndex]['dj'];
		var jfJe=(jfSl*dj).toFixed(2);
		$('#feeMzcsds').datagrid('updateRow',{
			index: editIndex,
			row: {
				jfJe: jfJe
			}
		});
		$('#feeMzcsds').datagrid('statistics');
		
		editIndex = undefined;
		return true;
	} else {
		return false;
	}
}
function onClickRow(index){
	if (editIndex != index){
		if (endEditing()){
			$('#feeMzcsds').datagrid('selectRow', index)
					.datagrid('beginEdit', index);
			editIndex = index;
		} else {
			$('#feeMzcsds').datagrid('selectRow', editIndex);
		}
	}
}
function append(){
	if (endEditing()){
		$('#feeMzcsds').datagrid('appendRow',{status:'P'});
		editIndex = $('#feeMzcsds').datagrid('getRows').length-1;
		$('#feeMzcsds').datagrid('selectRow', editIndex)
				.datagrid('beginEdit', editIndex);
	}
}
function removeit(){
	if (editIndex == undefined){return}
	$('#feeMzcsds').datagrid('cancelEdit', editIndex)
			.datagrid('deleteRow', editIndex);
	editIndex = undefined;
}
function accept(){
	if (endEditing()){
		$('#feeMzcsds').datagrid('acceptChanges');
	}
}
function reject(){
	$('#feeMzcsds').datagrid('rejectChanges');
	editIndex = undefined;
}
function getChanges(){
	var rows = $('#feeMzcsds').datagrid('getChanges');
	alert(rows.length+' rows are changed!');
}
