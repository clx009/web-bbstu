var editIndex = undefined;
function endEditing(){
	if (editIndex == undefined){return true}
	if ($('#xmZycsdsDetail').datagrid('validateRow', editIndex)){
		var ed = $('#xmZycsdsDetail').datagrid('getEditor', {index:editIndex,field:'jfLy'});
		var ruleName = $(ed.target).combobox('getText');
		$('#xmZycsdsDetail').datagrid('getRows')[editIndex]['jfLy'] = ruleName;
		$('#xmZycsdsDetail').datagrid('endEdit', editIndex);
		
		var jfSl=$('#xmZycsdsDetail').datagrid('getRows')[editIndex]['jfSl'];
		var dj=$('#xmZycsdsDetail').datagrid('getRows')[editIndex]['dj'];
		var jfJe=(jfSl*dj).toFixed(2);
		$('#xmZycsdsDetail').datagrid('updateRow',{
			index: editIndex,
			row: {
				jfJe: jfJe
			}
		});
		$('#xmZycsdsDetail').datagrid('statistics');
		
		editIndex = undefined;
		return true;
	} else {
		return false;
	}
}
function onClickRow(index){
	if (editIndex != index){
		if (endEditing()){
			$('#xmZycsdsDetail').datagrid('selectRow', index)
					.datagrid('beginEdit', index);
			editIndex = index;
		} else {
			$('#xmZycsdsDetail').datagrid('selectRow', editIndex);
		}
	}
}
function append(){
	if (endEditing()){
		$('#xmZycsdsDetail').datagrid('appendRow',{status:'P'});
		editIndex = $('#xmZycsdsDetail').datagrid('getRows').length-1;
		$('#xmZycsdsDetail').datagrid('selectRow', editIndex)
				.datagrid('beginEdit', editIndex);
	}
}
function removeit(){
	if (editIndex == undefined){return}
	$('#xmZycsdsDetail').datagrid('cancelEdit', editIndex)
			.datagrid('deleteRow', editIndex);
	editIndex = undefined;
}
function accept(){
	if (endEditing()){
		$('#xmZycsdsDetail').datagrid('acceptChanges');
	}
}
function reject(){
	$('#xmZycsdsDetail').datagrid('rejectChanges');
	editIndex = undefined;
}
function getChanges(){
	var rows = $('#xmZycsdsDetail').datagrid('getChanges');
	alert(rows.length+' rows are changed!');
}

