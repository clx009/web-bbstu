var editIndex = undefined;
function endEditing(){
	if (editIndex == undefined){return true}
	if ($('#xmZyfsdsDetail').datagrid('validateRow', editIndex)){
		var ed = $('#xmZyfsdsDetail').datagrid('getEditor', {index:editIndex,field:'jfLy'});
		var ruleName = $(ed.target).combobox('getText');
		$('#xmZyfsdsDetail').datagrid('getRows')[editIndex]['jfLy'] = ruleName;
		$('#xmZyfsdsDetail').datagrid('endEdit', editIndex);
		
		var jfSl=$('#xmZyfsdsDetail').datagrid('getRows')[editIndex]['jfSl'];
		var dj=$('#xmZyfsdsDetail').datagrid('getRows')[editIndex]['dj'];
		var jfJe=(jfSl*dj).toFixed(2);
		$('#xmZyfsdsDetail').datagrid('updateRow',{
			index: editIndex,
			row: {
				jfJe: jfJe
			}
		});
		$('#xmZyfsdsDetail').datagrid('statistics');
		
		editIndex = undefined;
		return true;
	} else {
		return false;
	}
}
function onClickRow(index){
	if (editIndex != index){
		if (endEditing()){
			$('#xmZyfsdsDetail').datagrid('selectRow', index)
					.datagrid('beginEdit', index);
			editIndex = index;
		} else {
			$('#xmZyfsdsDetail').datagrid('selectRow', editIndex);
		}
	}
}
function append(){
	if (endEditing()){
		$('#xmZyfsdsDetail').datagrid('appendRow',{status:'P'});
		editIndex = $('#xmZyfsdsDetail').datagrid('getRows').length-1;
		$('#xmZyfsdsDetail').datagrid('selectRow', editIndex)
				.datagrid('beginEdit', editIndex);
	}
}
function removeit(){
	if (editIndex == undefined){return}
	$('#xmZyfsdsDetail').datagrid('cancelEdit', editIndex)
			.datagrid('deleteRow', editIndex);
	editIndex = undefined;
}
function accept(){
	if (endEditing()){
		$('#xmZyfsdsDetail').datagrid('acceptChanges');
	}
}
function reject(){
	$('#xmZyfsdsDetail').datagrid('rejectChanges');
	editIndex = undefined;
}
function getChanges(){
	var rows = $('#xmZyfsdsDetail').datagrid('getChanges');
	alert(rows.length+' rows are changed!');
}

