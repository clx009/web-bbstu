<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" />
		<script type="text/javascript" src="<%=basePath%>/plug-in/jquery/jquery-1.8.3.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/dataformat.js"></script>
		<link id="easyuiTheme" rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/default/easyui.css" type="text/css"></link>
		<link rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/icon.css" type="text/css"></link>
		<link rel="stylesheet" type="text/css" href="<%=basePath%>/plug-in/accordion/css/accordion.css">
		<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/jquery.easyui.min.1.3.2.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/syUtil.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>
		<link rel="stylesheet" href="<%=basePath%>/plug-in/tools/css/common.css" type="text/css"></link>
		<script type="text/javascript" src="<%=basePath%>/plug-in/lhgDialog/lhgdialog.min.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/curdtools.js"></script>
		<script type="text/javascript" src="<%=basePath%>/plug-in/tools/easyuiextend.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>
		<style>
		#leftDiv1 table
		 {
		     border-collapse: collapse;
		     border: none;
		 }
		#leftDiv1 td
		 {
		     border: solid RGB(149,183,231) 1px;
		     width:100px;
		 }
		.prop{
			text-align: right;
			background-color: #E0ECFF;
		}
		.topProp{
			text-align: left;
			background-color: #E0ECFF;
		}
		</style>
<div style="margin:0px;padding:0px;">
	<div id="leftDiv" style="width: 100%; height: 100%; float: left;text-align: center;margin:0px;padding:0px;">
		<div id="leftDiv1" style="width: 100%;overflow:hidden;height: 90%; float: left; text-align: center;margin:0px;padding:0px;">
			<table width="100%">
				<tr>
					<td class="prop">申诉意见：</td>
					<td ><textarea rows="8" cols="80"></textarea></td>
				</tr>
				<tr>
					<td class="prop">选择常用意见：</td>
					<td>
						<select id="opinionSelect" onchange="$('#opinion').val($('#opinionSelect option:selected').text())">
							<option>请选择..</option>	
							<option>经审核，同意拒付</option>	
							<option>经审核，不同意拒付</option>	
						</select>
					</td>
				</tr>
				<tr>
					<td class="prop">申诉材料：</td>
					<td >
					<t:upload name="filePath" buttonText="上传文件" uploader="sysFileController.do?doAdd" multi="false" extend="office" id="file_upload" formData="fileName,fileType,fileCat"></t:upload>
						<div class="form" id="filediv" style="height: 30px"></div>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							文件路径
						</label>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
