var editIndex = undefined;
function endEditing(){
	if (editIndex == undefined){return true}
	if ($('#feeMzfsds').datagrid('validateRow', editIndex)){
		var ed = $('#feeMzfsds').datagrid('getEditor', {index:editIndex,field:'jfLy'});
		var ruleName = $(ed.target).combobox('getText');
		$('#feeMzfsds').datagrid('getRows')[editIndex]['jfLy'] = ruleName;
		$('#feeMzfsds').datagrid('endEdit', editIndex);
		
		var jfSl=$('#feeMzfsds').datagrid('getRows')[editIndex]['jfSl'];
		var dj=$('#feeMzfsds').datagrid('getRows')[editIndex]['dj'];
		var jfJe=(jfSl*dj).toFixed(2);
		$('#feeMzfsds').datagrid('updateRow',{
			index: editIndex,
			row: {
				jfJe: jfJe
			}
		});
		$('#feeMzfsds').datagrid('statistics');
		
		editIndex = undefined;
		return true;
	} else {
		return false;
	}
}
function onClickRow(index){
	if (editIndex != index){
		if (endEditing()){
			$('#feeMzfsds').datagrid('selectRow', index)
					.datagrid('beginEdit', index);
			editIndex = index;
		} else {
			$('#feeMzfsds').datagrid('selectRow', editIndex);
		}
	}
}
function append(){
	if (endEditing()){
		$('#feeMzfsds').datagrid('appendRow',{status:'P'});
		editIndex = $('#feeMzfsds').datagrid('getRows').length-1;
		$('#feeMzfsds').datagrid('selectRow', editIndex)
				.datagrid('beginEdit', editIndex);
	}
}
function removeit(){
	if (editIndex == undefined){return}
	$('#feeMzfsds').datagrid('cancelEdit', editIndex)
			.datagrid('deleteRow', editIndex);
	editIndex = undefined;
}
function accept(){
	if (endEditing()){
		$('#feeMzfsds').datagrid('acceptChanges');
	}
}
function reject(){
	$('#feeMzfsds').datagrid('rejectChanges');
	editIndex = undefined;
}
function getChanges(){
	var rows = $('#feeMzfsds').datagrid('getChanges');
	alert(rows.length+' rows are changed!');
}
