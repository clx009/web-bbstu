var rejectScreeningUrl = "screeningController.do?rejectScreening&isIframe";
$(function() {
	var widthTmp = fixWidth(0.5);
	$("#westDiv").parent().width(widthTmp);
	$("#westDiv").width(widthTmp);
	$("#step1Container").width(widthTmp);
	$("#centerDiv").parent().width(widthTmp);
	var leftpx = widthTmp+"px";
	$("#centerDiv").parent().css({"left":leftpx});
	$("#centerDiv").width(widthTmp);
	$("#step2Container").width(widthTmp);
	
	$('#step1Container')
			.highcharts(
					{
						chart : {
							plotBackgroundColor : null,
							plotBorderWidth : null,
							plotShadow : false
						},
						title : {
							text : '规则比例图'
						},
						credits : {
							enabled : false
						},
						tooltip : {
							pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
						},
						plotOptions : {
							pie : {
								allowPointSelect : true,
								cursor : 'pointer',
								dataLabels : {
									enabled : true,
									format : '<b>{point.name}</b>: {point.percentage:.1f} %',
									style : {
										color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
												|| 'black'
									}
								}
							},
							series : {
								cursor : 'pointer',
								events : {
									click : function(e) {
										reloadChart(e.point.id);
									}
								}
							}
						},
						series : [ {
							type : 'pie',
							name : '规则类型比例',
							data : [{
								id : '1',
								name : '超量开药',
								y : 45.0
							},{
								id : '2',
								name : '超药品用法用量',
								y : 31.5
							},{
								id : '3',
								name : '用量过少',
								y : 8.5
							},{
								id : '4',
								name : '违反抗生素用药管理',
								y : 15
							}]
						} ]
					});
	reloadChart();
});


function addOneTab(subtitle, url, icon) {
	if (icon == '') {
		icon = 'icon folder';
	}
	window.top.$.messager.progress({
		text : '页面加载中....',
		interval : 300
	});
	window.top.$('#maintabs').tabs({
		onClose : function(subtitle, index) {
			window.top.$.messager.progress('close');
		}
	});
	if (window.top.$('#maintabs').tabs('exists', subtitle)) {
		window.top.$('#maintabs').tabs('select', subtitle);
		window.top.$('#maintabs').tabs('update', {
			tab : window.top.$('#maintabs').tabs('getSelected'),
			options : {
				title : subtitle,
				href:url,
				//content : '<iframe name="tabiframe"  scrolling="no" frameborder="0"  src="' + url + '" style="width:100%;height:99%;"></iframe>',
				closable : true,
				icon : icon
			}
		});
	} else {
		if (url.indexOf('isIframe') != -1) {
			window.top.$('#maintabs').tabs('add', {
				title : subtitle,
				content : '<iframe src="' + url + '" frameborder="0" style="border:0;width:100%;height:99.4%;"></iframe>',
				closable : true,
				icon : icon
			});
		}else {
			window.top.$('#maintabs').tabs('add', {
				title : subtitle,
				href:url,
				closable : true,
				icon : icon
			});
		}
	}
}

function reloadChart(ruleId){
	var url = '';
	if(!ruleId){
		url='webpage/audit/step2Screening.json';
	}else{
		url='webpage/audit/step2Screening'+ruleId+'.json';
	}
	$.getJSON(url, function(data) {
		$('#step2Container')
		.highcharts(
				{
					chart : {
						plotBackgroundColor : null,
						plotBorderWidth : null,
						plotShadow : false
					},
					title : {
						text : '规则细则比例图'
					},
					credits : {
						enabled : false
					},
					tooltip : {
						pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
					},
					plotOptions : {
						pie : {
							allowPointSelect : true,
							cursor : 'pointer',
							dataLabels : {
								enabled : true,
								format : '<b>{point.name}</b>: {point.percentage:.1f} %',
								style : {
									color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
									|| 'black'
								}
							}
						},
						series : {
							cursor : 'pointer',
							events : {
								click : function(e) {
									rejectScreeningUrl += '&ruleId=' + e.point.id;
									window.top.addTab('拒付筛选',rejectScreeningUrl,'picture'); 
								}
							}
						}
					},
					series : data
				});
	});
}

