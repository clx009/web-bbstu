function view(value,rowData,rowIndex) {  
	return "<a href='javascript:void(0);' onclick='showPreMainheadListByPatient();'>查看详情</a>";
}

function showPreMainheadListByPatient(){
	window.top.addTab('参保人【代振江】待审核门诊处方','preMainheadController.do?preMainheadByPatient','picture'); 
}

function addOneTab(subtitle, url, icon) {
	if (icon == '') {
		icon = 'icon folder';
	}
	window.top.$.messager.progress({
		text : '页面加载中....',
		interval : 300
	});
	window.top.$('#maintabs').tabs({
		onClose : function(subtitle, index) {
			window.top.$.messager.progress('close');
		}
	});
	if (window.top.$('#maintabs').tabs('exists', subtitle)) {
		window.top.$('#maintabs').tabs('select', subtitle);
		window.top.$('#maintabs').tabs('update', {
			tab : window.top.$('#maintabs').tabs('getSelected'),
			options : {
				title : subtitle,
				href : url,
				// content : '<iframe name="tabiframe" scrolling="no"
				// frameborder="0" src="' + url + '"
				// style="width:100%;height:99%;"></iframe>',
				closable : true,
				icon : icon
			}
		});
	} else {
		if (url.indexOf('isIframe') != -1) {
			window.top
					.$('#maintabs')
					.tabs(
							'add',
							{
								title : subtitle,
								content : '<iframe src="'
										+ url
										+ '" frameborder="0" style="border:0;width:100%;height:99.4%;"></iframe>',
								closable : true,
								icon : icon
							});
		} else {
			window.top.$('#maintabs').tabs('add', {
				title : subtitle,
				href : url,
				closable : true,
				icon : icon
			});
		}
	}
}


var categories = [0,1,2,3,4,5,6,7,8,9,10];
$(function() {
	var chart1=null;

		// Create the chart
		chart1 = new Highcharts.StockChart({
         chart: {
            renderTo: 'rejectScreeningContainer'
         },
         tooltip: {
	        formatter: function() {
                return '数量：'+this.y;
	        }
	    },
         rangeSelector : {
             enabled: false
         },
         navigator: {
             enabled: false
         },
         credits : {
				enabled : false
		},
		scrollbar : {
			enabled : false
		},
	    title: {
	        text: '图形化拒付'
	    },
	    xAxis: {
            labels: {
                formatter: function(){
                    return categories[this.value];
                }
            }
        },
        yAxis: {
        	opposite: false
        },
        
	    series: [{
	        name: '占百分比3',
	        data: rejectScreeningData
	    }],
	    plotOptions : {
			series : {
				events : {
					click : function(event){
						//alert(event.point.x); // X轴值
						//alert(event.point.y); // Y轴值
						//chart1.options.plotOptions.series.enableMouseTracking = false;
						//alert(chart1.options.plotOptions.series.enableMouseTracking);
						chart1.xAxis[0].removePlotLine('plotline-1'); 
						chart1.xAxis[0].addPlotLine({
							id : 'plotline-1',
							value : event.point.x,
							color : 'red',
							dashStyle : 'shortdash',
							width : 2,
							label : {
								text : '拒付线'
							}
						
						});
					}
				}
			}
			
		}
			
			
		});
	//});
	$("#btn").click(function(){
		if(chart1.xAxis[0].plotLinesAndBands[0]){
			alert(chart1.xAxis[0].plotLinesAndBands[0].options.value);
		}
	});
});