<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!-- context path -->
<t:base type="jquery,easyui"></t:base>

<script src="plug-in/Highstock-2.0.1/js/highstock.js"></script>
<script src="plug-in/Highstock-2.0.1/js/modules/exporting.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/webpage/audit/rejectScreening.js"></script>
<script type="text/javascript"
	src="<%=basePath%>/webpage/audit/rejectScreening_data.js"></script>
<script type="text/javascript">
$(function(){
	$('#dgrid').datagrid({  
		title:'违规项列表',
		width: fixWidth(0.5),
		method:'get', 
	    url:'<%=basePath%>/webpage/audit/datagrid_data1.json',   
	    rownumbers:true,
	    columns:[[   
	        {field:'ck',title:'选择',width:fixWidth(0.08),checkbox:true},   
	        {field:'idNum',title:'医保证号',width:fixWidth(0.0)},   
	        {field:'userName',title:'患者姓名',width:fixWidth(0.04)}, 
	        {field:'presTime',title:'处方时间',width:fixWidth(0.05)},
	        {field:'hospital',title:'医疗机构',width:fixWidth(0.08)},
	        {field:'disease',title:'主要诊断',width:fixWidth(0.09)},
	        {field:'money',title:'处方金额',width:fixWidth(0.05)},
	         {field:'btn',title:'操作',width:fixWidth(0.05),formatter:view}
	    ]]   
	});  
	$('#dgrid').datagrid({
		toolbar: '#tb'
	})
});
</script>
</head>
<body>
<div id="rejectScreeningContainer" style="height: 100%;width:50%;float:left;"></div>
<div style="height:100%;width:50%;float:left;">
	<table id="dgrid">
	</table>
	<div id="tb" class="datagrid-toolbar">
		<a href="#" class="easyui-linkbutton" data-options="iconCls:'',plain:true">加入可拒付列表</a>
	</div>
</div>
</body>