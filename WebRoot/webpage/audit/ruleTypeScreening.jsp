<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!-- context path -->
<t:base type="jquery,easyui"></t:base>
<script type="text/javascript" src="plug-in/Highcharts-4.0.1/js/highcharts.src.js"></script>
<script type="text/javascript" src="plug-in/Highcharts-4.0.1/js/modules/exporting.src.js"></script>
<script type="text/javascript" src="<%=basePath%>/webpage/audit/ruleTypeScreening.js"></script>
</head>
<body>
<c:set var="ctxPath" value="${pageContext.request.contextPath}" />

<div  class="easyui-layout" fit="true">
	<div region="west" style="overflow: hidden;" split="true" id="westDiv">
		<div id="step1Container"></div>
	</div>
	<div region="center" style="overflow: hidden;" split="true" id="centerDiv">
		<div id="step2Container"></div>
	</div>
</div>
</body>