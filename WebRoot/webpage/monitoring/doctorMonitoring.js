$(function(){
	var ruleUrl = 'webpage/monitoring/ruleChartData.json';
	$.getJSON(ruleUrl,function(data) {
		$('#doctorMoniteringWestDiv').highcharts({
			chart : {
				plotBackgroundColor : null,
				plotBorderWidth : null,
				plotShadow : false
			},
			title : {
				text : '医师疑点统计'
			},
			subtitle : {
				text : '',
				style : {
					display : 'none'
				}
			},
			credits : {
				enabled : false
			},
			legend : {},
			exporting : {
				enabled : false
			},
			tooltip : {
				pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions : {
				pie : {
					allowPointSelect : true,
					cursor : 'pointer',
					dataLabels : {
						enabled : true,
						format : '<b>{point.name}</b>: {point.percentage:.1f} %',
						style : {
							color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
									|| 'black'
						}
					}
				},
				series : {
					cursor : 'pointer',
					events : {
						click : function(e) {
							$('#doctorMoniteringCenterDiv1').datagrid('reload');
						}
					}
				}
			},
			series : data
		});
	});
});