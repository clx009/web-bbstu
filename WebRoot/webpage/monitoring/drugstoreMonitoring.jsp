<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/jquery/jquery-1.8.3.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/tools/dataformat.js"></script>
		<link id="easyuiTheme" rel="stylesheet"
			href="<%=basePath%>/plug-in/easyui/themes/default/easyui.css"
			type="text/css"></link>
		<link rel="stylesheet"
			href="<%=basePath%>/plug-in/easyui/themes/icon.css" type="text/css"></link>

		<link rel="stylesheet" type="text/css"
			href="<%=basePath%>/plug-in/accordion/css/accordion.css"></link>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/easyui/jquery.easyui.min.1.3.2.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/easyui/locale/easyui-lang-zh_CN.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/tools/syUtil.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>

		<script type="text/javascript"
			src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/highcharts.src.js"></script>
		<script type="text/javascript"
			src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/modules/exporting.src.js"></script>
		<script type="text/javascript" src="<%=basePath%>/webpage/monitoring/drugstoreMonitoring.js"></script>
		<script type="text/javascript">
			function view(value,rowData,rowIndex) {  
				return "<a href='javascript:void(0);' onclick='reloadSouthDiv()'>查看</a>";
			}
			function view2(){
				return "<a href='javascript:void(0);' onclick='showPreMainheadList()'>查看</a>";
				
			}
			function reloadSouthDiv(){
				$('#drugstoreMoniteringSouthDiv1').datagrid('reload');
			}
			function showPreMainheadList(){
				window.top.addTab('【益民药房】行为监控','preMainheadController.do?auditMonitoring','pictures');
			}
			$(function(){
				$('#dgridDoctorMonitoring').datagrid({  
					title:'违规药店列表', 
					method:'get', 
				    url:'<%=basePath%>/webpage/monitoring/datagrid_drugstoreViolations_data.json',   
				    singleSelect:true,
				    rownumbers:true,
				    columns:[[   
				        {field:'ck',title:'选择',width:fixWidth(0.05),checkbox:true},   
				        {field:'drugstoreId',title:'药店编号',width:fixWidth(0.1)},   
				        {field:'drugstore',title:'药店名称',width:fixWidth(0.1)}, 
				        {field:'address',title:'地址',width:fixWidth(0.09)}, 
				        {field:'violationsCount',title:'违规数',width:fixWidth(0.08)}, 
				        {field:'btn',title:'操作',width:fixWidth(0.07),formatter:view}
				    ]]   
				});  
				
				$('#drugstoreMoniteringSouthDiv1').datagrid({  
					title:'违规规则列表', 
					method:'get', 
				    url:'<%=basePath%>/webpage/monitoring/datagrid_patientViolationsRule_data.json',   
				    singleSelect:true,
				    rownumbers:true,
				    columns:[[   
				         {field:'ck',title:'选择',width:fixWidth(0.1),checkbox:true},   
						{field:'timeArea',title:'时间/时间段',width:fixWidth(0.18)},   
						{field:'ruleName',title:'规则名称',width:fixWidth(0.18)},   
						{field:'actualValue',title:'实际值',width:fixWidth(0.1)}, 
						{field:'thresholdValue',title:'阀值',width:fixWidth(0.1)}, 
						{field:'weight',title:'权重',width:fixWidth(0.1)}, 
						{field:'desc',title:'规则说明',width:fixWidth(0.18)}, 
						{field:'btn',title:'操作',width:fixWidth(0.1),formatter:view2}
				    ]]   
				});  
			});
		</script>
		<style>
			#highcharts-0{border-top:#95B8E7 1px solid;}
		</style>
	</head>
<body style="margin:0px;padding:0px;" fit="true">
	<div id="drugstoreMoniteringNorthDiv" style="float:left;width:100%;overflow:hidden;height:4%;"  fit="true">
		<table cellpadding="0" cellspacing="1">
		<tr>
				<td align="right" width="10%">
					<label class="Validform_label">开始时间:</label>
				</td>
				<td align="center" width="20%">
					<input id="drugstore_createTime_begin"   class="easyui-datebox" name="drugstore_createTime_begin" type="text"/>
				</td>
				<td align="right" width="10%" >
					<label class="Validform_label">结束时间:</label>
				</td>
				<td align="center" width="20%">
					<input id="drugstore_createTime_end" class="easyui-datebox" name="drugstore_createTime_end" type="text"/>
				</td>
				<td align="left" width="40%">
					&nbsp;&nbsp;&nbsp;&nbsp;<a name="submitIdByPatient" href="#" class="easyui-linkbutton" type="button" iconcls="icon-search" >查询</a>
				</td>
		</tr>
		</table>
	</div>
    <div id="drugstoreMoniteringWestDiv" style="float:left;width:50%;height:50%;"  fit="true"></div>  
    
    <div id="drugstoreMoniteringCenterDiv" style="overflow:hidden;float:left;width:50%;height:50%;" fit="true">
    	<div id="dgridDoctorMonitoring" style="width:100%;height:100%;" fit="true"></div>
    </div>  
    <div id="drugstoreMoniteringSouthDiv" style="overflow:hidden;float:left;width:100%;height:46%;" fit="true">
    	<div id="drugstoreMoniteringSouthDiv1" fit="true"></div>
    </div>  
</body>
</html>