function addOneTab(subtitle, url, icon) {
	if (icon == '') {
		icon = 'icon folder';
	}
	window.top.$.messager.progress({
		text : '页面加载中....',
		interval : 300
	});
	window.top.$('#maintabs').tabs({
		onClose : function(subtitle, index) {
			window.top.$.messager.progress('close');
		}
	});
	if (window.top.$('#maintabs').tabs('exists', subtitle)) {
		window.top.$('#maintabs').tabs('select', subtitle);
		window.top.$('#maintabs').tabs('update', {
			tab : window.top.$('#maintabs').tabs('getSelected'),
			options : {
				title : subtitle,
				href : url,
				// content : '<iframe name="tabiframe" scrolling="no"
				// frameborder="0" src="' + url + '"
				// style="width:100%;height:99%;"></iframe>',
				closable : true,
				icon : icon
			}
		});
	} else {
		if (url.indexOf('isIframe') != -1) {
			window.top
					.$('#maintabs')
					.tabs(
							'add',
							{
								title : subtitle,
								content : '<iframe src="'
										+ url
										+ '" frameborder="0" style="border:0;width:100%;height:99.4%;"></iframe>',
								closable : true,
								icon : icon
							});
		} else {
			window.top.$('#maintabs').tabs('add', {
				title : subtitle,
				href : url,
				closable : true,
				icon : icon
			});
		}
	}
}

$(function(){
	var ruleUrl = 'webpage/monitoring/ruleChartData.json';
	$.getJSON(ruleUrl,function(data) {
		$('#drugstoreMoniteringWestDiv').highcharts({
			chart : {
				plotBackgroundColor : null,
				plotBorderWidth : null,
				plotShadow : false
			},
			title : {
				text : '药店疑点统计'
			},
			subtitle : {
				text : '',
				style : {
					display : 'none'
				}
			},
			credits : {
				enabled : false
			},
			legend : {},
			exporting : {
				enabled : false
			},
			tooltip : {
				pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions : {
				pie : {
					allowPointSelect : true,
					cursor : 'pointer',
					dataLabels : {
						enabled : true,
						format : '<b>{point.name}</b>: {point.percentage:.1f} %',
						style : {
							color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
									|| 'black'
						}
					}
				},
				series : {
					cursor : 'pointer',
					events : {
						click : function(e) {
							$('#dgridDoctorMonitoring').datagrid('reload');
						}
					}
				}
			},
			series : data
		});
	});
});