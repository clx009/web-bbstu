<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE HTML>
<html>
	<head>
		<script type="text/javascript" src="<%=basePath%>/plug-in/jquery/jquery-1.8.3.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/tools/dataformat.js"></script>
	<link id="easyuiTheme" rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/default/easyui.css" type="text/css"></link>
	<link rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/icon.css" type="text/css"></link>
	<link rel="stylesheet" type="text/css" href="<%=basePath%>/plug-in/accordion/css/accordion.css">
	<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/jquery.easyui.min.1.3.2.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/locale/easyui-lang-zh_CN.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/tools/syUtil.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>
	<link rel="stylesheet" href="<%=basePath%>/plug-in/tools/css/common.css" type="text/css"></link>
	<script type="text/javascript" src="<%=basePath%>/plug-in/lhgDialog/lhgdialog.min.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/tools/curdtools.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/tools/easyuiextend.js"></script>
	<script type="text/javascript"
		src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>
	<script type="text/javascript"
		src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/highcharts.src.js"></script>
	<script type="text/javascript"
		src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/modules/exporting.src.js"></script>
		<script type="text/javascript" src="<%=basePath%>/webpage/monitoring/medicalInstitutionMonitoring.js"></script>
		<script type="text/javascript">
			function view(value,rowData,rowIndex) {  
				return "<a href='javascript:void(0);' onclick='reloadSouthDiv()'>查看</a>";
			}
			function view2(){
				return "<a href='javascript:void(0);' onclick='showPreMainheadList()'>查看</a>";
				
			}
			function reloadSouthDiv(){
				$('#medicalInstitutionMoniteringSouthDiv1').datagrid('reload');
			}
			function showPreMainheadList(){
				window.top.addTab('【沧州市中心医院】行为监控','preMainheadController.do?auditMonitoring','pictures');
			}
			$(function(){
				$('#dgridDoctorMonitoring').datagrid({  
					title:'违规医疗机构列表', 
					method:'get', 
				    url:'<%=basePath%>/webpage/monitoring/datagrid_medicalInstitutionViolations_data.json',   
				    singleSelect:true,
				    rownumbers:true,
				    columns:[[   
				        {field:'ck',title:'选择',width:fixWidth(0.05),checkbox:true},   
				        {field:'medicalInstitutionId',title:'医疗机构编号',width:fixWidth(0.1)},   
				        {field:'medicalInstitution',title:'医疗机构名称',width:fixWidth(0.1)}, 
				        {field:'address',title:'地址',width:fixWidth(0.1)}, 
				        {field:'violationsCount',title:'违规数',width:fixWidth(0.08)}, 
				        {field:'btn',title:'操作',width:fixWidth(0.05),formatter:view}
				    ]]   
				});  
				
				$('#medicalInstitutionMoniteringSouthDiv1').datagrid({  
					title:'违规规则列表', 
					method:'get', 
				    url:'<%=basePath%>/webpage/monitoring/datagrid_patientViolationsRule_data.json',   
				    singleSelect:true,
				    rownumbers:true,
				    columns:[[   
						{field:'ck',title:'选择',width:fixWidth(0.1),checkbox:true},   
						{field:'timeArea',title:'时间/时间段',width:fixWidth(0.18)},   
						{field:'ruleName',title:'规则名称',width:fixWidth(0.18)},   
						{field:'actualValue',title:'实际值',width:fixWidth(0.1)}, 
						{field:'thresholdValue',title:'阀值',width:fixWidth(0.1)}, 
						{field:'weight',title:'权重',width:fixWidth(0.1)}, 
						{field:'desc',title:'规则说明',width:fixWidth(0.18)}, 
						{field:'btn',title:'操作',width:fixWidth(0.1),formatter:view2}
				    ]]   
				});  
			});
		</script>
		<style>
			#highcharts-0{border-top:#95B8E7 1px solid;}
		</style>
	</head>
<body style="margin:0px;padding:0px;"  fit="true">
	<div id="medicalInstitutionMoniteringNorthDiv" style="float:left;width:100%;overflow:hidden;height:4%;"  fit="true">
		<table cellpadding="0" cellspacing="1">
		<tr>
				<td align="right" width="10%">
					<label class="Validform_label">开始时间:</label>
				</td>
				<td align="center" width="20%">
					<input id="medicalInstitution_createTime_begin"   class="easyui-datebox" name="medicalInstitution_createTime_begin" type="text"/>
				</td>
				<td align="right" width="10%" >
					<label class="Validform_label">结束时间:</label>
				</td>
				<td align="center" width="20%">
					<input id="medicalInstitution_createTime_end" class="easyui-datebox" name="medicalInstitution_createTime_end" type="text"/>
				</td>
				<td align="left" width="40%">
					&nbsp;&nbsp;&nbsp;&nbsp;<a name="submitIdByPatient" href="#" class="easyui-linkbutton" type="button" iconcls="icon-search" >查询</a>
				</td>
		</tr>
		</table>
	</div>
    <div id="medicalInstitutionMoniteringWestDiv" fit="true" style="float:left;width:50%;height:50%;"></div>  
    
    <div id="medicalInstitutionMoniteringCenterDiv" fit="true" style="overflow:hidden;float:left;width:50%;height:50%;">
    	<div id="dgridDoctorMonitoring" fit="true" style="height:100%;"></div>
    </div>  
    <div id="medicalInstitutionMoniteringSouthDiv" fit="true" style="overflow:hidden;float:left;width:100%;height:46%;">
    	<div id="medicalInstitutionMoniteringSouthDiv1"  fit="true"></div>
    </div>  
</body>
</html>