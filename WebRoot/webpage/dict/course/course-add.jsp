<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>科目管理</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="courseController.do?doAdd" tiptype="3">
				<input id="id" name="id" type="hidden" value="${coursePage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">name:</label>
		     	 <input id="name" name="name" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">comments:</label>
		     	 <input id="comments" name="comments" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </t:formvalid>
 </body>
  <script src = "webpage/dict/course/course.js"></script>		