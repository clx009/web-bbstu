function view1(value,rowData,rowIndex) {  
	return "<a href='javascript:void(0);' onclick='showPreMainheadListByDoctor();'>查看</a>";
}
function view2(value,rowData,rowIndex) {  
	return "<a href='javascript:void(0);' onclick='showMedicalgroupGrid();'>查看</a>";
}
function showMedicalgroupGrid(){
	$("#ruleGridContainer").hide();
	$("#medicalgroupGridContainer").clone(true).attr("id","medicalgroupGridContainerTmp").appendTo("#leftDiv");
	
}

function showPreMainheadListByDoctor(){
	window.top.addTab('【李玉堂】医师 待审核处方','preMainheadController.do?preMainheadByDoctor','picture'); 
}
		
$(function() {
	reloadChart();
});

function addOneTab(subtitle, url, icon) {
	if (icon == '') {
		icon = 'icon folder';
	}
	window.top.$.messager.progress({
		text : '页面加载中....',
		interval : 300
	});
	window.top.$('#maintabs').tabs({
		onClose : function(subtitle, index) {
			window.top.$.messager.progress('close');
		}
	});
	if (window.top.$('#maintabs').tabs('exists', subtitle)) {
		window.top.$('#maintabs').tabs('select', subtitle);
		window.top.$('#maintabs').tabs('update', {
			tab : window.top.$('#maintabs').tabs('getSelected'),
			options : {
				title : subtitle,
				href : url,
				// content : '<iframe name="tabiframe" scrolling="no"
				// frameborder="0" src="' + url + '"
				// style="width:100%;height:99%;"></iframe>',
				closable : true,
				icon : icon
			}
		});
	} else {
		if (url.indexOf('isIframe') != -1) {
			window.top
					.$('#maintabs')
					.tabs(
							'add',
							{
								title : subtitle,
								content : '<iframe src="'
										+ url
										+ '" frameborder="0" style="border:0;width:100%;height:99.4%;"></iframe>',
								closable : true,
								icon : icon
							});
		} else {
			window.top.$('#maintabs').tabs('add', {
				title : subtitle,
				href : url,
				closable : true,
				icon : icon
			});
		}
	}
}

function reloadChart() {
	var ruleUrl = './ruleChartData.json';
	var medicalgroupUrl = './medicalgroupChartData.json';
	$.getJSON(ruleUrl,function(data) {
		$('#ruleChartContainer').highcharts({
			chart : {
				plotBackgroundColor : null,
				plotBorderWidth : null,
				plotShadow : false
			},
			title : {
				text : '',
				style : {
					display : 'none'
				}
			},
			subtitle : {
				text : '',
				style : {
					display : 'none'
				}
			},
			credits : {
				enabled : false
			},
			legend : {},
			exporting : {
				enabled : false
			},
			tooltip : {
				pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions : {
				pie : {
					allowPointSelect : true,
					cursor : 'pointer',
					dataLabels : {
						enabled : true,
						format : '<b>{point.name}</b>: {point.percentage:.1f} %',
						style : {
							color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
									|| 'black'
						}
					}
				},
				series : {
					cursor : 'pointer',
					events : {
						click : function(e) {
							$("#medicalgroupGridContainerTmp").remove();
							$("#ruleGridContainer").show();
							$('#dgridRule').datagrid('reload');
						}
					}
				}
			},
			series : data
		});
	});
	
	$.getJSON(medicalgroupUrl,function(data) {
		$('#medicalgroupChartContainer').highcharts({
			chart : {
				plotBackgroundColor : null,
				plotBorderWidth : null,
				plotShadow : false
			},
			title : {
				text : '',
				style : {
					display : 'none'
				}
			},
			subtitle : {
				text : '',
				style : {
					display : 'none'
				}
			},
			credits : {
				enabled : false
			},
			legend : {},
			exporting : {
				enabled : false
			},
			tooltip : {
				pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions : {
				pie : {
					allowPointSelect : true,
					cursor : 'pointer',
					dataLabels : {
						enabled : true,
						format : '<b>{point.name}</b>: {point.percentage:.1f} %',
						style : {
							color : (Highcharts.theme && Highcharts.theme.contrastTextColor)
									|| 'black'
						}
					}
				},
				series : {
					cursor : 'pointer',
					events : {
						click : function(e) {
							$('#dgridMedicalgroup').datagrid('reload');
						}
					}
				}
			},
			series : data
		});
	});
}