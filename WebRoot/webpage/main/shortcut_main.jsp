<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="/webpage/common/baseJsp.jsp"%>
<title> 宝宝留学系统</title>
<t:base type="jquery,easyui,tools,DatePicker,autocomplete"></t:base>
<link rel="shortcut icon" href="images/favicon.ico">
<style type="text/css">
a {
	color: Black;
	text-decoration: none;
}

a:hover {
	color: black;
	text-decoration: none;
}
a.easyui-menubutton{
	color: #333;}
.panel-with-icon{margin-left:23px;}
.fb{
	font-weight: bold;
}
</style>
<SCRIPT type="text/javascript">
	$(function() {
	
	});
</SCRIPT>
</head>
	<body class="easyui-layout" style="overflow-y: hidden" scroll="no" id="mainLayout">
		<div region="north" border="false" title="" style="background:url(plug-in/login/images/logo.jpg) no-repeat 6px 10px #37b337;border-bottom:#2c6f2d 1px solid; height:58px;height:55px\9;*height:57px;padding: 1px; overflow: hidden;">
									<ul class="shortcut">
										${primaryMenuList }
									</ul>
								
					<div style="background: url(plug-in/login/images/top_right.jpg) no-repeat left top;position:absolute;right:0;top:0;color:#fff;text-align:left;">
										<div style="float: left; line-height:19px; margin-left: 70px;">
											<span>当前用户:</span>
											<span>${userName }</span>&nbsp;&nbsp;&nbsp;&nbsp;
										</div>
										<div style="float: left; margin-left: 18px;">
											<div style="right: 0px; bottom: 0px;">
												<a href="javascript:void(0);" class="easyui-menubutton" menu="#layout_north_kzmbMenu" iconCls="icon-comturn" style="height:19px;padding:0;margin:0;">
													控制面板
												</a>&nbsp;&nbsp;
												<a href="javascript:void(0);" class="easyui-menubutton" menu="#layout_north_zxMenu" iconCls="icon-exit" style="height:19px;padding:0;margin:0;">
													注销
												</a>
											</div>
											<div id="layout_north_kzmbMenu" style="width: 100px; display: none;">
												<div onclick="openwindow('用户信息','userController.do?userinfo')">个人信息</div>
												<div class="menu-sep"></div>
												<div onclick="add('修改密码','userController.do?changepassword')">修改密码</div>
												<!--<div class="menu-sep"></div>
												 <div onclick="add('修改首页风格','userController.do?changestyle')">首页风格</div> -->
											</div>
											<div id="layout_north_zxMenu" style="width: 100px; display: none;">
												<div class="menu-sep"></div>
												<div onclick="exit('loginController.do?logout','确定退出该系统吗 ?',1);">退出系统</div>
											</div>
										</div>
									</div>
								

		</div>
		<div region="west" split="false" id="left_menu__new" href="loginController.do?shortcut_top" title="导航菜单" iconCls="icon-mainnav" style="width: 170px; padding: 1px;"></div>
		<div id="mainPanle" region="center" style="overflow: hidden;">
			<div id="maintabs" class="easyui-tabs" fit="true" border="false">
				<div class="easyui-tab" title="工作桌面" href="loginController.do?home" style="padding: 2px; overflow: hidden;"></div>
				<c:if test="${map=='1'}">
					<div class="easyui-tab" title="地图" style="padding: 1px; overflow: hidden;">
						<iframe name="myMap" id="myMap" scrolling="no" frameborder="0" src="mapController.do?map" style="width: 100%; height: 99.5%;"></iframe>
					</div>
				</c:if>
			</div>
		</div>
		 <%
		 	String roleCodes=request.getSession().getAttribute("roleCodes").toString().trim();
			String urlStr = "";
			if((roleCodes.indexOf("sysmgr")!=-1)||(roleCodes.indexOf("admin")!=-1)){
				urlStr="/webpage/main/adminEastDiv.jsp";
			}else{
				urlStr="/webpage/main/normalEastDiv.jsp";
			}
		 	
		 %>
		 <jsp:include page="<%=urlStr%>" flush="true" ></jsp:include>
		<div region="south" border="false" style="height: 25px; overflow: hidden;">
			<div align="center" style="color: #2caf2d; padding-top: 2px">
				&copy; 版权所有 <span class="tip"> 宝宝留学系统 V2.0 (推荐谷歌浏览器，获得更快响应速度) </span>
			</div>
		</div>
		<div id="mm" class="easyui-menu" style="width: 150px;">
			<div id="mm-tabupdate">刷新</div>
			<div id="mm-tabclose">关闭</div>
			<div id="mm-tabcloseall">关闭全部</div>
			<div id="mm-tabcloseother">关闭其他标签页</div>
			<div class="menu-sep"></div>
			<div id="mm-tabcloseleft">关闭左侧标签页</div>
		</div>
	</body>
</html>