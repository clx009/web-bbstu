﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
<head>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!-- context path -->
<t:base type="jquery,easyui"></t:base>
</head>
<body>
<%
	String roleCodes=request.getSession().getAttribute("roleCodes").toString().trim();
	String urlStr = "";
	if(roleCodes.indexOf("sysmgr")!=-1){
		urlStr="webpage/desktop/sysmgrDesktop.jsp";
	}else if(roleCodes.indexOf("firstaudit")!=-1){
		urlStr="webpage/desktop/firstauditDesktop.jsp";
	}else if(roleCodes.indexOf("reaudit")!=-1){
		urlStr="webpage/desktop/reauditDesktop.jsp";
	}else if(roleCodes.indexOf("approval")!=-1){
		urlStr="webpage/desktop/approvalDesktop.jsp";
	}else if(roleCodes.indexOf("leader")!=-1){
		urlStr="webpage/desktop/leaderDesktop.jsp";
	}else if(roleCodes.indexOf("headLeader")!=-1){
		urlStr="webpage/desktop/headLeaderDesktop.jsp";
	}else if(roleCodes.indexOf("hospLeader")!=-1){
		urlStr="webpage/desktop/hospLeaderDesktop.jsp";
	}else{
		urlStr="webpage/desktop/welcome.jsp";
	}

%>
<iframe  frameborder=”no” border=”0″ src="<%=urlStr %>" width="100%" height="100%">
</iframe>
</body>