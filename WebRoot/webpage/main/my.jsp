<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@include file="/webpage/common/baseJsp.jsp"%>
<!-- context path -->
<c:set var="ctxPath" value="${pageContext.request.contextPath}" />
<html>
  <head>
  	<script type="text/javascript" src="<%=basePath%>/plug-in/jquery/jquery-1.8.3.js"></script>
  	<script type="text/javascript" src="<%=basePath%>/plug-in/tools/dataformat.js"></script>
  	<link id="easyuiTheme" rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/default/easyui.css" type="text/css"></link>
  	<link rel="stylesheet" href="<%=basePath%>/plug-in/easyui/themes/icon.css" type="text/css"></link>
  	
  	<link rel="stylesheet" type="text/css" href="<%=basePath%>/plug-in/accordion/css/accordion.css"></link>
  	<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/jquery.easyui.min.1.3.2.js"></script>
  	<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/locale/easyui-lang-zh_CN.js"></script>
  	<script type="text/javascript" src="<%=basePath%>/plug-in/tools/syUtil.js"></script>
  	<script type="text/javascript" src="<%=basePath%>/plug-in/easyui/extends/datagrid-scrollview.js"></script>


  	<script type="text/javascript" src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/highcharts.src.js"></script>
	<script type="text/javascript" src="<%=basePath%>/plug-in/Highcharts-4.0.1/js/modules/exporting.src.js"></script>
	<script type="text/javascript" src="<%=basePath%>/webpage/main/my.js"></script>
	<script type="text/javascript">

	</script>
	
  </head>
  <body>
  	<div class="easyui-layout" data-options="fit:true">
	<div id="leftDiv" class="easyui-panel"
		data-options="region:'west',split:false,border:true"
		style="width: 700px;height:1000px;float:left;">
		<div style="font-size: 21px;text-align: center">规则比例图</div>
		<div id="ruleChartContainer" style="width: 695px;height:300px;float:left;text-align: center;"></div>
		<div id="ruleGridContainer">
			<table id="dgridRule" class="easyui-datagrid" style="width:698px;height:300px" title="医疗机构列表" data-options="singleSelect:true,rownumbers:true,url:'<%=basePath%>/webpage/main/datagrid_rule_data.json',method:'get'">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true"></th>
						<th data-options="field:'hospital',width:300">医疗机构</th>
						<th data-options="field:'prescriptionCount',width:200">处方量</th>
						<th data-options="field:'btn',width:120,formatter:view2">操作</th>
					</tr>
				</thead>
		    </table>
		</div>

	</div>
	<div class="easyui-panel"
		data-options="region:'center',split:false,border:true"
		style="width: 700px;height:1000px;float:left;">
		<div style="font-size: 21px;text-align: center">医疗机构比例图</div>
		<div id="medicalgroupChartContainer" style="width: 695px;height:300px;text-align: center;"></div>
		<div id="medicalgroupGridContainer"">
			<table id="dgridMedicalgroup" class="easyui-datagrid" style="width:698px;height:300px" title="医师列表" data-options="singleSelect:true,rownumbers:true,url:'<%=basePath%>/webpage/main/datagrid_medicalgroup_data.json',method:'get'">
				<thead>
					<tr>
						<th data-options="field:'ck',checkbox:true"></th>
						<th data-options="field:'doctor',width:300">医师</th>
						<th data-options="field:'prescriptionCount',width:200">处方量</th>
						<th data-options="field:'btn',width:100,formatter:view1">操作</th>
					</tr>
				</thead>
		    </table>
		</div>
	</div>
</div>
</body>
</html>
