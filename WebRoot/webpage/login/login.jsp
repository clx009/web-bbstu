<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<%
	String path = request.getContextPath();
%>
<title>宝宝留学系统</title>
<link href="plug-in/login/css/zice.style.css" rel="stylesheet" type="text/css" />
<link href="plug-in/login/css/buttons.css" rel="stylesheet" type="text/css" />
<link href="plug-in/login/css/icon.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="plug-in/login/css/tipsy.css" media="all" />
<style>
body, form, p, a, input, i{margin:0; padding:0;}
	body{font:16px "Microsoft Yahei"; background:url(<%=path%>/plug-in/login/images/loginBg.jpg) top center; height:100%;}
	form{ width:410px; height:320px; position:absolute; left:50%; margin:270px 0 0 -205px;}
		p{ margin:16px 0;}
		p i{float:left; height:30px;line-height:30px;font-style:normal; color:#fff; display:inline-block; width:110px; text-align:right;}
		p input.txt{ height:25px;line-height:25px; font-size:16px; padding:2px 10px;width:235px; border:0;}
		p input.yzm{width:63px; }
		input{margin-right:5px;}
			.w90{width:95px;float:none; font-size:13px;}
			p.but{ padding-left:110px;}
			p.but input{ border:0; background:url(<%=path%>/plug-in/login/images/loginBut.jpg) top left; width:82px; height:35px; cursor:pointer;}
			p.but input.reset{background:url(<%=path%>/plug-in/login/images/loginBut.jpg) 85px 0;}
</style>
</head>

<body> 
<div id="alertMessage"></div>
    <div id="successLogin"></div>
    <form name="formLogin" id="formLogin" action="loginController.do?login" check="loginController.do?checkuser" method="post">
        <input name="userKey" type="hidden" id="userKey" value="D1B5CC2FE46C4CC983C073BCA897935608D926CD32992B5900" />
    	<p><i>用户名：</i><input class="txt" name="userName" type="text" id="userName" title="用户名" iscookie="true" value="" nullmsg="请输入用户名!" /></p>
    	<p><i>密&nbsp;&nbsp;&nbsp;码：</i><input class="txt" name="password" type="password" id="password" title="密码" value="" nullmsg="请输入密码!" /></p>
        <p><i>验证码：</i><input class="txt yzm"  name="randCode" type="text" id="randCode"  title="" value="" nullmsg="请输入验证码!" /><img align="absmiddle" id="randCodeImage" src="randCodeImage" height="28" /><i class="w90">  <input id="on_off"  name="remem" type="checkbox"  value="0" checked="true"/>记住用户名</i></p>
        <p class="but">
          <input name="tijiao"  id="but_login" type="button" title="提交" value="" />
          <input id="forgetpass" name="chongzhi" type="reset" class="reset" title="重置"  value="" />
        </p>
	</form>
</body>
</html>
    <script type="text/javascript" src="plug-in/jquery/jquery-1.8.3.min.js"></script>
    <script type="text/javascript" src="plug-in/jquery/jquery.cookie.js"></script>
    <script type="text/javascript" src="plug-in/login/js/jquery-jrumble.js"></script>
    <script type="text/javascript" src="plug-in/login/js/jquery.tipsy.js"></script>
    <script type="text/javascript" src="plug-in/login/js/iphone.check.js"></script>
    <script type="text/javascript" src="plug-in/login/js/login.js"></script>
    <script type="text/javascript" src="plug-in/lhgDialog/lhgdialog.min.js"></script>