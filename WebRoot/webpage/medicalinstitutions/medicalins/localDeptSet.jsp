<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
	<div region="center" style="padding: 0px;" border="false">
		<t:datagrid name="medicalInsbaseLocal__DeptList" fitColumns="false" checkbox="true" title="地区科室管理" actionUrl="medicalInstitutionsController.do?localDeptsDatagrid&hosCode=${hosCode}" idField="id" fit="true" queryMode="group">
			<t:dgCol title="编号"  field="id"  hidden="false"  queryMode="group" ></t:dgCol>
			<t:dgCol title="地区科室编码"  field="locDeptCode"  hidden="true" query="true" queryMode="single"  ></t:dgCol>
			<t:dgCol title="地区科室名称"  field="locDeptName"  hidden="true" query="true" queryMode="single" ></t:dgCol>
			<t:dgCol title="医疗机构编码"  field="hosCode"  hidden="false"  queryMode="group"></t:dgCol>
			<t:dgCol title="医疗机构名称"  field="hosName"  hidden="false"  queryMode="group"></t:dgCol>
			<t:dgCol title="标准科室码"  field="staDeptCode"  hidden="true"  queryMode="group"></t:dgCol>
			<t:dgCol title="标准科室名称"  field="staDeptName"  hidden="true"  queryMode="group"></t:dgCol>
			<t:dgCol title="操作" field="opt"></t:dgCol>
			<t:dgDelOpt title="删除" url="medicalInstitutionsController.do?doDel&id={id}" />
			<t:dgToolBar title="添加" icon="icon-add" url="medicalInstitutionsController.do?goLocalDeptAdd&hosCode=${hosCode}&hosName=${hosName}" funname="add"></t:dgToolBar>
			<t:dgToolBar title="编辑" icon="icon-edit" url="medicalInstitutionsController.do?goLocalDeptUpdate&hosCode=${hosCode}&hosName=${hosName}" funname="update"></t:dgToolBar>
			<t:dgToolBar title="查看" icon="icon-search" url="medicalInstitutionsController.do?goLocalDeptUpdate" funname="detail"></t:dgToolBar>
			<t:dgToolBar title="批量删除"  icon="icon-remove" url="medicalInstitutionsController.do?doLocalDeptBatchDel" funname="deleteALLSelect"></t:dgToolBar>
		</t:datagrid>
	</div>
</div>
<script type="text/javascript">
	$(function(){
		editOnClick("medicalInstitutionsController.do?goLocalDeptUpdate","medicalInsbaseLocal__DeptList","1");
	});
</script>