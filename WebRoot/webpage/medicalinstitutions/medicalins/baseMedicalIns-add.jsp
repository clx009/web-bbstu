<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>BASE_MEDICAL_INS</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
  <script type="text/javascript" src="plug-in/ckeditor_new/ckeditor.js"></script>
  <script type="text/javascript" src="plug-in/ckfinder/ckfinder.js"></script>
  <script type="text/javascript">
  //编写自定义JS代码
  </script>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="medicalInstitutionsController.do?doAdd" tiptype="1">
					<input id="id" name="id" type="hidden" value="${baseMedicalInsPage.id }">
		<table style="width: 600px;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right">
						<label class="Validform_label">
							医疗机构代码:
						</label>
					</td>
					<td class="value">
					     	 <input id="insCode" name="insCode" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">医疗机构代码</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							医疗机构名称:
						</label>
					</td>
					<td class="value">
					     	 <input id="insName" name="insName" type="text" style="width: 150px" class="inputxt"  
								               
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">医疗机构名称</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							机构类型:
						</label>
					</td>
					<td class="value">
							  <t:dictSelect field="insType" type="list"
									typeGroupCode="instype" defaultVal="${baseMedicalInsPage.insType}" hasLabel="false"  title="机构类型"></t:dictSelect>     
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">机构类型</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							机构类别:
						</label>
					</td>
					<td class="value">
							  <t:dictSelect field="insCat" type="list"
									typeGroupCode="inscat" defaultVal="${baseMedicalInsPage.insCat}" hasLabel="false"  title="机构类别"></t:dictSelect>     
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">机构类别</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							机构等级:
						</label>
					</td>
					<td class="value">
							  <t:dictSelect field="insGrade" type="list"
									typeGroupCode="insgrade" defaultVal="${baseMedicalInsPage.insGrade}" hasLabel="false"  title="机构等级"></t:dictSelect>     
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">机构等级</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							所属区域:
						</label>
					</td>
					<td class="value">
					     	 <input id="areaCode" name="areaCode" type="text" value="${baseMedicalInsPage.areaCode}" style="width: 150px" class="inputxt" readonly="readonly"  >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">所属区域</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							机构地址:
						</label>
					</td>
					<td class="value">
						  	 <textarea id="addr" name="addr"></textarea>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">机构地址</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							负责人:
						</label>
					</td>
					<td class="value">
					     	 <input id="manager" name="manager" type="text" style="width: 150px" class="inputxt"  
								               
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">负责人</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							联系电话:
						</label>
					</td>
					<td class="value">
					     	 <input id="tel" name="tel" type="text" style="width: 150px" class="inputxt"  
								               
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">联系电话</label>
						</td>
				</tr>
			</table>
		</t:formvalid>
 </body>
  <script src = "webpage/basedata/baseMedicalIns/baseMedicalIns.js"></script>		