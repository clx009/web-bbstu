<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<script type="text/javascript">
$(function() {
	$('#baseMedicalInsList').datagrid({
		onSelect: function(rowIndex, rowData){
			setLocalDeptsByMedicalInsCode(rowData.insCode, rowData.insName);
		}
	});
})
</script>
<div class="easyui-layout" fit="true">
	<div region="center" style="padding: 0px;" border="false">
		<t:datagrid name="baseMedicalInsList" fitColumns="false" title="医疗机构列表" actionUrl="medicalInstitutionsController.do?datagrid&areaCode=${areaCode}" idField="id" queryMode="group">
		   <t:dgCol title="编号"  field="id"  hidden="false"  queryMode="group"  ></t:dgCol>
		   <t:dgCol title="医疗机构代码"  field="insCode"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
		   <t:dgCol title="医疗机构名称"  field="insName"  hidden="true" query="true" queryMode="single"   ></t:dgCol>
		   <t:dgCol title="机构类型"  field="insType"  hidden="true" query="true" queryMode="single" dictionary="instype"  ></t:dgCol>
		   <t:dgCol title="机构类别"  field="insCat"  hidden="true"  queryMode="group" dictionary="inscat"></t:dgCol>
		   <t:dgCol title="机构等级"  field="insGrade"  hidden="true" query="true" queryMode="single" dictionary="insgrade" ></t:dgCol>
		   <t:dgCol title="所属区域"  field="areaCode"  hidden="true"  queryMode="group" ></t:dgCol>
		   <t:dgCol title="机构地址"  field="addr"  hidden="false"  queryMode="group"  ></t:dgCol>
		   <t:dgCol title="负责人"  field="manager"  hidden="false"  queryMode="group" ></t:dgCol>
		   <t:dgCol title="联系电话"  field="tel"  hidden="false"  queryMode="group" ></t:dgCol>
		   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
		   <t:dgDelOpt title="删除" url="medicalInstitutionsController.do?doDel&id={id}" />
		   <t:dgToolBar title="添加" icon="icon-add" url="medicalInstitutionsController.do?goAdd&areaCode=${areaCode}" funname="add"></t:dgToolBar>
		   <t:dgToolBar title="编辑" icon="icon-edit" url="medicalInstitutionsController.do?goUpdate&areaCode=${areaCode}" funname="update"></t:dgToolBar>
		   <t:dgToolBar title="查看" icon="icon-search" url="medicalInstitutionsController.do?goUpdate" funname="detail"></t:dgToolBar>
		  </t:datagrid>
	</div>
</div>
<div region="east" style="width: 480px; overflow: hidden;" split="true">
	<div class="easyui-panel" style="padding: 1px;" fit="true" border="false" id="localDeptList-panel"></div>
</div>
<script type="text/javascript">
	function setLocalDeptsByMedicalInsCode(insCode,insName) {
		$("#localDeptList-panel").panel(
			{
				title :"【"+insName+"】机构有以下科室:",
				href:"medicalInstitutionsController.do?funLocalDepts&hosCode=" +insCode + "&hosName=" + insName
			}
		);
		$('#localDeptList-panel').panel("refresh" );
	}

	$(function(){
		editOnClick("medicalInstitutionsController.do?goUpdate","baseMedicalInsList","1");
	});
	
</script>
