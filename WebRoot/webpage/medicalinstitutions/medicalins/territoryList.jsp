<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<script type="text/javascript">
$(function() {
	$('#medicalIns__TerritoryList').treegrid({
		onClickCell: function(field,row){
			setMedicalInsByArea(row.id, row.text);
		}
	});
})
</script>

<div id="system_territory_territoryList" class="easyui-layout" fit="true">
	<div region="west" style="width: 200px;" border="false" split="true" >
		
		<table title="地域列表" id="medicalIns__TerritoryList" name="_territoryList" class="easyui-treegrid" pagination="false"
            data-options="
                url: 'basePatientController.do?territoryGrid',
                method: 'get',
                rownumbers: true,
                idField: 'id',
                fit:true,
                treeField: 'text'
            ">
	        <thead>
	            <tr>
	                <th data-options="field:'id',hidden:true"  >编号</th>
	                <th data-options="field:'text'" width="120"  align="left">地域名称</th>
	            </tr>
	        </thead>
	    </table>
		
	</div>
	<div region="center" >
		<div class="easyui-panel"  fit="true" border="false" id="medicalinsPanel"></div>
	</div>
</div>


<script type="text/javascript">
	function setMedicalInsByArea(areaCode,territoryName) {
		$("#medicalinsPanel").panel(
			{
				title :territoryName+"有以下医疗机构：",
				href:"medicalInstitutionsController.do?fun&areaCode="+areaCode
			}
		);
		$('#medicalinsPanel').panel("refresh" );
	}
</script>