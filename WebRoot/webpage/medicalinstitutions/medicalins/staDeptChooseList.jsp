<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html >
<html>
<head>
<title>标准科室</title>
<t:base type="jquery,easyui,tools"></t:base>
</head>
<body style="overflow-y: hidden" scroll="no">
<t:datagrid name="staDeptsList" title="标准科室选择"  fit="true" actionUrl="baseLocalDeptController.do?datagridStaDepts" idField="id" showRefresh="false" queryMode="group">
	<t:dgCol title="科室编号" field="staDeptCode" queryMode="single"  query="true"  ></t:dgCol>
	<t:dgCol title="科室名称" field="deptName" queryMode="single"  query="true" width="40"  ></t:dgCol>
</t:datagrid>
</body>
</html>
