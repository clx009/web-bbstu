<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>地区科室管理</title>
		<t:base type="jquery,easyui,tools,DatePicker"></t:base>
		<script type="text/javascript" src="plug-in/ckeditor_new/ckeditor.js"></script>
		<script type="text/javascript" src="plug-in/ckfinder/ckfinder.js"></script>
		<script type="text/javascript">
	//编写自定义JS代码
</script>
	</head>
	<body>
		<t:formvalid formid="formobj" dialog="true" usePlugin="password"
			layout="table" action="medicalInstitutionsController.do?doLocalDeptAdd" tiptype="1">
			<input id="id" name="id" type="hidden" value="${baseLocalDeptPage.id }">
			<table style="width: 600px;" cellpadding="0" cellspacing="1"
				class="formtable">
				<tr>
					<td align="right">
						<label class="Validform_label">
							地区科室编码:
						</label>
					</td>
					<td class="value">
						<input id="locDeptCode" name="locDeptCode" type="text"
							style="width: 150px" class="inputxt" datatype="*">
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							地区科室编码
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							地区科室名称:
						</label>
					</td>
					<td class="value">
						<input id="locDeptName" name="locDeptName" type="text"
							style="width: 150px" class="inputxt">
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							地区科室名称
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							医疗机构编码:
						</label>
					</td>
					<td class="value">
						<input id="hosCode" name="hosCode" type="text" value="${baseLocalDeptPage.hosCode}" readonly="readonly"  style="width: 150px" class="inputxt" datatype="*">
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							医疗机构编码
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							医疗机构名称:
						</label>
					</td>
					<td class="value">
						<input id="hosName" name="hosName" type="text" value="${baseLocalDeptPage.hosName}" readonly="readonly" style="width: 150px" class="inputxt">
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							医疗机构名称
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							标准科室码:
						</label>
					</td>
					<td class="value">
						<input name="staDeptCode" value="${staDeptCode}" id="staDeptCode" readonly="readonly">
						<t:choose hiddenName="staDeptCode" hiddenid="staDeptCode"
							url="baseLocalDeptController.do?stadepts" name="staDeptsList" icon="icon-search"
							title="标准科室列表" textname="deptName" isclear="true"></t:choose>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							标准科室码
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							标准科室名称:
						</label>
					</td>
					<td class="value">
						<%-- 情况特殊 id和name值不一样  --%>
						<input id="deptName" name="staDeptName" type="text" value="${deptName}" style="width: 150px" class="inputxt" readonly="readonly">
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							标准科室名称
						</label>
					</td>
				</tr>
			</table>
		</t:formvalid>
		<script src="webpage/basedata/baseLocalDept/baseLocalDept.js"></script>
	</body>
	
</html>