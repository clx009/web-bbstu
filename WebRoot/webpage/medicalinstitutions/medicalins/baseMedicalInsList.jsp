<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="baseMedicalInsList" checkbox="true" fitColumns="false" title="医疗机构管理" actionUrl="baseMedicalInsController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="编号"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="医疗机构代码"  field="insCode"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="医疗机构名称"  field="insName"  hidden="true" query="true" queryMode="single"  width="120"></t:dgCol>
   <t:dgCol title="机构类型"  field="insType"  hidden="true" query="true" queryMode="single" dictionary="instype" width="120"></t:dgCol>
   <t:dgCol title="机构类别"  field="insCat"  hidden="true"  queryMode="group" dictionary="inscat" width="120"></t:dgCol>
   <t:dgCol title="机构等级"  field="insGrade"  hidden="true" query="true" queryMode="single" dictionary="insgrade" width="120"></t:dgCol>
   <t:dgCol title="所属区域"  field="areaCode"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="机构地址"  field="addr"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="负责人"  field="manager"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="联系电话"  field="tel"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="baseMedicalInsController.do?doDel&id={id}" />
   <t:dgToolBar title="添加" icon="icon-add" url="baseMedicalInsController.do?goAdd" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="baseMedicalInsController.do?goUpdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="批量删除"  icon="icon-remove" url="baseMedicalInsController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="baseMedicalInsController.do?goUpdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/basedata/baseMedicalIns/baseMedicalInsList.js"></script>		
 <script type="text/javascript">
 	$(function(){
		editOnClick("baseMedicalInsController.do?goUpdate","baseMedicalInsList","1");
	});
 </script>