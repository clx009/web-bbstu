<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="baseDoctorList" checkbox="true" fitColumns="false" title="医生管理" actionUrl="baseDoctorController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="编号"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="医师编号"  field="docCode"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="医师姓名"  field="docName"  hidden="true" query="true" queryMode="single"  width="120"></t:dgCol>
   <t:dgCol title="职称级别"  field="grade"  hidden="true" query="true" queryMode="single" dictionary="grade" width="120"></t:dgCol>
   <t:dgCol title="出生日期"  field="birthday" formatter="yyyy-MM-dd" hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="性别"  field="sex"  hidden="true"  queryMode="group" dictionary="sex" width="120"></t:dgCol>
   <t:dgCol title="所属科室代码"  field="deptCode"  hidden="true" query="true" queryMode="single"  width="120"></t:dgCol>
   <t:dgCol title="联系电话"  field="tel"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="所属医疗机构代码"  field="insCode"  hidden="true" query="true" queryMode="single"  width="120"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="baseDoctorController.do?doDel&id={id}" />
   <t:dgToolBar title="添加" icon="icon-add" url="baseDoctorController.do?goAdd" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="baseDoctorController.do?goUpdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="批量删除"  icon="icon-remove" url="baseDoctorController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="baseDoctorController.do?goUpdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/medicalinstitutions/baseDoctor/baseDoctorList.js"></script>		
 <script type="text/javascript">
 $(document).ready(function(){
	//给时间控件加上样式
	$("#baseDoctorListtb").find("input[name='birthday_begin']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
	$("#baseDoctorListtb").find("input[name='birthday_end']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
	editOnClick("baseDoctorController.do?goUpdate","baseDoctorList","1");
 });
 </script>