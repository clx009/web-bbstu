<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html >
<html>
<head>
<title>本地科室</title>
<t:base type="jquery,easyui,tools"></t:base>
</head>
<body style="overflow-y: hidden" scroll="no">
<t:datagrid name="medicalInslocalDeptsList" title="科室选择"  fit="true" actionUrl="baseDoctorController.do?datagridLocalDepts&hosCode=${hosCode}" idField="id" showRefresh="false" queryMode="group">
	<t:dgCol title="科室编号" field="locDeptCode" queryMode="single"  query="true"  ></t:dgCol>
	<t:dgCol title="科室名称" field="locDeptName" queryMode="single"  query="true" width="40"  ></t:dgCol>
</t:datagrid>
</body>
</html>
