<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html >
<html>
<head>
<title>本地医疗机构</title>
<t:base type="jquery,easyui,tools"></t:base>
</head>
<body style="overflow-y: hidden" scroll="no">
<t:datagrid name="localMedicalInsList" title="医疗机构选择"  fit="true" actionUrl="baseDoctorController.do?localMedicalInsDatagrid" idField="id" showRefresh="false" queryMode="group">
	<t:dgCol title="机构编号" field="insCode" queryMode="single"  query="true"  ></t:dgCol>
	<t:dgCol title="机构名称" field="insName" queryMode="single"  query="true" width="40"  ></t:dgCol>
</t:datagrid>
</body>
</html>
