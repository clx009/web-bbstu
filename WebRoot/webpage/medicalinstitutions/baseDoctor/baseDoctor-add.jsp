<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>医生管理</title>
		<t:base type="jquery,easyui,tools,DatePicker"></t:base>
		<script type="text/javascript" src="plug-in/ckeditor_new/ckeditor.js"></script>
		<script type="text/javascript" src="plug-in/ckfinder/ckfinder.js"></script>
		<script type="text/javascript">
	//编写自定义JS代码
</script>
	</head>
	<body>
		<t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="baseDoctorController.do?doAdd" tiptype="1">
			<input id="id" name="id" type="hidden" value="${baseDoctorPage.id }">
			<table style="width: 600px;" cellpadding="0" cellspacing="1"
				class="formtable">
				<tr>
					<td align="right">
						<label class="Validform_label">
							医师编号:
						</label>
					</td>
					<td class="value">
						<input id="docCode" name="docCode" type="text"
							style="width: 150px" class="inputxt" datatype="*">
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							医师编号
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							医师姓名:
						</label>
					</td>
					<td class="value">
						<input id="docName" name="docName" type="text"
							style="width: 150px" class="inputxt">
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							医师姓名
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							职称级别:
						</label>
					</td>
					<td class="value">
						<t:dictSelect field="grade" type="list" typeGroupCode="grade"
							defaultVal="${baseDoctorPage.grade}" hasLabel="false"
							title="职称级别"></t:dictSelect>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							职称级别
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							出生日期:
						</label>
					</td>
					<td class="value">
						<input id="birthday" name="birthday" type="text" style="width: 150px" class="Wdate" onClick="WdatePicker();">
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							出生日期
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							性别:
						</label>
					</td>
					<td class="value">
						<t:dictSelect field="sex" type="list" typeGroupCode="sex" defaultVal="${baseDoctorPage.sex}" hasLabel="false" title="性别"></t:dictSelect>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							性别
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							所属医疗机构代码:
						</label>
					</td>
					<td class="value">
						<input id="insCode" name="insCode" type="text" style="width: 150px" class="inputxt">
						<input name="insName" class="inputxt" type="hidden" id="insName"  datatype="*" />
						<t:choose hiddenName="insCode" hiddenid="insCode"
							url="baseDoctorController.do?localMedicalIns" name="localMedicalInsList" icon="icon-search"
							title="机构列表" textname="insName" isclear="true" fun="setMedicalInsDepts"></t:choose>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							所属医疗机构代码
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							所属科室代码:
						</label>
					</td>
					<td class="value">
						<input id="locDeptCode" name="deptCode" type="text" style="width: 150px" readonly="readonly" class="inputxt">
						<input name="locDeptName" class="inputxt" type="hidden" id="locDeptName"  datatype="*" />
						<a href='javascript:void(0);' id="deptSelect" disabled="true" class='easyui-linkbutton' plain='true' icon='icon-search'>选择</a>
						<a href='javascript:void(0);' id="deptCleaer" disabled="true" class='easyui-linkbutton' plain='true' icon='icon-redo' >清空</a>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							所属科室代码
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							联系电话:
						</label>
					</td>
					<td class="value">
						<input id="tel" name="tel" type="text" style="width: 150px"
							class="inputxt">
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							联系电话
						</label>
					</td>
				</tr>
			</table>
		</t:formvalid>
	</body>
	<script src="webpage/medicalinstitutions/baseDoctor/baseDoctor.js"></script>
	<script type="text/javascript">
		function listChoose(title,url){
			$.dialog({
				content: 'url:'+url,
				zIndex: 2022,
				title: title,
				lock : true,
				width :400,
				height :350,
				left :'65%',
				top :'45%',
				opacity : 0.4,
				button : [
							{
								name : '确认',
								callback : function() {
									iframe = this.iframe.contentWindow;
									var locDeptCode = "";
									locDeptCode = iframe.getmedicalInslocalDeptsListSelections('locDeptCode');
									$('#locDeptCode').val(locDeptCode);
									var locDeptName = "";
									locDeptName = iframe.getmedicalInslocalDeptsListSelections('locDeptName');
									$('#locDeptName').val(locDeptName);
									return true;
								},
								focus : true
							},
							{
								name : '取消',
								callback : function() {}
							} 
						]
				});
		}
		
		function clearsChoose(hiddenid,hiddenName,toname,textname){
			var textnamearr;
			if(textname){
				if(toname){
					textname = toname;
				}
				textnamearr = textname.split(",");
				for(var i=0; i<textnamearr.length;i++ ){
					if(textnamearr[i]){
						$('#' + textnamearr[i]).val("");
						//$('#' + textnamearr[i]).blur();
					}
				}
			}
			if(hiddenid!== undefined && hiddenid!=""){
				$('#'+ hiddenid).val("");
			}else{
				$('#'+ hiddenName).val("");
			}
		}
		
		function setMedicalInsDepts(){
			clearsChoose("locDeptCode","locDeptName","locDeptName","locDeptName");
			var url = "${pageContext.request.contextPath}/baseDoctorController.do?localDepts&hosCode="+$("#insCode").val();
			$("#deptSelect").attr("class","easyui-linkbutton l-btn l-btn-plain");
			$("#deptSelect").bind("click",function(){
				listChoose("科室选择",url);
			});
			$("#deptCleaer").attr("class","easyui-linkbutton l-btn l-btn-plain");
			$("#deptCleaer").bind("click",function(){
				clearsChoose("locDeptCode","locDeptName","locDeptName","locDeptName");
			});
		}
	</script>