<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>t_s_user</title>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
</head>
<body>
	<t:formvalid formid="formobj" dialog="true" usePlugin="password"
		layout="div" action="studentController.do?doUpdate" tiptype="3">
		<input id="id" name="id" type="hidden" value="${studentPage.id }">
		<fieldset class="step">
			<div class="form">
				<label class="Validform_label">邮箱:</label> <input id="email"
					name="email" type="text" style="width: 150px" class="inputxt"
					datatype="e" value='${studentPage.email}'> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">电话:</label> <input id="mobilePhone"
					name="mobilePhone" type="text" style="width: 150px" class="inputxt"
					datatype="*" value='${studentPage.mobilePhone}'> <span
					class="Validform_checktip"></span>
			</div>

			<div class="form">
				<label class="Validform_label">昵称:</label> <input id="nickname"
					name="nickname" type="text" style="width: 150px" class="inputxt"
					datatype="*" value='${studentPage.nickname}'> <span
					class="Validform_checktip"></span>
			</div>

			<div class="form">
				<label class="Validform_label">性别:</label> <select
					id="sex" name="sex">
					<option value="1"
						<c:if test="${studentPage.sex==1}">selected="selected"</c:if>>男</option>
					<option value="2"
						<c:if test="${studentPage.sex==2}">selected="selected"</c:if>>女</option>
				</select> <span class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">年级:</label> <input id="grade"
					name="grade" type="text" style="width: 150px" class="inputxt"
					datatype="*" value='${studentPage.grade}'> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">留学季:</label> <input id="season"
					name="season" type="text" style="width: 150px" class="inputxt"
					datatype="*" value='${studentPage.season}'> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">生日:</label> <input id="birthday"
					name="birthday" type="text" style="width: 150px"
					class="easyui-datebox" value='${studentPage.birthday}'
					readonly="readonly"> <span class="Validform_checktip"></span>
			</div>
		</fieldset>
	</t:formvalid>
</body>
<script src="webpage/stuinfo/student/student.js"></script>