<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="studentList" checkbox="true" fitColumns="false" title="学生基本信息" actionUrl="studentController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="id"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="email"  field="email"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="电话"  field="mobilePhone"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="昵称"  field="nickname"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="性别"  field="sex" replace="男_1,女_2"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="年级"  field="grade"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="留学季"  field="season"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="生日"  field="birthday"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
  <%--  <t:dgCol title="操作" field="opt" width="100"></t:dgCol> --%>
   <%-- <t:dgDelOpt title="删除" url="studentController.do?doDel&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="studentController.do?goAdd" funname="add"></t:dgToolBar> --%>
   <t:dgToolBar title="编辑" icon="icon-edit" url="studentController.do?goUpdate" funname="update"></t:dgToolBar>
  <%--  <t:dgToolBar title="批量删除"  icon="icon-remove" url="studentController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar> --%>
   <t:dgToolBar title="查看" icon="icon-search" url="studentController.do?goUpdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/stuinfo/student/studentList.js"></script>		
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
 });
 </script>