<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<div style="overflow:auto;">
	<div class="subwin hntab" style="bottom: 0; width: 100%; height: 100%; top: 0; border-right: 0;">
		<ul id="areaReportTab">
			<li id="Tab1" class="act">
				<b>日报</b>
			</li>
			<li id="Tab2">
				<b>周报</b>
			</li>
			<li id="Tab3">
				<b>月报</b>
			</li>
			<li id="Tab4">
				<b>季报</b>
			</li>
			<li id="Tab5">
				<b>半年报</b>
			</li>
			<li id="Tab6">
				<b>年报</b>
			</li>
		</ul>
		<script type="text/javascript">
	
			$(function(){
				$(".winMax1").click(function(){
					$(this).parent().css({"top":"0","bottom":"0","z-index":"5","height":"100%"});	
					$(".winMax1").hide();
					$(".winMin1").show();
				});
				
				$(".winMin1").click(function(){
					$(this).parent().css({"width": "100%","bottom":"0","top":"53%"});
					$(".winMin1").hide();
					$(".winMax1").show();
				});
				
			});
	
			for(var i =0 ; i < dataTypes.length ; i++){
				var className = "";
				if(i==0){
					className = "tabshow";
				}
				var htmlStr  = "<div class=\""+className+"\"> " ;
					htmlStr += "	<div> ";
					htmlStr += "	<div style=\"overflow:auto;\"> ";
					htmlStr += "		<table class=\"datalist\" >";
					htmlStr += "			<thead>";
					htmlStr += "				<tr>";
					htmlStr += "					<td align=\"center\">序号</td>";
					htmlStr += "					<td>日期</td>";
					htmlStr += "					<td>统计对象</td>";
					htmlStr += "					<td>处方总数</td>";
					htmlStr += "					<td>明细总数</td>";
					htmlStr += "					<td>异常总数</td>";
					htmlStr += "					<td>拒付总数</td>";
					htmlStr += "					<td>处方总额</td>";
					htmlStr += "					<td>异常总额</td>";
					htmlStr += "					<td>拒付总额</td>";
					htmlStr += "					<td>异常金额比例</td>";
					htmlStr += "					<td>拒付金额比例</td>";
					htmlStr += "				</tr> ";
					htmlStr += "			</thead> ";
					htmlStr += "			<tbody id=\"tbody_"+dataTypes[i]+"\">";
					htmlStr += "				<tr id=\"tmp_"+dataTypes[i]+"\" style=\"display: none; cursor: pointer;\">";
					htmlStr += "					<td id=\"rep_grid_xh_"+dataTypes[i]+"\" align=\"center\"></td>";
					htmlStr += "					<td id=\"rep_grid_dateValue_"+dataTypes[i]+"\"></td>";
					htmlStr += "					<td id=\"rep_grid_obj_"+dataTypes[i]+"\"></td>";
					htmlStr += "					<td id=\"rep_grid_presTotalcount_"+dataTypes[i]+"\"></td>";
					htmlStr += "					<td id=\"rep_grid_detailTotalcount_"+dataTypes[i]+"\"></td>";
					htmlStr += "					<td id=\"rep_grid_errorTotalcount_"+dataTypes[i]+"\"></td>";
					htmlStr += "					<td id=\"rep_grid_rejectTotalcount_"+dataTypes[i]+"\"></td>";
					htmlStr += "					<td id=\"rep_grid_presTotalmoney_"+dataTypes[i]+"\"></td>";
					htmlStr += "					<td id=\"rep_grid_errorTotalmoney_"+dataTypes[i]+"\"></td>";
					htmlStr += "					<td id=\"rep_grid_rejectTotalmoney_"+dataTypes[i]+"\"></td>";
					htmlStr += "					<td id=\"rep_grid_errorMoneyrate_"+dataTypes[i]+"\"></td>";
					htmlStr += "					<td id=\"rep_grid_rejectMoneyrate_"+dataTypes[i]+"\"></td>";
					htmlStr += "				</tr>";
					htmlStr += "			</tbody>";
					htmlStr += "		</table>";
					htmlStr += "		<div class=\"paging\">";
					htmlStr += "			<a id=\"home_page_"+dataTypes[i]+"\">&lt; 首页</a>";
					htmlStr += "			<a id=\"backup_page_"+dataTypes[i]+"\">上页</a>";
					htmlStr += "			<a id=\"next_page_"+dataTypes[i]+"\">下页</a>";
					htmlStr += "			<a id=\"end_page_"+dataTypes[i]+"\">末页 &gt;</a>";
					htmlStr += "			<span id=\"total_current_page_"+dataTypes[i]+"\">共0条记录 当前第0页</span>";
					htmlStr += "			<span>到第 <input name=\"currentPage_"+dataTypes[i]+"\" id=\"currentPage_"+dataTypes[i]+"\" type=\"text\"> 页</span>";
					htmlStr += "			<a id=\"go_page_"+dataTypes[i]+"\" class=\"bnt\">确定</a>";
					htmlStr += "		</div>";
					htmlStr += "	</div>";
					htmlStr += "	</div>";
					htmlStr += "	<div>";
					htmlStr += "	<div style=\"overflow:auto;\"> ";
					htmlStr += "		<div class=\"subwin hntab\" id=\"div_chart\" style=\"width: 100%; height: 100%;overflow:auto\">";
					htmlStr += "			<a href=\"#\" class=\"winMax1\" title=\"窗口放大\"></a><a href=\"#\" class=\"winMin1\" title=\"窗口还原\"></a>";
					htmlStr += "			<ul>";
					htmlStr += "				<li id=\"Tab_"+dataTypes[i]+"1\" onclick=\"changeChartType('j','${param.chartUrl}')\" class=\"act\">";
					htmlStr += "					<b>按金额统计</b>";
					htmlStr += "				</li>";
					htmlStr += "				<li id=\"Tab_"+dataTypes[i]+"2\" onclick=\"changeChartType('s','${param.chartUrl}')\">";
					htmlStr += "					<b>按数量统计</b>";
					htmlStr += "				</li>";
					htmlStr += "			</ul>";
					htmlStr += "			<div class=\"tabshow\">";
					htmlStr += "				<div id=\"dayMoneyContainer_"+dataTypes[i]+"\" style=\"width: 1200px; height: 280px; overflow: hidden;\"></div>";
					htmlStr += "			</div>";
					htmlStr += "			<div style=\"width: 100%;\">";
					htmlStr += "				<div id=\"dayAmountContainer_"+dataTypes[i]+"\" style=\"width: 1200px; height: 280px; overflow: hidden;\"></div>";
					htmlStr += "			</div>";
					htmlStr += "		</div>";
					htmlStr += "	</div>";
					htmlStr += "	</div>";
					htmlStr += "</div>";
					document.write(htmlStr);
			}
		</script>
	</div>
</div>