<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>ecm_content</title>
  <t:base type="jquery,easyui,tools,DatePicker,ckeditor"></t:base> 
<script type="text/javascript">
	$(function() {
		//初始化ckeditor
		var editorOption = {
			height : 200,
			customConfig : '${pageContext.request.contextPath}/plug-in/ckeditor/ftlConfig.js'
		};
		CKEDITOR.replace('content_editor', editorOption);
	});

	function getEditorData() {
		$("input[name='content']").attr("value",CKEDITOR.instances.content_editor.getData());
	}
</script>
 </head>
 <body>
      <%-- <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="ecmContentController.do?doAdd" tiptype="3" beforeCheck="getEditorData();"> --%>
   <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" tiptype="3" beforeSubmit="upload" beforeCheck="getEditorData();"> 
		<input id="id" name="id" type="hidden" value="${ecmContentPage.id }">
		<input id="catId" name="catId" type="hidden" value="${catId}">
		 <input id="content" name="content" type="hidden" datatype="*0-50000">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">标题:</label>
		     	 <input id="title" name="title" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">内容:</label>
		    </div>
		    <div class="form">
		   	 <textarea id="content_editor" name="content_editor"> </textarea> 
		   	 <span class="Validform_checktip"></span>
		    </div>
		    
		    <div class="form" id="filediv"></div>
			<div class="form"><t:upload name="file_upload" uploader="ecmContentController.do?doAdd" extend="pic" id="file_upload" formData="id,catId,title,content_editor"></t:upload></div>
			
	    </fieldset>
  </t:formvalid>
 </body>
  <script src = "webpage/ecm/ecmContent/ecmContent.js"></script>		