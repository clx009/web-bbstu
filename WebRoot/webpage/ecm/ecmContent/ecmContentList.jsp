<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
	<div region="west" style="padding: 1px;width:200px">
		<div tools="#tt" class="easyui-panel" title="栏目选择" style="padding: 10px;" fit="true" border="false" id="cat-panel"></div>
	</div>
  <div region="center" style="padding:1px;width:600px">
	  <t:datagrid name="ecmContentList" checkbox="true" fitColumns="false" title="内容管理" actionUrl="ecmContentController.do?datagrid" idField="id" fit="true" queryMode="group">
	   <t:dgCol title="id"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
	   <t:dgCol title="标题"  field="title"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
	   <t:dgCol title="附件"  field="attachmentPath"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
	  <%--  <t:dgCol title="内容"  field="content"  hidden="true"  queryMode="group"  width="120"></t:dgCol> --%>
	   <t:dgCol title="创建时间"  field="createTime"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
	   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
	   <t:dgToolBar title="录入" icon="icon-add"  funname="goAdd(id)" ></t:dgToolBar>
	   <t:dgDelOpt title="删除" url="ecmContentController.do?doDel&id={id}" />
	   <t:dgToolBar title="编辑" icon="icon-edit" url="ecmContentController.do?goUpdate" funname="update"></t:dgToolBar>
	   <t:dgToolBar title="批量删除"  icon="icon-remove" url="ecmContentController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
	   <t:dgToolBar title="查看" icon="icon-search" url="ecmContentController.do?goUpdate" funname="detail"></t:dgToolBar>
	  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/ecm/ecmContent/ecmContentList.js"></script>		
 <script type="text/javascript">
 var curCatId;
 $(document).ready(function(){
	 $('#cat-panel').tree({
			checkbox : false,
			url : 'ecmContentController.do?queryCategoryTree',
			onLoadSuccess : function(node) {
				expandAll();
			},
			onClick: function(node){
				var isRoot =  $('#cat-panel').tree('getChildren', node.target);
				curCatId = node.id;
				if(isRoot==''){
					$('#ecmContentList').datagrid('reload',{
						catId: node.id
						//TSDepart_departname: $("#TSDepart_departname").val(),
						//realName:  $("#realName").val()
					});
					//$('#ecmContentList').panel("refresh", "ecmContentController.do?datagrid&catId="+node.id);
				}else {
				}
			}
		});
 });
	function expandAll() {
		var node = $('#cat-panel').tree('getSelected');
		if (node) {
			$('#cat-panel').tree('expandAll', node.target);
		} else {
			$('#cat-panel').tree('expandAll');
		}
	}
	function goAdd(){
			createwindow("录入","ecmContentController.do?goAdd&id=${id}&catId="+curCatId,700,400);
			$('#ecmContentList').datagrid('reload',{catId: curCatId});
	}
 </script>