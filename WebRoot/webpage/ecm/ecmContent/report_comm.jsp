<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script type="text/javascript" src="<%=path%>/plug-in/jquery/jquery-1.6.1.min.js"></script>
<script type="text/javascript" src="<%=path%>/plug-in/easyui/jquery.easyui.min.1.3.2.js"></script>
<link rel="stylesheet" href="<%=path%>/plug-in/easyui/themes/icon.css" type="text/css">
<script type="text/javascript" src="<%=path%>/plug-in/easyui/locale/easyui-lang-zh_CN.js"></script>
<link id="easyuiTheme" rel="stylesheet" href="<%=path%>/plug-in/easyui/themes/default/easyui.css" type="text/css">
<link type="text/css" href="<%=path%>/css/common/css.css" rel="stylesheet" />
<script src="<%=path%>/webpage/common/js/pub.js" type="text/javascript"></script>
<script type="text/javascript" src="plug-in/Highcharts-4.0.1/js/highcharts.src.js"></script>
<script type="text/javascript" src="plug-in/Highcharts-4.0.1/js/modules/exporting.src.js"></script>
<script type="text/javascript">
	var curChartType 	= "j";//当前报表统计类型 j:默认按金额统计,s：按数量统计 
	var dataTypes 		= ['D','W','M','S','H','Y'];
	var curDataType 	= 'D';
</script>
<script type="text/javascript" src="<%=basePath%>/webpage/statistics/js/util.js"></script>
<script type="text/javascript" src="<%=basePath%>/webpage/statistics/js/reportComm.js"></script>
<script type="text/javascript" src="<%=basePath%>/webpage/statistics/js/reportDatagrid.js"></script>
<style>
	html,body {
		overflow: hidden;
	}
</style>