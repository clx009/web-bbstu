<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<table title="地域列表" id="leftTreegrid"  name="leftTreegrid" style="width:200px;height:250px"
        data-options="
            url: '${param.treeGridUrl }',
            method: 'get',
            rownumbers: true,
            idField: 'id',
           	fit:true,
            treeField: 'text'
        ">
     <thead>
         <tr>
             <th data-options="field:'id',hidden:true" >编号</th>
             <th data-options="field:'text'" width="180"  align="left"> ${param.nodeLable}名称 </th>
         </tr>
     </thead>
</table>