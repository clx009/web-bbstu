<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>栏目管理</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
  <script type="text/javascript" src="plug-in/ckeditor_new/ckeditor.js"></script>
  <script type="text/javascript" src="plug-in/ckfinder/ckfinder.js"></script>
  <script type="text/javascript">
  //编写自定义JS代码
  </script>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="ecmCategoryController.do?doAdd" tiptype="1">
					<input id="id" name="id" type="hidden" value="${ecmCategoryPage.id }">
		<table style="width: 600px;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right">
						<label class="Validform_label">
							name:
						</label>
					</td>
					<td class="value">
					     	 <input id="name" name="name" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">name</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							type:
						</label>
					</td>
					<td class="value">
					     	 <input id="type" name="type" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">type</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							code:
						</label>
					</td>
					<td class="value">
					     	 <input id="code" name="code" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">code</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							isEnable:
						</label>
					</td>
					<td class="value">
					     	 <input id="isEnable" name="isEnable" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">isEnable</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							parentCategoryId:
						</label>
					</td>
					<td class="value">
					     	 <input id="parentCategoryId" name="parentCategoryId" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">parentCategoryId</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							description:
						</label>
					</td>
					<td class="value">
					     	 <input id="description" name="description" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">description</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							createTime:
						</label>
					</td>
					<td class="value">
					     	 <input id="createTime" name="createTime" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">createTime</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							modifyTime:
						</label>
					</td>
					<td class="value">
					     	 <input id="modifyTime" name="modifyTime" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">modifyTime</label>
						</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							createUserId:
						</label>
					</td>
					<td class="value">
					     	 <input id="createUserId" name="createUserId" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">createUserId</label>
						</td>
				</tr>
			</table>
		</t:formvalid>
 </body>
  <script src = "webpage/ecm/ecmCategory/ecmCategory.js"></script>		