<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>栏目信息</title>
		<t:base type="jquery,easyui,tools"></t:base>
		<script type="text/javascript">
	$(function() {
		$('#cc').combotree( {
			url : 'ecmCategoryController.do?setPEcmCategory',
			panelHeight : 'auto',
			onClick : function(node) {
				$("#ecmCategoryId").val(node.id);
			}
		});

		if ($('#ecmCategoryLevel').val() == '1') {
			$('#pfun').show();
		} else {
			$('#pfun').hide();
		}

		$('#ecmCategoryLevel').change(function() {
			if ($(this).val() == '1') {
				$('#pfun').show();
				var t = $('#cc').combotree('tree');
				var nodes = t.tree('getRoots');
				if (nodes.length > 0) {
					$('#cc').combotree('setValue', nodes[0].id);
					$("#ecmCategoryId").val(nodes[0].id);
				}
			} else {
				var t = $('#cc').combotree('tree');
				var node = t.tree('getSelected');
				if (node) {
					$('#cc').combotree('setValue', null);
				}
				$('#pfun').hide();
			}
		});
	});
</script>
	</head>
	<body style="overflow-y: hidden" scroll="no">
		<t:formvalid formid="formobj" layout="div" dialog="true"
			refresh="true" action="ecmCategoryController.do?saveEcmCategory">
			<input name="id" type="hidden" value="${categoryEntity.id}">
			<fieldset class="step">
				<div class="form">
					<label class="Validform_label">
						栏目名称:
					</label>
					<input name="name" class="inputxt"
						value="${categoryEntity.name}" datatype="s2-15">
					<span class="Validform_checktip">栏目名称范围2~15位字符,且不为空</span>
				</div>
				<div class="form">
					<label class="Validform_label">
						栏目等级:
					</label>
					<select name="level" id="ecmCategoryLevel" datatype="*">
						<option value="0"
							<c:if test="${categoryEntity.level eq 0}">selected="selected"</c:if>>
							一级栏目
						</option>
						<option value="1"
							<c:if test="${categoryEntity.level>0}"> selected="selected"</c:if>>
							下级栏目
						</option>
					</select>
					<span class="Validform_checktip"></span>
				</div>
				<div class="form" id="pfun">
					<label class="Validform_label">
						父地域:
					</label>
					<input id="cc"
						<c:if test="${categoryEntity.ecmCategoryEntity.level eq 0}">
					value="${categoryEntity.ecmCategoryEntity.name}"</c:if>
						<c:if test="${categoryEntity.ecmCategoryEntity.level > 0}">
					value="${categoryEntity.ecmCategoryEntity.name}"</c:if>>
					<input id="ecmCategoryId" name="ecmCategoryEntity.id"
						style="display: none;" value="${categoryEntity.ecmCategoryEntity.id}">
				</div>
				<div class="form" id="funorder">
					<label class="Validform_label">
						栏目码:
					</label>
					<input name="code" class="inputxt"
						value="${categoryEntity.code}" datatype="*6-16">
				</div>
				<div class="form" id="funorder">
					<label class="Validform_label">
						显示顺序:
					</label>
					<input name="sort" class="inputxt"
						value="${categoryEntity.sort}" datatype="n1-3">
				</div>
				<div class="form" id="funorder">
					<label class="Validform_label">
						描述:
					</label>
					<input name="description" class="inputxt"
						value="${categoryEntity.description}" >
				</div>
			</fieldset>
		</t:formvalid>
	</body>
</html>
