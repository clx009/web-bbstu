<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="ecmCategoryList"  fitColumns="false" title="栏目管理" actionUrl="ecmCategoryController.do?ecmCategoryGrid" 
  idField="id" fit="true" queryMode="group" treegrid="true" pagination="false">
   <t:dgCol title="编号"  field="id"  hidden="false" treefield="id"   width="120"></t:dgCol>
   <t:dgCol title="栏目名"  field="name"  treefield="text" queryMode="group"  width="240"></t:dgCol>
   <t:dgCol title="栏目编码"  field="code"   treefield="src" width="120"></t:dgCol>
   <t:dgCol title="显示顺序"  field="sort"  treefield="order"  width="120"></t:dgCol>
   <t:dgCol title="描述"  field="description" treefield="description" width="120"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="ecmCategoryController.do?doDel&id={id}" />
   <t:dgToolBar title="添加" icon="icon-add" url="ecmCategoryController.do?addorupdate" funname="addCategory"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="ecmCategoryController.do?addorupdate" funname="update"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/ecm/ecmCategory/ecmCategoryList.js"></script>		
<script type="text/javascript">
$(function() {
	var li_east = 0;
});
function addCategory(title,url, id) {
	var rowData = $('#'+id).datagrid('getSelected');
	if (rowData) {
		url += '&ecmCategoryEntity.id='+rowData.id;
	}
	add(title,url,'ecmCategoryList');
}
</script>