<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>t_s_kaoshi</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="tSKaoshiController.do?doUpdate" tiptype="3">
				<input id="id" name="id" type="hidden" value="${tSKaoshiPage.id }">
		<fieldset class="step">
			<div class="form">
				<label class="Validform_label">学生:</label> <input id="userId"
					name="userId" type="text" style="width: 150px" class="inputxt"
					value='${tSKaoshiPage.userId}'> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
		      <label class="Validform_label">科目:</label>
		     	 <input id="subjectName" name="subjectName" type="text" style="width: 150px" class="inputxt"  
					               datatype="*" value='${tSKaoshiPage.subjectName}'
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">日期:</label>
		     	 <input id="testtime" name="testtime" type="text" style="width: 150px"  
					             class="easyui-datetimebox" value='${tSKaoshiPage.testtime}'
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">得分:</label>
		     	 <input id="score" name="score" type="text" style="width: 150px" class="inputxt"  
					               datatype="*" value='${tSKaoshiPage.score}'
					               >
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </t:formvalid>
 </body>
  <script src = "webpage/kaoshi/tSKaoshi/tSKaoshi.js"></script>		