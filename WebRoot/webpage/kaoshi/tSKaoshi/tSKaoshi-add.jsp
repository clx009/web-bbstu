<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>t_s_kaoshi</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" layout="div" action="tSKaoshiController.do?doAdd" tiptype="3">
			
		<fieldset class="step">
		    <div class="form">
				<label class="Validform_label">学生:</label> <input id="userId"
					name="userId" type="text" style="width: 150px" class="inputxt"
					> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
		      <label class="Validform_label">科目:</label>
		     	 <input id="subjectName" name="subjectName" type="text" style="width: 150px" class="inputxt" 
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">日期:</label>
		     	 <input id="testtime" name="testtime" type="text" style="width: 150px"  
					              class="easyui-datetimebox"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">得分:</label>
		     	 <input id="score" name="score" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </t:formvalid>
 </body>
  <script src = "webpage/kaoshi/tSKaoshi/tSKaoshi.js"></script>		