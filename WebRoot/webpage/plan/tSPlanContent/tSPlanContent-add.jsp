<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>t_s_plan_content</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body>
 <t:formvalid formid="formobj" layout="div" dialog="true" beforeSubmit="upload">
				<input id="id" name="id" type="hidden" value="${tSPlanContentPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">学生:</label>
		      	<input type="hidden" id="userid" >
		     	 <input name="userid" type="text" style="width: 150px" class="inputxt"  >
		      <span class="Validform_checktip"></span>
		    </div>
		    <div class="form">
				<label class="Validform_label">学年:</label> <select id="schoolYear"
					name="schoolYear">
					</select> <span
					class="Validform_checktip"></span>
			</div> 			
			<div class="form">
				<label class="Validform_label">学期:</label> <select id="term"
					name="term">
					<option value="春">春</option>
					<option value="夏">夏</option>
					<option value="秋">秋</option>
					<option value="冬">冬</option>
				</select><span
					class="Validform_checktip"></span>
			</div>
<!-- 			<div class="form">
		      <label class="Validform_label">规划内容:</label>
		     	 <input id="name" name="name" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div> -->
			<div class="form">
		      <label class="Validform_label">执行结果:</label>
		     	 <select id="result" name="result" type="text" style="width: 150px" class="inputxt"  
					               datatype="*">
					<option value="良好">良好</option>
					<option value="正常">正常</option>
					<option value="滞后">滞后</option>
					</select>
		      <span class="Validform_checktip"></span>
		    </div>
		    <div class="form">
		      <label class="Validform_label">升学指数:</label>
		     	 <input id="entrancescore" name="entrancescore" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
		    <div class="form">
		      <label class="Validform_label">导师:</label>
		     	 <input id="teacher" name="teacher" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
		    <div class="form" id="filediv"></div>
			<div class="form"><t:upload name="file_upload" uploader="tSPlanContentController.do?doAdd" extend="office" id="file_upload" formData="id,userid,schoolYear,term,result,entrancescore,teacher"></t:upload></div>
	
	    </fieldset>
  </t:formvalid>
 </body>
 <script type="text/javascript"> 
window.onload=function(){ 
//设置年份的选择 
var myDate= new Date(); 
var startYear=myDate.getFullYear();//起始年份 
var endYear=myDate.getFullYear()+50;//结束年份 
var obj=document.getElementById('schoolYear'); 
for (var i=startYear;i<=endYear;i++) 
{ 
obj.options.add(new Option(i,i)); 
} 
};
</script> 
<script src = "webpage/plan/tSPlanContent/tSPlanContent.js"></script>		