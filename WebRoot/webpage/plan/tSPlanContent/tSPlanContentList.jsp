<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="tSPlanContentList" checkbox="true" fitColumns="false" title="升学规划内容" actionUrl="tSPlanContentController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="id"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="学生" replace="${userReplace}"   field="userId"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="学年"  field="schoolYear"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="学期"  field="term"  hidden="true"  queryMode="group"  width="120"></t:dgCol> 
<%--    <t:dgCol title="规划内容"  field="name"  hidden="true"  queryMode="group"  width="120"></t:dgCol> --%>
   <t:dgCol title="执行结果"  field="result"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="升学指数"  field="entrancescore"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="导师"  field="teacher"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="附件"  field="path"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="tSPlanContentController.do?doDel&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="tSPlanContentController.do?goAdd" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="tSPlanContentController.do?goUpdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="批量删除"  icon="icon-remove" url="tSPlanContentController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="tSPlanContentController.do?goUpdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/plan/tSPlanContent/tSPlanContentList.js"></script>		
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
 });
 </script>