<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>自定义流程</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
  <script type="text/javascript" src="plug-in/ckeditor_new/ckeditor.js"></script>
  <script type="text/javascript" src="plug-in/ckfinder/ckfinder.js"></script>
  <script type="text/javascript">
  //编写自定义JS代码
  </script>
 </head>
 <body>
     <div>
     	<c:if test="${'40288105450658b0014506595fa800c8' == processId}">
     	   <img alt="" src="<%=path%>/images/workflow/process.gif">
     	</c:if>
     	<c:if test="${'5123514b927e46009b16e8fbe90aabab' == processId}">
     		<img alt="" src="<%=path%>/images/workflow/process2.gif"> 
     	</c:if>
     </div>
 </body>
  <script src = "webpage/system/wfDefineProcess/wfDefineProcess.js"></script>		