<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true" id="processDefList">
  <div region="center" style="padding:1px;">
  <t:datagrid name="wfDefineProcessList" checkbox="true" fitColumns="false" title="自定义流程" actionUrl="wfDefineProcessController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="流程定义主键"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="流程名称"  field="processName"  hidden="true"  queryMode="group"  width="180"></t:dgCol>
   <t:dgCol title="流程期限（天）"  field="duration"  hidden="true"  queryMode="group"  width="100"></t:dgCol>
   <t:dgCol title="流程创建人"  field="createUserId"  hidden="true"  queryMode="group"  width="100"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="160"></t:dgCol>
   
   <t:dgToolBar operationCode="add" onclick = "addProcess();" title="新增流程" icon="icon-add" url="wfDefineProcessController.do?goAdd" funname="add"></t:dgToolBar>
   <t:dgFunOpt funname="queryActivity(id)" title="活动定义"></t:dgFunOpt>
    <t:dgFunOpt funname="editProcess(id)" title="编辑流程"></t:dgFunOpt>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/system/wfDefineProcess/wfDefineProcessList.js"></script>		
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
 			$("#wfDefineProcessListtb").find("input[name='createDate_begin']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
 			$("#wfDefineProcessListtb").find("input[name='createDate_end']").attr("class","Wdate").attr("style","height:20px;width:90px;").click(function(){WdatePicker({dateFmt:'yyyy-MM-dd'});});
 });
 </script>
 <div data-options="region:'east',
	title:'活动定义列表',
	collapsed:true,
	split:true,
	border:false,
	onExpand : function(){
		li_east = 1;
	},
	onCollapse : function() {
	    li_east = 0;
	}"
	style="overflow: hidden;" id="eastDivPro">
<div class="easyui-panel" style="padding: 1px;" fit="true" border="false" id="activityListpanel"></div>
</div>

<script type="text/javascript">

$(function() {
	var li_east = 0;
	$("#eastDivPro").width(fixWidth(0.25));
});



//function queryUsersByRowData(rowData){
//	if(li_east == 0){
//	   $('#processDefList').layout('expand','east'); 
//	}
//	$('#activityListpanel').panel("refresh", "departController.do?userList&departid=" + rowData.id);
//}

function queryActivity(processId){
	if(li_east == 0){
	   $('#processDefList').layout('expand','east'); 
	}
	$('#activityListpanel').panel("refresh", "wfDefineActivityController.do?wfDefineActivity&processId=" + processId);
}
//-->

 function addProcess(){
 			var url = "wfDefineProcessController.do?goAdd";
 			addTab("添加流程",url,"picture");
  }
 function editProcess(id){
 	var url = "wfDefineProcessController.do?goUpdate&processId="+id;
 	addTab("编辑流程",url,"picture");
 }

</script>