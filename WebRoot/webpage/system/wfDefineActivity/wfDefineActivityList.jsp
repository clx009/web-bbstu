<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="wfDefineActivityList" checkbox="true" fitColumns="false" title="流程活动定义" actionUrl="wfDefineActivityController.do?datagrid&processId=${processId}" idField="id" fit="true" queryMode="group">
   <t:dgCol title="主键"  field="id"  hidden="false"  queryMode="group"  ></t:dgCol>
   <t:dgCol title="流程定义主键"  field="processId"  hidden="false"  queryMode="group"  ></t:dgCol>
   <t:dgCol title="活动名称"  field="activityName"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="150"></t:dgCol>
   <t:dgOpenOpt url="wfDefineActivityController.do?wfDefineSecurity&activityId={id}&processId={processId}" width="800" height="500" title="活动用户分配"></t:dgOpenOpt>
  	
  
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/system/wfDefineActivity/wfDefineActivityList.js"></script>		
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
 });
 </script>