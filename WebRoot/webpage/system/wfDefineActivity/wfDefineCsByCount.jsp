<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="/webpage/common/baseJsp.jsp"%>
<title> 宝宝留学系统</title>
<t:base type="jquery,easyui,tools,DatePicker,autocomplete"></t:base>
</head>
<body>
<div class="easyui-layout" fit="true" id="securityCsUser">
  <div region="center" style="padding:1px;" id="southSec">
	<t:datagrid name="userCsByCountList" actionUrl="wfDefineActivityController.do?userdatagrid" title="" fit="true" fitColumns="true" idField="id" queryMode="group">
		<t:dgCol title="编号" field="id" hidden="false" ></t:dgCol>
		<t:dgCol title="用户名" sortable="false" field="userName" query="true" width="80"></t:dgCol>
		<t:dgCol title="部门" field="TSDepart_id" query="true" replace="${departsReplace}" width="80"></t:dgCol>
		<t:dgCol title="真实姓名" field="realName" query="true" width="80"></t:dgCol>
		<t:dgCol title="状态" sortable="true" field="status" replace="正常_1,禁用_0,超级管理员_-1" width="80"></t:dgCol>
		<t:dgCol title="处方量" field="preCount" width="80"></t:dgCol>
	    <t:dgToolBar  operationCode="" title="平均分配" icon="icon-pingjun" funname="avgPut"></t:dgToolBar>
	    <t:dgToolBar  operationCode="" title="人工分配" icon="icon-rengong" funname="handPut"></t:dgToolBar>
	    <t:dgToolBar  operationCode="" title="确认保存" icon="icon-save" funname=""></t:dgToolBar>
	</t:datagrid>
	<p style="padding-left:10px;font-weight:bold;">
	已分配处方量：<font id="distributed" style="color:green;"></font>条&nbsp;|&nbsp;未分配处方量：<font id="undistribute" style="color:red;"></font>条	
	</p>
  </div>
 </div>
</body>
<script type="text/javascript">
	 var distributed=0;
	 var undistribute=6000;

	 $(document).ready(function(){
 		//$("#southSec").height(fixHeight(0.72));
 		$("#distributed").text(distributed);
 		$("#undistribute").text(undistribute);
 	 });	
 	 
 	 function avgPut(){
 	 	var rows = $("#userCsByCountList").datagrid("getRows");
 	 	var eachCount=undistribute/rows.length;
 	 	for(var i=0;i<rows.length;i++){
			$('#userCsByCountList').datagrid('updateRow',{
				index: i,
				row: {
					preCount: eachCount
				}
			});
		}
		$("#distributed").text(undistribute);
		$("#undistribute").text("0");
 	 }
 	 
	 function handPut(){
	 	var rows = $("#userCsByCountList").datagrid("getRows");
	 	var eachCount=undistribute/rows.length;
 	 	for(var i=0;i<rows.length;i++){
			$('#userCsByCountList').datagrid('updateRow',{
				index: i,
				row: {
					preCount: "<input type=\"text\" id=\"count"+i+"\" value=\""+eachCount+"\" onblur=\"checkLeave(this.id,this.value)\"  onkeyup=\"this.value=this.value.replace(/\\D/g,'')\" onafterpaste=\"this.value=this.value.replace(/\\D/g,'')\"   />"
				}
			});
		}
		$("#distributed").text(undistribute);
		$("#undistribute").text("0");
	 }
	 
	 function checkLeave(nid,nval){
	 	$("#"+nid).val($("#"+nid).val().replace(/\D/g,''));
	 	var rows = $("#userCsByCountList").datagrid("getRows");
	 	var eachCount=undistribute/rows.length;
	 	var realdistribute=0;
 	 	for(var i=0;i<rows.length;i++){
 	 		if($("#count"+i).val()!=''){
 	 			realdistribute+=parseInt($("#count"+i).val());
 	 		}
 	 	}
 	 	if(undistribute-realdistribute<0){
 	 		alert('当前分配处方量已超出未分配处方总量，请重新分配！');
 	 		$("#"+nid).val('');
 	 		$("#"+nid).focus();
 	 		return;
 	 	}else{
 	 		$("#distributed").text(realdistribute);
			$("#undistribute").text(undistribute-realdistribute);
 	 	}
	 }
</script>
</html>