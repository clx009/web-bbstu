<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>流程活动定义</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
  <script type="text/javascript" src="plug-in/ckeditor_new/ckeditor.js"></script>
  <script type="text/javascript" src="plug-in/ckfinder/ckfinder.js"></script>
  <script type="text/javascript">
  //编写自定义JS代码
  </script>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="wfDefineActivityController.do?doAdd" tiptype="1">
					<input id="id" name="id" type="hidden" value="${wfDefineActivityPage.id }">
		<table style="width: 600px;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right">
						<label class="Validform_label">
							流程定义主键:
						</label>
					</td>
					<td class="value">
					     	 <input id="processId" name="processId" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">流程定义主键</label>
						</td>
					<td align="right">
						<label class="Validform_label">
							活动类型主键:
						</label>
					</td>
					<td class="value">
					     	 <input id="activityTypeId" name="activityTypeId" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">活动类型主键</label>
						</td>
					</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							活动名称:
						</label>
					</td>
					<td class="value">
					     	 <input id="activityName" name="activityName" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">活动名称</label>
						</td>
					<td align="right">
						<label class="Validform_label">
							活动序号:
						</label>
					</td>
					<td class="value">
					     	 <input id="activityNo" name="activityNo" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">活动序号</label>
						</td>
					</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							活动处理时间:
						</label>
					</td>
					<td class="value">
					     	 <input id="duration" name="duration" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">活动处理时间</label>
						</td>
					<td align="right">
						<label class="Validform_label">
							是否可以回到起点:
						</label>
					</td>
					<td class="value">
					     	 <input id="isTostart" name="isTostart" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">是否可以回到起点</label>
						</td>
					</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							是否异步？:
						</label>
					</td>
					<td class="value">
					     	 <input id="isAsync" name="isAsync" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">是否异步？</label>
						</td>
					<td align="right">
						<label class="Validform_label">
							是否允许打回:
						</label>
					</td>
					<td class="value">
					     	 <input id="isBack" name="isBack" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">是否允许打回</label>
						</td>
					</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							是否被撤回:
						</label>
					</td>
					<td class="value">
					     	 <input id="isRevocation" name="isRevocation" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">是否被撤回</label>
						</td>
					<td align="right">
						<label class="Validform_label">
							可视化定制参数:
						</label>
					</td>
					<td class="value">
						  	 <textarea id="visualProperties" name="visualProperties"></textarea>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">可视化定制参数</label>
						</td>
					</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							拆分类型  [ AND   OR    XOR   会签 ]:
						</label>
					</td>
					<td class="value">
					     	 <input id="splitType" name="splitType" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">拆分类型  [ AND   OR    XOR   会签 ]</label>
						</td>
					<td align="right">
						<label class="Validform_label">
							合并类型  [ AND        XOR    RULE ]:
						</label>
					</td>
					<td class="value">
					     	 <input id="joinType" name="joinType" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">合并类型  [ AND        XOR    RULE ]</label>
						</td>
					</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							转移类型  [ 手工  自动 ]:
						</label>
					</td>
					<td class="value">
					     	 <input id="transferType" name="transferType" type="text" style="width: 150px" class="inputxt"  
								               datatype="*"
								               >
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">转移类型  [ 手工  自动 ]</label>
						</td>
				<td align="right">
					<label class="Validform_label">
					</label>
				</td>
				<td class="value">
				</td>
					</tr>
			</table>
		</t:formvalid>
 </body>
  <script src = "webpage/system/wfDefineActivity/wfDefineActivity.js"></script>		