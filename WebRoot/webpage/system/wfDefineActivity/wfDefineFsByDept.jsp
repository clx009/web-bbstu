<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<%@include file="/webpage/common/baseJsp.jsp"%>
<title> 宝宝留学系统</title>
<t:base type="jquery,easyui,tools,DatePicker,autocomplete"></t:base>
</head>
<body>
<div class="easyui-layout" fit="true" id="securityFsUser">
  <div region="center" style="padding:1px;">
<t:datagrid name="userFsByDeptList" actionUrl="wfDefineActivityController.do?userdatagrid" title="可分配用户" fit="true" fitColumns="true" idField="id" queryMode="group">
	<t:dgCol title="编号" field="id" hidden="false" ></t:dgCol>
	<t:dgCol title="用户名" sortable="false" field="userName" query="true" width="100"></t:dgCol>
	<t:dgCol title="部门" field="TSDepart_id" query="true" replace="${departsReplace}" width="100"></t:dgCol>
	<t:dgCol title="真实姓名" field="realName" query="true" width="100"></t:dgCol>
	<t:dgCol title="状态" sortable="true" field="status" replace="正常_1,禁用_0,超级管理员_-1" width="100"></t:dgCol>
	<t:dgCol title="操作" field="opt" width="150"></t:dgCol>
	<t:dgFunOpt funname="setUserMedical(id,realName)" title="审核科室管理"></t:dgFunOpt>
</t:datagrid>
  </div>
 </div>
<div region="east" style="width:300px; overflow: hidden;" split="true">
	<div class="easyui-panel" style="padding: 1px;" fit="true" border="false" id="medical-panel" title="用户负责审核的医疗机构及科室">
		<ul id="medical"></ul>
		<br/>
		<span id="msgContent" style="font-size: 13px;font-weight:bold;padding-left:20px;color:red;"></span>
		<br/>
		<br/>
		<input type="hidden" id="userId" name="userId"/>
	</div>
</div>
</body>
 <script type="text/javascript">
	
	function setUserMedical(userid,realName) {
		var pid='${processId}';
		var aid='${activityId}';
		$("#userId").val(userid);
		$("#medical-panel").panel(
			{
				title:"【"+realName+"】负责审核的医疗机构及科室",
				tools:[{iconCls:'icon-save',handler:function(){submitSave(pid,aid);}}]
			}
		);
		$('#medical-panel').panel("refresh");
		$('#medical').tree({    
		    url: 'wfDefineActivityController.do?medicalTree&pid='+pid+'&aid='+aid+'&uid='+userid,    
		    checkbox:true,
		    animate: true,
		    onLoadSuccess:function(node,data){
		    	if(data==''){
		    		$('#msgContent').text('医疗机构及科室均已分配!');
		    	}else{
		    		$('#msgContent').text('');
		    		$('#medical').tree('expandAll'); 
		    	}
		    }
		});
	}
	
	function submitSave(processId,activityId){
		var userId=$("#userId").val();
		var nodestr = GetNode();
		doSubmit("wfDefineActivityController.do?saveSecurity&pid="+processId+"&aid="+activityId+"&nstr="+nodestr+"&uid="+userId);
	}
	
	function GetNode() {
		var cnode = $('#medical').tree('getChecked');
		var nodestr = '';
		var pnode = null; 
		for ( var i = 0; i < cnode.length; i++) {
			if ($('#medical').tree('isLeaf', cnode[i].target)) {
				pnode = $('#medical').tree('getParent', cnode[i].target); //获取当前节点的父节点
				if(pnode!=null) {
					nodestr += pnode.id + ',';
				}
				nodestr += cnode[i].id + '|';
			}
		}
		return nodestr;
	}
	
</script>
</html>