<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>参数管理</title>
		<t:base type="jquery,easyui,tools,DatePicker"></t:base>
		<script type="text/javascript" src="plug-in/ckeditor_new/ckeditor.js"></script>
		<script type="text/javascript" src="plug-in/ckfinder/ckfinder.js"></script>
	</head>
	<body>
		<t:formvalid formid="formobj" dialog="true" usePlugin="password" beforeSubmit="checkForm"
			layout="table" action="sysParamterController.do?doUpdate" tiptype="1">
			<input id="id" name="id" type="hidden" value="${sysParamterPage.id }">
			<table style="width: 600px;" cellpadding="0" cellspacing="1"
				class="formtable">
				<tr>
					<td align="right">
						<label class="Validform_label">
							参数名称:
						</label>
					</td>
					<td class="value">
						<input id="name" name="name" type="text" style="width: 150px"
							class="inputxt" value='${sysParamterPage.name}'>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							参数名称
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							参数类型:
						</label>
					</td>
					<td class="value">
						<t:dictSelect field="codeType" type="list" id="codeType"
							typeGroupCode="codetype" defaultVal="${sysParamterPage.codeType}"
							hasLabel="false" title="参数类型"></t:dictSelect>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							参数类型
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							参数代码:
						</label>
					</td>
					<td class="value">
						<input id="code" name="code" type="text" style="width: 150px"
							class="inputxt" value='${sysParamterPage.code}'>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							参数代码
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							参数数值:
						</label>
					</td>
					<td class="value">
						<input id="val" name="val" type="text" style="width: 150px"
							class="inputxt" value='${sysParamterPage.val}'>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							参数数值
						</label>
					</td>
				</tr>
			</table>
		</t:formvalid>
		<script src="webpage/system/sysParamter/sysParamter.js"></script>
	</body>
</html>