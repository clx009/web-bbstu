<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>

<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="sysParamterList" checkbox="true" fitColumns="false" title="参数管理" actionUrl="sysParamterController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="编号"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="参数名称"  field="name"  hidden="true" query="true" queryMode="single"  width="120"></t:dgCol>
   <t:dgCol title="参数类型"  field="codeType"  hidden="true"  queryMode="single" dictionary="codetype" width="120"></t:dgCol>
   <t:dgCol title="参数代码"  field="code"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="参数数值"  field="val"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="sysParamterController.do?doDel&id={id}" />
   <t:dgToolBar title="添加" icon="icon-add" url="sysParamterController.do?goAdd" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="sysParamterController.do?goUpdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="批量删除"  icon="icon-remove" url="sysParamterController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="sysParamterController.do?goUpdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/system/sysParamter/sysParamterList.js"></script>
 <script type="text/javascript">
	$(document).ready(function(){
		editOnClick("sysParamterController.do?goUpdate","sysParamterList","1");
	});
 </script>