
function update(title,url, id,width,height) {
	var rowsData = $('#'+id).datagrid('getSelections');
	if (!rowsData || rowsData.length==0) {
		tip('请选择编辑项目');
		return;
	}
	if (rowsData.length>1) {
		tip('请选择一条记录再编辑');
		return;
	}
	
	url += '&id='+rowsData[0].id;
	createwindow(title,url,width,height);
}

function checkForm(){
	var parameterCodeType = $("#codeType").val();
	var $val =  $("#val") ; 
	var currentVal = $val.val();
	if(currentVal== null || currentVal.length == 0 ){
		tip("请填写参数值!");
		$val.focus();
		return false;
	}
	var sreg = /^[\s\S]{1,100}$/
	var nreg = /^\d+(\.\d+)?$/;
	if(typeof(parameterCodeType) != "undefined"){
		if(parameterCodeType == 20 ){
			if(!nreg.test(currentVal)){
				tip("请填写数值型参数值!");
				$val.focus();
				return false ;
			}else if(currentVal.length > 100){
				tip("不能超过100位!");
				$val.focus();
				return false;
			}
		}else if(parameterCodeType == 10){
			if(!sreg.test(currentVal)){
				tip("请填写1-100之间的参数值!");
				$val.focus();
				return false;
			}
		}
	}
}