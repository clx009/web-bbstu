<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>系统配置</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
  <script type="text/javascript" src="plug-in/ckeditor_new/ckeditor.js"></script>
  <script type="text/javascript" src="plug-in/ckfinder/ckfinder.js"></script>
  <script type="text/javascript">
  //编写自定义JS代码
  </script>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="tSConfigController.do?doUpdate" tiptype="1">
					<input id="id" name="id" type="hidden" value="${tSConfigPage.id }">
		<table style="width: 600px;" cellpadding="0" cellspacing="1" class="formtable">
					<tr>
						<td align="right">
							<label class="Validform_label">
								code:
							</label>
						</td>
						<td class="value">
						     	 <input id="code" name="code" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${tSConfigPage.code}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">code</label>
						</td>
					</tr>
					<tr>
						<td align="right">
							<label class="Validform_label">
								content:
							</label>
						</td>
						<td class="value">
						     	 <input id="content" name="content" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${tSConfigPage.content}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">content</label>
						</td>
					</tr>
					<tr>
						<td align="right">
							<label class="Validform_label">
								name:
							</label>
						</td>
						<td class="value">
						     	 <input id="name" name="name" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${tSConfigPage.name}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">name</label>
						</td>
					</tr>
					<tr>
						<td align="right">
							<label class="Validform_label">
								note:
							</label>
						</td>
						<td class="value">
						     	 <input id="note" name="note" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${tSConfigPage.note}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">note</label>
						</td>
					</tr>
					<tr>
						<td align="right">
							<label class="Validform_label">
								userid:
							</label>
						</td>
						<td class="value">
						     	 <input id="userid" name="userid" type="text" style="width: 150px" class="inputxt"  
									               datatype="*"
									                 value='${tSConfigPage.userid}'>
							<span class="Validform_checktip"></span>
							<label class="Validform_label" style="display: none;">userid</label>
						</td>
					</tr>
			</table>
		</t:formvalid>
 </body>
  <script src = "webpage/system/tSConfig/tSConfig.js"></script>		