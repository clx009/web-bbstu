$(function(){
	$(document).ready(function(){
		var chart;
		$.ajax({
			type : "POST",
			url : "reportDemoController.do?studentCount&reportType=pie",
			success : function(jsondata){
				data = eval(jsondata);
				chart = new Highcharts.Chart({
						chart : {
							renderTo : 'containerline',
							plotBackgroundColor : null,
							plotBorderWidth : null,
							plotShadow : false
						},
						title : {
							text : '班级学生人数比例分析-<b>折线图</b>'
						},
						xAxis : {
							categories : [ '1班', '2班', '3班', '4班', '5班']
						},
						tooltip : {
							shadow: false,
							percentageDecimals : 1,
							formatter: function() {
            					return  '<b>'+this.point.name + '</b>:' +  Highcharts.numberFormat(this.percentage, 1) +'%';
         					}

						},
						exporting:{  
			                filename:'折线图',  
			                 url:'reportDemoController.do?export'  
			            },  
						plotOptions : {
							pie : {
								allowPointSelect : true,
								cursor : 'pointer',
								showInLegend : true,
								dataLabels : {
									enabled : true,
									color : '#000000',
									connectorColor : '#000000',
									formatter : function() {
										return '<b>' + this.point.name + '</b>: ' + Highcharts.numberFormat(this.percentage, 1)+"%";
									}
								}
							}
						},
						series : data
					});
			}
		});
	});
	
});