<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>专业排名维护</title>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<script type="text/javascript">
	$(function() {
		$("#schoolId").combobox({ disabled: true });
		//$("#majorId").combobox({ disabled: true });
	});
</script>
</head>
<body>
	<t:formvalid formid="formobj" dialog="true" usePlugin="password"
		layout="div" action="schoolMajorController.do?doUpdate" tiptype="3">
		<input id="id" name="id" type="hidden" value="${schoolMajorPage.id }">
		<fieldset class="step">
			<div class="form">
				<label class="Validform_label">学校:</label> <input id="schoolId"
					name="school.id" type="text" style="width: 150px" class="inputxt"
					datatype="*" value='${schoolMajorPage.school.id}'> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">专业:</label> <input id="majorId"
					name="major.id" type="text" style="width: 150px" class="inputxt"
					datatype="*" value='${schoolMajorPage.major.id}'> <span
					class="Validform_checktip"></span>
			</div>
			<%-- <div class="form">
				<label class="Validform_label">综合排名:</label> <input
					id="comprehensiveRanking" name="comprehensiveRanking" type="text"
					style="width: 150px" class="inputxt" datatype="n"
					value='${schoolMajorPage.comprehensiveRanking}'> <span
					class="Validform_checktip"></span>
			</div>  --%>
			<div class="form">
				<label class="Validform_label">排名:</label> <input
					id="ranking" name="ranking" type="text"
					style="width: 150px" class="inputxt" datatype="n"
					value='${schoolMajorPage.ranking}'> <span
					class="Validform_checktip"></span>
			</div>
		</fieldset>
	</t:formvalid>
</body>
<script src="webpage/school/schoolMajor/schoolMajor.js"></script>