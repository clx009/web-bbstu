<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<script type="text/javascript" charset="utf-8">
	function schoolMajorListImportXls() {
		openuploadwin('Excel导入', 'schoolMajorController.do?upload', "schoolMajorList");
	}
	
	/*
	 *	excel导出
	 */
	function schoolMajorListExportXls() { 

		BpExcelExport("schoolMajorController.do?exportXls","schoolMajorList");
	}
	
</script>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="schoolMajorList" checkbox="true" fitColumns="false" title="专业排名维护" actionUrl="schoolMajorController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="id"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="专业"  field="major.name"    hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="学校"  field="school.chineseName"   hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <%-- <t:dgCol title="综合排名"  field="comprehensiveRanking"  hidden="true"  queryMode="group"  width="120"></t:dgCol> --%>
   <t:dgCol title="排名"  field="ranking"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="schoolMajorController.do?doDel&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="schoolMajorController.do?goAdd" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="schoolMajorController.do?goUpdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="批量删除"  icon="icon-remove" url="schoolMajorController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="schoolMajorController.do?goUpdate" funname="detail"></t:dgToolBar>
   <t:dgToolBar title="导入Excel" icon="icon-search" onclick="schoolMajorListImportXls()"></t:dgToolBar>
   <t:dgToolBar title="导出Excel模板" icon="icon-search" onclick="schoolMajorListExportXls()"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/school/schoolMajor/schoolMajorList.js"></script>		
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
 });
 </script>