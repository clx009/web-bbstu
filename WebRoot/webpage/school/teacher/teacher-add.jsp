<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>教师资料维护</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="teacherController.do?doAdd" tiptype="3">
				<input id="id" name="id" type="hidden" value="${teacherPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">姓名:</label>
		     	 <input id="name" name="name" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </t:formvalid>
 </body>
  <script src = "webpage/school/teacher/teacher.js"></script>		