<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<script type="text/javascript" charset="utf-8">
	/*
	* excel导入
	*/
	function schoolListImportXls() {
		openuploadwin('Excel导入', 'schoolController.do?upload', "schoolList");
	}
	
	/*
	 *	excel导出
	 */
	function schoolListExportXls() {  
		BpExcelExport("schoolController.do?exportXls","schoolList");
	}
	
	//var viewMapDialog;
</script> 
<input id="longitude" name="longitude" type="hidden" value="" />
<input id="latitude" name="latitude" type="hidden" value="" />

<div class="easyui-layout" fit="true">
	<div region="center" style="padding:1px;">
		<t:datagrid name="schoolList" checkbox="true" fitColumns="false"
			title="学校资料维护" actionUrl="schoolController.do?datagrid" idField="id"
			fit="true" queryMode="group">
			<t:dgCol title="id" field="id" hidden="false" queryMode="group"
				width="120"></t:dgCol>
			<t:dgCol title="学校编号" field="code" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="英文名称" field="englishName" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="中文名称" field="chineseName" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="建校时间" field="builtUpTime" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="综合排名" field="comprehensiveRanking" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="学校类型"  field="publicOrPrivate" replace="公立_0,私立_1" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="排名" field="ranking" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="托福要求" field="toeflRequirement" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="SAT最低限制" field="satReqMin" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="SAT最高限制" field="satReqMax" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="学费" field="tuition" hidden="true" queryMode="group"
				width="120"></t:dgCol>
			<t:dgCol title="录取比例" field="acceptanceRate" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<%-- <t:dgCol title="地址" field="address" hidden="true" queryMode="group"
				width="120"></t:dgCol> --%>
			<t:dgCol title="操作" field="opt" width="100"></t:dgCol>
			<t:dgDelOpt title="删除" url="schoolController.do?doDel&id={id}" />
			<t:dgToolBar title="录入" icon="icon-add" url="schoolController.do?goAdd" funname="add"></t:dgToolBar>
			<t:dgToolBar title="编辑" icon="icon-edit"
				url="schoolController.do?goUpdate" funname="update"></t:dgToolBar>
			<t:dgToolBar title="批量删除" icon="icon-remove"
				url="schoolController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
			<t:dgToolBar title="查看" icon="icon-search"
				url="schoolController.do?goUpdate" funname="detail"></t:dgToolBar>
			<t:dgToolBar title="导入Excel" icon="icon-search" onclick="schoolListImportXls()"></t:dgToolBar>
			<t:dgToolBar title="导出Excel模板" icon="icon-search" onclick="schoolListExportXls();"></t:dgToolBar>
		</t:datagrid>
	</div>
</div> 
<!-- 

<script src="webpage/school/school/schoolList.js"></script>
<script type="text/javascript">
	
	
	$(document).ready(function(){ 
	}); 
</script> -->