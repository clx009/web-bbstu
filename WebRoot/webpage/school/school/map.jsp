<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
<head>
   <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <meta name="viewport" content="initial-scale=1, maximum-scale=1,user-scalable=no" />
   <title>ArcGIS API for JavaScript | Basic Search</title>
   <link rel="stylesheet" href="https://js.arcgis.com/3.17/esri/themes/calcite/dijit/calcite.css">
   <link rel="stylesheet" href="https://js.arcgis.com/3.17/esri/themes/calcite/esri/esri.css">
   <script type="text/javascript" src="<%=request.getContextPath()%>/plug-in/jquery/jquery-1.8.3.js"></script>
   <style>
      html,
      body,
      #map {
         height: 100%;
         width: 100%;
         margin: 0;
         padding: 0;
      }
      #search {
         display: block;
         position: absolute;
         z-index: 2;
         top: 20px;
         left: 74px;
      }
   </style>
   <script src="https://js.arcgis.com/3.17/"></script>
   <t:base type="jquery"></t:base>       
   <script>
      require([

        "esri/map",
        "esri/dijit/Search",
        "dojo/domReady!"

      ], function (Map, Search) {
      	 var latitude = window.parent.document.getElementById("latitude").value;
      	 var longitude = window.parent.document.getElementById("longitude").value;
      	 if(latitude && latitude != '' &&  longitude && longitude != ''){
      	 	 document.getElementById("latitude").value = latitude;
	         document.getElementById("longitude").value = longitude;
         	 var map = new Map("map", {
	            basemap: "gray",
	            center: [longitude,latitude], // lon, lat
	            zoom: 7
         	});
         	var search = new Search({
	            map: map
	         }, "search");
	         search.startup();
			 map.on("click", function(evt) {
	              document.getElementById("latitude").value = evt.mapPoint.getLatitude();
	              document.getElementById("longitude").value = evt.mapPoint.getLongitude();
	              document.getElementsByClassName('ui_state_highlight').display="none";
	      	  });
         	
      	 }else{
      	 	var map = new Map("map", {
	            basemap: "gray",
	            center: [-120.435, 46.159], // lon, lat
	            zoom: 7
	         });
	         var search = new Search({
	            map: map
	         }, "search");
	         search.startup();
	         map.on("click", function(evt) {
	              document.getElementById("latitude").value = evt.mapPoint.getLatitude();
	              document.getElementById("longitude").value = evt.mapPoint.getLongitude();
	              document.getElementsByClassName('ui_state_highlight').display="none";
	      	 });
	      }
      });
   </script>
</head>
<body class="calcite" onload="">
         纬度： <input type="text"  name ="latitude" id="latitude" value="" />  &nbsp;&nbsp; 
          经度： <input type="text"  name ="longitude" id="longitude" value="" />
          <input type="button" name="确定" value="确定" onclick="javascript:makesureLatAndlong();" />
   <div id="search"></div>
   <div id="map"></div>
</body>

<script type="text/javascript">
	
	function makesureLatAndlong(){
		if($("#latitude").val() == ''  ||  $("#longitude").val() == ''){
			alert("请单击鼠标左键，选择坐标！");
			return;
		}
		window.parent.document.getElementById("latitude").value = $("#latitude").val();
     	window.parent.document.getElementById("longitude").value = $("#longitude").val();
		window.parent.viewMapDialog.close();
	}
	
	$(document).ready(function(){ 
		$(".ui_state_highlight").click(function(){
			alert("test!!!");
		});
	}); 
		
</script>
</html>