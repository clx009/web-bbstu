<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>学校信息管理</title>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<script type="text/javascript" src="plug-in/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="plug-in/ckfinder/ckfinder.js"></script>
<script type="text/javascript">
	//编写自定义JS代码
	$(function() {
		var combobox_territory = $("#cityId").combobox({
			url : 'territoryController.do?comboTree',
			valueField : 'id',
			textField : 'territoryname',
			mode : 'remote'
		});
		//初始化ckeditor
		var editorOption = {
			height : 200,
			customConfig : '${pageContext.request.contextPath}/plug-in/ckeditor/ftlConfig.js'
		};
		CKEDITOR.replace('introduction_editor', editorOption);
		window.parent.document.getElementById("latitude").value = '${schoolPage.latitude}';
     	window.parent.document.getElementById("longitude").value = '${schoolPage.longitude}';
	});

	function getEditorData() {
		$("#introduction").val(CKEDITOR.instances.introduction_editor.getData());
		var parentLatitudeValue = window.parent.document.getElementById("latitude").value;
		var parentLongitudeValue = window.parent.document.getElementById("longitude").value;
		if(parentLatitudeValue && parentLatitudeValue != ''
				&& parentLongitudeValue && parentLongitudeValue != ''){
			$("#latitude").attr("value",window.parent.document.getElementById("latitude").value); 
	     	$("#longitude").attr("value",window.parent.document.getElementById("longitude").value);
		}
	}
	function viewMap(){
		createwindow4ChooseSch("选择学校","${pageContext.request.contextPath}/webpage/school/school/map.jsp",800,520);
	}
	function createwindow4ChooseSch(title, addurl,width,height) {
		width = width?width:700;
		height = height?height:400;
		if(width=="100%" || height=="100%"){
			width = document.body.offsetWidth;
			height =document.body.offsetHeight-100;
		}
		if(typeof(windowapi) == 'undefined'){
			window.parent.viewMapDialog = $.dialog({
				content: 'url:'+addurl,
				lock : true,
				width:width,
				height:height,
				title:title,
				opacity : 0.3,
				cache:false
			});
		}else{
			window.parent.viewMapDialog = W.$.dialog({
				content: 'url:'+addurl,
				lock : true,
				width:width,
				height:height,
				parent:windowapi,
				title:title,
				opacity : 0.3,
				cache:false
			});
		}
	}
</script>
</head>
<body>
	<t:formvalid formid="formobj" dialog="true" layout="div"
		action="schoolController.do?doUpdate" beforeCheck="getEditorData();">
		<input id="id" name="id" type="hidden" value="${schoolPage.id }">
		<input id="longitude" name="longitude" type="hidden" value="${schoolPage.longitude}">
		<input id="latitude" name="latitude" type="hidden" value="${schoolPage.latitude}" >
		<fieldset class="step">
			<div class="form">
				<label class="Validform_label">学校编号:</label> <input id="code"
					name="code" type="text" style="width: 150px" class="inputxt" validType="t_sch_school,code,id"
					datatype="*1-64" value='${schoolPage.code}'> <span class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label"> 英文名称: </label> <input
					id="englishName" name="englishName" type="text"
					style="width: 150px" class="inputxt" datatype="*1-100"
					value='${schoolPage.englishName}'> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label"> 建校时间: </label> <input
					id="builtUpTime" name="builtUpTime" type="text"
					style="width: 150px" class="easyui-datebox"
					value='${schoolPage.builtUpTime}'> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">所在城市:</label> <input id="cityId"
					name="cityId" type="text" style="width: 150px" value='${schoolPage.cityId}'> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label"> 综合排名: </label> <input
					id="comprehensiveRanking" name="comprehensiveRanking" type="text"
					style="width: 150px" class="inputxt" datatype="n"
					value='${schoolPage.comprehensiveRanking}'> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label"> 学校类型: </label> <select
					id="publicOrPrivate" name="publicOrPrivate">
					<option value="0"
						<c:if test="${schoolPage.publicOrPrivate==0}">selected="selected"</c:if>>公立</option>
					<option value="1"
						<c:if test="${schoolPage.publicOrPrivate==1}">selected="selected"</c:if>>私立</option>
				</select> <span class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label"> 排名: </label> <input id="ranking"
					name="ranking" type="text" style="width: 150px" class="inputxt"
					datatype="n" value='${schoolPage.ranking}'> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">托福要求:</label> <input
					id="toeflRequirement" name="toeflRequirement" type="text"
					style="width: 150px" class="inputxt" datatype="n" value='${schoolPage.toeflRequirement}'> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">SAT要求:</label> <input id="satReqMin"
					name="satReqMin" type="text" style="width: 150px" class="inputxt"
					datatype="n0-4" value='${schoolPage.satReqMin}'>-<input id="satReqMax" name="satReqMax"
					type="text" style="width: 150px" class="inputxt" datatype="n0-4" value='${schoolPage.satReqMax}'>
				<span class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label"> 学费: </label> <input id="tuition"
					name="tuition" type="text" style="width: 150px" class="inputxt"
					datatype="d" errormsg="请输入数字可保留2位小数" value='${schoolPage.tuition}'>
				<span class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label"> 录取比例: </label> <input
					id="acceptanceRate" name="acceptanceRate" type="text"
					style="width: 150px" class="inputxt"
					datatype="/^\d{0,2}(\.\d{0,2})?$/"
					value='${schoolPage.acceptanceRate}'> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label"> 特色专业: </label> <input id="featureMajor"
					name="featureMajor" type="text" style="width: 150px" class="inputxt"
					datatype="*0-1000" value='${schoolPage.featureMajor}'><span class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label"> 经纬度: </label><input type="button" value="选择经纬度"  onclick="javascript:viewMap();"  /> 
			</div>
			<!-- <div class="form">
				<label class="Validform_label"> 经度: </label> <input id="longitude"
					name="longitude" type="text" style="width: 150px" class="inputxt"
					datatype="*0-10"  onclick="javascript:viewMap();" ><span class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label"> 纬度: </label> <input id="latitude"
					name="latitude" type="text" style="width: 150px" class="inputxt"
					datatype="*0-10" onclick="javascript:viewMap();" ><span class="Validform_checktip"></span>
			</div> -->
			<div class="form">
				<label class="Validform_label"> 学校邮箱: </label> <input id="email"
					name="email" type="text" style="width: 150px" class="inputxt"
					datatype="*0-50" value='${schoolPage.email}'><span class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label"> 地址: </label> <input id="address"
					name="address" type="text" style="width: 150px" class="inputxt"
					datatype="*0-100" value='${schoolPage.address}'><span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">简介:</label> <input id="introduction"
					name="introduction" type="hidden" datatype="*0-10000"> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<textarea id="introduction_editor" name="introduction_editor">${schoolPage.introduction}</textarea>
			</div>
		</fieldset>
	</t:formvalid>
</body>
<script src="webpage/school/school/school.js"></script>