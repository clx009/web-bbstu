<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<script type="text/javascript" charset="utf-8">

	function importXml() {
		openuploadwin('Excel导入', 'majorController.do?upload', "majorList");
	}
	
</script>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="majorList" checkbox="true" fitColumns="false" title="专业资料维护" actionUrl="majorController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="主键"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="父专业"  field="parent.name"  hidden="true"  queryMode="group"    width="120"></t:dgCol>
   <t:dgCol title="编号"  field="code"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="名称"  field="name"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="是否热门"   field="isHot" replace="热门_1,普通_0"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="majorController.do?doDel&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="majorController.do?goAdd" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="majorController.do?goUpdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="批量删除"  icon="icon-remove" url="majorController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="majorController.do?goUpdate" funname="detail"></t:dgToolBar>
   <t:dgToolBar title="导入Excel" icon="icon-search" onclick="importXml()"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/school/major/majorList.js"></script>		
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
 });
 </script>