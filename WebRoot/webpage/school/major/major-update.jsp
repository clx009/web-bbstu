<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>t_sch_major</title>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
</head>
<body>
	<t:formvalid formid="formobj" dialog="true" usePlugin="password"
		layout="div" action="majorController.do?doUpdate" tiptype="3">
		<input id="id" name="id" type="hidden" value="${majorPage.id }">
		<fieldset class="step">
			<div class="form">
				<label class="Validform_label">父专业:</label> <input id="parentId"
					name="parent.id" type="text" style="width: 150px" class="inputxt"
					value='${majorPage.parent.id}'> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
		      <label class="Validform_label">编号:</label>
		     	 <input id="code" name="code" type="text" style="width: 150px" class="inputxt"  
					               datatype="*" validType="t_sch_major,code,id"
					                 value='${majorPage.code}'>
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
				<label class="Validform_label">名称:</label> <input id="name"
					name="name" type="text" style="width: 150px" class="inputxt"
					datatype="*" value='${majorPage.name}'> <span
					class="Validform_checktip"></span>
			</div>
			<t:dictSelect type="select" typeGroupCode="majorIsHot" id="isHot" field="isHot" defaultVal="${majorPage.isHot}"></t:dictSelect>
		</fieldset>
	</t:formvalid>
</body>
<script src="webpage/school/major/major.js"></script>