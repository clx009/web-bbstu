<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>t_s_message</title>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<script type="text/javascript" src="plug-in/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="plug-in/ckfinder/ckfinder.js"></script>
<script type="text/javascript">
	//编写自定义JS代码
	//编写自定义JS代码 
	$(function() {
		//初始化ckeditor
		var editorOption = {
			height : 200,
			customConfig : '${pageContext.request.contextPath}/plug-in/ckeditor/ftlConfig.js'
		};
		CKEDITOR.replace('content_editor', editorOption);
	});

	function getEditorData() {
		var content = CKEDITOR.instances.content_editor.getData();
		$("#content_").val(content);
	}
</script>
</head>
<body>
	<t:formvalid formid="formobj" dialog="true" usePlugin="password"
		layout="div" action="messageController.do?doUpdate" tiptype="3"
		beforeCheck="getEditorData();">
		<input id="id" name="id" type="hidden" value="${messagePage.id }">
		<input id="creatorId" name="creatorId" type="hidden"
			value="${messagePage.creatorId} ">


		<fieldset class="step">
			<div class="form">
				<label class="Validform_label"> 标题: </label> <input id="title"
					name="title" type="text" style="width: 150px" class="inputxt"
					datatype="*" value='${messagePage.title}'> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label"> 类型: </label> <select id="type"
					name="type">
					<option value="-1">-请选择 -</option>
					<option value="0"
						<c:if test="${messagePage.type=='0'}">selected="selected"</c:if>>系统消息
					</option>
				</select><span class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label"> 内容: </label> <input id="content_"
					name="content" type="hidden" datatype="*1-1000"> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<textarea id="content_editor" name="content_editor">${messagePage.content}</textarea>
			</div>
		</fieldset>


	</t:formvalid>
</body>
<script src="webpage/message/message.js"></script>