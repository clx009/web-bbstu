<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
	<div region="center" style="padding:1px;">
		<t:datagrid name="messageList" checkbox="true" fitColumns="false"
			title="消息管理" actionUrl="messageController.do?datagrid"
			idField="id" fit="true" queryMode="group">
			<t:dgCol title="id" field="id" hidden="false" queryMode="group"
				width="120"></t:dgCol>
			<t:dgCol title="标题" field="title" hidden="true" queryMode="group"
				width="120"></t:dgCol>
			<t:dgCol title="内容" field="content" hidden="true" queryMode="group"
				width="120"></t:dgCol>
			<t:dgCol title="创建时间" field="createTime" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<%--    <t:dgCol title="创建人id，对应到t_s_user.id"  field="creatorId"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
  --%>
			<t:dgCol title="类型" field="type" hidden="true" queryMode="group"  replace="系统消息_0"
				width="120"></t:dgCol>
			<t:dgCol title="操作" field="opt" width="100"></t:dgCol>
			<t:dgDelOpt title="删除" url="messageController.do?doDel&id={id}" />
			<t:dgToolBar title="录入" icon="icon-add"
				url="messageController.do?goAdd" funname="add"></t:dgToolBar>
			<t:dgToolBar title="编辑" icon="icon-edit"
				url="messageController.do?goUpdate" funname="update"></t:dgToolBar>
			<t:dgToolBar title="批量删除" icon="icon-remove"
				url="messageController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
			<t:dgToolBar title="查看" icon="icon-search"
				url="messageController.do?goUpdate" funname="detail"></t:dgToolBar>
		</t:datagrid>
	</div>
</div>
<script src="webpage/message/messageList.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//给时间控件加上样式
	});
</script>