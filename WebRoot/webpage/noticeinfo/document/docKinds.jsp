<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<t:base type="jquery,easyui,tools,DatePicker,autocomplete"></t:base>
<html>
<head>
<script type="text/javascript">
	$(function() {
		$('#docKindsList').datagrid( {
			onSelect : function(rowIndex, rowData) {
				showDocsList(rowData.name, rowData.code);
			}
		});
	})
</script>
</head>
<body>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'west',split:true,border:false" style="width:200px;overflow: hidden;">
		<table id="docKindsList" name="docKindsList" fit="true" class="easyui-datagrid" title="文档分类"  rownumbers="true"
            data-options="singleSelect:true,url:'sysFileController.do?docKinds&type=${type }',method:'get'">
	        <thead>
	            <tr>
	                <th data-options="field:'name',width:120">文档分类</th>
	                <th data-options="field:'code',width:120,hidden:true">文档编码</th>
	            </tr>
	        </thead>
	    </table>
	</div>
	<div data-options="region:'center'" border="false" >
		<div class="easyui-panel"  border="false" id="sysFilesPanel" fit="true"></div>
	</div>
</div>
</body>
 <script type="text/javascript">
	function showDocsList(name,code) {
		$("#sysFilesPanel").panel(
			{
				title :"【"+name+"】文档分类:",
				href:"sysFileController.do?sysFile&fileCat=" +code
			}
		);
		$('#sysFilesPanel').panel("refresh");
	}
</script>
</html>