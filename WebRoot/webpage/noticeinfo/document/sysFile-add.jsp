<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
	<head>
		<title>文档管理</title>
		<t:base type="jquery,easyui,tools,DatePicker"></t:base>
		<script type="text/javascript" src="plug-in/ckeditor_new/ckeditor.js"></script>
		<script type="text/javascript" src="plug-in/ckfinder/ckfinder.js"></script>
		<script type="text/javascript">
  //编写自定义JS代码
  </script>
	</head>
	<body>
		<t:formvalid formid="formobj" dialog="true" layout="table" beforeSubmit="upload" >
			<table style="width: 600px;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right">
						<label class="Validform_label">
							文件名:
						</label>
					</td>
					<td class="value">
						<input id="fileName" name="fileName" type="text" style="width: 150px" class="inputxt">
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							文件名
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							文件类型:
						</label>
					</td>
					<td class="value">
						<t:dictSelect field="fileType" id="fileType" type="list" typeGroupCode="filetype" defaultVal="${sysFilePage.fileType}" hasLabel="false" title="文件类型"></t:dictSelect>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							文件类型
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							文件分类:
						</label>
					</td>
					<td class="value">
						<t:dictSelect field="fileCat" id="fileCat" type="list" typeGroupCode="filecat" defaultVal="${sysFilePage.fileCat}" hasLabel="false" title="文件分类"></t:dictSelect>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							文件分类
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							文件路径:
						</label>
					</td>
					<td class="value">
						<t:upload name="filePath" buttonText="上传文件" uploader="sysFileController.do?doAdd" multi="false" extend="office" id="file_upload" formData="fileName,fileType,fileCat"></t:upload>
						<div class="form" id="filediv" style="height: 50px"></div>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							文件路径
						</label>
					</td>
				</tr>
			</table>
		</t:formvalid>
	</body>
	<script src="webpage/document/sysFile.js"></script>