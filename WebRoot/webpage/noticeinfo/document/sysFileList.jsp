<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
  <t:datagrid name="sysFileList" checkbox="true" fitColumns="false" title="文档管理" 
  	actionUrl="sysFileController.do?datagrid&fileCat=${fileCat}" idField="id" fit="true" queryMode="group">
   <t:dgCol title="编号"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="文件名"  field="fileName"  hidden="true" query="true" queryMode="single"  width="350"></t:dgCol>
   <t:dgCol title="文件类型"  field="fileType"  hidden="true"  queryMode="group" dictionary="filetype" width="120"></t:dgCol>
   <t:dgCol title="文件分类"  field="fileCat"  hidden="true"  queryMode="group" dictionary="filecat" width="120"></t:dgCol>
   <t:dgCol title="SWF路径"  field="fileSwfpath"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="文件路径"  field="filePath"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="150"></t:dgCol>
   <t:dgDefOpt url="sysFileController.do?downFile&fileid={id}" title="下载"></t:dgDefOpt>
   <t:dgFunOpt funname="showDocDetail(id,fileName)"  title="预览"></t:dgFunOpt>
   
   <%
    String roleCodes=request.getSession().getAttribute("roleCodes").toString().trim();
	if(roleCodes.indexOf("hospLeader")==-1){
	%> 
   <t:dgDelOpt title="删除" url="sysFileController.do?doDel&id={id}" />
   <t:dgToolBar title="添加" icon="icon-add" url="sysFileController.do?goAdd&fileCat=${fileCat}" funname="add"></t:dgToolBar>
   <t:dgToolBar title="批量删除"  icon="icon-remove" url="sysFileController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
  <% } %>
  
  </t:datagrid>
 <script src = "webpage/noticeinfo/document/sysFileList.js"></script>
 
 <script type="text/javascript"  >
 	function showDocDetail(id,fileName){
 		var url = "sysFileController.do?openViewFile&isIframe&fileid=" + id;
 		window.top.addTab(fileName,url,'pictures');
 	}
 </script>