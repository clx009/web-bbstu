<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#newsKinds__List').datagrid( {
			onSelect : function(rowIndex, rowData) {
				showNewsList(rowData.name, rowData.code);
			}
		});
	})
</script>
<div class="easyui-layout" data-options="fit:true">
	<div data-options="region:'west',split:true,border:false" style="width:200px;overflow: hidden;">
		<table id=newsKinds__List name="newsKinds__List" fit="true" class="easyui-datagrid" title="文档分类"  rownumbers="true"
            data-options="singleSelect:true,url:'sysNewsController.do?newsKinds',method:'get'">
	        <thead>
	            <tr>
	                <th data-options="field:'name',width:120">新闻分类</th>
	                <th data-options="field:'code',width:120,hidden:true">新闻编码</th>
	            </tr>
	        </thead>
	    </table>
	</div>
	<div data-options="region:'center'" border="false" >
		<div class="easyui-panel" style="padding: 0px;" fit="true" border="false" id="newsListPanel"></div>
	</div>
</div>
 <script type="text/javascript">
	function showNewsList(name,code) {
		$("#newsListPanel").panel(
			{
				title :"【"+name+"】新闻分类:",
				href:"sysNewsController.do?sysNews&cat=" +code
			}
		);
		$('#newsListPanel').panel("refresh");
	}
</script>