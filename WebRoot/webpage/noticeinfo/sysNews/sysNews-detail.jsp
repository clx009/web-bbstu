<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<script type="text/javascript">
	function closeCurWin(){
		parent.reloadTabGrid(3);
		closetab('信息查看');
	}	
</script>
<!DOCTYPE html>
<html>
	<head>
		<title>新闻管理</title>
		<t:base type="jquery,easyui,tools,DatePicker"></t:base>
		<script type="text/javascript" src="${pageContext.request.contextPath}/plug-in/ckeditor/ckeditor.js"></script>
	</head>
	<body>
		<t:formvalid formid="sysNewsEdit" dialog="false"  layout="table" tiptype="1">
			<input id="id" name="id" type="hidden" value="${sysNewsPage.id }">
			<table style="width: 98%;" cellpadding="0" cellspacing="1" class="formtable">
				<tr>
					<td align="right" width="150px;">
						<label class="Validform_label">
							标题:
						</label>
					</td>
					<td class="value">
						<input id="subject" readonly="readonly" name="subject" type="text"　style="width: 150px" class="inputxt" value='${sysNewsPage.subject}'　/>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							标题
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							类型:
						</label>
					</td>
					<td class="value">
						<t:dictSelect field="newstype" type="list" typeGroupCode="newstype" defaultVal="${sysNewsPage.newstype}"
							hasLabel="false" title="类型"></t:dictSelect>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							类型
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							分类:
						</label>
					</td>
					<td class="value">
						<t:dictSelect field="cat" type="list" typeGroupCode="newscat" defaultVal="${sysNewsPage.cat}" hasLabel="false" title="分类"></t:dictSelect>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							分类
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							级别:
						</label>
					</td>
					<td class="value">
						<t:dictSelect field="grade" type="list" typeGroupCode="newsgrade" defaultVal="${sysNewsPage.grade}" hasLabel="false" title="级别"></t:dictSelect>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							级别
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							创建时间:
						</label>
					</td>
					<td class="value">
						<input id="createtime" readonly="readonly" name="createtime" type="text" style="width: 150px" class="Wdate" onClick="WdatePicker()" value='<fmt:formatDate value='${sysNewsPage.createtime}' type="date" pattern="yyyy-MM-dd"/>'>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							创建时间
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							修改时间:
						</label>
					</td>
					<td class="value">
						<input id="updatetime" readonly="readonly" name="updatetime" type="text" style="width: 150px" class="Wdate" onClick="WdatePicker()" value='<fmt:formatDate value='${sysNewsPage.updatetime}' type="date" pattern="yyyy-MM-dd"/>'>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							修改时间
						</label>
					</td>
				</tr>
				<tr>
					<td align="right">
						<label class="Validform_label">
							内容:
						</label>
					</td>
					<td class="value">
						<textarea rows="27" cols="80" name="cont" id="con" >${sysNewsPage.cont}</textarea>
						<script type="text/javascript"> 
							window.onload=function(){
								var editor = CKEDITOR.replace('cont',{
									width : 1200,//宽度
									height: 260   
								});
								editor.setReadOnly(true);
							} 
						</script>
						<span class="Validform_checktip"></span>
						<label class="Validform_label" style="display: none;">
							内容
						</label>
					</td>
				</tr>
				
				<tr height="40">
					<td class="upload" colspan="2" align="center">
						<a href="#" class="easyui-linkbutton" onclick="closeCurWin();"  iconCls="icon-back">返回</a>
						<a href="#" class="easyui-linkbutton" onclick="closeCurWin();"  iconCls="icon-cancel">关闭</a>
					</td>
				</tr>
			</table>
		</t:formvalid>
	</body>
	<script src="webpage/noticeinfo/sysNews/sysNews.js"></script>