<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<script type="text/javascript">
	function reloadTabGrid(type)
    {
	    if(type === 1 )tip('新闻添加成功');
	    if(type === 2 )tip('新闻修改成功');
		$("#sysNewsList").datagrid("reload");
    }
</script>
<t:datagrid name="sysNewsList" checkbox="true" fitColumns="false"
	title="新闻管理" actionUrl="sysNewsController.do?datagrid&cat=${cat}" idField="id"
	fit="true" queryMode="group">
	<t:dgCol title="编号" field="id" hidden="false" queryMode="group"
		width="120"></t:dgCol>
	<t:dgCol title="标题" field="subject" hidden="true" query="true"
		queryMode="single" width="120"></t:dgCol>
	<t:dgCol title="类型" field="newstype" hidden="true" query="true"
		queryMode="single" dictionary="newstype" width="120"></t:dgCol>
	<t:dgCol title="分类" field="cat" hidden="true" queryMode="group"
		dictionary="newscat" width="120"></t:dgCol>
	<t:dgCol title="级别" field="grade" hidden="false" query="true"
		queryMode="single" dictionary="newsgrade" width="120"></t:dgCol>
	<t:dgCol title="内容" field="cont" hidden="false" queryMode="group"
		width="120"></t:dgCol>
	<t:dgCol title="发表时间" field="createtime" formatter="yyyy-MM-dd"
		hidden="true" queryMode="group" width="120"></t:dgCol>
	<t:dgCol title="修改时间" field="updatetime" formatter="yyyy-MM-dd"
		hidden="true" queryMode="group" width="120"></t:dgCol>
	<t:dgCol title="发布者编号" field="userid" hidden="false" queryMode="group"
		width="120"></t:dgCol>
	<t:dgCol title="操作" field="opt" width="100"></t:dgCol>
	<t:dgDelOpt title="删除" url="sysNewsController.do?doDel&id={id}" />
	<t:dgToolBar title="添加" icon="icon-add" funname="addNews"></t:dgToolBar>
	<t:dgToolBar title="编辑" icon="icon-edit" funname="editNews"></t:dgToolBar>
	<t:dgToolBar title="批量删除" icon="icon-remove" url="sysNewsController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
	<t:dgToolBar title="查看" icon="icon-search" funname="showDetail"></t:dgToolBar>
</t:datagrid>
<script src="webpage/noticeinfo/sysNews/sysNewsList.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//给时间控件加上样式
			$("#sysNewsListtb").find("input[name='createtime_begin']").attr(
					"class", "Wdate").attr("style", "height:20px;width:90px;")
					.click(function() {
						WdatePicker( {
							dateFmt : 'yyyy-MM-dd'
						});
					});
			$("#sysNewsListtb").find("input[name='createtime_end']").attr(
					"class", "Wdate").attr("style", "height:20px;width:90px;")
					.click(function() {
						WdatePicker( {
							dateFmt : 'yyyy-MM-dd'
						});
					});
			$("#sysNewsListtb").find("input[name='updatetime_begin']").attr(
					"class", "Wdate").attr("style", "height:20px;width:90px;")
					.click(function() {
						WdatePicker( {
							dateFmt : 'yyyy-MM-dd'
						});
					});
			$("#sysNewsListtb").find("input[name='updatetime_end']").attr(
					"class", "Wdate").attr("style", "height:20px;width:90px;")
					.click(function() {
						WdatePicker( {
							dateFmt : 'yyyy-MM-dd'
						});
					});

			editOnClick("sysNewsController.do?goUpdate","sysNewsList","2");
		});
	
		function editNews() {
			var selectionRows = $("#sysNewsList").datagrid("getSelections");
			var rowLen = selectionRows.length;
			if(rowLen==1){
				window.top.addTab("信息修改", "sysNewsController.do?goUpdate&isIframe&id="+selectionRows[0].id);
			}else if(rowLen > 1){
				tip("请选择一条记录");
				return;
			}else{
				tip("请选择需要编辑的记录");
				return;
			}
		}

		function showDetail(){
			var selectionRows = $("#sysNewsList").datagrid("getSelections");
			var rowLen = selectionRows.length;
			if(rowLen==1){
				window.top.addTab("信息查看", "sysNewsController.do?goDetail&isIframe&id="+selectionRows[0].id);
			}else if(rowLen > 1){
				tip("请选择一条记录");
				return;
			}else{
				tip("请选择需要查看的记录");
				return;
			}
		}
		
		function addNews() {
			window.top.addTab("信息发布", "sysNewsController.do?goAdd&isIframe&cat=${cat}");
		}
</script>