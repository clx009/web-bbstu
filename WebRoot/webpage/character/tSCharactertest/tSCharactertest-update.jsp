<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>t_s_charactertest</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body>
  <t:formvalid formid="formobj" layout="div" dialog="true" beforeSubmit="upload">
				<input id="id" name="id" type="hidden" value="${tSCharactertestPage.id }">
		<fieldset class="step">
		    <div class="form">
		      <label class="Validform_label">学生:</label>
		      	 <input type="hidden" id="userid" value="${tSCharactertestPage.userId}">
		     	 <input name="userid" type="text" style="width: 150px" value='${tSCharactertestPage.userId}' class="inputxt" >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">项目:</label>
		     	 <input id="projectname" name="projectname" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					                 value='${tSCharactertestPage.projectName}'>
		      <span class="Validform_checktip"></span>
		    </div>
		 	<div class="form">
		      <label class="Validform_label">分数:</label>
		     	 <input id="result" name="result" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					                 value='${tSCharactertestPage.result}'>
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">备注:</label>
				 <textarea rows="3" cols="80"  id="description" name="description"
						type="text" style="width: 300px" class="inputxt" height="200px"
						autofocus="true" wrap="soft" datatype="*">${tSCharactertestPage.description}</textarea>
		      <span class="Validform_checktip"></span>
		    </div>
		    
		    <div class="form" id="filediv"></div>
			<div class="form"><t:upload name="file_upload" uploader="tSCharactertestController.do?doAdd" extend="*.png" id="file_upload" formData="id,userid,projectname,result,description"></t:upload></div>
	    </fieldset>
  </t:formvalid>
 </body>
  <script src = "webpage/character/tSCharactertest/tSCharactertest.js"></script>		