<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="tSCharactertestList" checkbox="true" fitColumns="false" title="性格测试及潜力评估" actionUrl="tSCharactertestController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="id"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="项目"  field="projectName"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <%-- <t:dgCol title="结果"  field="result"  hidden="true"  queryMode="group"  width="120"></t:dgCol> --%>
   <t:dgCol title="分数"  field="result"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="备注"  field="description"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="附件"  field="path"  hidden="true"  queryMode="group"  width="240"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="tSCharactertestController.do?doDel&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="tSCharactertestController.do?goAdd" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="tSCharactertestController.do?goUpdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="批量删除"  icon="icon-remove" url="tSCharactertestController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="tSCharactertestController.do?goUpdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/character/tSCharactertest/tSCharactertestList.js"></script>		
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
 });
 </script>