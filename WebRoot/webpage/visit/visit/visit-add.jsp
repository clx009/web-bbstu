<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>访问申请</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="visitController.do?doAdd" tiptype="3">
				<input id="id" name="id" type="hidden" value="${visitPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">委托人id，外键：关联到t_s_user.id:</label>
		     	 <input id="clientId" name="clientId" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">委托时间:</label>
		     	 <input id="entrustTime" name="entrustTime" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">补充:</label>
		     	 <input id="addenda" name="addenda" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">期望访问时间:</label>
		     	 <input id="expectTime" name="expectTime" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">创建时间:</label>
		     	 <input id="createTime" name="createTime" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">创建人id，外键关联到t_s_user.id:</label>
		     	 <input id="creatorId" name="creatorId" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">状态：0-待接单，1-已接单，2-已访问，3，已归档:</label>
		     	 <input id="status" name="status" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </t:formvalid>
 </body>
  <script src = "webpage/visit/visit/visit.js"></script>		