<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
<head>

<title>访问申请</title>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<script>
	function delFile(id) {
		$.ajax({
			url : "commonController.do?delObjFile",
			data : {
				fileKey : id
			},
			dataType : "json",
			success : function(data) {
				$.messager.show(data);
				$("#file_" + id).remove();
			}
		});
	}
</script>
</head>
<body>
	<t:formvalid formid="formobj" dialog="true" usePlugin="password"
		beforeSubmit="upload" layout="div" tiptype="3">
		<input id="businessKey" name="businessKey" type="hidden"
			value="${visitPage.id }">
		<fieldset class="step">
			<div class="form">
				<table style="width:100%">
					<tr>
						<td class="Validform_label" align="left" width="20%">委托人:</td>
						<td align="left" width="30%">${visitPage.creator.realName}</td>
						<td class="Validform_label" align="left" width="20%">委托时间:</td>
						<td width="30%">${visitPage.createTime}</td>
					<tr>
					<tr>
						<td class="Validform_label" align="left">状态:</td>
						<td align="left">${visitPage.status}</td>
						<td class="Validform_label" align="left">期望时间:</td>
						<td align="left">${visitPage.expectTime}</td>
					<tr>
				</table>
			</div>

			<div class="form">
				<label class="Validform_label">关注内容:</label>
				<ol>
					<c:forEach var="o" items="${visitPage.visitOptions}">

						<li>${o.option.name}</li>
					</c:forEach>
				</ol>
			</div>

			<div class="form">
				<label class="Validform_label">补充:</label>${visitPage.addenda}
			</div>
			<div class="form">
				<div style="margin: 5px">
					<c:forEach var="report" items="${reports}">
						<div id="file_${report.id}">
							${report.attachmenttitle}&nbsp;&nbsp;${report.createdate } <a
								href="commonController.do?viewFile&fileid=${report.id}&subclassname=${report.subclassname}">下载
							</a> <a href="javascript:void(0);" onclick="delFile('${report.id}');">删除</a>
							<br>
						</div>
					</c:forEach>
				</div>

				<t:upload id="visitMaterial" name="visitMaterial" auto="false"
					view="true" buttonText="上传访谈材料" multi="true"
					queueID="visitMaterialDiv"
					uploader="visitController.do?doUploadMaterial" extend="office"
					formData="businessKey"></t:upload>
				<div class="form" id="visitMaterialDiv"></div>
			</div>

		</fieldset>
	</t:formvalid>
</body>
<script src="webpage/visit/visit/visit.js"></script>