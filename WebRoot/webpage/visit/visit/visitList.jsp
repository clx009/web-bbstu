<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
	<div region="center" style="padding:1px;">
		<t:datagrid name="visitList" checkbox="true" fitColumns="false"
			title="访问申请" actionUrl="visitController.do?datagrid" idField="id"
			fit="true" queryMode="group">
			<t:dgCol title="id" field="id" hidden="false" queryMode="group"
				width="120"></t:dgCol>
			<t:dgCol title="创建人" field="creator.nickname" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="委托时间" field="createTime" hidden="true" 
				queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="期望访问时间" field="expectTime" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="创建时间" field="createTime" hidden="true"
				queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="访谈类型" replace="家访_1,校访_2,专家咨询_4" field="type"
				hidden="true" queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="状态" replace="待接单_0,已接单_1,已访问_2,已归档_3" field="status"
				hidden="true" queryMode="group" width="120"></t:dgCol>

			<t:dgCol title="委托人" field="client" hidden="true" queryMode="group" width="120"></t:dgCol>
			<t:dgCol title="操作" field="opt" width="150"></t:dgCol>
		<%-- 	<t:dgOpenOpt title="处理" url="visitController.do?goUploadMaterial&id={id}"
				openModel="OpenWin" width="800" height="400" /> --%>
			<%--	<t:dgFunOpt title="打印" funname="goPrint(id)"></t:dgFunOpt>--%>
			<t:dgDelOpt title="删除" url="visitController.do?doDel&id={id}" />
			<%-- <t:dgToolBar title="录入" icon="icon-add" url="visitController.do?goAdd" funname="add"></t:dgToolBar>--%>
            <t:dgToolBar title="处理" icon="icon-edit" url="visitController.do?goUpdate" funname="update"></t:dgToolBar>
			<t:dgToolBar title="访谈资料" icon="icon-comturn"
				url="visitController.do?goUploadMaterial" funname="update"></t:dgToolBar>
			<t:dgToolBar title="访谈报告" icon="icon-comturn"
				url="visitController.do?goUploadReport" funname="update"></t:dgToolBar>
			<t:dgToolBar title="查看" icon="icon-search"
				url="visitController.do?goView" funname="detail"></t:dgToolBar>

		</t:datagrid>
	</div>
</div>
<script src="webpage/visit/visit/visitList.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//给时间控件加上样式
	});

	function goPrint(id) {
		console.log(id);
		window.open("visitController.do?goPrint&id=" + id);
	}
</script>