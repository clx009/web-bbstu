<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>访问申请</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="visitController.do?doUpdate" tiptype="3">
				<input id="id" name="id" type="hidden" value="${visitPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">委托人:</label>
		     	 <input id="client" name="client" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					                 value='${visitPage.client}'>
		      <span class="Validform_checktip"></span>
		    </div>
		<%-- 	<div class="form">
		      <label class="Validform_label">委托时间:</label>
		     	 <input id="entrustTime" name="entrustTime" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					                 value='${visitPage.createTime}'>
		      <span class="Validform_checktip"></span>
		    </div> --%>
			<div class="form">
		      <label class="Validform_label">补充:</label>
		     	 <input id="addenda" name="addenda" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					                 value='${visitPage.addenda}'>
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">期望访问时间:</label>
		     	 <input id="expectTime" name="expectTime" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					                 value='${visitPage.expectTime}'>
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">委托时间:</label>
		     	 <input id="createTime" name="createTime" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					                 value='${visitPage.createTime}'>
		      <span class="Validform_checktip"></span>
		    </div>
			 
			<div class="form">
		      <label class="Validform_label">状态:</label>
		     	 
				 <select id="status" name="status">
					<option value="0" <c:if test="${visitPage.status=='0' }">checked="checked"</c:if>>待接单</option>
					<option value="1" <c:if test="${visitPage.status=='1' }">checked="checked"</c:if>>已接单</option>
					<option value="2" <c:if test="${visitPage.status=='2' }">checked="checked"</c:if>>已访问</option>
					<option value="3" <c:if test="${visitPage.status=='3' }">checked="checked"</c:if>>已归档</option> 
				</select>
		      <span class="Validform_checktip"></span>
		    </div>
		    
		    
	    </fieldset>
  </t:formvalid>
 </body>
  <script src = "webpage/visit/visit/visit.js"></script>		