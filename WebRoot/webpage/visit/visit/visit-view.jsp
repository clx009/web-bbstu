<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
<head>

<title>访问申请</title>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>

</head>
<body>
	<t:formvalid formid="formobj" dialog="true" usePlugin="password"
		beforeSubmit="upload" layout="div" tiptype="3">
		<input id="businessKey" name="businessKey" type="hidden"
			value="${visitPage.id }">
		<fieldset class="step">
			<div class="form">
				<table style="width:100%">
					<tr>
						<td class="Validform_label" align="left" width="20%">委托人:</td>
						<td align="left" width="30%">${visitPage.creator.realName}</td>
						<td class="Validform_label" align="left" width="20%">委托时间:</td>
						<td width="30%">${visitPage.createTime}</td>
					<tr>
					<tr>
						<td class="Validform_label" align="left">状态:</td>
						<td align="left">${visitPage.status}</td>
						<td class="Validform_label" align="left">期望时间:</td>
						<td align="left">${visitPage.expectTime}</td>
					<tr> 
				</table>
			</div>
			<div class="form">
				<div>
					<label class="Validform_label">关注内容:</label>
				</div>
				<div style="float:left;">
					<ol>
						<c:forEach var="o" items="${visitPage.visitOptions}">
							<li>${o.option.name}</li>
						</c:forEach>
					</ol>
				</div>
			</div>
			<div class="form">
				<div>
					<label class="Validform_label">补充:</label>
				</div>
				<div style="float:left;width: 70%;">${visitPage.addenda}</div>
			</div>
			<div class="form">
				<div>
					<label class="Validform_label">访谈报告:</label>
				</div>
				<div style="float:left;">
					<c:forEach var="report" items="${reports}"> 
							${report.attachmenttitle}&nbsp;&nbsp;${report.createdate } <a
							href="commonController.do?viewFile&fileid=${report.id}&subclassname=${report.subclassname}">下载
						</a>
						<br>
					</c:forEach>
				</div>
			</div>
			<div class="form">
				<div>
					<label class="Validform_label">访谈资料:</label>
				</div>
				<div>
					<c:forEach var="material" items="${materials}"> 
						${material.attachmenttitle}&nbsp;&nbsp;${material.createdate } <a
							href="commonController.do?viewFile&fileid=${material.id}&subclassname=${material.subclassname}">下载
						</a>
						<br>
					</c:forEach>
				</div>
			</div>
		</fieldset>
	</t:formvalid>
</body>
<script src="webpage/visit/visit/visit.js"></script>