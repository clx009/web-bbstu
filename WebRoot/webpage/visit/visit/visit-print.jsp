<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
<head>

<title>访问申请</title>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
<style media="print" type="text/css">
.Noprint {
	display: none;
}

.PageNext {
	page-break-after: always;
}
</style>
</head>
<body>
	<table>
		<tr>
			<td>委托人:</td>
			<td>${visitPage.client.realName}</td>
			<td>委托时间:</td>
			<td>${visitPage.entrustTime}</td>
		<tr>
		<tr>
			<td>状态:</td>
			<td>${visitPage.status}</td>
			<td>期望时间:</td>
			<td>${visitPage.expectTime}</td>
		<tr>
		<tr>
			<td>申请时间:</td>
			<td>${visitPage.createTime}</td>
		<tr>
		<tr>
			<td>关注内容:</td>
			<td><ol>
					<c:forEach var="o" items="${visitPage.visitOptions}">
						<li>${o.option.name}</li>
					</c:forEach>
				</ol></td>
		<tr>
		<tr>
			<td>补充:</td>
			<td>${visitPage.addenda}</td>
		<tr>
	</table>
	<button onclick="window.print();" class="Noprint">打印</button>
</body>
<script src="webpage/visit/visit/visit.js"></script>