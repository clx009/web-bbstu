<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>访问关注内容</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="optionController.do?doAdd" tiptype="3">
				<input id="id" name="id" type="hidden" value="${optionPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">名称:</label>
		     	 <input id="name" name="name" type="text" style="width:300px" class="inputxt"  
					               datatype="*1-20"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">描述:</label>
		      <textarea rows="3" cols="80" id="description" name="description" type="text"
					style="width: 300px" class="inputxt" height="200px"   datatype="*0-100"
					autofocus="true" wrap="soft"/></textarea>
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">类型:</label>
		      	<select id="type" name="type" >
		      		<option value="1">家访</option>
		      		<option value="2">校访</option>
		      		<option value="3">专家咨询</option>
		      	</select> 
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </t:formvalid>
 </body>
  <script src = "webpage/visit/option/option.js"></script>		