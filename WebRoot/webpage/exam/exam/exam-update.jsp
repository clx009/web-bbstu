<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>t_exam</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="examController.do?doUpdate" tiptype="3">
				<input id="id" name="id" type="hidden" value="${examPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">考试名称:</label>
		     	 <input id="name" name="name" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					                 value='${examPage.name}'>
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </t:formvalid>
 </body>
  <script src = "webpage/exam/exam/exam.js"></script>		