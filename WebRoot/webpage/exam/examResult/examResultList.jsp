<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="examResultList" checkbox="true" fitColumns="false" title="成绩维护" actionUrl="examResultController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="主键"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="考试" field="exam.name"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="姓名" replace="${userReplace}" field="userId"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="学年"  field="schoolYear"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="学期"  field="term"  hidden="true"  queryMode="group"  width="120"></t:dgCol> 
     <t:dgCol title="难度"  field="diffLevel"  hidden="true"  queryMode="group"  width="120"></t:dgCol> 
   <t:dgCol title="课程" replace="${subjectChaniseNameReplace}" field="courseId"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
    <t:dgCol title="分数"  field="score"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="评级"  field="scorelevel"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="教师" replace="${teacherReplace}"  field="teacherId"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
<%--    <t:dgCol title="创建时间"  field="createTime"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="录入人" replace="${userReplace}"  field="creatorId"  hidden="true"  queryMode="group"  width="120"></t:dgCol> --%>
   <t:dgCol title="备注"  field="comments"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="examResultController.do?doDel&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="examResultController.do?goAdd" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="examResultController.do?goUpdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="批量删除"  icon="icon-remove" url="examResultController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="examResultController.do?goUpdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/exam/examResult/examResultList.js"></script>		
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
 });
 </script>