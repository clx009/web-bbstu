<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>考试科目</title>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
</head>
<body>
	<t:formvalid formid="formobj" dialog="true" usePlugin="password"
		layout="div" action="subjectController.do?doAdd" tiptype="3">
		<input id="id" name="id" type="hidden" value="${subjectPage.id }">
		<fieldset class="step">
			<div class="form">
				<label class="Validform_label">科目:</label> <input id="itemId"
					name="examItem.id" type="text" style="width: 250px" class="inputxt">
				<span class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">中文名称:</label> <input id="chineseName"
					name="chineseName" type="text" style="width: 250px" class="inputxt"
					datatype="*1-20"> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">英文名称:</label> <input id="englishName"
					name="englishName" type="text" style="width: 250px" class="inputxt"
					datatype="*1-50"> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">学分:</label> <input id="credit"
					name="credit" type="text" style="width: 250px" class="inputxt"
					datatype="n1-4"> <span
					class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">说明:</label> <input id="comments"
					name="comments" type="text" style="width: 250px" class="inputxt"
					datatype="*1-100"> <span
					class="Validform_checktip"></span>
			</div>
		</fieldset>
	</t:formvalid>
</body>
<script src="webpage/exam/subject/subject.js"></script>