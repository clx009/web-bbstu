<%@ page language="java" import="java.util.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
 <head>
  <title>t_exam_item</title>
  <t:base type="jquery,easyui,tools,DatePicker"></t:base>
 </head>
 <body>
  <t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="div" action="examItemController.do?doUpdate" tiptype="3">
				<input id="id" name="id" type="hidden" value="${examItemPage.id }">
		<fieldset class="step">
			<div class="form">
		      <label class="Validform_label">中文名称:</label>
		     	 <input id="chineseName" name="chineseName" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					                 value='${examItemPage.chineseName}'>
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">英文名称:</label>
		     	 <input id="englishName" name="englishName" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					                 value='${examItemPage.englishName}'>
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
		      <label class="Validform_label">备注:</label>
		     	 <input id="comments" name="comments" type="text" style="width: 150px" class="inputxt"  
					               datatype="*"
					                 value='${examItemPage.comments}'>
		      <span class="Validform_checktip"></span>
		    </div>
	    </fieldset>
  </t:formvalid>
 </body>
  <script src = "webpage/exam/examItem/examItem.js"></script>		