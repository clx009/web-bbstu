<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<div class="easyui-layout" fit="true">
  <div region="center" style="padding:1px;">
  <t:datagrid name="timetableList" checkbox="true" fitColumns="false" title="课程表管理" actionUrl="timetableController.do?datagrid" idField="id" fit="true" queryMode="group">
   <t:dgCol title="主键"  field="id"  hidden="false"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="姓名" replace="${userReplace}" field="userId"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="学年"  field="schoolYear"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="学期"  field="term"  hidden="true"  queryMode="group"  width="120"></t:dgCol> 
   <t:dgCol title="开始时间"  field="startTime"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="结束时间"  field="endTime"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="课程" replace="${subjectChaniseNameReplace}" field="courseId"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="老师" replace="${teacherReplace}"  field="teacherId"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="教室"  field="place"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="状态"  field="status"  hidden="true"  queryMode="group"  width="120"></t:dgCol>
   <t:dgCol title="操作" field="opt" width="100"></t:dgCol>
   <t:dgDelOpt title="删除" url="timetableController.do?doDel&id={id}" />
   <t:dgToolBar title="录入" icon="icon-add" url="timetableController.do?goAdd" funname="add"></t:dgToolBar>
   <t:dgToolBar title="编辑" icon="icon-edit" url="timetableController.do?goUpdate" funname="update"></t:dgToolBar>
   <t:dgToolBar title="批量删除"  icon="icon-remove" url="timetableController.do?doBatchDel" funname="deleteALLSelect"></t:dgToolBar>
   <t:dgToolBar title="查看" icon="icon-search" url="timetableController.do?goUpdate" funname="detail"></t:dgToolBar>
  </t:datagrid>
  </div>
 </div>
 <script src = "webpage/exam/timeTable/timeTableList.js"></script>		
 <script type="text/javascript">
 $(document).ready(function(){
 		//给时间控件加上样式
 });
 </script>