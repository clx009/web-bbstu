<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/webpage/common/baseJsp.jsp"%>
<!DOCTYPE html>
<html>
<head>
<title>成绩维护</title>
<t:base type="jquery,easyui,tools,DatePicker"></t:base>
</head>
<body>
	<t:formvalid formid="formobj" dialog="true" usePlugin="password"
		layout="div" action="timetableController.do?doAdd" tiptype="3">
		<input id="id" name="id" type="hidden" value="${timeTablePage.id }">
		<fieldset class="step">
			<div class="form">
				<label class="Validform_label">学生:</label> <input id="userId"
					name="userId" type="text" style="width: 150px" class="inputxt">
				<span class="Validform_checktip"></span>
			</div>
 			<div class="form">
				<label class="Validform_label">学年:</label> <select id="schoolYear"
					name="schoolYear">
					</select> <span
					class="Validform_checktip"></span>
			</div> 			
			<div class="form">
				<label class="Validform_label">学期:</label> <select id="term"
					name="term">
					<option value="春">春</option>
					<option value="夏">夏</option>
					<option value="秋">秋</option>
					<option value="冬">冬</option>
				</select><span
					class="Validform_checktip"></span>
			</div>
<!-- 			<div class="form">
		      <label class="Validform_label">课程开始时间:</label>
		      	<input id="starttime" type="hidden">
		     	 <input name="starttime" type="text" style="width: 150px"> 
		      <span class="Validform_checktip"></span>
		    </div>
		    <div class="form">
		      <label class="Validform_label">课程结束时间:</label>
		      		      	<input id="endtime" type="hidden">
		     	 <input name="endtime" type="text" style="width: 150px"> 
		      <span class="Validform_checktip"></span>
		    </div> -->
		    			<div class="form">
		      <label class="Validform_label">课程开始时间:</label>
		      <input id="starttime" type="hidden">
		     	 <input name="starttime" type="text" style="width: 150px"  
					              class="easyui-datetimebox"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
		    <div class="form">
		      <label class="Validform_label">课程结束时间:</label>
		      <input id="endtime" type="hidden">
		     	 <input name="endtime" type="text" style="width: 150px"  
					              class="easyui-datetimebox"
					               >
		      <span class="Validform_checktip"></span>
		    </div>
			<div class="form">
				<label class="Validform_label">课程:</label> <input id="courseId"
					name="courseId" type="text" style="width: 150px" class="inputxt">
					
				<span class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">老师:</label> <input id="teacherId"
					name="teacherId" type="text" style="width: 150px" class="inputxt">
				<span class="Validform_checktip"></span>
			</div>
			<div class="form">
				<label class="Validform_label">教室:</label> <input id="place"
					name="place" type="text" style="width: 150px" class="inputxt"
					datatype="*0-100"> <span
					class="Validform_checktip"></span>
			</div>
		</fieldset>
	</t:formvalid>
</body>
<script type="text/javascript"> 
window.onload=function(){ 
//设置年份的选择 
var myDate= new Date(); 
var startYear=myDate.getFullYear();//起始年份 
var endYear=myDate.getFullYear()+50;//结束年份 
var obj=document.getElementById('schoolYear'); 
for (var i=startYear;i<=endYear;i++) 
{ 
obj.options.add(new Option(i,i)); 
} 
};
</script> 
<script src="webpage/exam/timeTable/timeTable.js"></script>