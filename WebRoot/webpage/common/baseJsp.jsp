<%@ taglib prefix="t" uri="/easyui-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt_rt"%>
<% 
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path;
%>
<c:set var="webRoot" value="<%=basePath%>" />
<script src = "<%=basePath%>/webpage/common/js/common.js"></script>
<script src = "<%=basePath%>/webpage/common/js/format.js"></script>
<link rel="stylesheet" href="<%=basePath%>/webpage/common/css/common.css" type="text/css"></link>
